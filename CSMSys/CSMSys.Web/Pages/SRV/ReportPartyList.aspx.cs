﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CSMSys.Web.Pages.SRV
{
    public partial class ReportPartyList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string code = string.IsNullOrEmpty(Request.QueryString["PC"]) ? string.Empty : Request.QueryString["PC"];
                hdnSearch.Value = code;
                lblSearch.Text = code;
            }
        }
    }
}