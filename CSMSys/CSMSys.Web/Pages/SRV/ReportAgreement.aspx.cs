﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.SRV;
using CSMSys.Lib.Manager.INV;
using CSMSys.Lib.Manager.Administration.Application;

namespace CSMSys.Web.Pages.SRV
{
    public partial class ReportAgreement : System.Web.UI.Page
    {
        #region Private Properties
        private int RegistrationID
        {
            get
            {
                if (ViewState["RegistrationID"] == null)
                    ViewState["RegistrationID"] = -1;
                return (int)ViewState["RegistrationID"];
            }
            set
            {
                ViewState["RegistrationID"] = value;
            }
        }

        SRVRegistration _registration;
        #endregion
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                RegistrationID = string.IsNullOrEmpty(Request.QueryString["RID"]) ? 0 : int.Parse(Request.QueryString["RID"]);
                lblRegistrationID.Text = RegistrationID.ToString();

                getRegistrationInfo();

                grvRegistration.DataBind();
                grvDelivery.DataBind();
            }
        }

        private void getRegistrationInfo()
        {
            if (RegistrationID <= 0) return;
            _registration = new RegistrationManager().GetRegistrationByID(RegistrationID);

            lblSR.Text = _registration.SerialNo;
            lblCarrying.Text = _registration.CarryingLoan.ToString();

            int partyID = (Int32)_registration.PartyID;

            INVParty _party = new PartyManager().GetPartyByID(partyID);

            lblName.Text = _party.PartyName;
            lblFather.Text = _party.FatherName;
            lblVill.Text = _party.AreaVillageName;
            lblPO.Text = _party.AreaPOName;
            lblCode.Text = _party.PartyCode;
            lblType.Text = _party.PartyType;
            lblContact.Text = _party.ContactNo;
            int upazilla = (Int32)_party.UpazilaPSID;

            ADMUpazilaPS _upzillaPS = new UpazilaPSManager(false).GetUpazilaPSByID(upazilla);
            lblPS.Text = _upzillaPS.UpazilaPSName;

            int district = (Int32)_party.DistrictID;

            ADMDistrict _district = new DistrictManager(false).GetDistrictByID(district);
            lblDist.Text = _district.DistrictName;
        }

        #region SqlDataSource Control Event Handlers
        protected void dsRegistration_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@RegistrationID"].Value = RegistrationID;
        }

        protected void dsDelivery_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@RegistrationID"].Value = RegistrationID;
        }
        #endregion
    }
}