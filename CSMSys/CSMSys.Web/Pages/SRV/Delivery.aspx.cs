﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.INV;

namespace CSMSys.Web.Pages.SRV
{
    public partial class Delivery : System.Web.UI.Page
    {
        #region Private Properties
        private string strSearch = string.Empty;

        private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }

        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateFrom = DateTime.Today;
                txtFromDate.Text = DateFrom.ToShortDateString();
                DateTo = DateTime.Today;
                txtToDate.Text = DateTo.ToShortDateString();

                grvDelivery.DataBind();
            }
        }

        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }
        #endregion

        #region Methods For Grid
        protected void grvDelivery_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvDelivery.PageIndex = e.NewPageIndex;
            grvDelivery.DataBind();
        }

        protected void grvDelivery_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[0].Controls.Count > 0))
            {
                ImageButton btnEdit = (ImageButton)e.Row.FindControl("imgEdit");
                btnEdit.Attributes.Add("onClick", "javascript:ShowEditModal('" + (DataBinder.Eval(e.Row.DataItem, "CollectionID")).ToString() + "');");

                HyperLink hpl = (HyperLink)e.Row.FindControl("hplprint");
                hpl.NavigateUrl = "~/Pages/Loan/DeliveryReport.aspx?CID=" + (DataBinder.Eval(e.Row.DataItem, "CollectionID")).ToString() +  "&PID=" + (DataBinder.Eval(e.Row.DataItem, "PartyID")).ToString();

            }
        }
        #endregion

        #region SqlDataSource Control Event Handlers
        protected void dsDisburse_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@DateFrom"].Value = DateFrom;
            e.Command.Parameters["@DateTo"].Value = DateTo;
        }
        #endregion

        #region Methods For Button
        protected void imgRefresh_Click(object sender, EventArgs e)
        {
            grvDelivery.DataBind();
        }

        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text.Trim();
            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
            dateInfo.ShortDatePattern = "MM/dd/yyyy";

            DateFrom = Convert.ToDateTime(txtFromDate.Text.ToString(), dateInfo);
            DateTo = Convert.ToDateTime(txtToDate.Text.ToString(), dateInfo);
            grvDelivery.DataBind();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            grvDelivery.DataBind();
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            string fromDate = txtFromDate.Text;
            string toDate = txtToDate.Text;
            Response.Redirect("~/Pages/Loan/ReportDisburse.aspx?FD=" + fromDate + "&TD=" + toDate + "&PC=" + txtSearch.Text);
        }
        #endregion
    }
}