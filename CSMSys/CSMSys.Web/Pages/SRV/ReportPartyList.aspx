﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ReportPartyList.aspx.cs" Inherits="CSMSys.Web.Pages.SRV.ReportPartyList" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="hdnSearch" runat="server" />
        <div class="feature-box-full">
            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                <tbody>
                <tr>
                    <td align="center" valign="top" style="font-size:10px;">
                        বিসমিল্লাহির রাহমানির রাহিম
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:28px;">
                        শাহ্‌ ইসমাঈল গাজী (রহঃ) কোল্ড ষ্টোরেজ লিঃ
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">
                        সায়েকপুর, খেজমতপুর, পীরগঞ্জ, রংপুর। মোবাঃ ০১৭১৫৫৯৭৫৯১,০১৭১৯৭০৮৬২১, ০১৭৩২১১২১৯৬, হেড অফিসঃ ধানমণ্ডি, ঢাকা
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:20px;">
                        পার্টি রেজিষ্টার
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">
                        Search By : <asp:Label runat="server" ID="lblSearch"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        &nbsp;
                    </td>
                </tr>
			    <tr>
				    <td align="left">
                        <asp:GridView ID="grvParty" DataKeyNames="PartyID" runat="server" Width="100%" AutoGenerateColumns="False"
                            CellPadding="4" HorizontalAlign="Left" EmptyDataText="No Records Found" DataSourceID="dsParty">
                            <Columns>
                                <asp:TemplateField HeaderText="Sl #" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSl" Text='<%# Eval("SlNo") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PartyID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPartyID" Text='<%# Eval("PartyID") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="14%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPartyType" Text='<%# Eval("PartyType") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPartyCode" Text='<%# Eval("PartyCode") %>' runat="server" HorizontalAlign="Left"  Font-Size="13px"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPartyName" Text='<%# Eval("PartyName") %>' runat="server" Font-Size="14px" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Father" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFather" Text='<%# Eval("FatherName") %>' runat="server" HorizontalAlign="Left"  Font-Size="14px"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Contact No" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContactNo" Text='<%# Eval("ContactNo") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Area/Village" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAreaVillage" Text='<%# Eval("AreaVillageName") %>' runat="server" HorizontalAlign="Left"  Font-Size="14px"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="PO" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAreaPO" Text='<%# Eval("AreaPOName") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="#CC0000" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="dsParty" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>" 
                            SelectCommand="SELECT ROW_NUMBER() OVER (ORDER BY ip.PartyID) As SlNo, ip.PartyID, ip.PartyType, ip.PartyCode,ip.bagcount, 
                                ip.PartyName, ip.FatherName, ip.ContactNo, ip.AreaVillageName, ip.AreaPOName, au.UpazilaPSName, ad.DistrictName, ip.Tel, ip.Cell, ip.Email 
                                FROM INVParty AS ip INNER JOIN ADMDistrict AS ad ON ip.DistrictID = ad.DistrictID INNER JOIN
                                ADMUpazilaPS AS au ON ip.UpazilaPSID = au.UpazilaPSID"
                            FilterExpression="PartyName LIKE '%{0}%' OR PartyCode LIKE '{1}%'  OR FatherName LIKE '{2}%' OR AreaVillageName LIKE '{3}%' OR ContactNo LIKE '{4}%' OR PartyType LIKE '{5}%'">
                            <FilterParameters>
                                <asp:ControlParameter Name="PartyName" ControlID="hdnSearch" PropertyName="Value" />
                                <asp:ControlParameter Name="PartyCode" ControlID="hdnSearch" PropertyName="Value" />
                                <asp:ControlParameter Name="FatherName" ControlID="hdnSearch" PropertyName="Value" />
                                <asp:ControlParameter Name="AreaVillageName" ControlID="hdnSearch" PropertyName="Value" />
                                <asp:ControlParameter Name="ContactNo" ControlID="hdnSearch" PropertyName="Value" />
                                <asp:ControlParameter Name="PartyType" ControlID="hdnSearch" PropertyName="Value" />
                            </FilterParameters>
                        </asp:SqlDataSource>
                    </td>
			    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
