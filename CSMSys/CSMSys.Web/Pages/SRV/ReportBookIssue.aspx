﻿<%@ Page Title="Book/Token Issue Register" Language="C#" AutoEventWireup="true" CodeBehind="ReportBookIssue.aspx.cs" Inherits="CSMSys.Web.Pages.SRV.ReportBookIssue" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="hdnSearch" runat="server" />
        <div class="feature-box-full">
            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                <tbody>
                <tr>
                    <td align="center" valign="top" style="font-size:10px;">
                        বিসমিল্লাহির রাহমানির রাহিম
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:28px;">
                        শাহ্‌ ইসমাঈল গাজী (রহঃ) কোল্ড ষ্টোরেজ লিঃ
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">
                        সায়েকপুর, খেজমতপুর, পীরগঞ্জ, রংপুর। মোবাঃ ০১৭১৫৫৯৭৫৯১,০১৭১৯৭০৮৬২১, ০১৭৩২১১২১৯৬, হেড অফিসঃ ধানমণ্ডি, ঢাকা
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:20px;">
                        বুক/টোকেন ইস্যু রেজিষ্টার
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">                        
                        তারিখ: <asp:Label ID="lblfrom" runat="server" Text=""></asp:Label>
                        হতেঃ <asp:Label ID="lblto" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top">
                        &nbsp;
                    </td>
                </tr>
			    <tr>
				    <td align="left">
                        <asp:GridView ID="grvBookIssue" DataKeyNames="BookID" runat="server" Width="100%" AutoGenerateColumns="False"
                            CellPadding="4" HorizontalAlign="Left" EmptyDataText="No Records Found" DataSourceID="dsBookIssue">
                            <Columns>
                                <asp:BoundField DataField="SlNo" HeaderText="SlNo" SortExpression="SlNo" />
                                <asp:BoundField DataField="IssueDate" HeaderText="তারিখ" SortExpression="IssueDate" />
                                <asp:BoundField DataField="PartyCode" HeaderText="পার্টি কোড" SortExpression="PartyCode" />
                                <asp:BoundField DataField="PartyName" HeaderText="পার্টির নাম" SortExpression="PartyName" />
                                <asp:BoundField DataField="ContactNo" HeaderText="মোবাইল নং" SortExpression="ContactNo" />
                                <asp:BoundField DataField="BookID" HeaderText="বুক আইডি" SortExpression="BookID" />
                                <asp:BoundField DataField="BookNumber" HeaderText="Book No." SortExpression="BookNumber" />
                                <asp:BoundField DataField="PageNo" HeaderText="Page No" SortExpression="PageNo" />
                                <asp:BoundField DataField="Remarks" HeaderText="মন্তব্য" SortExpression="Remarks" />
                            </Columns>
                        </asp:GridView>

                         <asp:SqlDataSource ID="dsBookIssue" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>"
                            SelectCommand="SP_BookIssue_Register" SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="FromDate" QueryStringField="FD" Type="DateTime" />
                                <asp:QueryStringParameter Name="ToDate" QueryStringField="TD" Type="DateTime" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
			    </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
