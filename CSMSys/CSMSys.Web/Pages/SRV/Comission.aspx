﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Comission.aspx.cs"
 MasterPageFile="~/Default.Master" Inherits="CSMSys.Web.Pages.SRV.Comission" Title="Commision & Discount" %>
 
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../../App_Themes/TableSorter/Blue/style.css" rel="stylesheet" type="text/css" />
    <link href="../../../App_Themes/TableSorter/Green/style.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function ShowEditModal(CommisonID) {
            var frame = $get('IframeEdit');
            frame.src = "../../Controls/SRV/CommisonPopup.aspx?UIMODE=EDIT&CID=" + CommisonID;
            $find('EditModalPopup').show();
        }
        function EditCancelScript() {
            var frame = $get('IframeEdit');
            frame.src = "../../../Controls/Loading.aspx";
        }
        function EditOkayScript() {
            RefreshDataGrid();
            EditCancelScript();
        }
        function RefreshDataGrid() {
            $get('btnRefresh').click();
        }
        function NewOkayScript() {
            $get('btnRefresh').click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="content" style="height:620px;">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="title">
        <table width="100%" border="0" cellpadding="2" cellspacing="4">
            <tbody>
                <tr>
                    <td align="left" style="width: 20%;">
                        <h2>Commision/Discount</h2>
                    </td>
                    <td align="right" valign="bottom" style="width: 76%;">
                        Search by Name/Code/Village/Contact : 
                        <asp:TextBox ID="txtSearch" runat="server" Width="107px"></asp:TextBox>
                         &nbsp;and Date From 
                        <asp:TextBox ID="txtFromDate" runat="server" Width="87px"></asp:TextBox>
                        <cc1:CalendarExtender ID="ceFromDate" runat="server" TargetControlID="txtFromDate" />
                         &nbsp;To 
                        <asp:TextBox ID="txtToDate" runat="server" Width="87px"></asp:TextBox>
                        <cc1:CalendarExtender ID="ceToDate" runat="server" TargetControlID="txtToDate" />
                    </td>
                    <td align="center" valign="bottom" style="width: 2%;">
                        <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" ImageUrl="~/App_Themes/Default/Images/gridview/Search.png"
                            ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch_Click" />
                    </td>
                    <td align="center" valign="bottom" style="width: 2%;">
                        <asp:ImageButton ID="imgRefresh" runat="server" CommandName="Refresh" ImageUrl="~/App_Themes/Default/Images/gridview/Refresh.png"
                            ToolTip="Refresh" Width="16px" Height="16px" OnClick="imgRefresh_Click" />
                    </td>
                    <td align="center" valign="bottom" style="width: 3%;">
                        <asp:ImageButton ID="imgNew" runat="server" CommandName="New" ImageUrl="~/App_Themes/Default/Images/gridview/New.png"
                            ToolTip="New" Width="16px" Height="16px" />
                        <cc1:ModalPopupExtender ID="ModalPopupExtender1" BackgroundCssClass="ModalPopupBG"
                            runat="server" CancelControlID="ButtonNewCancel" OkControlID="ButtonNewDone"
                            TargetControlID="imgNew" PopupControlID="DivNewWindow" OnOkScript="NewOkayScript();">
                        </cc1:ModalPopupExtender>
                        <div class="popup_Buttons" style="display: none">
                            <input id="ButtonNewDone" value="Done" type="button" />
                            <input id="ButtonNewCancel" value="Cancel" type="button" />
                        </div>
                        <div id="DivNewWindow" style="display: none;" class="popupDelivery">
                            <iframe id="IframeNew" frameborder="0" width="1050px" height="540px" src="../../Controls/SRV/CommisonPopup.aspx?UIMODE=NEW"
                                class="frameborder" scrolling="no"></iframe>
                        </div>
                        <asp:Button ID="btnRefresh" runat="server" Text="Refresh" Style="display: none" OnClick="btnRefresh_Click" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="feature-box-full">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td align="left" valign="top">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td align="left" valign="top">
                                                <asp:GridView ID="grvCommision" DataKeyNames="CommisonID" runat="server" Width="100%" AutoGenerateColumns="False"
                                                    CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvCommision_PageIndexChanging" ShowHeaderWhenEmpty="true" 
                                                    OnRowDataBound="grvCommision_RowDataBound" 
                                                    EmptyDataText="No Records Found" CssClass="tablesorterBlue" AllowPaging="True" PageSize="15" DataSourceID="dsCommsion">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="CommisionID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeliveryID" Text='<%# Eval("CommisonID") %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Party ID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPartyID" Text='<%# Eval("PartyID") %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="কোড" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPartyCode" Text='<%# Eval("PartyCode") %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="নাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPartyName" Text='<%# Eval("PartyName") %>' runat="server" HorizontalAlign="Left" style="font-size:12px;" />
                                                              </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="মোবাইল নং" Visible="true" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%" >
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblContactNo" Text='<%# Eval("ContactNo") %>' runat="server" HorizontalAlign="Left" />                                       
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="তারিখ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%" >
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeliveryDate" Text='<%# Eval("Date","{0:dd/MM/yyyy}") %>' runat="server" HorizontalAlign="Left" />                                       
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" >
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWBags" Text='<%# Eval("BagCount") %>' runat="server" HorizontalAlign="Left" />                                       
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="বস্তা প্রতি কমিশন" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblCommisionPerBag" Text='<%# Eval("CommisionPerBag") %>' runat="server" HorizontalAlign="Center" />                                                               
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="মোট কমিশন" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalCommision" Text='<%# Eval("TotalCommision","{0:N}") %>' runat="server" HorizontalAlign="Center" />                                                               
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="বস্তা প্রতি ডিস্কাউন্ট" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDiscountPerBag" Text='<%# Eval("DiscountPerBag") %>' runat="server" HorizontalAlign="Center" />                                                               
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="মোট ডিস্কাউন্ট" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalDiscout" Text='<%# Eval("TotalDiscout","{0:N}") %>' runat="server" HorizontalAlign="Center" />                                                               
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                            ItemStyle-Width="2%">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hplprint" runat="server"  Text="Print" Target="_blank"></asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="2%"  ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgEdit" runat="server" CommandName="Edit" ImageUrl="~/App_Themes/Default/Images/gridview/Edit.png" ToolTip="Edit" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                                                    <AlternatingRowStyle BackColor="#E5EAE8" />
                                                    <EditRowStyle BackColor="#999999" />
                                                    <EmptyDataRowStyle ForeColor="#CC0000" />
                                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                </asp:GridView>
                                                <asp:SqlDataSource ID="dsCommsion" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>" OnSelecting="dsDisburse_Selecting" 
                                                    SelectCommand="SELECT ROW_NUMBER() OVER (ORDER BY srvcomm.CommisonID) As SlNo,ip.PartyCode,ip.PartyName, ip.FatherName, ip.ContactNo, ip.PartyID,ip.AreaVillageName,
				srvcomm.CommisonID,srvcomm.BagCount,srvcomm.DiscountPerBag,srvcomm.CommisionPerBag,srvcomm.TotalCommision,srvcomm.TotalDiscout,srvcomm.Date
                                                        FROM SRVCommision AS srvcomm 
                                                        INNER JOIN INVParty AS ip ON srvcomm.PartyID = ip.PartyID
                                                        WHERE ((srvcomm.Date >= @DateFrom) AND (srvcomm.Date  <= @DateTo)) ORDER BY srvcomm.CommisonID desc" 
                                                    FilterExpression="PartyName LIKE '%{0}%' OR ContactNo LIKE '{1}%' OR AreaVillageName LIKE '{2}%' OR PartyCode LIKE '{3}%'">
                                                    <FilterParameters>
                                                        <asp:ControlParameter Name="PartyName" ControlID="txtSearch" PropertyName="Text" />
                                                        <asp:ControlParameter Name="ContactNo" ControlID="txtSearch" PropertyName="Text" />
                                                        <asp:ControlParameter Name="AreaVillageName" ControlID="txtSearch" PropertyName="Text" />
                                                        <asp:ControlParameter Name="PartyCode" ControlID="txtSearch" PropertyName="Text" />
                                                    </FilterParameters>
                                                    <SelectParameters>
                                                        <asp:Parameter Name="DateFrom" Type="DateTime" />
                                                        <asp:Parameter Name="DateTo" Type="DateTime" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <div class="feature-box-actionBar">
                                                    <span class="failureNotification">
                                                        <asp:Literal ID="lblFailure" runat="server"></asp:Literal>
                                                    </span>
                                                    <asp:Button ID="btnReport" runat="server" CssClass="button" 
                                                        Text="Comission Register" onclick="btnReport_Click"  />
                                                        <asp:Button ID="btnReport1" runat="server" CssClass="button" 
                                                        Text="Discount Register" onclick="btnReport1_Click"  />
                                                       
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
      </div>
    
    <asp:Button ID="ButtonEdit" runat="server" Text="Submit" Style="display: none" />
    <cc1:ModalPopupExtender ID="ModalPopupExtender2" BackgroundCssClass="ModalPopupBG"
        runat="server" CancelControlID="ButtonEditCancel" OkControlID="ButtonEditDone"
        TargetControlID="ButtonEdit" PopupControlID="DivEditWindow" OnCancelScript="EditCancelScript();"
        OnOkScript="EditOkayScript();" BehaviorID="EditModalPopup">
    </cc1:ModalPopupExtender>
    <div class="popup_Buttons" style="display: none">
        <input id="ButtonEditDone" value="Done" type="button" />
        <input id="ButtonEditCancel" value="Cancel" type="button" />
    </div>
    <div id="DivEditWindow" style="display: none;" class="popupDelivery">
        <iframe id="IframeEdit" frameborder="0" width="1050px" height="540px" class="frameborder"
            scrolling="no"></iframe>
    </div>
</div>
</asp:Content>
