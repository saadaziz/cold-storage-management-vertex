﻿<%@ Page Title="Agreement Report" Language="C#" AutoEventWireup="true" CodeBehind="ReportAgreement.aspx.cs" Inherits="CSMSys.Web.Pages.SRV.ReportAgreement" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="~/App_Themes/Default/Styles/Default.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
        function btnDrPrint_onclick() {

        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div class="feature-box-full">
        <div id="printReport" class="VerticalyMiddle">
            <table width="100%"  border="0" cellpadding="0" cellspacing="4">
                <tbody>
                <tr>
                    <td align="center" valign="top" style="font-size:10px;">
                        বিসমিল্লাহির রাহমানির রাহিম
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:28px;">
                        শাহ্‌ ইসমাঈল গাজী (রহঃ) কোল্ড ষ্টোরেজ লিঃ
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:10px;">
                        সায়েকপুর, খেজমতপুর, পীরগঞ্জ, রংপুর। মোবাঃ ০১৭১৫৫৯৭৫৯১,০১৭১৯৭০৮৬২১, ০১৭৩২১১২১৯৬, হেড অফিসঃ ধানমণ্ডি, ঢাকা
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:22px;">
                        সংরক্ষিত আলুর মূল দলিল
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <table border="0" cellpadding="0" cellspacing="4" width="100%">
                            <tbody>
                                <tr>
                                    <td align="left" colspan="2" style="width:50%;">
                                        বুকিং নং :
                                        <asp:Label ID="lblRegistrationID" runat="server"></asp:Label>
                                    </td>
                                    <td align="right" colspan="2">
                                        দলিল নং :
                                        <asp:Label ID="lblSR" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2">
                                        পার্টির নাম : 
                                        <asp:Label ID="lblName" runat="server"></asp:Label>
                                    </td>
                                    <td align="left" colspan="2">
                                        পিতার নাম : 
                                        <asp:Label ID="lblFather" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width:25%;">
                                        গ্রাম : 
                                        <asp:Label ID="lblVill" runat="server"></asp:Label>
                                    </td>
                                    <td align="left" style="width:25%;">
                                        ডাকঘর : 
                                        <asp:Label ID="lblPO" runat="server"></asp:Label>
                                    </td>
                                    <td align="left" style="width:25%;">
                                        থানা : 
                                        <asp:Label ID="lblPS" runat="server"></asp:Label>
                                    </td>
                                    <td align="left" style="width:25%;">
                                        জেলা : 
                                        <asp:Label ID="lblDist" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width:25%;">
                                        পার্টি কোড : 
                                        <asp:Label ID="lblCode" runat="server"></asp:Label>
                                    </td>
                                    <td align="left" style="width:25%;">
                                        পার্টির ধরন : 
                                        <asp:Label ID="lblType" runat="server"></asp:Label>
                                    </td>
                                    <td align="left" colspan="2" style="width:50%;">
                                        মোবাইল নং : 
                                        <asp:Label ID="lblContact" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" style="width:25%;">
                                        ক্যারিং লোন : 
                                        <asp:Label ID="lblCarrying" runat="server"></asp:Label>
                                    </td>
                                    <td align="left" colspan="3" style="width:75%;">
                                        মন্তব্য : 
                                        <asp:Label ID="lblRemarks" runat="server"></asp:Label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%" style="height:122px;">
                            <tbody>
                                <tr>
                                    <td align="left" >
                                        <asp:GridView ID="grvRegistration" DataKeyNames="RegistrationID" runat="server" Width="100%" AutoGenerateColumns="False"
                                            CellPadding="4" HorizontalAlign="Left" EmptyDataText="" GridLines="Both" ShowHeaderWhenEmpty="true" DataSourceID="dsRegistration">
                                            <Columns>
                                                <asp:TemplateField HeaderText="RegistrationID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRegistrationID" Text='<%# Eval("RegistrationID") %>' runat="server" HorizontalAlign="Left" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="আলুর বিবরণ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblRemarks" Text='<%# Eval("Remarks") %>' runat="server" HorizontalAlign="Left" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="বস্তার সংখ্যা" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBags" Text='<%# Eval("Bags") %>' runat="server" HorizontalAlign="Left" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="প্রতি বস্তার ওজন" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBagWeight" Text='<%# Eval("BagWeight") %>' runat="server" HorizontalAlign="Left" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="পরিবহন" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ItemStyle-Width="12%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCarryingLoan" Text='<%# Eval("CarryingLoan","{0:N}") %>' runat="server" Font-Size="14px" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="খালি বস্তা" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblBagLoan" Text='<%# Eval("BagLoan") %>' runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="আলুর জাত" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="30%">
                                                    <ItemTemplate>
                                                        <table border="0" cellpadding="0" cellspacing="2" width="100%">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="left" >
                                                                        পাকড়ি / কার্ডিনাল / বার্মা / সাদাগুটি /
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" >
                                                                        গ্রানুলা / স্টিক / ডায়মন্ড / জাম /
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" >
                                                                        ফেন্সিলা / রোজেটা / লরা / শিল বিলাতী
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle Font-Bold="false" Wrap="true" VerticalAlign="Top" />
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="dsRegistration" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>" OnSelecting="dsRegistration_Selecting" 
                                            SelectCommand="SELECT sr.RegistrationID, sr.SerialID, sr.SerialNo, ss.Bags, sr.BagLoan, sr.CarryingLoan, sr.BagWeight, sr.Remarks
                                                FROM SRVRegistration AS sr INNER JOIN
                                                INVStockSerial AS ss ON sr.SerialID = ss.SerialID
                                                WHERE (sr.RegistrationID = @RegistrationID)">
                                            <SelectParameters>
                                                <asp:Parameter Name="RegistrationID" Type="Int32" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:18px;">
                        <p>লোন গ্রহীতাগণকে ১৫ই সেপ্টেম্বরের মধ্যে অবশ্যই লোন পরিশোধ করিতে হইবে।
                            অন্যথায় কোম্পানি নিজ দায়িত্বে আলু বিক্রয় করিয়া লোন আদায় করিবে।</p>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="font-size:12px;">
                        <p>অপর পৃষ্ঠায় বর্ণিত শর্তাবলী মানিয়া আলু জমা রাখিলাম।</p>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table border="0" cellpadding="0" cellspacing="4" width="100%">
                            <tbody>
                                <tr>
                                    <td align="center" valign="top" style="width: 20%;">
                                        পার্টির স্বাক্ষর
                                    </td>
                                    <td align="center" valign="top" style="width: 20%;">
                                        প্রস্তুতকারীর স্বাক্ষর
                                    </td>
                                    <td align="center" valign="top" style="width: 20%;">
                                        অডিট ম্যানাজারের স্বাক্ষর</td>
                                    <td align="center" valign="top" style="width: 20%;">
                                        ষ্টোর কিপারের স্বাক্ষর
                                    </td>
                                    <td align="center" valign="top" style="width: 20%;">
                                        মহা-ব্যবস্থাপকের স্বাক্ষর
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
                            <tbody>
                                <tr>
                                    <td align="center" colspan="4" style="font-size:18px;">
                                        ডেলিভারী তথ্য
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        <table border="0" cellpadding="0" cellspacing="4" width="100%">
                                            <tbody>
                                                <tr>
                                                    <td align="left" >
                                                        <asp:GridView ID="grvDelivery" DataKeyNames="RegistrationID" runat="server" Width="100%" AutoGenerateColumns="False"
                                                            CellPadding="4" HorizontalAlign="Left" EmptyDataText="" GridLines="Both" ShowHeaderWhenEmpty="true" DataSourceID="dsDelivery">
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="DeliveryID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDeliveryID" Text='<%# Eval("RegistrationID") %>' runat="server" HorizontalAlign="Left" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="তারিখ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDeliveryDate" Text='<%# Eval("CollectionDate","{0:dd/MM/yyyy}") %>' runat="server" HorizontalAlign="Left" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="গেট পাশ নং" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblGatePass" Text='<%# Eval("CollectionID") %>' runat="server" HorizontalAlign="Left" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="বস্তা সংখ্যা" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ItemStyle-Width="5%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblBagCount" Text='<%# Eval("WBags") %>' runat="server" HorizontalAlign="Left" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="মূল ঋণ টাকা" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTotalLoan" Text='<%# Eval("PotatoLoan","{0:N}") %>' runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="পরিবহন ঋণ টাকা" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblCarryingLoan" Text='<%# Eval("CarryingLoan","{0:N}") %>' runat="server"/>
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="দিন সংখ্যা" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ItemStyle-Width="5%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblDayCount" Text='<%# Eval("Days") %>' runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ইন্টারেস্ট" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblServiceCharge" Text='<%# Eval("Interest","{0:N}") %>' runat="server" />
                                                                    </ItemTemplate>                              
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="ভাড়া টাকা" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblKeepingCharge" Text='<%# Eval("BagCharge","{0:N}") %>' runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="খালি বস্তা টাকা" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblEmptyBagLoan" Text='<%# Eval("BagLoan","{0:N}") %>' runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="সর্বমোট" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblTotal" Text='<%# Eval("TotalAmount","{0:N}") %>' runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="স্বাক্ষর" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Top" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblSign" runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle Font-Bold="false" Wrap="true" VerticalAlign="Top" />
                                                        </asp:GridView>
                                                        <asp:SqlDataSource ID="dsDelivery" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>" OnSelecting="dsDelivery_Selecting" 
                                                            SelectCommand= "SELECT      rgr.RegistrationID,lc.CollectionDate, lcd.CollectionID, lcd.WBags, lcd.SerialID, lcd.BagLoan, lcd.CarryingLoan, lcd.PotatoLoan, lcd.Days, lcd.Interest, lcd.BagCharge, 
                                                                            lcd.TotalAmount
                                                                            FROM         dbo.SRVRegistration AS rgr INNER JOIN
                                                                            dbo.SRVLoanCollectionDetail AS lcd ON rgr.SerialID = lcd.SerialID INNER JOIN
                                                                            dbo.SRVLoanCollection AS lc ON lcd.CollectionID = lc.CollectionID
                                                                            WHERE (rgr.RegistrationID = @RegistrationID)">
                                                            <SelectParameters>
                                                                <asp:Parameter Name="RegistrationID" Type="Int32" />
                                                            </SelectParameters>
                                                        </asp:SqlDataSource>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top" style="width: 25%;">
                                        প্রস্তুতকারীর স্বাক্ষর
                                    </td>
                                    <td align="center" valign="top" style="width: 25%;">
                                        অডিট ম্যানাজারের স্বাক্ষর
                                    </td>
                                    <td align="center" valign="top" style="width: 25%;">
                                        ষ্টোর কিপারের স্বাক্ষর
                                    </td>
                                    <td align="center" valign="top" style="width: 25%;">
                                        মহা-ব্যবস্থাপকের স্বাক্ষর
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="4">
                                        &nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table> 
        </div>
    </div>
    </form>
</body>
</html>
