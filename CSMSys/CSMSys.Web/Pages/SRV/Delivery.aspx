﻿<%@ Page Title="Delivery" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" 
CodeBehind="Delivery.aspx.cs" Inherits="CSMSys.Web.Pages.SRV.Delivery" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../../App_Themes/TableSorter/Blue/style.css" rel="stylesheet" type="text/css" />
    <link href="../../../App_Themes/TableSorter/Green/style.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function ShowEditModal(DeliveryID) {
            var frame = $get('IframeEdit');
            frame.src = "../../Controls/SRV/Delivery.aspx?UIMODE=EDIT&DID=" + DeliveryID;
            $find('EditModalPopup').show();
        }
        function EditCancelScript() {
            var frame = $get('IframeEdit');
            frame.src = "../../../Controls/Loading.aspx";
        }
        function EditOkayScript() {
            RefreshDataGrid();
            EditCancelScript();
        }
        function RefreshDataGrid() {
            $get('btnRefresh').click();
        }
        function NewOkayScript() {
            $get('btnRefresh').click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="content" style="height:620px;">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="title">
        <table width="100%" border="0" cellpadding="2" cellspacing="4">
            <tbody>
                <tr>
                    <td align="left" style="width: 20%;">
                        <h2>Collection/Delivery</h2>
                    </td>
                    <td align="right" valign="bottom" style="width: 76%;">
                        Search by Name/Code/Village/Contact : 
                        <asp:TextBox ID="txtSearch" runat="server" Width="107px"></asp:TextBox>
                         &nbsp;and Date From 
                        <asp:TextBox ID="txtFromDate" runat="server" Width="87px"></asp:TextBox>
                        <cc1:CalendarExtender ID="ceFromDate" runat="server" TargetControlID="txtFromDate" />
                         &nbsp;To 
                        <asp:TextBox ID="txtToDate" runat="server" Width="87px"></asp:TextBox>
                        <cc1:CalendarExtender ID="ceToDate" runat="server" TargetControlID="txtToDate" />
                    </td>
                    <td align="center" valign="bottom" style="width: 2%;">
                        <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" ImageUrl="~/App_Themes/Default/Images/gridview/Search.png"
                            ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch_Click" />
                    </td>
                    <td align="center" valign="bottom" style="width: 2%;">
                        <asp:ImageButton ID="imgRefresh" runat="server" CommandName="Refresh" ImageUrl="~/App_Themes/Default/Images/gridview/Refresh.png"
                            ToolTip="Refresh" Width="16px" Height="16px" OnClick="imgRefresh_Click" />
                    </td>
                    <td align="center" valign="bottom" style="width: 3%;">
                        <asp:ImageButton ID="imgNew" runat="server" CommandName="New" ImageUrl="~/App_Themes/Default/Images/gridview/New.png"
                            ToolTip="New" Width="16px" Height="16px" />
                        <cc1:ModalPopupExtender ID="ModalPopupExtender1" BackgroundCssClass="ModalPopupBG"
                            runat="server" CancelControlID="ButtonNewCancel" OkControlID="ButtonNewDone"
                            TargetControlID="imgNew" PopupControlID="DivNewWindow" OnOkScript="NewOkayScript();">
                        </cc1:ModalPopupExtender>
                        <div class="popup_Buttons" style="display: none">
                            <input id="ButtonNewDone" value="Done" type="button" />
                            <input id="ButtonNewCancel" value="Cancel" type="button" />
                        </div>
                        <div id="DivNewWindow" style="display: none;" class="popupDelivery">
                            <iframe id="IframeNew" frameborder="0" width="1050px" height="565px" src="../../Controls/SRV/Delivery.aspx?UIMODE=NEW&DID=0"
                                class="frameborder" scrolling="no"></iframe>
                        </div>
                        <asp:Button ID="btnRefresh" runat="server" Text="Refresh" Style="display: none" OnClick="btnRefresh_Click" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="feature-box-full">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td align="left" valign="top">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td align="left" valign="top">
                                                <asp:GridView ID="grvDelivery" DataKeyNames="CollectionID" runat="server" Width="100%" AutoGenerateColumns="False"
                                                    CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvDelivery_PageIndexChanging" ShowHeaderWhenEmpty="true" 
                                                    OnRowDataBound="grvDelivery_RowDataBound" 
                                                    EmptyDataText="No Records Found" CssClass="tablesorterBlue" AllowPaging="True" PageSize="15" DataSourceID="dsDisburse">
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="DeliveryID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeliveryID" Text='<%# Eval("CollectionID") %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Party ID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPartyID" Text='<%# Eval("PartyID") %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="কোড" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPartyCode" Text='<%# Eval("PartyCode") %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="নাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPartyName" Text='<%# Eval("PartyName") %>' runat="server" HorizontalAlign="Left" style="font-size:12px;" />
                                                              </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="পিতার নাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFatherName" Text='<%# Eval("FatherName") %>' runat="server" HorizontalAlign="Left" style="font-size:12px;" />
                                                              </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="গ্রাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAreaVillageName" Text='<%# Eval("AreaVillageName") %>' runat="server" HorizontalAlign="Left" style="font-size:12px;" />
                                                              </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="মোবাইল নং" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%" >
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblContactNo" Text='<%# Eval("ContactNo") %>' runat="server" HorizontalAlign="Left" />                                       
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="তারিখ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%" >
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDeliveryDate" Text='<%# Eval("CollectionDate","{0:dd/MM/yyyy}") %>' runat="server" HorizontalAlign="Left" />                                       
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" >
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblWBags" Text='<%# Eval("WBags") %>' runat="server" HorizontalAlign="Left" />                                       
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Total" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTotalAmount" Text='<%# Eval("TotalAmount","{0:N}") %>' runat="server" HorizontalAlign="Center" />                                                               
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Paid" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPaidAmount" Text='<%# Eval("PaidAmount","{0:N}") %>' runat="server" HorizontalAlign="Center" />                                                               
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Dues" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblDuesAmount" Text='<%# Eval("DuesAmount","{0:N}") %>' runat="server" HorizontalAlign="Center" />                                                               
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="TMID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblTMID" Text='<%# Eval("TMID") %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                            ItemStyle-Width="2%">
                                                            <ItemTemplate>
                                                                <asp:HyperLink ID="hplprint" runat="server"  Text="Print" Target="_blank"></asp:HyperLink>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField ItemStyle-Width="2%"  ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgEdit" runat="server" CommandName="Edit" ImageUrl="~/App_Themes/Default/Images/gridview/Edit.png" ToolTip="Edit" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                                                    <AlternatingRowStyle BackColor="#E5EAE8" />
                                                    <EditRowStyle BackColor="#999999" />
                                                    <EmptyDataRowStyle ForeColor="#CC0000" />
                                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                </asp:GridView>
                                                <asp:SqlDataSource ID="dsDisburse" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>" OnSelecting="dsDisburse_Selecting" 
                                                    SelectCommand="SELECT ROW_NUMBER() OVER (ORDER BY lc.CollectionID) As SlNo,lc.CollectionID, srvlcd.WBags,lc.CollectionDate,
                                                        ip.PartyCode,ip.PartyName, ip.FatherName, ip.ContactNo, ip.PartyID,ip.AreaVillageName, lc.SerialIDs, 
                                                        lc.TotalAmount, lc.PaidAmount, lc.DuesAmount,lc.TMID
                                                        FROM SRVLoanCollection AS lc 
                                                        INNER JOIN SRVLoanCollectionDetail as srvlcd on srvlcd.CollectionID=lc.CollectionID
                                                        INNER JOIN INVParty AS ip ON lc.PartyID = ip.PartyID
                                                        WHERE ((lc.CollectionDate >= @DateFrom) AND (lc.CollectionDate <= @DateTo)) ORDER BY lc.CollectionID" 
                                                    FilterExpression="PartyName LIKE '%{0}%' OR ContactNo LIKE '{1}%' OR SerialIDs LIKE '{2}%' OR AreaVillageName LIKE '{3}%' OR PartyCode LIKE '{4}%'">
                                                    <FilterParameters>
                                                        <asp:ControlParameter Name="PartyName" ControlID="txtSearch" PropertyName="Text" />
                                                        <asp:ControlParameter Name="ContactNo" ControlID="txtSearch" PropertyName="Text" />
                                                        <asp:ControlParameter Name="SerialNo" ControlID="txtSearch" PropertyName="Text" />
                                                        <asp:ControlParameter Name="AreaVillageName" ControlID="txtSearch" PropertyName="Text" />
                                                        <asp:ControlParameter Name="PartyCode" ControlID="txtSearch" PropertyName="Text" />
                                                    </FilterParameters>
                                                    <SelectParameters>
                                                        <asp:Parameter Name="DateFrom" Type="DateTime" />
                                                        <asp:Parameter Name="DateTo" Type="DateTime" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <div class="feature-box-actionBar">
                                                    <span class="failureNotification">
                                                        <asp:Literal ID="lblFailure" runat="server"></asp:Literal>
                                                    </span>
                                                    <asp:Button ID="btnReport" runat="server" CssClass="button" Text="Delivery Report" OnClick="btnReport_Click" />
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
      </div>
    
    <asp:Button ID="ButtonEdit" runat="server" Text="Submit" Style="display: none" />
    <cc1:ModalPopupExtender ID="ModalPopupExtender2" BackgroundCssClass="ModalPopupBG"
        runat="server" CancelControlID="ButtonEditCancel" OkControlID="ButtonEditDone"
        TargetControlID="ButtonEdit" PopupControlID="DivEditWindow" OnCancelScript="EditCancelScript();"
        OnOkScript="EditOkayScript();" BehaviorID="EditModalPopup">
    </cc1:ModalPopupExtender>
    <div class="popup_Buttons" style="display: none">
        <input id="ButtonEditDone" value="Done" type="button" />
        <input id="ButtonEditCancel" value="Cancel" type="button" />
    </div>
    <div id="DivEditWindow" style="display: none;" class="popupDelivery">
        <iframe id="IframeEdit" frameborder="0" width="1050px" height="540px" class="frameborder"
            scrolling="no"></iframe>
    </div>
</div>
</asp:Content>
