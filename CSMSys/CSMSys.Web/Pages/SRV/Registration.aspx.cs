﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.SRV;

namespace CSMSys.Web.Pages.SRV
{
    public partial class Registration : System.Web.UI.Page
    {
        #region Private Properties
        private string strSearch = string.Empty;
        private SRVRegistration _Registration;

        private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }

        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateFrom = DateTime.Today;
                txtFromDate.Text = DateFrom.ToShortDateString();
                DateTo = DateTime.Today;
                txtToDate.Text = DateTo.ToShortDateString();

                grvRegistration.DataBind();
            }
        }

        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }
        #endregion

        #region Methods For Grid
        protected void grvRegistration_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Delete"))
            {
                int intRegistrationID = Convert.ToInt32(e.CommandArgument.ToString());

                if (intRegistrationID > 0)
                {
                    DeleteRegistration(intRegistrationID);
                }
            }
        }

        protected void grvRegistration_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvRegistration.PageIndex = e.NewPageIndex;
            grvRegistration.DataBind();
        }

        protected void grvRegistration_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                ImageButton btnEdit = (ImageButton)e.Row.FindControl("imgEdit");
                btnEdit.Attributes.Add("onClick", "javascript:ShowEditModal('" + ((Label)e.Row.FindControl("lblRegistrationID")).Text + "');");

                HyperLink hpl = (HyperLink)e.Row.FindControl("hlPrint");
                hpl.NavigateUrl = "~/pages/SRV/ReportAgreement.aspx?RID=" + int.Parse(((Label)e.Row.FindControl("lblRegistrationID")).Text);
            }
        }
        #endregion

        #region SqlDataSource Control Event Handlers
        protected void dsRegistration_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@DateFrom"].Value = DateFrom;
            e.Command.Parameters["@DateTo"].Value = DateTo;
        }
        #endregion

        #region Methods For Button
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;
            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
            dateInfo.ShortDatePattern = "MM/dd/yyyy";

            DateFrom = Convert.ToDateTime(txtFromDate.Text.ToString(), dateInfo);
            DateTo = Convert.ToDateTime(txtToDate.Text.ToString(), dateInfo);

            grvRegistration.DataBind();
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvRegistration.DataBind();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            grvRegistration.DataBind();
        }
        #endregion

        #region Methods For Delete
        private void DeleteRegistration(int id)
        {
            _Registration = new SRVRegistration();
            _Registration.RegistrationID = id;
            if (new RegistrationManager().DeleteRegistration(_Registration))
            {
                grvRegistration.DataBind();
            }
        }
        #endregion
    }
}