﻿<%@ Page Language="C#" AutoEventWireup="true" Title="CSMSys :: Party Registration Register" CodeBehind="PartyRegistrationRegister.aspx.cs" Inherits="CSMSys.Web.Pages.ACC.Reports.PartyRegistrationRegister" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="feature-box-full">
            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                <tbody>
                <tr>
                    <td align="center" valign="top" style="font-size:10px;">
                        বিসমিল্লাহির রাহমানির রাহিম
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:28px;">
                        শাহ্‌ ইসমাঈল গাজী (রহঃ) কোল্ড ষ্টোরেজ লিঃ
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">
                        সায়েকপুর, খেজমতপুর, পীরগঞ্জ, রংপুর। মোবাঃ ০১৭১৫৫৯৭৫৯১,০১৭১৯৭০৮৬২১, ০১৭৩২১১২১৯৬, হেড অফিসঃ ধানমণ্ডি, ঢাকা
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:20px;">
                        পার্টি রেজিস্ট্রেশন রেজিষ্টার
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">                        
                        তারিখ: <asp:Label ID="lblfrom" runat="server" Text=""></asp:Label>
                        হতেঃ <asp:Label ID="lblto" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
 <tr>
                                <td align="center" valign="top">
                                    &nbsp;
                                </td>
                            </tr>
                            <%--<tr>
                                <td align="center" valign="top">
                                    &nbsp;
                                </td>
                            </tr>--%>
                            <tr>
                                <td align="left" valign="top">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                        <tbody style="font-size:12px;">
                                            <tr>
                                                <td align="left" colspan="2">
                                                    তারিখ :
                                                    <asp:Label runat="server" ID="lblDate"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    পার্টির নাম :
                                                    <asp:Label ID="lblName" runat="server"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    পার্টি কোড নং:
                                                    <asp:Label runat="server" ID="lblCode"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    পার্টির ধরন :
                                                    <asp:Label runat="server" ID="lblPartyType"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    পিতার নাম:
                                                    <asp:Label runat="server" ID="lblFName"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 25%;">
                                                    গ্রাম :
                                                    <asp:Label runat="server" ID="lblVillage"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 25%;">
                                                    ডাক :
                                                    <asp:Label runat="server" ID="lblPO"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 25%;">
                                                    উপজেলা :
                                                    <asp:Label runat="server" ID="lblUpazilla"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 25%;">
                                                    জেলা :
                                                    <asp:Label runat="server" ID="lblDistrict"></asp:Label>
                                                </td>
                                            </tr>
<%--                                            <tr>
                                                <td align="left" colspan="2">
                                                    <strong>তারিখ</strong> :
                                                    <asp:Label ID="lblFromDate" runat="server"></asp:Label>
                                                    <strong>থেকে</strong>
                                                    <asp:Label ID="lblToDate" runat="server"></asp:Label>
                                                    <strong>পর্যন্ত</strong>
                                                </td>
                                            </tr>--%>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    &nbsp;
                                </td>
                            </tr>
                <tr>
                    <td align="center" style="font-size:12px;">
                        <asp:GridView ID="grvPartyRegistrationRegister"  runat="server" Width="100%"
                            AutoGenerateColumns="False" CellPadding="3" HorizontalAlign="Left" ShowHeaderWhenEmpty="true"
                            EmptyDataText="No Records Found" DataSourceID="dsPartyRegistraionRegister"  AllowSorting="true"
                            OnRowDataBound="grvPartyRegistrationRegister_RowDataBound">
                            <Columns>
<asp:TemplateField HeaderText="Sl #" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSlNo" Text='<%# Eval("SlNo") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="তারিখ" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left"  >
                                    <ItemTemplate>
                                        <asp:Label ID="lblSerialDate" Text='<%# Eval("RegistrationDate","{0:dd/MM/yyyy}") %>' runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="রেজিস্ট্রেশন আইডি" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRegistrationID" Text='<%# Eval("RegistrationID") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="সিরিয়াল আইডি" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSerialID" Text='<%# Eval("SerialID") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="এস/আর" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSerialNo" Text='<%# Eval("SerialNo") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="বস্তা" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblBags" Text='<%# Eval("Bags") %>' runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ক্যারিং" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"  >
                                    <ItemTemplate>
                                        <asp:Label ID="lbCarryingLoan" Text='<%# Eval("CarryingLoan") %>' runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                    </asp:TemplateField>
                                 <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="খালি বস্তা" ItemStyle-HorizontalAlign="Left" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblBagLoan" Text='<%# Eval("BagLoan") %>' runat="server" HorizontalAlign="Left"  />
                                      </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="প্রতি বস্তার ওজন"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBagWeight" Text='<%# Eval("BagWeight") %>' runat="server" HorizontalAlign="Left" />
                                      </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                            <EmptyDataRowStyle ForeColor="#CC0000" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="dsPartyRegistraionRegister" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>" OnSelecting="dsPartyRegistraionRegister_Selecting"
                             SelectCommand="SELECT     ROW_NUMBER() OVER (ORDER BY srv.RegistrationID) As SlNo,srv.RegistrationDate, srv.RegistrationID, srv.SerialID,srv.SerialNo, srv.CarryingLoan, srv.Bags, srv.BagLoan, srv.BagWeight
                                                                                     FROM         dbo.SRVRegistration AS srv INNER JOIN
                                                        dbo.INVParty AS ip ON srv.PartyID = ip.PartyID
                                WHERE (srv.PartyID = @PartyID AND (srv.RegistrationDate >= @DateFrom) AND (srv.RegistrationDate <= @DateTo)) ORDER BY srv.RegistrationDate">
                            <SelectParameters>
                             <asp:Parameter Name="PartyID" Type="Int32" />
                                <asp:Parameter Name="DateFrom" Type="DateTime" />
                                <asp:Parameter Name="DateTo" Type="DateTime" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <%--<tr>
                    <td align="center" valign="top">
                        &nbsp;
                    </td>
                </tr>--%>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
