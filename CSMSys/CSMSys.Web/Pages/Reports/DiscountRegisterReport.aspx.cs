﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.INV;

namespace CSMSys.Web.Pages.ACC.Reports
{
    public partial class DiscountRegisterReport : System.Web.UI.Page
    {
        #region Method for Private
        private string printer = ConfigurationManager.AppSettings["StorePrinter"].ToString();
        private string connstring = ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString;
        private int TotalBags = 0;

      private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }

        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }

        private Int32 PartyID
        {
            get
            {
                if (ViewState["PartyID"] == null)
                    ViewState["PartyID"] = -1;
                return (Int32)ViewState["PartyID"];
            }
            set
            {
                ViewState["PartyID"] = value;
            }
        }

        private INVParty _party;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Form.Target = "_blank";
            GridViewHelper helper = new GridViewHelper(this.grvCommision);
            helper.RegisterGroup("date", true, true);
            //helper.RegisterSummary("Bags", SummaryOperation.Sum, "State");
            //helper.RegisterSummary("Bags", SummaryOperation.Sum);
            //helper.GroupSummary += new GroupEvent(helper_GroupSummary);
            helper.ApplyGroupSort();

            if (!IsPostBack)
            {
               // lblDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
               // int intPartyID = Convert.ToInt32(Request.QueryString["PID"]);
              //  PartyID = string.IsNullOrEmpty(Request.QueryString["PID"]) ? 0 : int.Parse(Request.QueryString["PID"]);
                DateFrom = string.IsNullOrEmpty(Request.QueryString["FD"]) ? DateTime.Today : Convert.ToDateTime(Request.QueryString["FD"]);
                DateTo = string.IsNullOrEmpty(Request.QueryString["TD"]) ? DateTime.Today : Convert.ToDateTime(Request.QueryString["TD"]);
              
                
                lblfrom.Text = DateFrom.ToShortDateString();
                lblto.Text = DateTo.ToShortDateString();
                //hdnSearch.Value = code;
                grvCommision.DataBind();

            }
        }

        private void helper_GroupSummary(string groupName, object[] values, GridViewRow row)
        {
            row.Cells[0].HorizontalAlign = HorizontalAlign.Right;
            row.Cells[0].Text = "মোট: ";
        }


        #region Methods For Grid

        #region SqlDataSource Control Event Handlers
        protected void dsLoading_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@DateFrom"].Value = DateFrom;
            e.Command.Parameters["@DateTo"].Value = DateTo;
        }
        #endregion
        #endregion


        
    }
}
