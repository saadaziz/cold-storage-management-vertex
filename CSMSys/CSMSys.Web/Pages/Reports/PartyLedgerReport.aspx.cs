﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CSMSys.Lib.AccountingEntity;
using CSMSys.Lib.AccountingUtility;
using CSMSys.Lib.AccountingDataAccess;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
//using CSMSys.Web.Pages.BagLoanRreport;

namespace CSMSys.Web.Pages.PartyLedger
{
    public partial class PartyLedgerReport : System.Web.UI.Page
    {
        #region Private Properties
        private string connstring = ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString;
        private decimal TotalDebit = (decimal)0.0;
        private decimal TotalCredit = (decimal)0.0;

        SqlConnection formCon = null;

        private int VoucherType
        {
            get
            {
                if (ViewState["VoucherType"] == null)
                    ViewState["VoucherType"] = -1;
                return (int)ViewState["VoucherType"];
            }
            set
            {
                ViewState["VoucherType"] = value;
            }
        }

        private int TransactionID
        {
            get
            {
                if (ViewState["TransactionID"] == null)
                    ViewState["TransactionID"] = -1;
                return (int)ViewState["TransactionID"];
            }
            set
            {
                ViewState["TransactionID"] = value;
            }
        }

        DaTransaction objDaTrans = null;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Form.Target = "_blank";
            MultiViewBagLoan.ActiveViewIndex = 0;
            GridViewHelper helper = new GridViewHelper(this.grvPartyLedger);
            helper.RegisterGroup("CreatedDate", true, true);
            helper.RegisterSummary("BagLoaded", SummaryOperation.Sum, "CreatedDate");
            helper.RegisterSummary("TotalBagLoan", SummaryOperation.Sum, "CreatedDate");
            helper.RegisterSummary("BagLoanAmount", SummaryOperation.Sum, "CreatedDate");
            helper.RegisterSummary("TotalCarry", SummaryOperation.Sum, "CreatedDate");
            helper.RegisterSummary("Loan", SummaryOperation.Sum, "CreatedDate");
            helper.RegisterSummary("TotalLoan", SummaryOperation.Sum, "CreatedDate");
            helper.RegisterSummary("BagLoaded", SummaryOperation.Sum);
            helper.RegisterSummary("TotalBagLoan", SummaryOperation.Sum);
            helper.RegisterSummary("BagLoanAmount", SummaryOperation.Sum);
            helper.RegisterSummary("TotalCarry", SummaryOperation.Sum);
            helper.RegisterSummary("Loan", SummaryOperation.Sum);
            helper.RegisterSummary("TotalLoan", SummaryOperation.Sum);
            helper.RegisterSummary("TotalAmount", SummaryOperation.Sum);
            helper.RegisterSummary("PaidAmount", SummaryOperation.Sum);
            helper.RegisterSummary("DuesAmount", SummaryOperation.Sum);

            if (!IsPostBack)
            {

                DateTime parseFromDate = Convert.ToDateTime(Request.QueryString["FD"]);
                string fromDate = parseFromDate.ToShortDateString();
                DateTime parseToDate = Convert.ToDateTime(Request.QueryString["TD"]);
                string toDate = parseToDate.ToShortDateString();
                lblFromDate.Text = fromDate;
                lblToDate.Text = toDate;
            }
        }
        
        #region Method for Load
        public void LoadDataGrid(string fromDate, string toDate)
            {

            }
        #endregion

       }
}