﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.INV;
using CSMSys.Web.Pages.Reports;

namespace CSMSys.Web.Pages.ACC.Reports
{
    public partial class PartyDeliveryRegister : System.Web.UI.Page
    {
        #region Method for Private
        private string printer = ConfigurationManager.AppSettings["StorePrinter"].ToString();
        private string connstring = ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString;
        private int TotalBags = 0;

      private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }

        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }

        private Int32 PartyID
        {
            get
            {
                if (ViewState["PartyID"] == null)
                    ViewState["PartyID"] = -1;
                return (Int32)ViewState["PartyID"];
            }
            set
            {
                ViewState["PartyID"] = value;
            }
        }

        private INVParty _party;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Form.Target = "_blank";
            GridViewHelper helper = new GridViewHelper(this.grvPartyDeliveryRegister);
            helper.RegisterGroup("CollectionDate", true, true);
            helper.ApplyGroupSort();

            if (!IsPostBack)
            {
                lblDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
               // int intPartyID = Convert.ToInt32(Request.QueryString["PID"]);
                PartyID = string.IsNullOrEmpty(Request.QueryString["PID"]) ? 0 : int.Parse(Request.QueryString["PID"]);
                DateFrom = string.IsNullOrEmpty(Request.QueryString["FD"]) ? DateTime.Today : Convert.ToDateTime(Request.QueryString["FD"]);
                DateTo = string.IsNullOrEmpty(Request.QueryString["TD"]) ? DateTime.Today : Convert.ToDateTime(Request.QueryString["TD"]);
                LoadAllValue(PartyID);
              
                
                lblfrom.Text = DateFrom.ToShortDateString();
                lblto.Text = DateTo.ToShortDateString();
                //hdnSearch.Value = code;
                grvPartyDeliveryRegister.DataBind();

            }
        }

    
        #region Method for Grid
        #region SqlDataSource Control Event Handlers
        protected void dsPartyDeliveryRegister_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@PartyID"].Value = PartyID;
            e.Command.Parameters["@DateFrom"].Value = DateFrom;
            e.Command.Parameters["@DateTo"].Value = DateTo;

        }
        #endregion
        #endregion


        #region Method for Load
        public void LoadAllValue(int intPartyID)
        {

            SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);


            cnn.Open();
            string PartyQuery = "exec SP_Party_Information " + intPartyID;
            SqlDataAdapter daItem = new SqlDataAdapter(PartyQuery, cnn);
            dsPartyInfo ds = new dsPartyInfo();
            daItem.Fill(ds, "PartyDetails");

            lblCode.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[1].ToString();
            lblName.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[2].ToString();
            lblFName.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[3].ToString();
            lblPartyType.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[9].ToString();
            lblVillage.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[5].ToString();
            lblPO.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[6].ToString();
            lblUpazilla.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[7].ToString();
            lblDistrict.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[8].ToString();
            cnn.Close();
        }
        #endregion
    }
}
