﻿<%@ Page Language="C#" AutoEventWireup="true" Title="CSMSys :: Unload Register" CodeBehind="UnloadReport.aspx.cs" Inherits="CSMSys.Web.Pages.ACC.Reports.UnloadReport" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="feature-box-full">
            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                <tbody>
                <tr>
                    <td align="center" valign="top" style="font-size:10px;">
                        বিসমিল্লাহির রাহমানির রাহিম
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:28px;">
                        শাহ্‌ ইসমাঈল গাজী (রহঃ) কোল্ড ষ্টোরেজ লিঃ
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">
                        সায়েকপুর, খেজমতপুর, পীরগঞ্জ, রংপুর। মোবাঃ ০১৭১৫৫৯৭৫৯১,০১৭১৯৭০৮৬২১, ০১৭৩২১১২১৯৬, হেড অফিসঃ ধানমণ্ডি, ঢাকা
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:20px;">
                        আনলোড রেজিষ্টার
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">                        
                        তারিখ: <asp:Label ID="lblfrom" runat="server" Text=""></asp:Label>
                        হতেঃ <asp:Label ID="lblto" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
 <tr>
                                <td align="center" valign="top">
                                    &nbsp;
                                </td>
                            </tr>
                            <%--<tr>
                                <td align="center" valign="top">
                                    &nbsp;
                                </td>
                            </tr>--%>
                            
                            <tr>
                                <td align="left">
                                    &nbsp;
                                </td>
                            </tr>
                <tr>
                    <td align="center" style="font-size:12px;">
                        <asp:GridView ID="grvLoading" DataKeyNames="UnloadID" runat="server" 
                            Width="100%" AutoGenerateColumns="False"
                                CellPadding="4" HorizontalAlign="Left"  ShowHeaderWhenEmpty="true" 
                                
                                EmptyDataText="No Records Found" DataSourceID="dsLoading">
                                <Columns>
                                    <asp:BoundField DataField="date" HeaderText="তারিখ" SortExpression="date" />
                                    <asp:BoundField DataField="PartyCode" HeaderText="পার্টি কোড" SortExpression="PartyCode" />
                                    <asp:BoundField DataField="PartyName" HeaderText="পার্টির নাম" SortExpression="PartyName" />
                                    <asp:BoundField DataField="ContactNo" HeaderText="মোবাইল নং" SortExpression="ContactNo" />
                                    <asp:BoundField DataField="SerialNo" HeaderText="এস আর" SortExpression="SerialNo" />
                                    <asp:BoundField DataField="Bags" HeaderText="লট" SortExpression="Bags" />
                                    <asp:BoundField DataField="State" HeaderText="লট" Visible="false" SortExpression="State" />
                                </Columns>
                            </asp:GridView>
                            <asp:SqlDataSource ID="dsLoading" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>"
                                                SelectCommand="SP_Unload_Report" SelectCommandType="StoredProcedure">
                                                <SelectParameters>
                                                    <asp:QueryStringParameter Name="FromDate" QueryStringField="FD" Type="DateTime" />
                                                    <asp:QueryStringParameter Name="ToDate" QueryStringField="TD" Type="DateTime" />
                                                </SelectParameters>
                                            </asp:SqlDataSource>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
