﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.INV;

namespace CSMSys.Web.Pages.ACC.Reports
{
    public partial class PartyWiseReport : System.Web.UI.Page
    {
        private string strSearch = string.Empty;

        private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }

        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                grvParty.DataBind();
            }
        }
        
        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }
        #endregion
        
        #region Methods For Grid

        protected void grvParty_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvParty.PageIndex = e.NewPageIndex;
            grvParty.DataBind();
        }

        protected void grvParty_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[0].Controls.Count > 0))
            {
                //if (!checkValidity()) return;
                //lblFailure.Text = "";                 
                HyperLink hplBookIssue = (HyperLink)e.Row.FindControl("hplBookIssue");
                hplBookIssue.NavigateUrl = "~/Pages/Reports/PartyBookRegister.aspx?PID=" + (DataBinder.Eval(e.Row.DataItem, "PartyID")).ToString() + "&FD=" + txtFromDate.Text + "&TD=" + txtToDate.Text;

                HyperLink hplBagLoan = (HyperLink)e.Row.FindControl("hplBagLoan");
                hplBagLoan.NavigateUrl = "~/Pages/Reports/PartyBagRegister.aspx?PID=" + (DataBinder.Eval(e.Row.DataItem, "PartyID")).ToString() + "&FD=" + txtFromDate.Text + "&TD=" + txtToDate.Text;

                HyperLink hplPotatoLoan = (HyperLink)e.Row.FindControl("hplPotatoLoan");
                hplPotatoLoan.NavigateUrl = "~/Pages/Reports/PartyLoanRegister.aspx?PID=" + (DataBinder.Eval(e.Row.DataItem, "PartyID")).ToString() + "&FD=" + txtFromDate.Text + "&TD=" + txtToDate.Text;

                HyperLink hplRegistration = (HyperLink)e.Row.FindControl("hplRegistration");
                hplRegistration.NavigateUrl = "~/pages/Reports/PartyRegistrationRegister.aspx?PID=" + (DataBinder.Eval(e.Row.DataItem, "PartyID")).ToString() + "&FD=" + txtFromDate.Text + "&TD=" + txtToDate.Text;

                 HyperLink hplLedger = (HyperLink)e.Row.FindControl("hplLedger");
                 hplLedger.NavigateUrl = "~/Pages/Reports/PartyWiseLedger.aspx?PID=" + (DataBinder.Eval(e.Row.DataItem, "PartyID")).ToString() + "&FD=" + txtFromDate.Text + "&TD=" + txtToDate.Text;

                 HyperLink hplDelivery = (HyperLink)e.Row.FindControl("hplDelivery");
                 hplDelivery.NavigateUrl = "~/Pages/Reports/PartyDeliveryRegister.aspx?PID=" + (DataBinder.Eval(e.Row.DataItem, "PartyID")).ToString() + "&FD=" + txtFromDate.Text + "&TD=" + txtToDate.Text;

            }
        }
        #endregion

        #region SqlDataSource Control Event Handlers
        protected void dsBagLoan_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@DateFrom"].Value = DateFrom;
            e.Command.Parameters["@DateTo"].Value = DateTo;
        }
        #endregion

        #region Methods For Button
        protected void imgRefresh_Click(object sender, EventArgs e)
        {
            grvParty.DataBind();
        }
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text.Trim();
            if (!checkValidity()) return;
            lblFailure.Text = "";    
            grvParty.DataBind();
        }
        #endregion

        private bool checkValidity()
        {
                DateTime dt = new DateTime();
                dt.ToString("yyyy/MM/dd")     ;
                //String.Format("{0:yyyy/MM/dd}", dt);
                if (!DateTime.TryParse(txtFromDate.Text, out dt))
                {
                lblFailure.Text = "Select From Date";
                txtFromDate.Focus();
                return false;
                 }

                if (!DateTime.TryParse(txtToDate.Text, out dt))
                {
                lblFailure.Text = "Select To Date";
                txtToDate.Focus();
                return false;

            }
            if (txtFromDate.Text == "" || txtToDate.Text == "")
            {
                lblFailure.Text = "Select Date";
                txtFromDate.Focus();
                return false;
            }
            return true;
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            grvParty.DataBind();
        }
    }
}