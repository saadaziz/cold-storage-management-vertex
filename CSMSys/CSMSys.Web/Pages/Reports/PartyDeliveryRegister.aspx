﻿<%@ Page Language="C#" AutoEventWireup="true" Title="CSMSys :: Party Delivery Register" CodeBehind="PartyDeliveryRegister.aspx.cs" Inherits="CSMSys.Web.Pages.ACC.Reports.PartyDeliveryRegister" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="feature-box-full">
            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                <tbody>
                <tr>
                    <td align="center" valign="top" style="font-size:10px;">
                        বিসমিল্লাহির রাহমানির রাহিম
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:28px;">
                        শাহ্‌ ইসমাঈল গাজী (রহঃ) কোল্ড ষ্টোরেজ লিঃ
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">
                        সায়েকপুর, খেজমতপুর, পীরগঞ্জ, রংপুর। মোবাঃ ০১৭১৫৫৯৭৫৯১,০১৭১৯৭০৮৬২১, ০১৭৩২১১২১৯৬, হেড অফিসঃ ধানমণ্ডি, ঢাকা
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:20px;">
                        পার্টি ডেলিভারি রেজিষ্টার
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">                        
                        তারিখ: <asp:Label ID="lblfrom" runat="server" Text=""></asp:Label>
                        হতেঃ <asp:Label ID="lblto" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
 <tr>
                                <td align="center" valign="top">
                                    &nbsp;
                                </td>
                            </tr>
                            <%--<tr>
                                <td align="center" valign="top">
                                    &nbsp;
                                </td>
                            </tr>--%>
                            <tr>
                                <td align="left" valign="top">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                        <tbody style="font-size:12px;">
                                            <tr>
                                                <td align="left" colspan="2">
                                                    তারিখ :
                                                    <asp:Label runat="server" ID="lblDate"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    পার্টির নাম :
                                                    <asp:Label ID="lblName" runat="server"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    পার্টি কোড নং:
                                                    <asp:Label runat="server" ID="lblCode"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    পার্টির ধরন :
                                                    <asp:Label runat="server" ID="lblPartyType"></asp:Label>
                                                </td>
                                                <td align="left">
                                                    পিতার নাম:
                                                    <asp:Label runat="server" ID="lblFName"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="width: 25%;">
                                                    গ্রাম :
                                                    <asp:Label runat="server" ID="lblVillage"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 25%;">
                                                    ডাক :
                                                    <asp:Label runat="server" ID="lblPO"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 25%;">
                                                    উপজেলা :
                                                    <asp:Label runat="server" ID="lblUpazilla"></asp:Label>
                                                </td>
                                                <td align="left" style="width: 25%;">
                                                    জেলা :
                                                    <asp:Label runat="server" ID="lblDistrict"></asp:Label>
                                                </td>
                                            </tr>
<%--                                            <tr>
                                                <td align="left" colspan="2">
                                                    <strong>তারিখ</strong> :
                                                    <asp:Label ID="lblFromDate" runat="server"></asp:Label>
                                                    <strong>থেকে</strong>
                                                    <asp:Label ID="lblToDate" runat="server"></asp:Label>
                                                    <strong>পর্যন্ত</strong>
                                                </td>
                                            </tr>--%>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    &nbsp;
                                </td>
                            </tr>
                <tr>
                    <td align="center" style="font-size:12px;">
                        <asp:GridView ID="grvPartyDeliveryRegister"  runat="server" Width="100%"
                            AutoGenerateColumns="False" CellPadding="3" HorizontalAlign="Left" ShowHeaderWhenEmpty="true"
                            EmptyDataText="No Records Found" CssClass="tablesorterBlue" DataSourceID="dsPartyDeliveryRegister"
                           >
                            <Columns>
                            <asp:BoundField DataField="SlNo" HeaderText="SlNo" SortExpression="SlNo" />
                             <asp:BoundField DataField="CollectionDate" HeaderText="তারিখ" SortExpression="CollectionDate" />
                                <asp:BoundField DataField="CollectionID" HeaderText="কালেকশন আইডি" SortExpression="CollectionID" />
                                <asp:BoundField DataField="SerialIDs" HeaderText="সিরিয়াল আইডি" SortExpression="SerialIDs" />
                                <asp:BoundField DataField="WBags" HeaderText="বস্তা উত্তোলন" SortExpression="WBags" />
                                <asp:BoundField DataField="TotalBagLoan" HeaderText="বস্তা লোন" SortExpression="TotalBagLoan" />
                                <asp:BoundField DataField="TotalCarryingLoan" HeaderText="পরিবহন লোন" SortExpression="TotalCarryingLoan" />
                                <asp:BoundField DataField="TotalPotatoLoan" HeaderText="আলু লোন" SortExpression="TotalPotatoLoan" />
                                <asp:BoundField DataField="TotalInterest" HeaderText="সুদ" SortExpression="TotalInterest" />
                                <asp:BoundField DataField="TotalBagCharge" HeaderText="বস্তা চার্জ" SortExpression="TotalBagCharge" />
                                <asp:BoundField DataField="TotalAmount" HeaderText="মোট" SortExpression="TotalAmount" />
                                <asp:BoundField DataField="PaidAmount" HeaderText="প্রদেয়" SortExpression="PaidAmount" />
                                <asp:BoundField DataField="DuesAmount" HeaderText="বকেয়া" SortExpression="DuesAmount" />
                            </Columns>
                            <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                            <EmptyDataRowStyle ForeColor="#CC0000" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        </asp:GridView>
                          <asp:SqlDataSource ID="dsPartyDeliveryRegister" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>"
                            SelectCommand="SP_Delivery_Register" SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="FromDate" QueryStringField="FD" Type="DateTime" />
                                <asp:QueryStringParameter Name="ToDate" QueryStringField="TD" Type="DateTime" />
                                <asp:QueryStringParameter Name="PartyID" QueryStringField="PID" Type="Int32" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <%--<tr>
                    <td align="center" valign="top">
                        &nbsp;
                    </td>
                </tr>--%>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
