﻿<%@ Page Language="C#" AutoEventWireup="true" Title="CSMSys :: Collection Register" CodeBehind="CollectionRegister.aspx.cs" Inherits="CSMSys.Web.Pages.ACC.Reports.CollectionRegister" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="feature-box-full">
            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                <tbody>
                <tr>
                    <td align="center" valign="top" style="font-size:10px;">
                        বিসমিল্লাহির রাহমানির রাহিম
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:28px;">
                        শাহ্‌ ইসমাঈল গাজী (রহঃ) কোল্ড ষ্টোরেজ লিঃ
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">
                        সায়েকপুর, খেজমতপুর, পীরগঞ্জ, রংপুর। মোবাঃ ০১৭১৫৫৯৭৫৯১,০১৭১৯৭০৮৬২১, ০১৭৩২১১২১৯৬, হেড অফিসঃ ধানমণ্ডি, ঢাকা
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:20px;">
                        কালেকশন রেজিষ্টার
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">                        
                        তারিখ: <asp:Label ID="lblfrom" runat="server" Text=""></asp:Label>
                        হতেঃ <asp:Label ID="lblto" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
 <tr>
                                <td align="center" valign="top">
                                    &nbsp;
                                </td>
                            </tr>
                            <%--<tr>
                                <td align="center" valign="top">
                                    &nbsp;
                                </td>
                            </tr>--%>
                            
                            <tr>
                                <td align="left">
                                    &nbsp;
                                </td>
                            </tr>
                <tr>
                    <td align="left" valign="top">
                        <asp:GridView ID="grvCollection" DataKeyNames="CollectionID" runat="server" Width="100%"
                            AutoGenerateColumns="False" CellPadding="4" HorizontalAlign="Left" ShowHeaderWhenEmpty="true"
                            EmptyDataText="No Records Found" CssClass="tablesorterBlue" DataSourceID="dsCollection">
                            <Columns>
                                <asp:BoundField DataField="CollectionDate" HeaderText="তারিখ" SortExpression="CollectionDate" />
                                <asp:BoundField DataField="CollectionID" HeaderText="কালেকশন আইডি" SortExpression="CollectionID" />
                                <asp:BoundField DataField="PartyCode" HeaderText="পার্টি কোড" SortExpression="PartyCode" />
                                <asp:BoundField DataField="PartyName" HeaderText="পার্টির নাম" SortExpression="PartyName" />
                                <asp:BoundField DataField="ContactNo" HeaderText="মোবাইল নং" SortExpression="ContactNo" />
                                <asp:BoundField DataField="SerialIDs" HeaderText="সিরিয়াল আইডি" SortExpression="SerialIDs" />
                                <asp:BoundField DataField="TotalBagLoan" HeaderText="বস্তা লোন" SortExpression="TotalBagLoan" />
                                <asp:BoundField DataField="TotalCarryingLoan" HeaderText="পরিবহন লোন" SortExpression="TotalCarryingLoan" />
                                <asp:BoundField DataField="TotalPotatoLoan" HeaderText="আলু লোন" SortExpression="TotalPotatoLoan" />
                                <asp:BoundField DataField="TotalInterest" HeaderText="সুদ" SortExpression="TotalInterest" />
                                <asp:BoundField DataField="TotalBagCharge" HeaderText="বস্তা চার্জ" SortExpression="TotalBagCharge" />
                                <asp:BoundField DataField="TotalAmount" HeaderText="মোট" SortExpression="TotalAmount" />
                                <asp:BoundField DataField="PaidAmount" HeaderText="প্রদেয়" SortExpression="PaidAmount" />
                                <asp:BoundField DataField="DuesAmount" HeaderText="বকেয়া" SortExpression="DuesAmount" />
                            </Columns>
                        </asp:GridView>
                        <asp:SqlDataSource ID="dsCollection" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>"
                            SelectCommand="SP_Loan_CollectionRegister" SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="FromDate" QueryStringField="FD" Type="DateTime" />
                                <asp:QueryStringParameter Name="ToDate" QueryStringField="TD" Type="DateTime" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <%--<tr>
                    <td align="center" valign="top">
                        &nbsp;
                    </td>
                </tr>--%>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
