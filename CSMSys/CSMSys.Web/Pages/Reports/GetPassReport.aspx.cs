﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CSMSys.Lib.AccountingEntity;
using CSMSys.Lib.AccountingUtility;
using CSMSys.Lib.AccountingDataAccess;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using System.Globalization;
using CSMSys.Lib.Model;
using CSMSys.Lib.Manager.SRV;
using CSMSys.Lib.Manager.INV;
using CSMSys.Lib.DataAccessLayer.Implementations;


namespace CSMSys.Web.Pages.Reports
{
    public partial class GetPassReport : System.Web.UI.Page
    {

        private int CollectionID
        {
            get
            {
                if (ViewState["CollectionID"] == null)
                    ViewState["CollectionID"] = -1;
                return (int)ViewState["CollectionID"];
            }
            set
            {
                ViewState["CollectionID"] = value;
            }
        }
       

        protected void Page_Load(object sender, EventArgs e)
        {
            //this.Form.Target = "_blank";
            //GridViewHelper helper = new GridViewHelper(this.GridView1);
            //helper.RegisterSummary("WBags", SummaryOperation.Sum);
            //helper.RegisterSummary("Bags", SummaryOperation.Sum);
            //helper.GroupSummary += new GroupEvent(helper_GroupSummary);
            //helper.ApplyGroupSort();
            if (!IsPostBack)
            {
                //lblDate.Text = DateTime.Today.ToString("dd/MM/yyyy"); 
                int intPartyID = Convert.ToInt32(Request.QueryString["PID"]);
                CollectionID = Convert.ToInt32(Request.QueryString["CID"]);
                //VoucherType = string.IsNullOrEmpty(Request.QueryString["Voucher"]) ? 1 : int.Parse(Request.QueryString["Voucher"]);
                //TransactionID = string.IsNullOrEmpty(Request.QueryString["TMID"]) ? 0 : int.Parse(Request.QueryString["TMID"]);
                //LoadValue()
                LoadAllValue(intPartyID, CollectionID);
            }
        }

        #region Method for Load
        public void LoadAllValue(int intPartyID, int unloadID)
        {
            SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

            cnn.Open();
            string PartyQuery = "exec SP_Party_Information " + intPartyID;
            SqlDataAdapter daItem = new SqlDataAdapter(PartyQuery, cnn);

            DataSet ds = new DataSet();
            daItem.Fill(ds, "PartyDetails");

            lblGpDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
            lblGpCode.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[1].ToString();
            lblGpName.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[2].ToString();
            cnn.Close();


            lblGpID.Text = CollectionID.ToString();
            
            
        }
        #endregion

        #region SqlDataSource Control Event Handlers
        protected void dsGetpass_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@CollectionID"].Value = CollectionID;

        }
        #endregion
    }
}