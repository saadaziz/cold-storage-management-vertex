﻿<%@ Page Language="C#" Title="CSMSys :: Comission Report" AutoEventWireup="true" CodeBehind="GetPassReport.aspx.cs" Inherits="CSMSys.Web.Pages.Reports.GetPassReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/App_Themes/Default/Styles/Default.css" rel="stylesheet" type="text/css" />

    <script language="javascript" type="text/javascript">
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
function btnDrPrint_onclick() {

}

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="Gate Pass">
                            <table width="100%" border="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td align="center" valign="top" style="font-size: 8px;">
                                            বিসমিল্লাহির রাহমানির রাহিম
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top" style="font-size: 16px;">
                                            শাহ্‌ ইসমাঈল গাজী (রহঃ) কোল্ড ষ্টোরেজ লিঃ
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top" style="font-size: 8px;">
                                            সায়েকপুর, খেজমতপুর, পীরগঞ্জ, রংপুর। মোবাঃ ০১৭১৫৫৯৭৫৯১,০১৭১৯৭০৮৬২১, ০১৭৩২১১২১৯৬,
                                            হেড অফিসঃ ধানমণ্ডি, ঢাকা
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top">
                                            <asp:Label ID="Label1" runat="server" Text="গেট পাশ" Font-Bold="True" Font-Size="Small"
                                                Style="text-decoration: underline"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                                <tbody>
                                                    <tr>
                                                        <td align="left">
                                                            রশিদ নং :
                                                            <asp:Label runat="server" ID="lblGpID"></asp:Label>
                                                        </td>
                                                        <td align="left">
                                                            তারিখ :
                                                            <asp:Label runat="server" ID="lblGpDate"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            পার্টির নাম :
                                                            <asp:Label ID="lblGpName" runat="server"></asp:Label>
                                                        </td>
                                                        <td align="left">
                                                            পার্টি কোড নং:
                                                            <asp:Label runat="server" ID="lblGpCode"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" colspan="2">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="4" align="left" valign="top" align="center" style="font-size: 12px;">
                                                            <%--   <div style="overflow:auto; height:148px; border: 1px solid #996600;">--%>
                                                            <div>
                                                                <asp:GridView ID="GridView1" DataKeyNames="CollectionDetailID" runat="server" Width="97%"
                                                                    AutoGenerateColumns="False" CellPadding="2" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found"
                                                                    AllowPaging="False" DataSourceID="dsGetpass" Height="150px" PageSize="5">
                                                                    <Columns>
                                                                        <asp:BoundField DataField="SerialNo" HeaderText="SR" SortExpression="SerialNo" />
                                                                        <asp:BoundField DataField="WBags" HeaderText="উ:বস্তা" SortExpression="WBags" />
                                                                        <asp:BoundField DataField="Bags" HeaderText="গ্রহণ ব্যাগ" SortExpression="Bags" />
                                                                    </Columns>
                                                                </asp:GridView>
                                                                <asp:SqlDataSource ID="dsGetpass" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>"
                                                                    SelectCommand="SP_GetPass" SelectCommandType="StoredProcedure">
                                                                    <SelectParameters>
                                                                        <asp:QueryStringParameter Name="CollectionID" QueryStringField="CID" Type="int32" />
                                                                    </SelectParameters>
                                                                </asp:SqlDataSource>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                                <tbody>
                                                    <tr>
                                                        <td align="center" style="width: 25%;">
                                                            অফিস সহকারীর স্বাক্ষর
                                                        </td>
                                                        <td align="center" style="width: 25%;">
                                                            যাচাইকারীর স্বাক্ষর
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
    </form>
</body>
</html>
