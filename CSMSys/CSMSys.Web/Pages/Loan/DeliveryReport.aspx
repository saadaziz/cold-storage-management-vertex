﻿<%@ Page Language="C#" Title="CSMSys :: Delivery Report" AutoEventWireup="true" CodeBehind="DeliveryReport.aspx.cs"
    Inherits="CSMSys.Web.Pages.Loan.DeliveryReport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="~/App_Themes/Default/Styles/Default.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
        function btnDrPrint_onclick() {

        }

    </script>
    <style type="text/css">
        .tablesorterBlue
        {
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div class="feature-box-full">
        <div id="printBagLoan" class="VerticalyMiddle">
            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                <tr>
                    <td width="50%" height="100%" valign="top">
                        <div class="Delivery Info">
                            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                <tbody>
                                    <tr>
                                        <td align="center" valign="top" style="font-size: 8px;">
                                            বিসমিল্লাহির রাহমানির রাহিম
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top" style="font-size: 16px;">
                                            শাহ্‌ ইসমাঈল গাজী (রহঃ) কোল্ড ষ্টোরেজ লিঃ
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top" style="font-size: 8px;">
                                            সায়েকপুর, খেজমতপুর, পীরগঞ্জ, রংপুর। মোবাঃ ০১৭১৫৫৯৭৫৯১,০১৭১৯৭০৮৬২১, ০১৭৩২১১২১৯৬,
                                            হেড অফিসঃ ধানমণ্ডি, ঢাকা
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top">
                                            <asp:Label ID="lblVoucher" runat="server" Text="সংরক্ষণ ভাড়া রশিদ" Font-Bold="True"
                                                Font-Size="Small" Style="text-decoration: underline"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                                <tbody>
                                                    <tr>
                                                        <td align="left" colspan="2">
                                                            রশিদ নং :
                                                            <asp:Label runat="server" ID="lblDeliveryID"></asp:Label>
                                                        </td>
                                                        <td align="left">
                                                            তারিখ :
                                                            <asp:Label runat="server" ID="lblDate"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            পার্টির নাম :
                                                            <asp:Label ID="lblName" runat="server"></asp:Label>
                                                        </td>
                                                        <td align="left">
                                                            পার্টি কোড নং:
                                                            <asp:Label runat="server" ID="lblCode"></asp:Label>
                                                        </td>
                                                        <td align="left">
                                                            পার্টির ধরন :
                                                            <asp:Label runat="server" ID="lblPartyType"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" colspan="4">
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                                                <tbody>
                                                                    <tr>
                                                                        <td colspan="4" align="left">
                                                                            <strong>দলিলের তথ্য</strong>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td colspan="4" align="left" valign="top" align="center" style="font-size: 12px;">
                                                                            <%--   <div style="overflow:auto; height:148px; border: 1px solid #996600;">--%>
                                                                            <div>
                                                                                <asp:GridView ID="grvSR" DataKeyNames="CollectionDetailID" runat="server" Width="97%"
                                                                                    AutoGenerateColumns="False" CellPadding="2" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found"
                                                                                    AllowPaging="False" DataSourceID="dsDelivery" Height="150px" PageSize="5">
                                                                                    <Columns>
                                                                                        <%-- <asp:TemplateField HeaderText="CollectionDetailID" Visible="false" HeaderStyle-HorizontalAlign="Left"
                                                                            ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSerialID" Text='<%# Eval("CollectionDetailID") %>' runat="server" HorizontalAlign="Left" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                                        <asp:TemplateField HeaderText="SR" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblSerialNo" Text='<%# Eval("SerialNo") %>' runat="server" HorizontalAlign="Left" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="ব্যাগ" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"
                                                                                            ItemStyle-Width="5%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblBags" Text='<%# Eval("Bags") %>' runat="server" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="লোন আইডি" Visible="false" HeaderStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-HorizontalAlign="Left">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblLoanID" Text='<%# Eval("LoanID") %>' runat="server" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--                                                                        <asp:TemplateField HeaderText="তারিখ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="8%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDisburseDate" Text='<%# Eval("DisburseDate","0:dd/MM/yyyy") %>' runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                                        <asp:TemplateField HeaderText="দিন" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"
                                                                                            ItemStyle-Width="3%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblDays" Text='<%# Eval("Days") %>' runat="server" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--                                                                        <asp:TemplateField HeaderText="লোন/ব্যাগ" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblLoanPerBag" runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                                        <asp:TemplateField HeaderText="উ:বস্তা" HeaderStyle-HorizontalAlign="Center"
                                                                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="4%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblWBags" Text='<%# Eval("WBags") %>' runat="server" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="লোন" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"
                                                                                            ItemStyle-Width="8%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblLoan" Text='<%# Eval("PotatoLoan","{0:N}") %>' runat="server" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="ইন্টারেস্ট" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"
                                                                                            ItemStyle-Width="8%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblInterest" Text='<%# Eval("Interest","{0:N}") %>' runat="server" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="ভাড়া" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"
                                                                                            ItemStyle-Width="8%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblBagCharge" Text='<%# Eval("BagCharge","{0:N}") %>' runat="server" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--<asp:TemplateField HeaderText="খালি বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="4%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblEmptyBags" Text='<%# Eval("EmptyBags") %>' runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>--%>
                                                                                        <asp:TemplateField HeaderText="বস্তা-লোন" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblBagLoan" Text='<%# Eval("BagLoan","{0:N}") %>' runat="server" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="পরি:লোন" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblCarring" Text='<%# Eval("CarryingLoan","{0:N}") %>' runat="server" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="মোট" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"
                                                                                            ItemStyle-Width="8%">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblLineTotal" Text='<%# Eval("TotalAmount") %>' runat="server" />
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                                <asp:SqlDataSource ID="dsDelivery" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>"
                                                                                    OnSelecting="dsDelivery_Selecting" SelectCommand="SELECT     lcd.CollectionDetailID, lcd.CollectionID, lcd.SerialID, lcd.SerialNo, lcd.Bags, lcd.WBags, lcd.BagLoan, lcd.CarryingLoan, lcd.PotatoLoan, lcd.Days, lcd.Interest, 
                                                           lcd.CaseID, lcd.LoanID, lcd.BagCharge, lcd.TotalAmount
                                                            FROM         dbo.SRVLoanCollectionDetail AS lcd INNER JOIN
                                                         dbo.SRVLoanCollection AS lc ON lcd.CollectionID = lc.CollectionID
                                                         WHERE lcd.CollectionID = @CollectionID ">
                                                                                    <SelectParameters>
                                                                                        <asp:Parameter Name="CollectionID" Type="Int32" />
                                                                                    </SelectParameters>
                                                                                </asp:SqlDataSource>
                                                                            </div>
                                                                        </td>
                                                                    </tr>

                                                                    <tr>
                                                                    <td align="left" valign="top">
                                            <table width="100%" border="0" cellspacing="4" style="padding-right: 20px;">
                                                <tbody>
                                                    <tr>
                                                                        <td align="left">
                                                                            &nbsp;
                                                                        </td>
                                                                        <td align="right" colspan="2">
                                                                            মোট উত্তোলনকৃত বস্তা :
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblWBags" runat="server" Text="0"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            বস্তা প্রতি লোন :
                                                                        </td>
                                                                        <td align="left">
                                                                            টাকা
                                                                            <asp:Label ID="lblAmtPerBag" runat="server" Text="0"></asp:Label>
                                                                            &nbsp; হারে
                                                                        </td>
                                                                        <td align="right">
                                                                            লোন : টাকা
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblLoan" runat="server" Text="0"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="3">
                                                                            ইন্টারেস্ট : টাকা
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblInterest" runat="server" Text="0"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="3">
                                                                            খালি বস্তা লোন : টাকা
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblBagLoan" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="3">
                                                                            পরিবহন লোন : টাকা
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblCarring" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            বস্তা প্রতি সংরক্ষণ ভাড়া :
                                                                        </td>
                                                                        <td align="left">
                                                                            টাকা
                                                                            <asp:Label ID="lblChargePerBag" runat="server"></asp:Label>
                                                                            &nbsp; হারে
                                                                        </td>
                                                                        <td align="right">
                                                                            মোট সংরক্ষণ ভাড়া : টাকা
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblCharge" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="3">
                                                                            মোট : টাকা
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblPayable" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="3">
                                                                            প্রদেয় : টাকা
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblPaid" runat="server"></asp:Label>
                                                                            <%--                      <asp:TextBox ID="txtPaid" runat="server" Text="0" Width="108px" Enabled="false" AutoPostBack="true"></asp:TextBox>--%>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right" colspan="3">
                                                                            বকেয়া : টাকা
                                                                        </td>
                                                                        <td align="right">
                                                                            <asp:Label ID="lblDues" runat="server"></asp:Label>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right">
                                                                            মন্তব্য :
                                                                        </td>
                                                                        <td align="left" colspan="3">
                                                                            <asp:Label ID="lblRemarks" runat="server"></asp:Label>
                                                                            <%--  <asp:TextBox ID="txtRemarks" runat="server" Width="388px"></asp:TextBox>--%>
                                                                        </td>
                                                                    </tr>
                                                                                                                    </tbody>
                                            </table>
                                        </td>
                                    </tr>

                                                                </tbody>
                                                            </table>
                                                            
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" colspan="2">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" colspan="3">
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="center" style="width: 25%;">
                                                                            পার্টির স্বাক্ষর
                                                                        </td>
                                                                        <td align="center" style="width: 25%;">
                                                                            অফিস সহকারীর স্বাক্ষর
                                                                        </td>
                                                                        <td align="center" style="width: 25%;">
                                                                            যাচাইকারীর স্বাক্ষর
                                                                        </td>
                                                                        <td align="center" style="width: 25%;">
                                                                            মহাব্যবস্থাপকের স্বাক্ষর
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" colspan="2">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" colspan="2">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                    <td width="25%" height="100%" valign="top">
                        <div class="Gate Pass">
                            <table width="100%" border="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td align="center" valign="top" style="font-size: 8px;">
                                            বিসমিল্লাহির রাহমানির রাহিম
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top" style="font-size: 16px;">
                                            শাহ্‌ ইসমাঈল গাজী (রহঃ) কোল্ড ষ্টোরেজ লিঃ
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top" style="font-size: 8px;">
                                            সায়েকপুর, খেজমতপুর, পীরগঞ্জ, রংপুর। মোবাঃ ০১৭১৫৫৯৭৫৯১,০১৭১৯৭০৮৬২১, ০১৭৩২১১২১৯৬,
                                            হেড অফিসঃ ধানমণ্ডি, ঢাকা
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top">
                                            <asp:Label ID="Label1" runat="server" Text="গেট পাশ" Font-Bold="True" Font-Size="Small"
                                                Style="text-decoration: underline"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                                <tbody>
                                                    <tr>
                                                        <td align="left">
                                                            রশিদ নং :
                                                            <asp:Label runat="server" ID="lblGpID"></asp:Label>
                                                        </td>
                                                        <td align="left">
                                                            তারিখ :
                                                            <asp:Label runat="server" ID="lblGpDate"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            পার্টির নাম :
                                                            <asp:Label ID="lblGpName" runat="server"></asp:Label>
                                                        </td>
                                                        <td align="left">
                                                            পার্টি কোড নং:
                                                            <asp:Label runat="server" ID="lblGpCode"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" colspan="2">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="4" align="left" valign="top" align="center" style="font-size: 12px;">
                                                            <%--   <div style="overflow:auto; height:148px; border: 1px solid #996600;">--%>
                                                            <div>
                                                                <asp:GridView ID="GridView1" DataKeyNames="CollectionDetailID" runat="server" Width="97%"
                                                                    AutoGenerateColumns="False" CellPadding="2" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found"
                                                                    AllowPaging="False" DataSourceID="dsDelivery" Height="150px" PageSize="5">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="SR" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSerialNo" Text='<%# Eval("SerialNo") %>' runat="server" HorizontalAlign="Left" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ব্যাগ" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"
                                                                            ItemStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblBags" Text='<%# Eval("Bags") %>' runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="উ:বস্তা" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="4%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblWBags" Text='<%# Eval("WBags") %>' runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                                <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>"
                                                                    OnSelecting="dsDelivery_Selecting" SelectCommand="SELECT     lcd.CollectionDetailID, lcd.CollectionID, lcd.SerialID, lcd.SerialNo, lcd.Bags, lcd.WBags, lcd.BagLoan, lcd.CarryingLoan, lcd.PotatoLoan, lcd.Days, lcd.Interest, 
                                                           lcd.CaseID, lcd.LoanID, lcd.BagCharge, lcd.TotalAmount
                                                            FROM         dbo.SRVLoanCollectionDetail AS lcd INNER JOIN
                                                          dbo.SRVLoanCollection AS lc ON lcd.CollectionID = lc.CollectionID
                                                         WHERE lcd.CollectionID = @CollectionID">
                                                                    <SelectParameters>
                                                                        <asp:Parameter Name="CollectionID" Type="Int32" />
                                                                    </SelectParameters>
                                                                </asp:SqlDataSource>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            মোট উত্তোলনকৃত বস্তা :
                                            <asp:Label ID="lblGPWBags" runat="server" Text="0"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                                <tbody>
                                                    <tr>
                                                        <td align="center" style="width: 25%;">
                                                            অফিস সহকারীর স্বাক্ষর
                                                        </td>
                                                        <td align="center" style="width: 25%;">
                                                            যাচাইকারীর স্বাক্ষর
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                    <td width="25%" height="100%" valign="top">
                        <div class="Gate Pass">
                            <table width="100%" border="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td align="center" valign="top" style="font-size: 8px;">
                                            বিসমিল্লাহির রাহমানির রাহিম
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top" style="font-size: 16px;">
                                            শাহ্‌ ইসমাঈল গাজী (রহঃ) কোল্ড ষ্টোরেজ লিঃ
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top" style="font-size: 8px;">
                                            সায়েকপুর, খেজমতপুর, পীরগঞ্জ, রংপুর। মোবাঃ ০১৭১৫৫৯৭৫৯১,০১৭১৯৭০৮৬২১, ০১৭৩২১১২১৯৬,
                                            হেড অফিসঃ ধানমণ্ডি, ঢাকা
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" valign="top">
                                            <asp:Label ID="Label2" runat="server" Text="ডেলিভারী অর্ডার" Font-Bold="True" Font-Size="Small"
                                                Style="text-decoration: underline"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                                <tbody>
                                                    <tr>
                                                        <td align="left">
                                                            রশিদ নং :
                                                            <asp:Label runat="server" ID="lblDoID"></asp:Label>
                                                        </td>
                                                        <td align="left">
                                                            তারিখ :
                                                            <asp:Label runat="server" ID="lblDoDate"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                            পার্টির নাম :
                                                            <asp:Label ID="lblDoName" runat="server"></asp:Label>
                                                        </td>
                                                        <td align="left">
                                                            পার্টি কোড নং:
                                                            <asp:Label runat="server" ID="lblDoCode"></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left" colspan="2">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                                <tbody>
                                                    <tr>
                                                        <td colspan="4" align="left" valign="top" align="center" style="font-size: 12px;">
                                                            <%--   <div style="overflow:auto; height:148px; border: 1px solid #996600;">--%>
                                                            <div>
                                                                <asp:GridView ID="GridView2" DataKeyNames="CollectionDetailID" runat="server" Width="97%"
                                                                    AutoGenerateColumns="False" CellPadding="2" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found"
                                                                    AllowPaging="False" DataSourceID="dsDelivery" Height="150px" PageSize="5">
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="SR" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="7%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSerialNo" Text='<%# Eval("SerialNo") %>' runat="server" HorizontalAlign="Left" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ব্যাগ" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right"
                                                                            ItemStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblBags" Text='<%# Eval("Bags") %>' runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="উ:বস্তা" HeaderStyle-HorizontalAlign="Center"
                                                                            ItemStyle-HorizontalAlign="Right" ItemStyle-Width="4%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblWBags" Text='<%# Eval("WBags") %>' runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                                <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>"
                                                                    OnSelecting="dsDelivery_Selecting" SelectCommand="SELECT     lcd.CollectionDetailID, lcd.CollectionID, lcd.SerialID, lcd.SerialNo, lcd.Bags, lcd.WBags, lcd.BagLoan, lcd.CarryingLoan, lcd.PotatoLoan, lcd.Days, lcd.Interest, 
                                                           lcd.CaseID, lcd.LoanID, lcd.BagCharge, lcd.TotalAmount
                                                            FROM         dbo.SRVLoanCollectionDetail AS lcd INNER JOIN
                                                         dbo.SRVLoanCollection AS lc ON lcd.CollectionID = lc.CollectionID
                                                         WHERE lcd.CollectionID = @CollectionID ">
                                                                    <SelectParameters>
                                                                        <asp:Parameter Name="CollectionID" Type="Int32" />
                                                                    </SelectParameters>
                                                                </asp:SqlDataSource>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            মোট উত্তোলনকৃত বস্তা :
                                            <asp:Label ID="lblDOWBags" runat="server" Text="0"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" valign="top">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">
                                            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                                <tbody>
                                                    <tr>
                                                        <td align="center" style="width: 25%;">
                                                            অফিস সহকারীর স্বাক্ষর
                                                        </td>
                                                        <td align="center" style="width: 25%;">
                                                            যাচাইকারীর স্বাক্ষর
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
