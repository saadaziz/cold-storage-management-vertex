﻿<%@ Page Title="Loan Approval" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" 
CodeBehind="LoanApproval.aspx.cs" Inherits="CSMSys.Web.Pages.Loan.LoanApproval" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../../App_Themes/TableSorter/Blue/style.css" rel="stylesheet" type="text/css" />
    <link href="../../../App_Themes/TableSorter/Green/style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="content" style="height:620px;">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:HiddenField ID="hdnSerialNos" runat="server" />
    <div class="title">
        <table width="100%" border="0" cellpadding="2" cellspacing="4">
            <tbody>
                <tr>
                    <td align="left" style="width: 20%;">
                        <h2>Loan Approval</h2>
                    </td>
                    <td align="right" valign="bottom" style="width: 76%;">
                        <asp:CheckBox ID="chkApproved" runat="server" Text="Approved" AutoPostBack="True" oncheckedchanged="chkApplied_CheckedChanged" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        Search by Name/Code/Village/Contact : 
                        <asp:TextBox ID="txtSearch" runat="server" Width="107px"></asp:TextBox>
                         &nbsp;and Date From 
                        <asp:TextBox ID="txtFromDate" runat="server" Width="87px"></asp:TextBox>
                        <cc1:CalendarExtender ID="ceFromDate" runat="server" TargetControlID="txtFromDate" />
                         &nbsp;To 
                        <asp:TextBox ID="txtToDate" runat="server" Width="87px"></asp:TextBox>
                        <cc1:CalendarExtender ID="ceToDate" runat="server" TargetControlID="txtToDate" />
                    </td>
                    <td align="center" valign="bottom" style="width: 2%;">
                        <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" ImageUrl="~/App_Themes/Default/Images/gridview/Search.png"
                            ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch_Click" />
                    </td>
                    <td align="center" valign="bottom" style="width: 2%;">
                        <asp:ImageButton ID="imgRefresh" runat="server" CommandName="Refresh" ImageUrl="~/App_Themes/Default/Images/gridview/Refresh.png"
                            ToolTip="Refresh" Width="16px" Height="16px" OnClick="imgRefresh_Click" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="feature-box-full">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td align="left" valign="top" style="width:22%;">
                                <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                    <tbody>
                                        <tr>
                                            <td align="left" colspan="2" style="background-color:#000066; color:#FFFFFF; padding-left:5px;">
                                                <strong>Customer Information</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="2">
                                                Party Name:
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="2">
                                                <asp:Label ID="lblName" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 40%;">
                                                Party Code:
                                            </td>
                                            <td align="left" style="width: 60%;">
                                                <asp:Label ID="lblCode" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 40%;">
                                                Group Code:
                                            </td>
                                            <td align="left" style="width: 60%;">
                                                <asp:Label ID="lblGCode" runat="server"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="2" style="background-color:#000066; color:#FFFFFF; padding-left:5px;">
                                                <strong>Loan Approval Information</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Total Bags :
                                            </td>
                                            <td align="left">
                                                <asp:Label ID="lblTotalBags" runat="server" Text="0"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" style="width: 40%;">
                                                Date : 
                                            </td>
                                            <td align="left" style="width: 60%;">
                                                <asp:TextBox ID="txtDate" runat="server" Width="87px"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfValidator1" runat="server" ControlToValidate="txtDate"
                                                    CssClass="failureNotification" ErrorMessage="Date is required." ToolTip="Date is required."
                                                    ValidationGroup="ValidationGroup"><img src="../../../App_Themes/Default/Images/Left_Arrow.png" 
                                                            alt="*" /></asp:RequiredFieldValidator>
                                                <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDate" PopupPosition="BottomLeft" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Apv Bags :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtBags" runat="server" Width="87px" 
                                                    ontextchanged="txtBags_TextChanged" AutoPostBack="True"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfValidator2" runat="server" ControlToValidate="txtBags"
                                                    CssClass="failureNotification" ErrorMessage="Apv Bags is required." ToolTip="Apv Bags is required."
                                                    ValidationGroup="ValidationGroup"><img src="../../../App_Themes/Default/Images/Left_Arrow.png" 
                                                            alt="*" /></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Amt/Bags :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtAmtPerBags" runat="server" Width="87px" Text="0" AutoPostBack="true" OnTextChanged="txtAmtPerBags_TextChanged"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                Apv Loans :
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtLoans" runat="server" Width="87px" AutoPostBack="True" 
                                                    ontextchanged="txtLoans_TextChanged"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="rfValidator3" runat="server" ControlToValidate="txtLoans"
                                                    CssClass="failureNotification" ErrorMessage="Apv Loans is required." ToolTip="Apv Loans is required."
                                                    ValidationGroup="ValidationGroup"><img src="../../../App_Themes/Default/Images/Left_Arrow.png" 
                                                            alt="*" /></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="2">
                                                <div class="feature-box-actionBar">
                                                    <asp:Button ID="btnApply" runat="server" CssClass="button" 
                                                        Text="Approve Loan" ValidationGroup="ValidationGroup" 
                                                        onclick="btnApply_Click" />
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td align="left" valign="top" style="width:78%;">
                                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td align="left" valign="top">
                                                <asp:GridView ID="grvRequisition" DataKeyNames="RequisitionID" runat="server" Width="100%" AutoGenerateColumns="False"
                                                    CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvRequisition_PageIndexChanging" ShowHeaderWhenEmpty="true" 
                                                    OnRowDataBound="grvRequisition_RowDataBound" OnRowCommand="grvRequisition_RowCommand"
                                                    EmptyDataText="No Records Found" CssClass="tablesorterBlue" AllowPaging="True" PageSize="15" DataSourceID="dsRequisition">
                                                    <Columns>
                                                        <asp:TemplateField ItemStyle-Width="2%"  ItemStyle-HorizontalAlign="Center">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="imgSelect" runat="server" CommandName="Select" ImageUrl="~/App_Themes/Default/Images/gridview/Select.png" ToolTip="Select" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Sl #" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="3%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSl" Text='<%# Eval("SlNo") %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="RequisitionID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRequisitionID" Text='<%# Eval("RequisitionID") %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="তারিখ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblRequisitionDate" Text='<%# Eval("RequisitionDate","{0:dd/MM/yyyy}") %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="এস আর" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="7%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblSerialIDs" Text='<%# HighlightText(Eval("SerialIDs").ToString()) %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="PartyID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPartyID" Text='<%# Eval("PartyID") %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ধরন" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPartyType" Text='<%# Eval("PartyType") %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="কোড" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="7%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPartyCode" Text='<%# HighlightText(Eval("PartyCode").ToString()) %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="নাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblPartyName" Text='<%# HighlightText(Eval("PartyName").ToString()) %>' runat="server" Font-Size="12px" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="পিতার নাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblFatherName" Text='<%# HighlightText(Eval("FatherName").ToString()) %>' runat="server" Font-Size="12px" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                         <asp:TemplateField HeaderText="গ্রাম" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblvill" Text='<%# HighlightText(Eval("AreaVillageName").ToString()) %>' runat="server" Font-Size="12px" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="মোবাইল নং" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblContactNo" Text='<%# HighlightText(Eval("ContactNo").ToString()) %>' runat="server" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="মোট বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblBags" Text='<%# Eval("Bags") %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="অনুঃ বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblApprovedBags" Text='<%# Eval("ApprovedBags") %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ঋণ/বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblAmountPerBag" Text='<%# Eval("AmountPerBag","{0:N}") %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="ঋণ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lblLoanAmount" Text='<%# Eval("LoanAmount","{0:N}") %>' runat="server" HorizontalAlign="Left" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                                                    <AlternatingRowStyle BackColor="#E5EAE8" />
                                                    <EditRowStyle BackColor="#999999" />
                                                    <EmptyDataRowStyle ForeColor="#CC0000" />
                                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                </asp:GridView>
                                                <asp:SqlDataSource ID="dsRequisition" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>" OnSelecting="dsRequisition_Selecting" 
                                                    SelectCommand="SELECT ROW_NUMBER() OVER (ORDER BY sr.RequisitionID) As SlNo,ip.FatherName,ip.AreaVillageName, sr.RequisitionID, sr.RequisitionDate, 
                                                        sr.PartyID, ip.PartyType, ip.PartyCode, ip.PartyName, ip.FatherName, ip.AreaVillageName, ip.ContactNo, sr.Bags, sr.ApprovedBags, sr.AmountPerBag, 
                                                        sr.LoanAmount, sr.Remarks, sr.SerialIDs
                                                        FROM SRVLoanRequisition AS sr INNER JOIN 
                                                        INVParty AS ip ON sr.PartyID = ip.PartyID 
                                                        WHERE ((sr.IsApproved = @IsApproved) AND (sr.RequisitionDate >= @DateFrom) AND (sr.RequisitionDate <= @DateTo)) ORDER BY sr.RequisitionDate, sr.RequisitionID" 
                                                    FilterExpression="PartyName LIKE '%{0}%' OR ContactNo LIKE '{1}%' OR SerialNo LIKE '{2}%' OR AreaVillageName LIKE '{3}%' OR PartyCode LIKE '{4}%'">
                                                    <FilterParameters>
                                                        <asp:ControlParameter Name="PartyName" ControlID="txtSearch" PropertyName="Text" />
                                                        <asp:ControlParameter Name="ContactNo" ControlID="txtSearch" PropertyName="Text" />
                                                        <asp:ControlParameter Name="SerialNo" ControlID="txtSearch" PropertyName="Text" />
                                                        <asp:ControlParameter Name="AreaVillageName" ControlID="txtSearch" PropertyName="Text" />
                                                        <asp:ControlParameter Name="PartyCode" ControlID="txtSearch" PropertyName="Text" />
                                                    </FilterParameters>
                                                    <SelectParameters>
                                                        <asp:Parameter Name="DateFrom" Type="DateTime" />
                                                        <asp:Parameter Name="DateTo" Type="DateTime" />
                                                        <asp:Parameter Name="IsApproved" Type="Boolean" />
                                                    </SelectParameters>
                                                </asp:SqlDataSource>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <div class="feature-box-actionBar">
                                                    <span class="failureNotification">
                                                        <asp:Literal ID="lblFailure" runat="server"></asp:Literal>
                                                    </span>
                                                    <asp:Button ID="btnReport" runat="server" CssClass="button" Text="Approval Report" OnClick="btnReport_Click" />
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
      </div>
</div>
</asp:Content>
