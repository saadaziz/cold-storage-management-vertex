﻿<%@ Page Title="Loan Requisition Report" Language="C#" AutoEventWireup="true" CodeBehind="ReportApplied.aspx.cs" Inherits="CSMSys.Web.Pages.Loan.ReportApplied" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdnSearch" runat="server" />
    <div class="feature-box-full">
            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                <tbody>
                <tr>
                    <td align="center" valign="top" style="font-size:10px;">
                        বিসমিল্লাহির রাহমানির রাহিম
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:28px;">
                        শাহ্‌ ইসমাঈল গাজী (রহঃ) কোল্ড ষ্টোরেজ লিঃ
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:8px;">
                        সায়েকপুর, খেজমতপুর, পীরগঞ্জ, রংপুর। মোবাঃ ০১৭১৫৫৯৭৫৯১,০১৭১৯৭০৮৬২১, ০১৭৩২১১২১৯৬, হেড অফিসঃ ধানমণ্ডি, ঢাকা
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:16px;">
                       ঋণ চাহিদা পূর্বানুমোদন পত্র
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">
                        তারিখ <asp:Label ID="lblfrom" runat="server" Text=""></asp:Label> 
                        হতে <asp:Label ID="lblto" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">
                        <asp:Label runat="server" ID="lblParty"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        পূর্বের জের : <asp:Label runat="server" ID="lblOpBalance"></asp:Label> বস্তা
                    </td>
                </tr>
                <tr>
                    <td align="center" style="font-size:12px;">
                        <asp:GridView ID="grvRequisition"  runat="server" Width="100%"
                            AutoGenerateColumns="False" CellPadding="3" HorizontalAlign="Left" ShowHeaderWhenEmpty="true" OnRowCreated="grvRequisition_RowCreated"
                            EmptyDataText="No Records Found" OnRowDataBound="grvRequisition_RowDataBound" DataSourceID="dsRequisition">
                            <Columns>
                                <asp:TemplateField HeaderText="RequisitionID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblRequisitionID" Text='<%# Eval("RequisitionID") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Party ID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPartyID" Text='<%# Eval("PartyID") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="কোড" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPartyCode" Text='<%# Eval("PartyCode") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="নাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPartyName" Text='<%# Eval("PartyName") %>' runat="server" HorizontalAlign="Left" style="font-size:12px;" />
                                      </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="পিতার নাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFatherName" Text='<%# Eval("FatherName") %>' runat="server" HorizontalAlign="Left" style="font-size:12px;" />
                                      </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="গ্রাম" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAreaVillageName" Text='<%# Eval("AreaVillageName") %>' runat="server" HorizontalAlign="Left" style="font-size:12px;" />
                                      </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="মোবাইল নং" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblContactNo" Text='<%# Eval("ContactNo") %>' runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="তারিখ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblLoanDate" Text='<%# Eval("LoanDate","{0:dd/MM/yyyy}") %>' runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblABags" Text='<%# Eval("Bags") %>' runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="অনুঃ বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblDBags" Text='<%# Eval("DBags") %>' runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ঋণ/বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDAmountPerBag" Text='<%# Eval("DAmountPerBag") %>' runat="server" HorizontalAlign="Center" />                                                               
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ঋণ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDLoanAmount" Text='<%# Eval("DLoanAmount") %>' runat="server" HorizontalAlign="Center" />                                                               
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="অবঃ বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBalance" Text='<%# Eval("Balance") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="তারিখ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblRequisitionDate" Text='<%# Eval("RequisitionDate","{0:dd/MM/yyyy}") %>' runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblBags" Text='<%# Eval("Bags") %>' runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="চাহিদা" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalBalance" runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="অনুঃ বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblApprovedBags" runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ঋণ/বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmountPerBag" runat="server" HorizontalAlign="Center" />                                                               
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ঋণ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLoanAmount" runat="server" HorizontalAlign="Center" />                                                               
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="অনুঃ" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIsApproved" Text='<%# Eval("IsApproved") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="মোট বস্তা" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalRequisitioned" runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="#CC0000" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="dsRequisition" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>" OnSelecting="dsRequisition_Selecting"
                            SelectCommand="SELECT TOP (100) PERCENT lr.RequisitionID, lr.PartyID, ip.PartyType, ip.PartyCode, ip.PartyName, ip.FatherName, ip.ContactNo, ip.AreaVillageName, ip.AreaPOName, 
                            ip.AccountID, ip.AccountNo, lr.RequisitionDate, lr.ApprovalDate, lr.SerialIDs, lr.Bags, lr.ApprovedBags, lr.AmountPerBag, lr.LoanAmount, 
                            lr.Bags - lr.ApprovedBags AS Balance, lr.IsApproved, ld.LoanID, ld.LoanDate, ld.Bags AS DBags, ld.AmountPerBag AS DAmountPerBag, 
                            ld.LoanAmount AS DLoanAmount, ld.TMID
                            FROM SRVLoanRequisition AS lr INNER JOIN
                            INVParty AS ip ON lr.PartyID = ip.PartyID LEFT OUTER JOIN
                            SRVLoanDisburse AS ld ON lr.RequisitionID = ld.RequisitionID
                            WHERE ((lr.RequisitionDate >= @DateFrom) AND (lr.RequisitionDate <= @DateTo)) ORDER BY lr.PartyID, lr.RequisitionID"
                            FilterExpression="PartyCode LIKE '{0}'">
                            <FilterParameters>
                                <asp:ControlParameter Name="PartyCode" ControlID="hdnSearch" PropertyName="Value" />
                            </FilterParameters>
                            <SelectParameters>
                                <asp:Parameter Name="DateFrom" Type="DateTime" />
                                <asp:Parameter Name="DateTo" Type="DateTime" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        মোট ঋণ চাহিদা : <asp:Label runat="server" ID="lblClBalance"></asp:Label> বস্তা
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
