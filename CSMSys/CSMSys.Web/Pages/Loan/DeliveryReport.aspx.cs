﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.INV;
using CSMSys.Web.Pages.ACC.Reports;
using CSMSys.Lib.Manager.SRV;
using CSMSys.Web.Pages.Reports;
using CSMSys.Lib.Manager.Loan;
using System.Globalization;

namespace CSMSys.Web.Pages.Loan
{
    public partial class DeliveryReport : System.Web.UI.Page
    {
        #region Method for Private
        private string printer = ConfigurationManager.AppSettings["StorePrinter"].ToString();
        private string connstring = ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString;


        private int CollectionID
        {
            get
            {
                if (ViewState["CollectionID"] == null)
                    ViewState["CollectionID"] = -1;
                return (int)ViewState["CollectionID"];
            }
            set
            {
                ViewState["CollectionID"] = value;
            }
        }

        private Int32 PartyID
        {
            get
            {
                if (ViewState["PartyID"] == null)
                    ViewState["PartyID"] = -1;
                return (Int32)ViewState["PartyID"];
            }
            set
            {
                ViewState["PartyID"] = value;
            }
        }

        SRVRegistration _registration;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Form.Target = "_blank";
            if (!IsPostBack)
            {
                lblDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
                PartyID = string.IsNullOrEmpty(Request.QueryString["PID"]) ? 0 : int.Parse(Request.QueryString["PID"]);
                CollectionID = string.IsNullOrEmpty(Request.QueryString["CID"]) ? 0 : int.Parse(Request.QueryString["CID"]);
                grvSR.DataBind();
                LoadAllValue(PartyID);
                LoadDeliveryValue(CollectionID);
                grvSR.DataBind();
                //LoadAllValue(intPartyID, bagLoanID);
            }
        }

        #region Method for Load
        public void LoadAllValue(int intPartyID)
        {

            SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);


            cnn.Open();
            string PartyQuery = "exec SP_Party_Information " + intPartyID;
            SqlDataAdapter daItem = new SqlDataAdapter(PartyQuery, cnn);
            dsPartyInfo ds = new dsPartyInfo();
            daItem.Fill(ds, "PartyDetails");

            lblCode.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[1].ToString();
            lblName.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[2].ToString();
            lblGpDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
            lblGpCode.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[1].ToString();
            lblGpName.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[2].ToString();
            lblDoDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
            lblDoCode.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[1].ToString();
            lblDoName.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[2].ToString();
            lblPartyType.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[9].ToString();


            cnn.Close();
        }
        #endregion

        #region Method for Delivery Load
        public void LoadDeliveryValue(int CollectionID)
        {

            SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);


            cnn.Open();
            string deliveryQuery = @"SELECT     lcd.CollectionID,SUM(lcd.WBags) as WBags,AVG(lcd.PLoanAmtPerBag) as PLoanAmtPerBag, AVG(lcd.RentPerBag) as RentPerBag  from                                      
                                        dbo.SRVLoanCollection AS lc inner join dbo.SRVLoanCollectionDetail lcd on lc.CollectionID=lcd.CollectionID 
                                            where  lcd.CollectionID=" + CollectionID + " group by lcd.CollectionID";

            SqlCommand cmd = new SqlCommand(deliveryQuery, cnn);
            SqlDataReader sdr = cmd.ExecuteReader();
            while (sdr.Read())
            {
                this.lblWBags.Text = sdr["WBags"].ToString();
                this.lblGPWBags.Text = sdr["WBags"].ToString();
                this.lblDOWBags.Text = sdr["WBags"].ToString();
                lblAmtPerBag.Text = sdr["PLoanAmtPerBag"].ToString();
                lblChargePerBag.Text = sdr["RentPerBag"].ToString();
            }
            cnn.Close();

            SRVLoanCollection srvlc = new LoanCollectionManager().GetLoanCollectionByID(CollectionID);
            this.lblLoan.Text = srvlc.TotalPotatoLoan.ToString();
            this.lblInterest.Text = srvlc.TotalInterest.ToString();
            this.lblBagLoan.Text = srvlc.TotalBagLoan.ToString();
            this.lblCharge.Text = srvlc.TotalBagCharge.ToString();
            this.lblCarring.Text = srvlc.TotalCarryingLoan.ToString();
            this.lblPayable.Text = srvlc.TotalAmount.ToString();
            this.lblPaid.Text = srvlc.PaidAmount.ToString();
            this.lblDues.Text = srvlc.DuesAmount.ToString();
            this.lblRemarks.Text = srvlc.Remarks.ToString();
            this.lblDeliveryID.Text = CollectionID.ToString();
            this.lblGpID.Text = CollectionID.ToString();
            this.lblDoID.Text = CollectionID.ToString();
          

        }
        #endregion

        //#region Get getRegistrationInfo
        //private void getRegistrationInfo()
        //{
        //    if (CollectionID <= 0) return;
        //    _registration = new RegistrationManager().GetRegistrationByID(CollectionID);

        //    lblDeliveryID.Text = _registration.SerialNo;

        //    int partyID = (Int32)_registration.PartyID;

        //}
        //#endregion
        #region SqlDataSource Control Event Handlers
        protected void dsDelivery_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@CollectionID"].Value = CollectionID;

        }
        #endregion
    }
}