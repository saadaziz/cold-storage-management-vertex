﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Model;
using CSMSys.Lib.Manager.SRV;
using CSMSys.Lib.Manager.INV;
using CSMSys.Lib.Manager.Administration.Application;

namespace CSMSys.Web.Pages.Loan
{
    public partial class ReportLoan : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Form.Target = "_blank";
            MultiViewBagLoan.ActiveViewIndex = 0;
            if (!IsPostBack)
            {
                lblDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
                int intLoanID = string.IsNullOrEmpty(Request.QueryString["LID"]) ? 0 : Convert.ToInt32(Request.QueryString["LID"]);
                LoadAllValue(intLoanID);
            }
        }

        #region Method for Load
        public void LoadAllValue(int intLoanID)
        {
            SRVLoanDisburse _loanDisb = new LoanManager().GetLoanDByID(intLoanID);

            int intPartyID = (Int32)_loanDisb.PartyID;

            lblLoanID.Text = _loanDisb.LoanID.ToString();
            lblCaseID.Text = _loanDisb.CaseID.ToString();
            lblSerialNo.Text = _loanDisb.SerialIDs.ToString();
            lblBagNo.Text = _loanDisb.Bags.ToString();
            lblAmnt.Text = _loanDisb.AmountPerBag.ToString();
            lblTotal.Text = _loanDisb.LoanAmount.ToString();

            NumberToBangla n2b = new NumberToBangla();
            lblWords.Text = n2b.changeCurrencyToWords(lblTotal.Text.ToString()).ToString();

            INVParty _party = new PartyManager().GetPartyByID(intPartyID);

            lblPartyCode.Text = _party.PartyCode.ToString();
            lblName.Text = _party.PartyName.ToString();
            lblFName.Text = _party.FatherName.ToString();
            lblVillage.Text = _party.AreaVillageName.ToString();
            lblPO.Text = _party.AreaPOName.ToString();

            int Upozilla = (Int32)_party.UpazilaPSID;
            ADMUpazilaPS _up = new UpazilaPSManager(false).GetUpazilaPSByID(Upozilla);
            lblUpazilla.Text = _up.UpazilaPSName.ToString();

            int district = (Int32)_party.DistrictID;
            ADMDistrict _dis = new DistrictManager(false).GetDistrictByID(district);
            lblDistrict.Text = _dis.DistrictName.ToString();

            //SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);
            //cnn.Open();
            //string PartyQuery = "exec SP_Loan_Disburse " + intCaseID;
            //SqlDataAdapter daItem = new SqlDataAdapter(PartyQuery, cnn);
            //DataSet ds = new DataSet();
            //daItem.Fill(ds, "LoanDisburseData");

            //lblPartyCode.Text = ds.Tables["LoanDisburseData"].Rows[0].ItemArray[1].ToString();
            //lblName.Text = ds.Tables["LoanDisburseData"].Rows[0].ItemArray[2].ToString();
            //lblFName.Text = ds.Tables["LoanDisburseData"].Rows[0].ItemArray[3].ToString();
            //lblVillage.Text = ds.Tables["LoanDisburseData"].Rows[0].ItemArray[5].ToString();
            //lblPO.Text = ds.Tables["LoanDisburseData"].Rows[0].ItemArray[6].ToString();
            //lblUpazilla.Text = ds.Tables["LoanDisburseData"].Rows[0].ItemArray[7].ToString();
            //lblDistrict.Text = ds.Tables["LoanDisburseData"].Rows[0].ItemArray[8].ToString();
            //lblLoanID.Text = ds.Tables["LoanDisburseData"].Rows[0].ItemArray[9].ToString();
            //lblCaseID.Text = ds.Tables["LoanDisburseData"].Rows[0].ItemArray[10].ToString();
            //lblSerialNo.Text = ds.Tables["LoanDisburseData"].Rows[0].ItemArray[11].ToString();
            //lblBagNo.Text = ds.Tables["LoanDisburseData"].Rows[0].ItemArray[12].ToString();
            //lblAmnt.Text = ds.Tables["LoanDisburseData"].Rows[0].ItemArray[13].ToString();

            //lblTotal.Text = (float.Parse(lblBagNo.Text) * float.Parse(lblAmnt.Text)).ToString();

            //NumberToBangla n2b = new NumberToBangla();
            //lblWords.Text = n2b.changeCurrencyToWords(lblTotal.Text.ToString()).ToString();

            //cnn.Close();
        }
        #endregion
    }
}