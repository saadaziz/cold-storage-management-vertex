﻿<%@ Page Language="C#" Title="CSMSys :: Empty Bag Loans" AutoEventWireup="true" MasterPageFile="~/Default.Master"
    CodeBehind="EmptyBagloan.aspx.cs" Inherits="CSMSys.Web.Pages.Loan.EmptyBagloan" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../../App_Themes/TableSorter/Blue/style.css" rel="stylesheet" type="text/css" />
    <link href="../../../App_Themes/TableSorter/Green/style.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function ShowEditModal(LoanID, TMID) {
            var frame = $get('IframeEdit');
            frame.src = "../../Controls/Loan/BagLoan.aspx?UIMODE=EDIT&LID=" + LoanID + "&TMID=" + TMID;
            $find('EditModalPopup').show();
        }
        function EditCancelScript() {
            var frame = $get('IframeEdit');
            frame.src = "../../../Controls/Loading.aspx";
        }
        function EditOkayScript() {
            RefreshDataGrid();
            EditCancelScript();
        }
        function RefreshDataGrid() {
            $get('btnRefresh').click();
        }
        function NewOkayScript() {
            $get('btnRefresh').click();
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="content" style="height:605px;">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="title">
        <table width="100%" border="0" cellpadding="2" cellspacing="4">
            <tbody>
                <tr>
                    <td align="left" style="width: 17%;">
                        <h2>Bag Loan</h2>
                    </td>
                    <td align="right" valign="bottom" style="width: 74%;">
                         Search by Name/Code/Village/Contact No : 
                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                          and Date From 
                        <asp:TextBox ID="txtFromDate" runat="server" Width="87px"></asp:TextBox>
                        <cc1:CalendarExtender ID="ceFromDate" runat="server" TargetControlID="txtFromDate" />
                         To 
                        <asp:TextBox ID="txtToDate" runat="server" Width="87px"></asp:TextBox>
                        <cc1:CalendarExtender ID="ceToDate" runat="server" TargetControlID="txtToDate" />
                    </td>
                    <td align="center" valign="bottom" style="width: 3%;">
                        <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" ImageUrl="~/App_Themes/Default/Images/gridview/Search.png"
                            ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch_Click" />
                    </td>
                    <td align="center" valign="bottom" style="width: 3%;">
                        <asp:ImageButton ID="imgRefresh" runat="server" CommandName="Refresh" ImageUrl="~/App_Themes/Default/Images/gridview/Refresh.png"
                            ToolTip="Refresh" Width="16px" Height="16px" OnClick="imgRefresh_Click" />
                    </td>
                    <td align="center" valign="bottom" style="width: 3%;">
                        <asp:ImageButton ID="imgNew" runat="server" CommandName="New" ImageUrl="~/App_Themes/Default/Images/gridview/New.png"
                            ToolTip="New" Width="16px" Height="16px" />
                        <cc1:ModalPopupExtender ID="ModalPopupExtender1" BackgroundCssClass="ModalPopupBG"
                            runat="server" CancelControlID="ButtonNewCancel" OkControlID="ButtonNewDone"
                            TargetControlID="imgNew" PopupControlID="DivNewWindow" OnOkScript="NewOkayScript();">
                        </cc1:ModalPopupExtender>
                        <div class="popup_Buttons" style="display: none">
                            <input id="ButtonNewDone" value="Done" type="button" />
                            <input id="ButtonNewCancel" value="Cancel" type="button" />
                        </div>
                        <div id="DivNewWindow" style="display: none;" class="popupBagLoan">
                            <iframe id="IframeNew" frameborder="0" width="1050px" height="468px" src="../../Controls/Loan/BagLoan.aspx?UIMODE=NEW&LID=0&TMID=0"
                                class="frameborder" scrolling="no"></iframe>
                        </div>
                        <asp:Button ID="btnRefresh" runat="server" Text="Refresh" Style="display: none" OnClick="btnRefresh_Click" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="feature-box-full">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td align="left">
                                <asp:GridView ID="grvBagLoan" DataKeyNames="BagLoanID" runat="server" Width="100%"
                                    AutoGenerateColumns="False" CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvBagLoan_PageIndexChanging"
                                    ShowHeaderWhenEmpty="true" OnRowDataBound="grvBagLoan_RowDataBound" OnRowCommand="grvBagLoan_RowCommand"
                                    EmptyDataText="No Records Found" CssClass="tablesorterBlue" AllowPaging="True"
                                    PageSize="14" DataSourceID="dsBagLoan">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sl #" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="4%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSlNo" Text='<%# Eval("SlNo") %>' runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BagLoanID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBagLoanID" Text='<%# Eval("BagLoanID") %>' runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="TMID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTMID" Text='<%# Eval("TMID") %>' runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Party ID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPartyID" Text='<%# Eval("PartyID") %>' runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Party Type" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPartyType" Text='<%# Eval("PartyType") %>' runat="server" HorizontalAlign="Left" Font-Size="12px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPartyCode" Text='<%# HighlightText(Eval("PartyCode").ToString()) %>' runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="নাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPartyName" Text='<%# HighlightText(Eval("PartyName").ToString()) %>' runat="server" Font-Size="12px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="পিতার নাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblfathrname" Text='<%# HighlightText(Eval("FatherName").ToString()) %>' runat="server" HorizontalAlign="Left" Font-Size="12px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="গ্রাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblvill" Text='<%# HighlightText(Eval("AreaVillageName").ToString()) %>' runat="server" HorizontalAlign="Left" Font-Size="12px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Contact No" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblContactNo" Text='<%# HighlightText(Eval("ContactNo").ToString()) %>' runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="তারিখ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLoanDate" Text='<%# Eval("LoanDate","{0:dd/MM/yyyy}") %>' runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBagNumber" Text='<%# Eval("BagNumber") %>' runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Amt/Bag" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAmountPerBag" Text='<%# Eval("AmountPerBag") %>' runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Loan Amt" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%"> 
                                            <ItemTemplate>
                                                <asp:Label ID="lblLoanAmount" Text='<%# Eval("LoanAmount") %>' runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Remarks" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblremarks" Text='<%# Eval("Remarks") %>' runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="2%">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hplprint" runat="server"  Text="Print" Target="_blank"></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-Width="2%"  ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgEdit" runat="server" CommandName="Edit" ImageUrl="~/App_Themes/Default/Images/gridview/Edit.png" ToolTip="Edit" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                                    <AlternatingRowStyle BackColor="#E5EAE8" />
                                    <EditRowStyle BackColor="#999999" />
                                    <EmptyDataRowStyle ForeColor="#CC0000" />
                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                </asp:GridView>
                               <asp:SqlDataSource ID="dsBagLoan" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>" OnSelecting="dsBagLoan_Selecting"
                                    SelectCommand="SELECT ROW_NUMBER() OVER (ORDER BY sl.BagLoanID) As SlNo, sl.BagLoanID, sl.PartyID, ip.PartyType, ip.PartyCode, ip.PartyName, ip.FatherName, ip.ContactNo, ip.AreaVillageName, 
                                        sl.LoanDate, sl.BagNumber, sl.AmountPerBag, sl.LoanAmount, sl.Remarks, sl.TMID 
                                        FROM SRVBagLoan AS sl INNER JOIN
                                        INVParty AS ip ON ip.PartyID = sl.PartyID 
                                        WHERE ((sl.LoanDate >= @DateFrom) AND (sl.LoanDate <= @DateTo)) ORDER BY sl.BagLoanID"
                                        FilterExpression="PartyName LIKE '%{0}%' OR ContactNo LIKE '{1}%' OR AreaVillageName LIKE '{2}%' OR PartyCode LIKE '{3}%'">
                                    <FilterParameters>
                                        <asp:ControlParameter Name="PartyName" ControlID="txtSearch" PropertyName="Text" />
                                        <asp:ControlParameter Name="ContactNo" ControlID="txtSearch" PropertyName="Text" />
                                        <asp:ControlParameter Name="AreaVillageName" ControlID="txtSearch" PropertyName="Text" />
                                        <asp:ControlParameter Name="PartyCode" ControlID="txtSearch" PropertyName="Text" />
                                    </FilterParameters>
                                    <SelectParameters>
                                        <asp:Parameter Name="DateFrom" Type="DateTime" />
                                        <asp:Parameter Name="DateTo" Type="DateTime" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
      </div>
    <div class="feature-box-actionBar">
        <span class="failureNotification">
            <asp:Literal ID="lblFailure" runat="server"></asp:Literal>
        </span>
    <asp:Button ID="btnReport" runat="server" Text="Bag Loan Register" CssClass="button" onclick="btnReport_Click" />
    </div>
    
    <asp:Button ID="ButtonEdit" runat="server" Text="Submit" Style="display: none" />
    <cc1:ModalPopupExtender ID="ModalPopupExtender2" BackgroundCssClass="ModalPopupBG"
        runat="server" CancelControlID="ButtonEditCancel" OkControlID="ButtonEditDone"
        TargetControlID="ButtonEdit" PopupControlID="DivEditWindow" OnCancelScript="EditCancelScript();"
        OnOkScript="EditOkayScript();" BehaviorID="EditModalPopup">
    </cc1:ModalPopupExtender>
    <div class="popup_Buttons" style="display: none">
        <input id="ButtonEditDone" value="Done" type="button" />
        <input id="ButtonEditCancel" value="Cancel" type="button" />
    </div>
    <div id="DivEditWindow" style="display: none;" class="popupBagLoan">
        <iframe id="IframeEdit" frameborder="0" width="1050px" height="468px" class="frameborder"
            scrolling="no"></iframe>
    </div>
</div>
</asp:Content>

