﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.SRV;
using CSMSys.Lib.Manager.INV;
using CSMSys.Lib.BusinessObjects;
using CSMSys.Lib.DataAccessLayer.Implementations;

namespace CSMSys.Web.Pages.Loan
{
    public partial class LoanApproval : System.Web.UI.Page
    {
        #region Private Properties
        private string strSearch = string.Empty;
        private SRVRegistration _srvReg;
        private SRVLoanRequisition _loanReq;

        private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }

        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }

        private bool IsApproved
        {
            get
            {
                if (ViewState["IsApproved"] == null)
                    ViewState["IsApproved"] = -1;
                return (bool)ViewState["IsApproved"];
            }
            set
            {
                ViewState["IsApproved"] = value;
            }
        }

        private int PartyID
        {
            get
            {
                if (ViewState["PartyID"] == null)
                    ViewState["PartyID"] = -1;
                return (int)ViewState["PartyID"];
            }
            set
            {
                ViewState["PartyID"] = value;
            }
        }

        private int RequisitionID
        {
            get
            {
                if (ViewState["RequisitionID"] == null)
                    ViewState["RequisitionID"] = -1;
                return (int)ViewState["RequisitionID"];
            }
            set
            {
                ViewState["RequisitionID"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateFrom = DateTime.Today;
                txtFromDate.Text = DateFrom.ToShortDateString();
                DateTo = DateTime.Today;
                txtToDate.Text = DateTo.ToShortDateString();
                IsApproved = false;
                hdnSerialNos.Value = string.Empty;

                grvRequisition.DataBind();
            }
        }

        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }

        private bool checkValidity()
        {
            if (string.IsNullOrEmpty(txtDate.Text))
            {
                lblFailure.Text = "Date is Required";
                txtDate.Focus();
                return false;
            }
            if (lblTotalBags.Text == "0")
            {
                lblFailure.Text = "Select SR Requisition";
                grvRequisition.Focus();
                return false;
            }
            return true;
        }

        private void ClearForm()
        {
            lblName.Text = string.Empty;
            lblCode.Text = string.Empty;
            lblTotalBags.Text = "0";
            txtDate.Text = string.Empty;
            txtBags.Text = "0";
            txtLoans.Text = "0";
            txtAmtPerBags.Text = "0";

            grvRequisition.DataBind();
        }

        private bool LoanApproved()
        {
            bool res = false;
            _loanReq = new RequisitionManager().GetRequisitionByID(RequisitionID);
            string serialIDs = _loanReq.SerialIDs;

            IList<INVStockSerial> serialList = new StockSerialNo().getSerialFromSerialIDS(serialIDs);

            foreach (INVStockSerial serial in serialList)
            {
                SRVRegistration temp = new SRVRegistration();
                SRVRegistration sr = new RegistrationDAOLinq().SearchRegistrationByNo(serial.SerialNo);
                temp.Requisitioned = "Approved";
                temp.RegistrationID = sr.RegistrationID;
                temp.RegistrationDate = sr.RegistrationDate;
                temp.AmountPerBag = sr.AmountPerBag;
                temp.BagWeight = sr.BagWeight;
                temp.SerialID = sr.SerialID;
                temp.SerialNo = sr.SerialNo;
                temp.Remarks = sr.Remarks;
                temp.BagLoan = sr.BagLoan;
                temp.CarryingLoan = sr.CarryingLoan;
                temp.PartyID = sr.PartyID;
                temp.LoanDate = null;
                temp.Bags = sr.Bags;
                temp.CaseID = sr.CaseID;
                temp.TMID = sr.TMID;
                temp.CreatedBy = sr.CreatedBy;
                temp.CreatedDate = sr.CreatedDate;
                temp.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                temp.ModifiedDate = DateTime.Now;

                res = (new RegistrationManager().SaveRegistration(temp));
            }

            return res;
        }

        private void UpdateRequisition()
        {
            _loanReq = new SRVLoanRequisition();
            if (new RequisitionManager().SaveRequisition(FormToObject()))
            {
                ClearForm();
            }
        }

        private SRVLoanRequisition FormToObject()
        {
            _loanReq = new SRVLoanRequisition();
            SRVLoanRequisition _loanApv = new RequisitionManager().GetRequisitionByID(RequisitionID);

            _loanReq.CreatedBy = _loanApv.CreatedBy;
            _loanReq.CreatedDate = _loanApv.CreatedDate;
            _loanReq.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
            _loanReq.ModifiedDate = System.DateTime.Now;

            _loanReq.RequisitionID = RequisitionID;
            _loanReq.PartyID = PartyID;
            _loanReq.RequisitionDate = _loanApv.RequisitionDate;
            _loanReq.ApprovalDate = DateTime.Parse(txtDate.Text);
            _loanReq.Bags = _loanApv.Bags;
            _loanReq.ApprovedBags = float.Parse(txtBags.Text);
            _loanReq.AmountPerBag = (txtAmtPerBags.Text != "0") ? Math.Round((float.Parse(txtAmtPerBags.Text)), 2) : 0;
            _loanReq.LoanAmount = float.Parse(txtLoans.Text);
            _loanReq.Remarks = string.Empty;
            _loanReq.SerialIDs = _loanApv.SerialIDs;
            _loanReq.IsApproved = true;
            _loanReq.IsDisbursed = false;

            return _loanReq;
        }
        #endregion

        #region Methods For Grid
        protected void grvRequisition_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                string[] word = e.CommandArgument.ToString().Split('@');

                PartyID = Convert.ToInt32(word[0].Trim());
                RequisitionID = int.Parse(word[1].Trim());

                INVParty _party = new PartyManager().GetPartyByID(PartyID);
                lblCode.Text = _party.PartyCode;
                lblName.Text = _party.PartyName;

                int parentID = (int)_party.ParentID;
                if (parentID > 0)
                {
                    _party = new PartyManager().GetPartyByID(parentID);
                    lblGCode.Text = _party.PartyCode;
                }

                _loanReq = new RequisitionManager().GetRequisitionByID(RequisitionID);
                if (_loanReq.IsApproved == true)
                {
                    txtDate.Text = DateTime.Parse(_loanReq.ApprovalDate.ToString()).ToShortDateString();
                    lblTotalBags.Text = _loanReq.Bags.ToString();
                    txtBags.Text = _loanReq.ApprovedBags.ToString();
                    txtLoans.Text = _loanReq.LoanAmount.ToString();
                    txtAmtPerBags.Text = _loanReq.AmountPerBag.ToString();
                }
                else
                {
                    txtDate.Text = DateTime.Parse(_loanReq.RequisitionDate.ToString()).ToShortDateString();
                    lblTotalBags.Text = _loanReq.Bags.ToString();
                    txtBags.Text = "0";
                    txtLoans.Text = "0";
                    txtAmtPerBags.Text = "0";
                }
            }
        }

        protected void grvRequisition_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvRequisition.PageIndex = e.NewPageIndex;
            grvRequisition.DataBind();
        }

        protected void grvRequisition_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                ImageButton btnSelect = (ImageButton)e.Row.FindControl("imgSelect");
                btnSelect.CommandArgument = (DataBinder.Eval(e.Row.DataItem, "PartyID")).ToString() + "@" +
                                            (DataBinder.Eval(e.Row.DataItem, "RequisitionID")).ToString();
            }
        }
        #endregion

        #region SqlDataSource Control Event Handlers
        protected void dsRequisition_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@DateFrom"].Value = DateFrom;
            e.Command.Parameters["@DateTo"].Value = DateTo;
            e.Command.Parameters["@IsApproved"].Value = IsApproved;
        }
        #endregion

        #region Methods For Button
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;
            DateFrom = DateTime.Parse(txtFromDate.Text.ToString());
            DateTo = DateTime.Parse(txtToDate.Text.ToString());
            IsApproved = chkApproved.Checked == true ? true : false;

            grvRequisition.DataBind();
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvRequisition.DataBind();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            grvRequisition.DataBind();
        }

        protected void chkApplied_CheckedChanged(object sender, EventArgs e)
        {
            strSearch = txtSearch.Text;

            DateFrom = DateTime.Parse(txtFromDate.Text.ToString());
            DateTo = DateTime.Parse(txtToDate.Text.ToString());
            IsApproved = chkApproved.Checked == true ? true : false;

            grvRequisition.DataBind();
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            if (!checkValidity()) return;
            if ((float.Parse(txtBags.Text) > 0) && (float.Parse(txtLoans.Text) > 0))
            {
                if (LoanApproved())
                {
                    UpdateRequisition();
                }
            }
        }

        protected void txtBags_TextChanged(object sender, EventArgs e)
        {
            float bags = string.IsNullOrEmpty(txtBags.Text) ? 0 : float.Parse(txtBags.Text);
            float amtBags = string.IsNullOrEmpty(txtAmtPerBags.Text) ? 0 : float.Parse(txtAmtPerBags.Text);
            float loans = string.IsNullOrEmpty(txtLoans.Text) ? 0 : float.Parse(txtLoans.Text);

            if (loans > 0)
            {
                txtAmtPerBags.Text = loans > bags ? (loans / bags).ToString() : "0";
                txtAmtPerBags.Text = String.Format("{0:N}", float.Parse(txtAmtPerBags.Text));
            }
            else if (amtBags > 0)
            {
                txtLoans.Text = (bags * amtBags).ToString();
                txtLoans.Text = String.Format("{0:N}", float.Parse(txtLoans.Text));
            }
            else
            {
                txtLoans.Text = (bags * amtBags).ToString();
                txtLoans.Text = String.Format("{0:N}", float.Parse(txtLoans.Text));
            }
        }

        protected void txtLoans_TextChanged(object sender, EventArgs e)
        {
            float bags = string.IsNullOrEmpty(txtBags.Text) ? 0 : float.Parse(txtBags.Text);
            float loans = string.IsNullOrEmpty(txtLoans.Text) ? 0 : float.Parse(txtLoans.Text);

            txtAmtPerBags.Text = loans > bags ? (loans / bags).ToString() : "0";
            txtAmtPerBags.Text = String.Format("{0:N}", float.Parse(txtAmtPerBags.Text));
        }

        protected void txtAmtPerBags_TextChanged(object sender, EventArgs e)
        {
            float bags = string.IsNullOrEmpty(txtBags.Text) ? 0 : float.Parse(txtBags.Text);
            float amtBags = string.IsNullOrEmpty(txtAmtPerBags.Text) ? 0 : float.Parse(txtAmtPerBags.Text);

            txtLoans.Text = (bags * amtBags).ToString();
            txtLoans.Text = String.Format("{0:N}", float.Parse(txtLoans.Text));
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            string fromDate = txtFromDate.Text;
            string toDate = txtToDate.Text;
            Response.Redirect("~/Pages/Loan/ReportApproved.aspx?FD=" + fromDate + "&TD=" + toDate + "&PC=" + txtSearch.Text);
        }
        #endregion
    }
}