﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.INV;

namespace CSMSys.Web.Pages.Loan
{
    public partial class EmptyBagloan : System.Web.UI.Page
    {
        private string strSearch = string.Empty;

        private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }

        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateFrom = DateTime.Today;
                txtFromDate.Text = DateFrom.ToShortDateString();
                DateTo = DateTime.Today;
                txtToDate.Text = DateTo.ToShortDateString();

                grvBagLoan.DataBind();
            }
        }
        
        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }
        #endregion
        
        #region Methods For Grid
        protected void grvBagLoan_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //if (e.CommandName.Equals("Edit"))
            //{
            //    int intPartyID = Convert.ToInt32(e.CommandArgument.ToString());

            //    if (intPartyID > 0)
            //    {
            //        //DeleteParty(intPartyID);
            //    }
            //}
        }

        protected void grvBagLoan_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvBagLoan.PageIndex = e.NewPageIndex;
            grvBagLoan.DataBind();
        }

        protected void grvBagLoan_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[0].Controls.Count > 0))
            {
                ImageButton btnEdit = (ImageButton)e.Row.FindControl("imgEdit");
                btnEdit.Attributes.Add("onClick", "javascript:ShowEditModal('" + ((Label)e.Row.Cells[1].Controls[1]).Text + "','" + ((Label)e.Row.Cells[2].Controls[1]).Text + "');");

                HyperLink hpl = (HyperLink)e.Row.FindControl("hplprint");
                hpl.NavigateUrl = "~/Pages/Loan/ReportBagLoan.aspx?PID=" + (DataBinder.Eval(e.Row.DataItem, "PartyID")).ToString() + "&BID=" + (DataBinder.Eval(e.Row.DataItem, "BagLoanID")).ToString();
         
            }
        }
        #endregion

        #region SqlDataSource Control Event Handlers
        protected void dsBagLoan_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@DateFrom"].Value = DateFrom;
            e.Command.Parameters["@DateTo"].Value = DateTo;
        }
        #endregion

        #region Methods For Button
        protected void imgRefresh_Click(object sender, EventArgs e)
        {
            grvBagLoan.DataBind();
        }
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text.Trim();


            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
            dateInfo.ShortDatePattern = "MM/dd/yyyy";

            DateFrom = Convert.ToDateTime(txtFromDate.Text.ToString(), dateInfo);
            DateTo = Convert.ToDateTime(txtToDate.Text.ToString(), dateInfo);
            grvBagLoan.DataBind();
        }
        #endregion

        protected void btnReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (!checkValidity()) return;
                string fromDate = txtFromDate.Text;
                string toDate = txtToDate.Text;
                Response.Redirect("~/Pages/Loan/EmptyBagRegister.aspx?FD=" + fromDate + "&TD=" + toDate + "&PC=" + txtSearch.Text);
            }
            catch (Exception ex)
            {
            }
        }

        private bool checkValidity()
        {
                DateTime dt = new DateTime();
                dt.ToString("yyyy/MM/dd")     ;
                //String.Format("{0:yyyy/MM/dd}", dt);
                if (!DateTime.TryParse(txtFromDate.Text, out dt))
                {
                lblFailure.Text = "Select From Date";
                txtFromDate.Focus();
                return false;
                 }

                if (!DateTime.TryParse(txtToDate.Text, out dt))
                {
                lblFailure.Text = "Select To Date";
                txtToDate.Focus();
                return false;

            }
            if (txtFromDate.Text == "" || txtToDate.Text == "")
            {
                lblFailure.Text = "Select Date";
                txtFromDate.Focus();
                return false;
            }
            return true;
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            grvBagLoan.DataBind();
        }
    }
}