﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CSMSys.Web.Pages;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.INV;
using System.Globalization;
using System.Resources;
using System.Threading;
using System.Reflection;
using System.Data.OleDb;

namespace CSMSys.Web.Pages.Loan
{
    public partial class EmptyBagRegister : System.Web.UI.Page
    {
        #region Private
        private string printer = ConfigurationManager.AppSettings["ServicePrinter"].ToString();
        private string connstring = ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString;
        private int TotalLoan = 0;
        private INVParty _party;

        private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }

        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }

        private Int32 PartyID
        {
            get
            {
                if (ViewState["PartyID"] == null)
                    ViewState["PartyID"] = -1;
                return (Int32)ViewState["PartyID"];
            }
            set
            {
                ViewState["PartyID"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            //this.Form.Target = "_blank";
            //GridViewHelper helper = new GridViewHelper(this.grvBagloan);
            //helper.RegisterGroup("LoanDate", true, true);
            ////helper.RegisterSummary("Bags", SummaryOperation.Sum, "State");
            ////helper.RegisterSummary("Bags", SummaryOperation.Sum);
            ////helper.GroupSummary += new GroupEvent(helper_GroupSummary);
            //helper.ApplyGroupSort();

            if (!IsPostBack)
            {
            DateFrom = string.IsNullOrEmpty(Request.QueryString["FD"]) ? DateTime.Today : Convert.ToDateTime(Request.QueryString["FD"]);
            DateTo = string.IsNullOrEmpty(Request.QueryString["TD"]) ? DateTime.Today : Convert.ToDateTime(Request.QueryString["TD"]);
            string code = string.IsNullOrEmpty(Request.QueryString["PC"]) ? string.Empty : Request.QueryString["PC"];

            lblfrom.Text = DateFrom.ToShortDateString();
            lblto.Text = DateTo.ToShortDateString();
            hdnSearch.Value = code;

            if (string.IsNullOrEmpty(code))
            {
                lblOpBalance.Text = getBagLoan(0, DateFrom).ToString();
            }
            else
            {
                _party = new PartyManager().SearchParentByCode(code);
                if (_party != null)
                {
                    int partyID = _party.PartyID != null ? Convert.ToInt32(_party.PartyID) : 0;

                    if (partyID > 0)
                    {
                        lblOpBalance.Text = getBagLoan(partyID, DateFrom).ToString();
                    }
                    lblParty.Text = string.IsNullOrEmpty(_party.PartyCodeName) ? string.Empty : _party.PartyCodeName;
                }
            }

            TotalLoan = int.Parse(string.IsNullOrEmpty(lblOpBalance.Text) ? "0" : lblOpBalance.Text.ToString());

            grvBagloan.DataBind();
            }
        }

        private int getBagLoan(int PartyID, DateTime dateFrom)
        {
            int opBalance = 0;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstring))
                {
                    sqlConn.Open();
                    string _query = "SELECT SUM(BagNumber) AS TotalBags ";
                    _query = _query + "FROM SRVBagLoan ";
                    _query = _query + "WHERE (LoanDate < CONVERT(DATETIME, '" + dateFrom.ToShortDateString() + " 00:00:00', 102)) ";
                    if (PartyID > 0)
                    {
                        _query = _query + "AND (PartyID = " + PartyID + ")";
                    }
                    SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    if (sqlReader.Read())
                    {
                        if (sqlReader.HasRows) opBalance = string.IsNullOrEmpty(sqlReader["TotalBags"].ToString()) ? 0 : int.Parse(sqlReader["TotalBags"].ToString());
                    }

                    sqlReader.Close();
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }

        #region Method for Grid
        protected void grvBagloan_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvBagloan.PageIndex = e.NewPageIndex;
            grvBagloan.DataBind();
        }

        protected void grvBagloan_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[3].Controls.Count > 0))
            {
                TotalLoan += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "BagNumber"));

                Label lblTotalLoaned = (Label)e.Row.FindControl("lblTotalLoaned");
                lblTotalLoaned.Text = TotalLoan.ToString();
            }
            lblClBalance.Text = TotalLoan.ToString();
        }

        #region SqlDataSource Control Event Handlers
        protected void dsBagloan_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@DateFrom"].Value = DateFrom;
            e.Command.Parameters["@DateTo"].Value = DateTo;
        }
        #endregion
        #endregion
    }
}
