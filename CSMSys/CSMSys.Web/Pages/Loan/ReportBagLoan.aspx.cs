﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CSMSys.Lib.AccountingEntity;
using CSMSys.Lib.AccountingUtility;
using CSMSys.Lib.AccountingDataAccess;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using System.Globalization;

namespace CSMSys.Web.Pages.Loan
{
    public partial class ReportBagLoan : System.Web.UI.Page
    {
        #region Private Properties
        private string connstring = ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString;
        private decimal TotalDebit = (decimal)0.0;
        private decimal TotalCredit = (decimal)0.0;

        SqlConnection formCon = null;

        private int VoucherType
        {
            get
            {
                if (ViewState["VoucherType"] == null)
                    ViewState["VoucherType"] = -1;
                return (int)ViewState["VoucherType"];
            }
            set
            {
                ViewState["VoucherType"] = value;
            }
        }

        private int TransactionID
        {
            get
            {
                if (ViewState["TransactionID"] == null)
                    ViewState["TransactionID"] = -1;
                return (int)ViewState["TransactionID"];
            }
            set
            {
                ViewState["TransactionID"] = value;
            }
        }

        DaTransaction objDaTrans = null;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Form.Target = "_blank";
            if (!IsPostBack)
            {
                //lblDate.Text = DateTime.Today.ToString("dd/MM/yyyy"); 
                int intPartyID = Convert.ToInt32(Request.QueryString["PID"]);
                int bagLoanID = Convert.ToInt32(Request.QueryString["BID"]);
                //VoucherType = string.IsNullOrEmpty(Request.QueryString["Voucher"]) ? 1 : int.Parse(Request.QueryString["Voucher"]);
                //TransactionID = string.IsNullOrEmpty(Request.QueryString["TMID"]) ? 0 : int.Parse(Request.QueryString["TMID"]);
                //LoadValue()
                LoadAllValue(intPartyID, bagLoanID);
            }
        }

        #region Method for Load
        public void LoadAllValue(int intPartyID, int bagLoanID)
        {
            SqlConnection cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

            cnn.Open();
            string PartyQuery = "exec SP_Party_Information " + intPartyID;
            SqlDataAdapter daItem = new SqlDataAdapter(PartyQuery, cnn);

            DataSet ds = new DataSet();
            daItem.Fill(ds, "PartyDetails");

            lblCode.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[1].ToString();
            lblName.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[2].ToString();
            lblFName.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[3].ToString();
            lblPartyType.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[9].ToString();
            lblVillage.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[5].ToString();
            lblPO.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[6].ToString();
            lblUpazilla.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[7].ToString();
            lblDistrict.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[8].ToString();

            lblGCode.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[1].ToString();
            lblGName.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[2].ToString();
            lblGFather.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[3].ToString();
            lblGType.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[9].ToString();
            lblGVill.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[5].ToString();
            lblGPO.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[6].ToString();
            lblGUpozilla.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[7].ToString();
            lblGDistrict.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[8].ToString();

            lblSCode.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[1].ToString();
            lblSName.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[2].ToString();
            lblSFather.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[3].ToString();
            lblSType.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[9].ToString();
            lblSVill.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[5].ToString();
            lblSPO.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[6].ToString();
            lblSUpozilla.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[7].ToString();
            lblSDistrict.Text = ds.Tables["PartyDetails"].Rows[0].ItemArray[8].ToString();
            cnn.Close();

            cnn.Open();
            string query = "exec SP_Bag_LoanReport " + bagLoanID;
            SqlDataAdapter dscmd = new SqlDataAdapter(query, cnn);
            dscmd.Fill(ds, "SRVBagLoan");
            lblBagLoanID.Text = ds.Tables["SRVBagLoan"].Rows[0].ItemArray[0].ToString();
            lblDate.Text = DateTime.Parse(ds.Tables["SRVBagLoan"].Rows[0].ItemArray[4].ToString()).ToShortDateString();

            //lblBagNo.Text = ds.Tables["SRVBagLoan"].Rows[0].ItemArray[2].ToString();
            lblBag.Text = ds.Tables["SRVBagLoan"].Rows[0].ItemArray[2].ToString();

            lblGLoan.Text = ds.Tables["SRVBagLoan"].Rows[0].ItemArray[0].ToString();
            lblGDate.Text = DateTime.Parse(ds.Tables["SRVBagLoan"].Rows[0].ItemArray[4].ToString()).ToShortDateString();
            lblGBags.Text = ds.Tables["SRVBagLoan"].Rows[0].ItemArray[2].ToString();

            lblSDate.Text = DateTime.Parse(ds.Tables["SRVBagLoan"].Rows[0].ItemArray[4].ToString()).ToShortDateString();
            lblSBags.Text = ds.Tables["SRVBagLoan"].Rows[0].ItemArray[2].ToString();
            cnn.Close();
        }
        #endregion
    }
}