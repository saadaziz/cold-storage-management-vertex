﻿<%@ Page Language="C#" Title="CSMSys :: Bag Loan Register" AutoEventWireup="true" CodeBehind="EmptyBagRegister.aspx.cs" Inherits="CSMSys.Web.Pages.Loan.EmptyBagRegister" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="hdnSearch" runat="server" />
    <div class="feature-box-full">
            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                <tbody>
                <tr>
                    <td align="center" valign="top" style="font-size:10px;">
                        বিসমিল্লাহির রাহমানির রাহিম
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:28px;">
                        শাহ্‌ ইসমাঈল গাজী (রহঃ) কোল্ড ষ্টোরেজ লিঃ
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:8px;">
                        সায়েকপুর, খেজমতপুর, পীরগঞ্জ, রংপুর। মোবাঃ ০১৭১৫৫৯৭৫৯১,০১৭১৯৭০৮৬২১, ০১৭৩২১১২১৯৬, হেড অফিসঃ ধানমণ্ডি, ঢাকা
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:16px;">
                       খালি বস্তা রেজিস্টার
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">
                        তারিখ <asp:Label ID="lblfrom" runat="server" Text=""></asp:Label> 
                        হতে <asp:Label ID="lblto" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">
                        <asp:Label runat="server" ID="lblParty"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        পূর্বের জের : <asp:Label runat="server" ID="lblOpBalance"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="font-size:12px;">
                        <asp:GridView ID="grvBagloan"  runat="server" Width="100%"
                            AutoGenerateColumns="False" CellPadding="3" HorizontalAlign="Left" ShowHeaderWhenEmpty="true"
                            EmptyDataText="No Records Found" OnRowDataBound="grvBagloan_RowDataBound" DataSourceID="dsBagloan">
                            <Columns>
                                <asp:TemplateField HeaderText="Sl #" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="4%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSlNo" Text='<%# Eval("SlNo") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BagLoanID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBagLoanID" Text='<%# Eval("BagLoanID") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="কোড" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPartyCode" Text='<%# Eval("PartyCode") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="নাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPartyName" Text='<%# Eval("PartyName") %>' runat="server" HorizontalAlign="Left" style="font-size:12px;" />
                                      </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="মোবাইল নং" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblContactNo" Text='<%# Eval("ContactNo") %>' runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="তারিখ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblLoanDate" Text='<%# Eval("LoanDate","{0:dd/MM/yyyy}") %>' runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblBagNumber" Text='<%# Eval("BagNumber") %>' runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Amt/Bag" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAmountPerBag" Text='<%# Eval("AmountPerBag") %>' runat="server" HorizontalAlign="Center" />                                                               
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Loan Amt" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLoanAmount" Text='<%# Eval("LoanAmount") %>' runat="server" HorizontalAlign="Center" />                                                               
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bags Loaded" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBagsLoaded" Text='<%# Eval("BagsLoaded") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total Loaded" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalLoaded" runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bags Loaned" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblBagsLoaned" Text='<%# Eval("BagsLoaned") %>' runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="মোট বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalLoaned" runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
<%--                                <asp:BoundField DataField="SlNo" HeaderText="Sl #" SortExpression="SlNo" />
                                <asp:BoundField DataField="BagLoanID" HeaderText="ব্যাগ লোন আইডি" SortExpression="BagLoanID" />
                                <asp:BoundField DataField="PartyCode" HeaderText="পার্টি কোড" SortExpression="PartyCode" />
                                <asp:BoundField DataField="PartyName" HeaderText="পার্টির নাম" SortExpression="PartyName" />
                                <asp:BoundField DataField="ContactNo" HeaderText="মোবাইল নং" SortExpression="ContactNo" />
                                <asp:BoundField DataField="LoanDate" visible="false" HeaderText="তারিখ" SortExpression="LoanDate" />
                                <asp:BoundField DataField="BagNumber" HeaderText="বস্তা" SortExpression="BagNumber" />
                                <asp:BoundField DataField="AmountPerBag" HeaderText="Amt/Bag" SortExpression="AmountPerBag" />
                                <asp:BoundField DataField="LoanAmount" HeaderText="বস্তা লোন" SortExpression="LoanAmount" />
                                <asp:BoundField DataField="BagsLoaded" HeaderText="Bags Loaded" SortExpression="BagsLoaded" />
                                <asp:BoundField DataField="BagsLoaned" HeaderText="	Bags Loaned" SortExpression="BagsLoaned" />--%>
                            </Columns>
                            <EmptyDataRowStyle ForeColor="#CC0000" />
                        </asp:GridView>
<%--                        <asp:SqlDataSource ID="dsBagloan" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>"
                            SelectCommand="SP_BagLoan_Register" SelectCommandType="StoredProcedure">
                            <SelectParameters>
                                <asp:QueryStringParameter Name="FromDate" QueryStringField="FD" Type="DateTime" />
                                <asp:QueryStringParameter Name="ToDate" QueryStringField="TD" Type="DateTime" />
                            </SelectParameters>
                        </asp:SqlDataSource>--%>
                        <asp:SqlDataSource ID="dsBagloan" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>" OnSelecting="dsBagloan_Selecting"
                            SelectCommand="SELECT ROW_NUMBER() OVER (ORDER BY bl.BagLoanID) As SlNo, bl.BagLoanID, bl.PartyID, ip.PartyType, ip.PartyCode, ip.PartyName, ip.FatherName, ip.ContactNo, ip.AreaVillageName, 
                                bl.LoanDate, bl.BagNumber, bl.AmountPerBag, bl.LoanAmount, bl.BagsLoaned, bl.BagsLoaded
                                FROM SRVBagLoan AS bl INNER JOIN
                                INVParty AS ip ON bl.PartyID = ip.PartyID 
                                WHERE ((bl.LoanDate >= @DateFrom) AND (bl.LoanDate <= @DateTo)) ORDER BY bl.BagLoanID"
                                FilterExpression="PartyCode LIKE '{0}'">
                            <FilterParameters>
                                <asp:ControlParameter Name="PartyCode" ControlID="hdnSearch" PropertyName="Value" />
                            </FilterParameters>
                            <SelectParameters>
                                <asp:Parameter Name="DateFrom" Type="DateTime" />
                                <asp:Parameter Name="DateTo" Type="DateTime" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top">
                        মোট প্রাপ্ত বস্তা : <asp:Label runat="server" ID="lblClBalance"></asp:Label>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
