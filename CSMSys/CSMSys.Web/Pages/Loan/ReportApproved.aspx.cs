﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CSMSys.Web.Pages;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.INV;
using System.Globalization;

namespace CSMSys.Web.Pages.Loan
{
    public partial class ReportApproved : System.Web.UI.Page
    {
        #region Private
        private string printer = ConfigurationManager.AppSettings["ServicePrinter"].ToString();
        private string connstring = ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString;
        private int TotalReq = 0;
        private INVParty _party;

        private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }

        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }

        private Int32 PartyID
        {
            get
            {
                if (ViewState["PartyID"] == null)
                    ViewState["PartyID"] = -1;
                return (Int32)ViewState["PartyID"];
            }
            set
            {
                ViewState["PartyID"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            DateFrom = string.IsNullOrEmpty(Request.QueryString["FD"]) ? DateTime.Today : Convert.ToDateTime(Request.QueryString["FD"]);
            DateTo = string.IsNullOrEmpty(Request.QueryString["TD"]) ? DateTime.Today : Convert.ToDateTime(Request.QueryString["TD"]);
            string code = string.IsNullOrEmpty(Request.QueryString["PC"]) ? string.Empty : Request.QueryString["PC"];

            lblfrom.Text = DateFrom.ToShortDateString();
            lblto.Text = DateTo.ToShortDateString();
            hdnSearch.Value = code;

            if (string.IsNullOrEmpty(code))
            {
                lblParty.Text = string.Empty;
            }
            else
            {
                _party = new PartyManager().SearchParentByCode(code);
                if (_party != null)
                {
                    lblParty.Text = string.IsNullOrEmpty(_party.PartyCodeName) ? string.Empty : _party.PartyCodeName;
                }
            }
            lblOpBalance.Text = getRequisition(DateFrom) <= 0 ? "0" : getRequisition(DateFrom).ToString();
            TotalReq = int.Parse(string.IsNullOrEmpty(lblOpBalance.Text) ? "0" : lblOpBalance.Text.ToString());

            grvRequisition.DataBind();
        }

        private int getRequisition(DateTime dateFrom)
        {
            int opBalance = 0;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstring))
                {
                    sqlConn.Open();
                    string _query = "SELECT SUM(LoanAmount) AS LoanAmount ";
                    _query = _query + "FROM SRVLoanRequisition ";
                    _query = _query + "WHERE (RequisitionDate < CONVERT(DATETIME, '" + dateFrom.ToShortDateString() + " 00:00:00', 102))";
                    SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    if (sqlReader.Read())
                    {
                        if (sqlReader.HasRows) opBalance = string.IsNullOrEmpty(sqlReader["LoanAmount"].ToString()) ? 0 : int.Parse(sqlReader["LoanAmount"].ToString());
                    }

                    sqlReader.Close();
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }

        private int getPreviousBalance(int partyID, DateTime dateFrom)
        {
            int opBalance = 0;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstring))
                {
                    sqlConn.Open();
                    string _query = "SELECT SUM(Bags - ApprovedBags) AS TotalBags ";
                    _query = _query + "FROM SRVLoanRequisition ";
                    _query = _query + "WHERE (RequisitionDate < CONVERT(DATETIME, '" + dateFrom.ToShortDateString() + " 00:00:00', 102)) ";
                    _query = _query + "AND PartyID = " + partyID + " AND IsApproved = 'True'";
                    SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    if (sqlReader.Read())
                    {
                        if (sqlReader.HasRows) opBalance = string.IsNullOrEmpty(sqlReader["TotalBags"].ToString()) ? 0 : int.Parse(sqlReader["TotalBags"].ToString());
                    }

                    sqlReader.Close();
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }

        #region Method for Grid
        protected void grvRequisition_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvRequisition.PageIndex = e.NewPageIndex;
            grvRequisition.DataBind();
        }

        protected void grvRequisition_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[0].Controls.Count > 0))
            {
                Label lblIsApproved = (Label)e.Row.FindControl("lblIsApproved");
                if (lblIsApproved.Text == "True")
                {
                    TotalReq += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "LoanAmount"));
                }

                Label lblTotalRequisitioned = (Label)e.Row.FindControl("lblTotalRequisitioned");
                lblTotalRequisitioned.Text = TotalReq.ToString();
            }
            lblClBalance.Text = TotalReq.ToString();
        }

        #region SqlDataSource Control Event Handlers
        protected void dsRequisition_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@DateFrom"].Value = DateFrom;
            e.Command.Parameters["@DateTo"].Value = DateTo;
        }
        #endregion
        #endregion
    }
}