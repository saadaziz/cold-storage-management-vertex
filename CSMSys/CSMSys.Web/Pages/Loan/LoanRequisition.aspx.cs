﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.SRV;
using CSMSys.Lib.Manager.INV;
using CSMSys.Lib.BusinessObjects;
using CSMSys.Lib.DataAccessLayer.Implementations;

namespace CSMSys.Web.Pages.Loan
{
    public partial class LoanRequisition : System.Web.UI.Page
    {
        #region Private Properties
        private string strSearch = string.Empty;
        private SRVRegistration _Registration;
        private SRVLoanRequisition _loanReq;

        private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }

        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }

        private string Requisitioned
        {
            get
            {
                if (ViewState["Requisitioned"] == null)
                    ViewState["Requisitioned"] = -1;
                return (string)ViewState["Requisitioned"];
            }
            set
            {
                ViewState["Requisitioned"] = value;
            }
        }

        private int PartyID
        {
            get
            {
                if (ViewState["PartyID"] == null)
                    ViewState["PartyID"] = -1;
                return (int)ViewState["PartyID"];
            }
            set
            {
                ViewState["PartyID"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateFrom = DateTime.Today;
                txtFromDate.Text = DateFrom.ToShortDateString();
                DateTo = DateTime.Today;
                txtToDate.Text = DateTo.ToShortDateString();
                Requisitioned = "Not Applied";
                hdnSerialNos.Value = string.Empty;

                grvRegistration.DataBind();
            }
        }

        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }

        private bool checkValidity()
        {
            if (string.IsNullOrEmpty(txtDate.Text))
            {
                lblFailure.Text = "Date is Required";
                txtDate.Focus();
                return false;
            }
            if (lblTotalBags.Text == "0")
            {
                lblFailure.Text = "Select SR Registration";
                grvRegistration.Focus();
                return false;
            }
            return true;
        }

        private void ClearForm()
        {
            lblName.Text = string.Empty;
            lblCode.Text = string.Empty;
            lstSerials.Items.Clear();
            lblTotalBags.Text = "0";
            txtDate.Text = string.Empty;

            grvRegistration.DataBind();
        }

        private bool SerialExistsInList(string srNo)
        {
            bool res = false;
            foreach (ListItem serial in lstSerials.Items)
            {
                if (serial.Text == srNo)
                {
                    res = true;
                    break;
                }
            }

            return res;
        }

        private bool AppliedForLoan()
        {
            bool res = false;
            hdnSerialNos.Value = string.Empty;
            foreach (ListItem serial in lstSerials.Items)
            {
                SRVRegistration temp = new SRVRegistration();
                SRVRegistration sr = new RegistrationDAOLinq().SearchRegistrationByNo(serial.Text);
                temp.Requisitioned = "Applied";
                temp.RegistrationID = sr.RegistrationID;
                temp.RegistrationDate = sr.RegistrationDate;
                temp.AmountPerBag = sr.AmountPerBag;
                temp.BagWeight = sr.BagWeight;
                temp.SerialID = sr.SerialID;
                temp.SerialNo = sr.SerialNo;
                temp.Remarks = sr.Remarks;
                temp.BagLoan = sr.BagLoan;
                temp.CarryingLoan = sr.CarryingLoan;
                temp.PartyID = sr.PartyID;
                temp.LoanDate = null;
                temp.Bags = sr.Bags;
                temp.CaseID = sr.CaseID;
                temp.TMID = sr.TMID;
                temp.CreatedBy = sr.CreatedBy;
                temp.CreatedDate = sr.CreatedDate;
                temp.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                temp.ModifiedDate = DateTime.Now;

                res = (new RegistrationManager().SaveRegistration(temp));

                hdnSerialNos.Value = string.IsNullOrEmpty(hdnSerialNos.Value) ? sr.SerialNo : hdnSerialNos.Value.ToString() + ", " + sr.SerialNo;
            }

            return res;
        }

        private void SaveRequisition()
        {
            _loanReq = new SRVLoanRequisition();
            if (new RequisitionManager().SaveRequisition(FormToObject()))
            {
                ClearForm();
            }
        }

        private SRVLoanRequisition FormToObject()
        {
            _loanReq = new SRVLoanRequisition();

            _loanReq.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
            _loanReq.CreatedDate = System.DateTime.Now;
            _loanReq.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
            _loanReq.ModifiedDate = System.DateTime.Now;

            _loanReq.RequisitionID = 0;
            _loanReq.PartyID = PartyID;
            _loanReq.RequisitionDate = DateTime.Parse(txtDate.Text);
            _loanReq.ApprovalDate = DateTime.Parse(txtDate.Text);
            _loanReq.Bags = float.Parse(lblTotalBags.Text);
            _loanReq.ApprovedBags = 0;
            _loanReq.AmountPerBag = 0;
            _loanReq.LoanAmount = 0;
            _loanReq.Remarks = string.Empty;
            _loanReq.SerialIDs = hdnSerialNos.Value.ToString();
            _loanReq.IsApproved = false;
            _loanReq.IsDisbursed = false;

            return _loanReq;
        }
        #endregion

        #region Methods For Grid
        protected void grvRegistration_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                string[] word = e.CommandArgument.ToString().Split('@');

                string strSRNo = word[0].Trim();
                int intPartyID = Convert.ToInt32(word[1].Trim());
                int intSerialID = Convert.ToInt32(word[2].Trim());

                INVParty _party = new PartyManager().GetPartyByID(intPartyID);
                int parentID = _party.ParentID != null ? (int)_party.ParentID : 0;
                if (parentID > 0)
                {
                    _party = new PartyManager().GetPartyByID(parentID);

                    if (PartyID != parentID)
                    {
                        lstSerials.Items.Clear();
                        lblTotalBags.Text = "0";
                    }

                    PartyID = parentID;
                    lblCode.Text = _party.PartyCode;
                    lblName.Text = _party.PartyName;
                    lblGCode.Text = string.Empty;
                }
                else
                {
                    if (PartyID != intPartyID)
                    {
                        lstSerials.Items.Clear();
                        lblTotalBags.Text = "0";
                    }

                    PartyID = intPartyID;
                    lblCode.Text = _party.PartyCode;
                    lblName.Text = _party.PartyName;
                    lblGCode.Text = string.Empty;
                }

                int serial = new StockSerialNo().getSerialID(strSRNo);
                int bag = new StockSerialNo().getbagcount(strSRNo);

                if (!SerialExistsInList(strSRNo))
                {
                    lblTotalBags.Text = (int.Parse(lblTotalBags.Text) + bag).ToString();
                    lstSerials.Items.Add(new ListItem(strSRNo, intSerialID.ToString()));
                }
                else
                {
                    lblTotalBags.Text = (int.Parse(lblTotalBags.Text) - bag).ToString();
                    lstSerials.Items.Remove(new ListItem(strSRNo, intSerialID.ToString()));
                }
            }
        }

        protected void grvRegistration_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvRegistration.PageIndex = e.NewPageIndex;
            grvRegistration.DataBind();
        }

        protected void grvRegistration_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                ImageButton btnSelect = (ImageButton)e.Row.FindControl("imgSelect");
                btnSelect.CommandArgument = (DataBinder.Eval(e.Row.DataItem, "SerialNo")).ToString() + "@" +
                                            (DataBinder.Eval(e.Row.DataItem, "PartyID")).ToString() + "@" +
                                            (DataBinder.Eval(e.Row.DataItem, "RegistrationID")).ToString();
            }
        }
        #endregion

        #region SqlDataSource Control Event Handlers
        protected void dsRegistration_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@DateFrom"].Value = DateFrom;
            e.Command.Parameters["@DateTo"].Value = DateTo;
            e.Command.Parameters["@Requisitioned"].Value = Requisitioned;
        }
        #endregion

        #region Methods For Button
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;
            DateFrom = DateTime.Parse(txtFromDate.Text.ToString());
            DateTo = DateTime.Parse(txtToDate.Text.ToString());
            Requisitioned = chkApplied.Checked == true ? "Applied" : "Not Applied";

            grvRegistration.DataBind();
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvRegistration.DataBind();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            grvRegistration.DataBind();
        }

        protected void chkApplied_CheckedChanged(object sender, EventArgs e)
        {
            strSearch = txtSearch.Text;

            DateFrom = DateTime.Parse(txtFromDate.Text.ToString());
            DateTo = DateTime.Parse(txtToDate.Text.ToString());
            Requisitioned = chkApplied.Checked == true ? "Applied" : "Not Applied";

            grvRegistration.DataBind();
        }

        protected void btnApply_Click(object sender, EventArgs e)
        {
            if (!checkValidity()) return;
            if (AppliedForLoan())
            {
                SaveRequisition();
            }
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            string fromDate = txtFromDate.Text;
            string toDate = txtToDate.Text;
            Response.Redirect("~/Pages/Loan/ReportApplied.aspx?FD=" + fromDate + "&TD=" + toDate + "&PC=" + txtSearch.Text);
        }
        #endregion

        #region Methods For Delete
        private void DeleteRegistration(int id)
        {
            _Registration = new SRVRegistration();
            _Registration.RegistrationID = id;
            if (new RegistrationManager().DeleteRegistration(_Registration))
            {
                grvRegistration.DataBind();
            }
        }
        #endregion
    }
}