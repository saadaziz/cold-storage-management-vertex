﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CSMSys.Web.Pages;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.INV;
using System.Globalization;

namespace CSMSys.Web.Pages.Loan
{
    public partial class ReportApplied : System.Web.UI.Page
    {
        #region Private
        private string printer = ConfigurationManager.AppSettings["ServicePrinter"].ToString();
        private string connstring = ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString;
        private int TotalReq = 0;
        private int TotalBalance = 0;
        private INVParty _party;

        private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }

        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }

        private Int32 PartyID
        {
            get
            {
                if (ViewState["PartyID"] == null)
                    ViewState["PartyID"] = -1;
                return (Int32)ViewState["PartyID"];
            }
            set
            {
                ViewState["PartyID"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            DateFrom = string.IsNullOrEmpty(Request.QueryString["FD"]) ? DateTime.Today : Convert.ToDateTime(Request.QueryString["FD"]);
            DateTo = string.IsNullOrEmpty(Request.QueryString["TD"]) ? DateTime.Today : Convert.ToDateTime(Request.QueryString["TD"]);
            string code = string.IsNullOrEmpty(Request.QueryString["PC"]) ? string.Empty : Request.QueryString["PC"];

            lblfrom.Text = DateFrom.ToShortDateString();
            lblto.Text = DateTo.ToShortDateString();
            hdnSearch.Value = code;

            if (string.IsNullOrEmpty(code))
            {
                lblParty.Text = string.Empty;
            }
            else
            {
                _party = new PartyManager().SearchParentByCode(code);
                if (_party != null)
                {
                    lblParty.Text = string.IsNullOrEmpty(_party.PartyCodeName) ? string.Empty : _party.PartyCodeName;
                }
            }
            lblOpBalance.Text = getRequisition(DateFrom) <= 0 ? "0" : getRequisition(DateFrom).ToString();
            TotalReq = int.Parse(string.IsNullOrEmpty(lblOpBalance.Text) ? "0" : lblOpBalance.Text.ToString());

            grvRequisition.DataBind();
        }

        private int getRequisition(DateTime dateFrom)
        {
            int opBalance = 0;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstring))
                {
                    sqlConn.Open();
                    string _query = "SELECT SUM(Bags - ApprovedBags) AS TotalBags ";
                    _query = _query + "FROM SRVLoanRequisition ";
                    _query = _query + "WHERE (RequisitionDate < CONVERT(DATETIME, '" + dateFrom.ToShortDateString() + " 00:00:00', 102))";
                    SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    if (sqlReader.Read())
                    {
                        if (sqlReader.HasRows) opBalance = string.IsNullOrEmpty(sqlReader["TotalBags"].ToString()) ? 0 : int.Parse(sqlReader["TotalBags"].ToString());
                    }

                    sqlReader.Close();
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }

        private int getPreviousBalance(int partyID, DateTime dateFrom)
        {
            int opBalance = 0;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstring))
                {
                    sqlConn.Open();
                    string _query = "SELECT SUM(Bags - ApprovedBags) AS TotalBags ";
                    _query = _query + "FROM SRVLoanRequisition ";
                    _query = _query + "WHERE (RequisitionDate < CONVERT(DATETIME, '" + dateFrom.ToShortDateString() + " 00:00:00', 102)) ";
                    _query = _query + "AND PartyID = " + partyID + " AND IsApproved = 'True'";
                    SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    if (sqlReader.Read())
                    {
                        if (sqlReader.HasRows) opBalance = string.IsNullOrEmpty(sqlReader["TotalBags"].ToString()) ? 0 : int.Parse(sqlReader["TotalBags"].ToString());
                    }

                    sqlReader.Close();
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }

        #region Method for Grid
        protected void grvRequisition_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvRequisition.PageIndex = e.NewPageIndex;
            grvRequisition.DataBind();
        }

        protected void grvRequisition_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[0].Controls.Count > 0))
            {
                Label lblPartyID = (Label)e.Row.FindControl("lblPartyID");
                Label lblRequisitionDate = (Label)e.Row.FindControl("lblRequisitionDate");
                string[] word = lblRequisitionDate.Text.ToString().Split('/');

                DateTime date = DateTime.Parse((int.Parse(word[1].Trim()) + "/" + int.Parse(word[0].Trim()) + "/" + Convert.ToInt32(word[2].Trim())).ToString());

                TotalBalance = Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Balance"));

                Label lblTotalBalance = (Label)e.Row.FindControl("lblTotalBalance");
                lblTotalBalance.Text = getPreviousBalance(int.Parse(lblPartyID.Text), date).ToString();

                TotalReq += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Balance"));

                Label lblTotalRequisitioned = (Label)e.Row.FindControl("lblTotalRequisitioned");
                lblTotalRequisitioned.Text = TotalReq.ToString();
            }
            lblClBalance.Text = TotalReq.ToString();
        }

        #region Method for Merge Grid
        protected void grvRequisition_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //Check if the grid row type is header or not
            if (e.Row.RowType == DataControlRowType.Header)
            {
                #region Object creation

                //Creating a gridview object            
                GridView objGridView = (GridView)sender;

                //Creating a gridview row object
                GridViewRow objgridviewrow = new GridViewRow(1, 0, DataControlRowType.Header, DataControlRowState.Insert);

                //Creating a table cell object
                TableCell objtablecell = new TableCell();

                #endregion

                #region Merge cells

                AddMergedCells(objgridviewrow, objtablecell, 3, "পার্টির তথ্য");
                AddMergedCells(objgridviewrow, objtablecell, 6, "অনুমোদিত ঋণ");
                AddMergedCells(objgridviewrow, objtablecell, 5, "বর্তমান চাহিদা");

                //Lastly add the gridrow object to the gridview object at the 0th position
                //Because,the header row position is 0.
                objGridView.Controls[0].Controls.AddAt(0, objgridviewrow);

                #endregion
            }
        }

        /// <summary>
        /// Function : AddMergedCells
        /// Purpose: Adds merged cell in the header
        /// </summary>
        /// <param name="objgridviewrow"></param>
        /// <param name="objtablecell"></param>
        /// <param name="colspan"></param>
        /// <param name="celltext"></param>
        private static void AddMergedCells(GridViewRow objgridviewrow, TableCell objtablecell, int colspan, string celltext)
        {
            objtablecell = new TableCell();
            objtablecell.Text = celltext;
            objtablecell.ColumnSpan = colspan;
            objgridviewrow.Cells.Add(objtablecell);
        }
        #endregion

        #region SqlDataSource Control Event Handlers
        protected void dsRequisition_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@DateFrom"].Value = DateFrom;
            e.Command.Parameters["@DateTo"].Value = DateTo;
        }
        #endregion
        #endregion
    }
}