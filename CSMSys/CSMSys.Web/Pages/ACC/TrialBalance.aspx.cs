﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CSMSys.Lib.AccountingEntity;
using CSMSys.Lib.AccountingUtility;
using CSMSys.Lib.AccountingDataAccess;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;

namespace CSMSys.Web.Pages.ACC
{
    public partial class TrialBalance : System.Web.UI.Page
    {
        #region Private Properties
        private string connstring = ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString;
        SqlConnection formCon = null;
        private int slNo = 0;
        private double TotalDebit = (double)0.0;
        private double TotalCredit = (double)0.0;

        private DateTime DateAs
        {
            get
            {
                if (ViewState["DateAs"] == null)
                    ViewState["DateAs"] = -1;
                return (DateTime)ViewState["DateAs"];
            }
            set
            {
                ViewState["DateAs"] = value;
            }
        }

        DaTransaction objDaTrans = null;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateAs = DateTime.Today;
                lblDate.Text = DateAs.ToShortDateString();
                int tbType = string.IsNullOrEmpty(Request.QueryString["TBT"]) ? 0 : int.Parse(Request.QueryString["TBT"]);

                DataTable dt = GetData();
                grvSummary.DataSource = dt;
                grvSummary.DataBind();
            }
        }

        #region Method for Load
        public float getOpeningBalance(int accountID, DateTime dateFrom)
        {
            float opBalance = 0;
            try
            {
                SqlConnection sqlConn = new SqlConnection(connstring);
                sqlConn.Open();

                objDaTrans = new DaTransaction();
                opBalance = objDaTrans.getOpeningBalance(sqlConn, accountID, dateFrom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }

        private DataTable GetData()
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstring))
                {
                    sqlConn.Open();

                    SqlCommand cmd = new SqlCommand("spRptTrialBalanceSummery", sqlConn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@atDate", SqlDbType.DateTime).Value = DateTime.Today;
                    cmd.Parameters.Add("@CompanyID", SqlDbType.Int).Value = 1;
                    cmd.ExecuteNonQuery();

                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        // Fill the DataSet using default values for DataTable names, etc
                        da.Fill(dt);
                    }
                }
            }
            catch
            {
                //Obravnava napak
            }

            return dt;
        }
        #endregion

        #region Method for Dr Accounts
        protected void grvSummary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[0].Controls.Count > 0))
            {
                Label lblSlNo = (Label)e.Row.FindControl("lblSl");
                lblSlNo.Text = (slNo + 1).ToString();
                slNo = int.Parse(lblSlNo.Text);

                Label lblCreditAmt = (Label)e.Row.FindControl("lblCreditAmt");
                float creditAmt = float.Parse(((Label)e.Row.Cells[3].Controls[1]).Text);
                lblCreditAmt.Text = creditAmt < 0 ? (creditAmt * (-1)).ToString() : "0";

                Label lblDebitAmt = (Label)e.Row.FindControl("lblDebitAmt");
                float debitAmt = float.Parse(((Label)e.Row.Cells[4].Controls[1]).Text);
                lblDebitAmt.Text = debitAmt > 0 ? debitAmt.ToString() : "0";

                TotalDebit += Convert.ToDouble(lblDebitAmt.Text);
                TotalCredit += Convert.ToDouble(lblCreditAmt.Text);
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblDebitTotal = (Label)e.Row.FindControl("lblDebitTotal");
                lblDebitTotal.Text = String.Format("{0:N}", TotalDebit);

                Label lblCreditTotal = (Label)e.Row.FindControl("lblCreditTotal");
                lblCreditTotal.Text = String.Format("{0:N}", TotalCredit);
            }
        }
        #endregion
    }
}