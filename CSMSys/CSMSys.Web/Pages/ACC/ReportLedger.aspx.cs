﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CSMSys.Lib.AccountingEntity;
using CSMSys.Lib.AccountingUtility;
using CSMSys.Lib.AccountingDataAccess;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;

namespace CSMSys.Web.Pages.ACC
{
    public partial class ReportLedger : System.Web.UI.Page
    {
        #region Private Properties
        private string connstring = ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString;
        private decimal TotalDebit = (decimal)0.0;
        private decimal TotalCredit = (decimal)0.0;
        private decimal TotalBalance = (decimal)0.0;

        SqlConnection formCon = null;

        private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }

        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }

        private int AccountID
        {
            get
            {
                if (ViewState["AccountID"] == null)
                    ViewState["AccountID"] = -1;
                return (int)ViewState["AccountID"];
            }
            set
            {
                ViewState["AccountID"] = value;
            }
        }

        DaTransaction objDaTrans = null;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateFrom = string.IsNullOrEmpty(Request.QueryString["FDate"]) ? DateTime.Today : DateTime.Parse(Request.QueryString["FDate"]);
                DateTo = string.IsNullOrEmpty(Request.QueryString["TDate"]) ? DateTime.Today : DateTime.Parse(Request.QueryString["TDate"]);
                AccountID = string.IsNullOrEmpty(Request.QueryString["AccID"]) ? 0 : int.Parse(Request.QueryString["AccID"]);
                lblDateFrom.Text = DateFrom.ToShortDateString();
                lblDateTo.Text = DateTo.ToShortDateString();

                getAccountInfo(AccountID);
                grvLedger.DataBind();
            }
        }

        #region Method for Load
        private void getAccountInfo(int accountID)
        {
            using (SqlConnection sqlConn = new SqlConnection(connstring))
            {
                sqlConn.Open();
                string _query = "SELECT [AccountNo], [AccountTitle], [Nature], [OpeningBalance] FROM [T_Account] WHERE [AccountID] = " + accountID;
                SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                if (sqlReader.Read())
                {
                    if (sqlReader.HasRows)
                    {
                        lblAccountTitle.Text = sqlReader["AccountTitle"].ToString();
                        lblAccountNo.Text = sqlReader["AccountNo"].ToString();
                        int nature = int.Parse(sqlReader["Nature"].ToString());
                        //double opBalance = double.Parse(sqlReader["OpeningBalance"].ToString());
                        float opBalance = getOpeningBalance(accountID, DateFrom);
                        //opBalance = opBalance * (float)nature;
                        lblOpBalance.Text = opBalance.ToString();

                        TotalBalance = decimal.Parse(string.IsNullOrEmpty(lblOpBalance.Text) ? "0" : lblOpBalance.Text.ToString());
                        if (nature <= 0)
                        {
                            lblType.Text = "Cr";
                        }
                        else
                        {
                            lblType.Text = "Dr";
                        }
                    }
                }

                sqlReader.Close();
                sqlConn.Close();
            }
        }

        public float getOpeningBalance(int accountID, DateTime dateFrom)
        {
            float opBalance = 0;
            try
            {
                SqlConnection sqlConn = new SqlConnection(connstring);
                sqlConn.Open();

                objDaTrans = new DaTransaction();
                opBalance = objDaTrans.getOpeningBalance(sqlConn, accountID, dateFrom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }
        #endregion

        #region Method for Dr Accounts
        protected void grvLedger_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[6].Controls.Count > 0))
            {
                TotalDebit += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "DebitAmt"));
                TotalCredit += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CreditAmt"));
                TotalBalance = TotalBalance + Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "LineTotal"));

                Label lblTotal = (Label)e.Row.FindControl("lblLineTotal");
                lblTotal.Text = String.Format("{0:N}", TotalBalance);
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblDebitTotal = (Label)e.Row.FindControl("lblDebitTotal");
                //lblDebitTotal.Text = String.Format("{0:N}", TotalDebit);

                Label lblCreditTotal = (Label)e.Row.FindControl("lblCreditTotal");
                //lblCreditTotal.Text = String.Format("{0:N}", TotalCredit);

                Label lblTotal = (Label)e.Row.FindControl("lblTotal");
                lblTotal.Text = String.Format("{0:N}", TotalBalance);

                //Label lblDebit = (Label)e.Row.FindControl("lblDebit");
                //lblDebit.Text = "কথায় : টাকা " + n2b.changeCurrencyToWords(TotalDebit.ToString()).ToString();
            }

            lblClBalance.Text = String.Format("{0:N}", TotalBalance);
        }
        #endregion

        #region SqlDataSource Control Event Handlers
        protected void dsLedger_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@AccountID"].Value = AccountID;
            e.Command.Parameters["@DateFrom"].Value = DateFrom;
            e.Command.Parameters["@DateTo"].Value = DateTo;
        }
        #endregion
    }
}