﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.INV;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Globalization;

namespace CSMSys.Web.Pages.PartyLedger
{
    public partial class PartyLedgerView : System.Web.UI.Page
    {

        private string strSearch = string.Empty;

        private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }

        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //DateFrom = DateTime.Today;
                //txtFromDate.Text = DateFrom.ToShortDateString();
                //DateTo = DateTime.Today;
                //txtToDate.Text = DateTo.ToShortDateString();
                grvPartyLedger.DataBind();
            }
        }
        
        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }
        #endregion
        #region Methods For Grid

        protected void grvPartyLedger_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvPartyLedger.PageIndex = e.NewPageIndex;
            grvPartyLedger.DataBind();
        }

        protected void grvPartyLedger_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {

                //if (!checkValidity()) return;
                string fromDate = txtFromDate.Text;
                string toDate = txtToDate.Text;
                //        Response.Redirect("~/Pages/PartyLedger/PartyWiseLedger.aspx?PID=" + partyID + "FD=" + fromDate + "&TD=" + toDate); ;

                HyperLink hplLedger = (HyperLink)e.Row.FindControl("hplLedger");
                hplLedger.NavigateUrl = "~/Pages/Reports/PartyWiseLedger.aspx?PID=" + (DataBinder.Eval(e.Row.DataItem, "PartyID")).ToString() + "&FD=" + txtFromDate.Text + "&TD=" + txtToDate.Text;

            }
        }
        #endregion
        #region Methods For Button
        protected void imgRefresh_Click(object sender, EventArgs e)
        {
            grvPartyLedger.DataBind();
        }
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text.Trim();
            if (rdbDate.Checked)
            {
                if (!checkValidity()) return;
                //grvPartyLedger.DataSource = Search();
                lblFailure.Text = "";
                dsPartyLedger.FilterParameters.Clear();
                dsPartyLedger.FilterExpression = "CreatedDate>='{0}' AND CreatedDate<='{1}'";
                // TextBox1.Text.ToString("MM/dd/yyyy");
                //DateTime txtFromDate =Convert.ToDateTime(txtFromDate.ToString("MM/dd/yyyy"));
                //dsPartyLedger.FilterParameters.Add("CreatedDate", txtFromDate.Text.ToString("MM/dd/yyyy"));
                dsPartyLedger.FilterParameters.Add("CreatedDate", Convert.ToDateTime(txtFromDate.Text).ToString("dd/MM/yyyy"));
                dsPartyLedger.FilterParameters.Add("CreatedDate", Convert.ToDateTime(txtToDate.Text).ToString("dd/MM/yyyy"));
                grvPartyLedger.DataBind();

            }
        }
                
        #endregion

        protected void btnReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (!checkValidity()) return;
                lblFailure.Text = "";
                string fromDate = Convert.ToDateTime(txtFromDate.Text).ToString("MM/dd/yyyy");
                string toDate = Convert.ToDateTime(txtToDate.Text).ToString("MM/dd/yyyy");
                Response.Redirect("~/Pages/Reports/PartyLedgerReport.aspx?FD=" + fromDate + "&TD=" + toDate);

            }
            catch (Exception ex)
            {
            }
        }
        private bool checkValidity()
        {
            DateTime dt = new DateTime();
            dt.ToString("yyyy/MM/dd");
            //String.Format("{0:yyyy/MM/dd}", dt);
            if (!DateTime.TryParse(txtFromDate.Text, out dt))
            {
                lblFailure.Text = "Select From Date";
                txtFromDate.Focus();
                return false;
            }

            if (!DateTime.TryParse(txtToDate.Text, out dt))
            {
                lblFailure.Text = "Select To Date";
                txtToDate.Focus();
                return false;

            }
            if (txtFromDate.Text == "" || txtToDate.Text == "")
            {
                lblFailure.Text = "Select Date";
                txtFromDate.Focus();
                return false;
            }
            return true;
        }
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            grvPartyLedger.DataBind();
        }

       protected void grvPartyLedger_SelectedIndexChanged(object sender, EventArgs e)
        {
            //int partyID = Convert.ToInt32(grvPartyLedger.SelectedRow.Cells[1].Text);
            string partyID = grvPartyLedger.SelectedRow.Cells[1].Text;
            //GridViewRow selectedRow = ((GridView)e.CommandSource).Rows[1];

                if (!checkValidity()) return;
                string fromDate = Convert.ToDateTime(txtFromDate.Text).ToString("MM/dd/yyyy");
                string toDate = Convert.ToDateTime(txtToDate.Text).ToString("MM/dd/yyyy");
                Response.Redirect("~/Pages/Reports/PartyWiseLedger.aspx?PID=" + partyID + "FD=" + fromDate + "&TD=" + toDate); ;

        }

       protected void rdbDate_CheckedChanged(object sender, EventArgs e)
       {
           if (rdbDate.Checked == true)
           {
               txtFromDate.Enabled = true;
               txtToDate.Enabled = true;
           }
           else
           {
               txtFromDate.Enabled = false;
               txtToDate.Enabled = false;
           }
       }

    }
}