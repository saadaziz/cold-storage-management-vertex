﻿<%@ Page Title="Trial Balance" Language="C#" AutoEventWireup="true" CodeBehind="TrialBalance.aspx.cs" Inherits="CSMSys.Web.Pages.ACC.TrialBalance" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="feature-box-full">
        <table width="100%" border="0" cellpadding="0" cellspacing="4">
            <tbody>
            <tr>
                <td align="center" valign="top" style="font-size:10px;">
                    বিসমিল্লাহির রাহমানির রাহিম
                </td>
            </tr>
            <tr>
                <td align="center" valign="top" style="font-size:28px;">
                    শাহ্‌ ইসমাঈল গাজী (রহঃ) কোল্ড ষ্টোরেজ লিঃ
                </td>
            </tr>
            <tr>
                <td align="center" valign="top" style="font-size:10px;">
                    সায়েকপুর, খেজমতপুর, পীরগঞ্জ, রংপুর। মোবাঃ ০১৭১৫৫৯৭৫৯১,০১৭১৯০৮৬২১, ০১৭৩২১১২১৯৬, হেড অফিসঃ ধানমণ্ডি, ঢাকা
                </td>
            </tr>
            <tr>
                <td align="center" style="font-size:28px;">
                    <asp:Label ID="lblHeader" runat="server" Text="Trial Balance"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center" valign="top">
                    As of <asp:Label ID="lblDate" runat="server"></asp:Label>&nbsp;
                </td>
            </tr>
            <tr>
                <td align="left" valign="top">
                    <table width="100%" border="0" cellpadding="0" cellspacing="4">
                        <tbody>
                        <tr>
                            <td align="left" colspan="3">
                                <asp:GridView ID="grvSummary" DataKeyNames="AccountID" runat="server" Width="100%" AutoGenerateColumns="False" GridLines="Both"
                                    CellPadding="4" HorizontalAlign="Left" ShowHeader="true" ShowFooter="true" OnRowDataBound="grvSummary_RowDataBound" 
                                    EmptyDataText="No Records Found">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Sl #" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSl" Text="1" runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="AccountID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAccountID" Text='<%# Eval("AccountID") %>' runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Account Title" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAccountTitle" Text='<%# Eval("AccountTitle") %>' runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblTotal" Text="Grand Total" runat="server" Font-Bold="true" HorizontalAlign="Left" />
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Cr Amount" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreditAmt" Text='<%# Eval("OpeningBalance","{0:N}") %>' runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblCreditTotal" Text="0" runat="server" Font-Bold="true" HorizontalAlign="Left" />
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Dr Amount" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDebitAmt" Text='<%# Eval("OpeningBalance","{0:N}") %>' runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Label ID="lblDebitTotal" Text="0" runat="server" Font-Bold="true" HorizontalAlign="Left" />
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EmptyDataRowStyle ForeColor="#CC0000" />
                                    <FooterStyle Font-Bold="True" ForeColor="#333333" />
                                </asp:GridView>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>
