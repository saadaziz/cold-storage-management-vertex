﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.AccountingEntity;
using CSMSys.Lib.AccountingUtility;
using CSMSys.Lib.AccountingDataAccess;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;

namespace CSMSys.Web.Pages.ACC
{
    public partial class LedgerBook : System.Web.UI.Page
    {
        #region Private Properties
        private string connstring = ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString;
        private decimal TotalDebit = (decimal)0.0;
        private decimal TotalCredit = (decimal)0.0;
        private decimal TotalBalance = (decimal)0.0;

        SqlConnection formCon = null;
        String strSearch = string.Empty;

        private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }

        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }

        private int AccountID
        {
            get
            {
                if (ViewState["AccountID"] == null)
                    ViewState["AccountID"] = -1;
                return (int)ViewState["AccountID"];
            }
            set
            {
                ViewState["AccountID"] = value;
            }
        }

        DaTransaction objDaTrans = null;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateFrom = DateTime.Today;
                txtDateFrom.Text = DateFrom.ToShortDateString();
                DateTo = DateTime.Today;
                txtDateTo.Text = DateTo.ToShortDateString();
            }
        }

        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }
        #endregion

        #region Methods For Ledger Grid
        protected void grvLedger_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                AccountID = Convert.ToInt32(e.CommandArgument.ToString());

                if (AccountID > 0)
                {
                    getAccountInfo(AccountID);
                }
            }
        }

        private void getAccountInfo(int accountID)
        {
            using (SqlConnection sqlConn = new SqlConnection(connstring))
            {
                sqlConn.Open();
                string _query = "SELECT [AccountNo], [AccountTitle], [Nature], [OpeningBalance] FROM [T_Account] WHERE [AccountID] = " + accountID;
                SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                if (sqlReader.Read())
                {
                    if (sqlReader.HasRows)
                    {
                        lblAccountTitle.Text = sqlReader["AccountTitle"].ToString();
                        lblAccountNo.Text = sqlReader["AccountNo"].ToString();
                        int nature = int.Parse(sqlReader["Nature"].ToString());
                        //double opBalance = double.Parse(sqlReader["OpeningBalance"].ToString());
                        float opBalance = getOpeningBalance(accountID, DateFrom);
                        //opBalance = opBalance * (float)nature;
                        lblOpBalance.Text = opBalance.ToString();
                        if (nature <= 0)
                        {
                            lblType.Text = "Cr";
                        }
                        else
                        {
                            lblType.Text = "Dr";
                        }
                    }
                }

                sqlReader.Close();
                sqlConn.Close();
            }
        }

        public float getOpeningBalance(int accountID, DateTime dateFrom)
        {
            float opBalance = 0;
            try
            {
                SqlConnection sqlConn = new SqlConnection(connstring);
                sqlConn.Open();

                objDaTrans = new DaTransaction();
                opBalance = objDaTrans.getOpeningBalance(sqlConn, accountID, dateFrom);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }

        private void getClosingBalance(int accountID)
        {
            double opBalance = double.Parse(lblOpBalance.Text.ToString());
            double clBalance = 0;
            using (SqlConnection sqlConn = new SqlConnection(connstring))
            {
                sqlConn.Open();
                string _query = "SELECT td.AccountID, SUM(td.DebitAmt - td.CreditAmt) AS ClosingBalance FROM T_Transaction_Detail AS td INNER JOIN T_Transaction_Master AS tm ON td.TransMID = tm.TransMID WHERE (tm.TransDate >= CONVERT(DATETIME, '" + DateFrom.ToShortDateString() + " 00:00:00', 102)) AND (tm.TransDate <= CONVERT(DATETIME, '" + DateTo.ToShortDateString() + " 00:00:00', 102)) GROUP BY td.AccountID HAVING ([AccountID] = " + accountID + ")";
                SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                if (sqlReader.Read())
                {
                    if (sqlReader.HasRows)
                    {
                        clBalance = double.Parse(sqlReader["ClosingBalance"].ToString());
                        lblClBalance.Text = (opBalance + clBalance).ToString();
                    }
                }
                else
                    lblClBalance.Text = opBalance.ToString();

                sqlReader.Close();
                sqlConn.Close();
            }
        }

        protected void grvLedger_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvLedger.PageIndex = e.NewPageIndex;
            grvLedger.DataBind();
        }

        protected void grvLedger_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                ImageButton btnSelect = (ImageButton)e.Row.FindControl("imgSelect");
                btnSelect.CommandArgument = ((Label)e.Row.Cells[1].Controls[1]).Text;
            }
        }
        #endregion

        #region Method for Dr Accounts
        protected void grvRegister_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[6].Controls.Count > 0))
            {
                TotalDebit += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "DebitAmt"));
                TotalCredit += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "CreditAmt"));
                TotalBalance = TotalBalance + Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "LineTotal"));

                Label lblTotal = (Label)e.Row.FindControl("lblLineTotal");
                lblTotal.Text = String.Format("{0:N}", TotalBalance);
            } 
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                Label lblDebitTotal = (Label)e.Row.FindControl("lblDebitTotal");
                //lblDebitTotal.Text = String.Format("{0:N}", TotalDebit);

                Label lblCreditTotal = (Label)e.Row.FindControl("lblCreditTotal");
                //lblCreditTotal.Text = String.Format("{0:N}", TotalCredit);

                Label lblTotal = (Label)e.Row.FindControl("lblTotal");
                lblTotal.Text = String.Format("{0:N}", TotalBalance);

                //Label lblDebit = (Label)e.Row.FindControl("lblDebit");
                //lblDebit.Text = "কথায় : টাকা " + n2b.changeCurrencyToWords(TotalDebit.ToString()).ToString();
            }

            lblClBalance.Text = String.Format("{0:N}", TotalBalance);
        }

        //protected void grvRegister_PageIndexChanging(object sender, GridViewPageEventArgs e)
        //{
        //    grvRegister.PageIndex = e.NewPageIndex;
        //    grvRegister.DataBind();
        //}
        #endregion

        #region SqlDataSource Control Event Handlers
        protected void dsRegister_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@AccountID"].Value = AccountID;
            e.Command.Parameters["@DateFrom"].Value = DateFrom;
            e.Command.Parameters["@DateTo"].Value = DateTo;
        }
        #endregion

        #region Methods For Button
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
            dateInfo.ShortDatePattern = "MM/dd/yyyy";

            DateFrom = Convert.ToDateTime(txtDateFrom.Text.ToString(), dateInfo);
            DateTo = Convert.ToDateTime(txtDateTo.Text.ToString(), dateInfo);

            float opBalance = getOpeningBalance(AccountID, DateFrom);
            lblOpBalance.Text = opBalance.ToString();

            TotalBalance = decimal.Parse(string.IsNullOrEmpty(lblOpBalance.Text) ? "0" : lblOpBalance.Text.ToString());
            grvRegister.DataBind();
        }

        protected void imgSearchLedger_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text.Trim();
            grvLedger.DataBind();
        }

        protected void btnReport_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Pages/ACC/ReportLedger.aspx?FDate=" + DateFrom + "&TDate=" + DateTo + "&Acc=" + lblAccountTitle.Text.Trim() + "&AccID=" + AccountID);
        }
        #endregion
    }
}