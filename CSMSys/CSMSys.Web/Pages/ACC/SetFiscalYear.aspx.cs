﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.AccountingDataAccess;
using CSMSys.Lib.AccountingEntity;
using CSMSys.Lib.AccountingUtility;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;

namespace CSMSys.Web.Pages.ACC
{
    public partial class SetFiscalYear : System.Web.UI.Page
    {
        private string strSearch = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                grvFiscal.DataBind();
            }
        }


        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }
        #endregion

        #region Methods For Grid
        protected void grvFiscal_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Delete"))
            {
                int intFiscalID = Convert.ToInt32(e.CommandArgument.ToString());

                if (intFiscalID > 0)
                {
                    //DeleteFiscal(intFiscalID);
                }
            }
        }

        protected void grvFiscal_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvFiscal.PageIndex = e.NewPageIndex;
            grvFiscal.DataBind();
        }

        protected void grvFiscal_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                ImageButton btnEdit = (ImageButton)e.Row.FindControl("imgEdit");
                btnEdit.Attributes.Add("onClick", "javascript:ShowEditModal('" + ((Label)e.Row.Cells[1].Controls[1]).Text + "');");

                ImageButton btnDelete = (ImageButton)e.Row.FindControl("imgDelete");
                btnDelete.CommandArgument = ((Label)e.Row.Cells[1].Controls[1]).Text;
            }
        }
        #endregion

        #region Methods For Button
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvFiscal.DataBind();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            grvFiscal.DataBind();
        }
        #endregion

        #region Methods For Delete
        private void DeleteFiscal(int id)
        {
            if (id > 0)
            {
                //SqlConnection sqlConn = new SqlConnection(connstring);
                //sqlConn.Open();
                //_objDaFY.DeleteFiscalYear(sqlConn, id);

                //grvFiscal.DataBind();
            }
        }
        #endregion
    }
}