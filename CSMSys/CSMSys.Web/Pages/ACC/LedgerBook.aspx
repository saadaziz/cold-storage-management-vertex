﻿<%@ Page Title="Ledger Book" Language="C#" MasterPageFile="~/Default.Master" AutoEventWireup="true" 
CodeBehind="LedgerBook.aspx.cs" Inherits="CSMSys.Web.Pages.ACC.LedgerBook" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../../App_Themes/TableSorter/Blue/style.css" rel="stylesheet" type="text/css" />
    <link href="../../../App_Themes/TableSorter/Green/style.css" rel="stylesheet" type="text/css" />
    <script src="../../App_Themes/GridView/HierarchicalGrid.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="content" style="height:590px;">

    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <div class="title">
  
        <table width="100%" border="0" cellpadding="2" cellspacing="4">
			<tbody>
			<tr>
                <td align="left" style="width:27%;">
                    <h2>
                        Ledger Book
                    </h2>
                </td>
                <td align="right" valign="bottom" style="width:70%;">
                    Date From : <asp:TextBox ID="txtDateFrom" runat="server" ></asp:TextBox>
                    &nbsp;&nbsp;
                    <cc1:CalendarExtender ID="txtDateFrom_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtDateFrom" PopupPosition="BottomLeft">
                    </cc1:CalendarExtender>
                    <asp:RegularExpressionValidator ID="reValidator11" runat="server" 
                        ControlToValidate="txtDateFrom" ErrorMessage="*" 
                        ValidationExpression="^[0-9d]{1,2}/[0-9m]{1,2}/[0-9y]{4}$">
                    </asp:RegularExpressionValidator>
                    &nbsp;&nbsp;
                    to : <asp:TextBox ID="txtDateTo" runat="server" ></asp:TextBox>
                    &nbsp;&nbsp;
                    <cc1:CalendarExtender ID="txtDateTo_CalendarExtender" runat="server" 
                        Enabled="True" TargetControlID="txtDateTo" PopupPosition="BottomLeft">
                    </cc1:CalendarExtender>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                        ControlToValidate="txtDateTo" ErrorMessage="*" 
                        ValidationExpression="^[0-9d]{1,2}/[0-9m]{1,2}/[0-9y]{4}$">
                    </asp:RegularExpressionValidator>
                </td>
                <td align="center" valign="bottom" style="width:3%;">
                    <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" ImageUrl="~/App_Themes/Default/Images/gridview/Search.png" ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch_Click" />
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="feature-box-full">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
			        <tbody>
                    <tr>
                        <td align="left">
                            Search Ledger : <asp:TextBox ID="txtSearch" runat="server" Width="126px" ></asp:TextBox>
                            &nbsp;&nbsp;
                            <asp:ImageButton ID="imgSearchLedger" runat="server" CommandName="Search" ImageUrl="~/App_Themes/Default/Images/gridview/Search.png" ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearchLedger_Click" />
                        </td>
                        <td align="left">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
			                    <tbody>
                                <tr>
                                    <td align="left">Account Title : 
                                        <asp:Label runat="server" ID="lblAccountTitle"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Account No : 
                                        <asp:Label runat="server" ID="lblAccountNo"></asp:Label>
                                        &nbsp;&nbsp;
                                        Account Type : 
                                        <asp:Label runat="server" ID="lblType"></asp:Label>
                                        &nbsp;&nbsp;
                                        Opening Balance : 
                                        <asp:Label runat="server" ID="lblOpBalance"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
			                    </tbody>
		                    </table>
                        </td>
                    </tr>
			        <tr>
				        <td align="left" valign="top" style="width:27%; overflow:auto; height:430px;">
                            <asp:GridView ID="grvLedger" DataKeyNames="AccountID" runat="server" Width="100%" AutoGenerateColumns="False"
                                CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvLedger_PageIndexChanging" ShowHeaderWhenEmpty="true" 
                                OnRowDataBound="grvLedger_RowDataBound" OnRowCommand="grvLedger_RowCommand"
                                EmptyDataText="No Records Found" CssClass="tablesorterBlue" AllowPaging="True" PageSize="10" DataSourceID="dsLedger">
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="2%"  ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgSelect" runat="server" CommandName="Select" ImageUrl="~/App_Themes/Default/Images/gridview/Select.png" ToolTip="Edit" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="AccountID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccountID" Text='<%# Eval("AccountID") %>' runat="server" HorizontalAlign="Left" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="No" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccountNo" Text='<%# Eval("AccountNo") %>' runat="server" HorizontalAlign="Left" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Title" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblAccountTitle" Text='<%# HighlightText(Eval("AccountTitle").ToString()) %>' runat="server" HorizontalAlign="Left" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Balance" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="lblCurrentBalance" Text='<%# Eval("CurrentBalance","{0:N}") %>' runat="server" HorizontalAlign="Left" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                                <AlternatingRowStyle BackColor="#E5EAE8" />
                                <EditRowStyle BackColor="#999999" />
                                <EmptyDataRowStyle ForeColor="#CC0000" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            </asp:GridView>
                            <asp:SqlDataSource ID="dsLedger" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>" 
                                SelectCommand="SELECT AccountID, AccountNo, AccountTitle, CurrentBalance
                                    FROM T_Account
                                    WHERE (AccOrGroup ='Account')" FilterExpression="AccountTitle LIKE '%{0}%'">
                                <FilterParameters>
                                    <asp:ControlParameter Name="AccountTitle" ControlID="txtSearch" PropertyName="Text" />
                                </FilterParameters>
                            </asp:SqlDataSource>
                        </td>
				        <td align="left" valign="top">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
			                    <tbody>
                                <tr>
                                    <td align="left">
                                        <asp:GridView ID="grvRegister" DataKeyNames="TransMID" runat="server" Width="100%" AutoGenerateColumns="False"
                                            CellPadding="4" HorizontalAlign="Left" ShowHeaderWhenEmpty="true" 
                                            OnRowDataBound="grvRegister_RowDataBound"
                                            EmptyDataText="No Records Found" CssClass="tablesorterBlue" DataSourceID="dsRegister">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sl #" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblSl" Text='<%# Eval("SlNo") %>' runat="server" HorizontalAlign="Left" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="TransDID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTransDID" Text='<%# Eval("TransDID") %>' runat="server" HorizontalAlign="Left" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="TransMID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTransMID" Text='<%# Eval("TransMID") %>' runat="server" HorizontalAlign="Left" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="AccountID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblAccountID" Text='<%# Eval("AccountID") %>' runat="server" HorizontalAlign="Left" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblTransDate" Text='<%# Eval("TransDate","{0:dd/MM/yyyy}") %>' runat="server" HorizontalAlign="Left" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDescription" Text='<%# Eval("Comments") %>' runat="server" HorizontalAlign="Left" />
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblDebit" Text="Total Amount : " runat="server" HorizontalAlign="Right" />
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Cr Amount" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCreditAmt" Text='<%# Eval("CreditAmt","{0:N}") %>' runat="server" HorizontalAlign="Left" />
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblCreditTotal" Text="0" runat="server" HorizontalAlign="Right" />
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Dr Amount" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDebitAmt" Text='<%# Eval("DebitAmt","{0:N}") %>' runat="server" HorizontalAlign="Left" />
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblDebitTotal" Text="0" runat="server" HorizontalAlign="Right" />
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Balance" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="15%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLineTotal" Text='<%# Eval("LineTotal","{0:N}") %>' runat="server" HorizontalAlign="Left" />
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                        <asp:Label ID="lblTotal" Text="0" runat="server" HorizontalAlign="Right" />
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                                            <AlternatingRowStyle BackColor="#E5EAE8" />
                                            <EditRowStyle BackColor="#999999" />
                                            <EmptyDataRowStyle ForeColor="#CC0000" />
                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                        </asp:GridView>
                                        <asp:SqlDataSource ID="dsRegister" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>" OnSelecting="dsRegister_Selecting" 
                                            SelectCommand="SELECT ROW_NUMBER() OVER (ORDER BY tm.TransMID) As SlNo, tm.TransMID, tm.TransDate, tm.VoucherNo, tm.VoucherType AS VoucherTypeID, vt.VoucherType, tm.TransMethodID, mt.TransMethod, tm.MethodRefID, 
                                                tm.MethodRefNo, tm.TransDescription, td.TransDID, td.AccountID, (ta.AccountNo + '-' + ta.AccountTitle) AS Description, ta.Nature, td.CreditAmt, td.DebitAmt, td.Comments, 
                                                ta.Nature * (td.DebitAmt - td.CreditAmt) AS LineTotal
                                                FROM T_Transaction_Master AS tm INNER JOIN
                                                T_Transaction_Detail AS td ON tm.TransMID = td.TransMID INNER JOIN
                                                T_Account AS ta ON td.AccountID = ta.AccountID LEFT OUTER JOIN
                                                T_VoucherType AS vt ON tm.VoucherType = vt.VoucherTypeID LEFT OUTER JOIN
                                                T_TransactionMethod AS mt ON tm.TransMethodID = mt.TransMethodID 
                                                WHERE ((tm.TransDate >= @DateFrom) AND (tm.TransDate <= @DateTo) AND (td.AccountID = @AccountID))">
                                            <SelectParameters>
                                                <asp:Parameter Name="AccountID" Type="Int32" />
                                                <asp:Parameter Name="DateFrom" Type="DateTime" />
                                                <asp:Parameter Name="DateTo" Type="DateTime" />
                                            </SelectParameters>
                                        </asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right">Closing Balance : 
                                        <asp:Label runat="server" ID="lblClBalance"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">
                                         <div class="feature-box-actionBar">
                                            <span class="failureNotification">
                                                <asp:Literal ID="lblFailure" runat="server"></asp:Literal>
                                            </span>
                                            <asp:Button ID="btnReport" runat="server" Text="Generate Report" CssClass="button" Width="120px"
                                                onclick="btnReport_Click" />
                                        </div>
                                    </td>
                                </tr>
			                    </tbody>
		                    </table>
                        </td>
			        </tr>
			        </tbody>
		        </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
</asp:Content>
