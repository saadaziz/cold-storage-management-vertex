﻿<%@ Page Language="C#" Title="CSMSys :: Party Ledger" AutoEventWireup="true" MasterPageFile="~/Default.Master"
    CodeBehind="PartyLedgerView.aspx.cs" Inherits="CSMSys.Web.Pages.PartyLedger.PartyLedgerView" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../../App_Themes/TableSorter/Blue/style.css" rel="stylesheet" type="text/css" />
    <link href="../../../App_Themes/TableSorter/Green/style.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
       
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="title">
        <table width="100%" border="0" cellpadding="2" cellspacing="4">
            <tbody>
                <tr>
                    <td align="left" style="width: 17%;">
                        <h2>
                            Party Ledger Register</h2>
                    </td>
                    <td align="right" valign="bottom" style="width: 74%;">
                        Search by Name/Code/Village/Contact No :
                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                        and
                        <asp:RadioButton ID="rdbDate" runat="server" Text=" and Date :" GroupName="Search"
                            AutoPostBack="True" OnCheckedChanged="rdbDate_CheckedChanged" />
                        Date From
                        <asp:TextBox ID="txtFromDate" runat="server" Width="87px"></asp:TextBox>
                        <cc1:CalendarExtender ID="ceFromDate" runat="server" TargetControlID="txtFromDate" />
                        To
                        <asp:TextBox ID="txtToDate" runat="server" Width="87px"></asp:TextBox>
                        <cc1:CalendarExtender ID="ceToDate" runat="server" TargetControlID="txtToDate" />
                    </td>
                    <td align="center" valign="bottom" style="width: 3%;">
                        <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" ImageUrl="~/App_Themes/Default/Images/gridview/Search.png"
                            ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch_Click" />
                    </td>
                    <td align="center" valign="bottom" style="width: 3%;">
                        <asp:ImageButton ID="imgRefresh" runat="server" CommandName="Refresh" ImageUrl="~/App_Themes/Default/Images/gridview/Refresh.png"
                            ToolTip="Refresh" Width="16px" Height="16px" OnClick="imgRefresh_Click" />
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="feature-box-full">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td align="left">
                                <asp:GridView ID="grvPartyLedger" DataKeyNames="PartyID" runat="server" Width="100%"
                                    AutoGenerateColumns="False" CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvPartyLedger_PageIndexChanging"
                                    ShowHeaderWhenEmpty="True" OnRowDataBound="grvPartyLedger_RowDataBound" 
                                    EmptyDataText="No Records Found" CssClass="tablesorterBlue" AllowPaging="True"
                                    PageSize="14" DataSourceID="dsPartyLedger" OnSelectedIndexChanged="grvPartyLedger_SelectedIndexChanged">
                                    <Columns>
                                        <asp:BoundField DataField="CreatedDate" HeaderText="নিবন্ধন তারিখ" SortExpression="CreatedDate"
                                            DataFormatString=" {0:dd/MM/yyyy}" />
                                       
                                        <asp:TemplateField HeaderText="পার্টি কোড " HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="7%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPartyCode" Text='<%# HighlightText(Eval("PartyCode").ToString()) %>'
                                                    runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    
                                        <asp:TemplateField HeaderText="পার্টির নাম " HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPartyName" Text='<%# HighlightText(Eval("PartyName").ToString()) %>'
                                                    runat="server" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Contact No" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="15%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblContactNo" Text='<%# HighlightText(Eval("ContactNo").ToString()) %>'
                                                    runat="server" HorizontalAlign="Left" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                 <asp:TemplateField HeaderText="গ্রাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblvill" Text='<%# HighlightText(Eval("AreaVillageName").ToString()) %>'
                                                    runat="server" HorizontalAlign="Left" Font-Size="12px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       
                                        <asp:TemplateField HeaderText="লোড বস্তা " HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBagLoaded" Text='<%# Eval("BagLoaded") %>' runat="server" HorizontalAlign="Left"
                                                    Font-Size="12px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="খালি বস্তার সংখ্যা" HeaderStyle-HorizontalAlign="Left"
                                            ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBagLoan" Text='<%# Eval("TotalBagLoan") %>' runat="server" HorizontalAlign="Left"
                                                    Font-Size="12px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                              
                                        <asp:TemplateField HeaderText="বস্তা লোন" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblBagLoanAmount" Text='<%# Eval("BagLoanAmount") %>' runat="server"
                                                    HorizontalAlign="Left" Font-Size="12px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="ক্যারিং লোন" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTotalCarry" Text='<%# Eval("TotalCarry") %>' runat="server" HorizontalAlign="Left"
                                                    Font-Size="12px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       
                                        <asp:TemplateField HeaderText="লোন " HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLoan" Text='<%# Eval("Loan") %>' runat="server" HorizontalAlign="Left"
                                                    Font-Size="12px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:TemplateField HeaderText="মোট লোন" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTotalLoan" Text='<%# Eval("TotalLoan") %>' runat="server" HorizontalAlign="Left"
                                                    Font-Size="12px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="মোট উত্তোলন" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTotalAmount" Text='<%# Eval("TotalAmount") %>' runat="server" HorizontalAlign="Left"
                                                    Font-Size="12px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="প্রদেয়" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaidAmount" Text='<%# Eval("PaidAmount") %>' runat="server" HorizontalAlign="Left"
                                                    Font-Size="12px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                                                                <asp:TemplateField HeaderText="বকেয়া" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="10%">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDuesAmount" Text='<%# Eval("DuesAmount") %>' runat="server" HorizontalAlign="Left"
                                                    Font-Size="12px" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ledger" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                            ItemStyle-Width="2%">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="hplLedger" runat="server"  Target="_blank" ImageUrl="~/App_Themes/Default/Images/printButton.png" ToolTip="Print"></asp:HyperLink>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>                                                                                                                                                                                                                              
                                    <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                                    <AlternatingRowStyle BackColor="#E5EAE8" />
                                    <EditRowStyle BackColor="#999999" />
                                    <EmptyDataRowStyle ForeColor="#CC0000" />
                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                </asp:GridView>
                                <asp:SqlDataSource ID="dsPartyLedger" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>"
                                    SelectCommand="SP_Party_ledger" SelectCommandType="StoredProcedure"
                                       FilterExpression="PartyName LIKE '%{0}%' OR ContactNo LIKE '{1}%' OR AreaVillageName LIKE '{2}%' OR PartyCode LIKE '{3}%'">
                                    <FilterParameters>
                                        <asp:ControlParameter Name="PartyName" ControlID="txtSearch" PropertyName="Text" />
                                        <asp:ControlParameter Name="ContactNo" ControlID="txtSearch" PropertyName="Text" />
                                        <asp:ControlParameter Name="AreaVillageName" ControlID="txtSearch" PropertyName="Text" />
                                        <asp:ControlParameter Name="PartyCode" ControlID="txtSearch" PropertyName="Text" />
                                    </FilterParameters>
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
            <%--<Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnRefresh" EventName="Click" />
            </Triggers>--%>
        </asp:UpdatePanel>
    </div>
    <div class="feature-box-actionBar">
        <span class="failureNotification">
            <asp:Literal ID="lblFailure" runat="server"></asp:Literal>
        </span>
        <asp:Button ID="btnReport" runat="server" Text="Generate Report" CssClass="button"
            OnClick="btnReport_Click" OnClientClick="document.getElementById('form1').target ='_blank';" />
        <%--<asp:Button ID="btnSave" runat="server" CssClass="button" Text="Save" 
                                ValidationGroup="SerialValidationGroup" OnClick="btnSave_Click" />--%>
    </div>
</asp:Content>
