﻿<%@ Page Language="C#" AutoEventWireup="true" Title="CSMSys :: Serial Register" CodeBehind="SerialRegister.aspx.cs" Inherits="CSMSys.Web.Pages.INV.SerialRegister" %>

<%@ Register assembly="CrystalDecisions.Web, Version=13.0.2000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="hdnSearch" runat="server" />
        <div class="feature-box-full">
            <table width="100%" border="0" cellpadding="0" cellspacing="4">
                <tbody>
                <tr>
                    <td align="center" valign="top" style="font-size:10px;">
                        বিসমিল্লাহির রাহমানির রাহিম
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:28px;">
                        শাহ্‌ ইসমাঈল গাজী (রহঃ) কোল্ড ষ্টোরেজ লিঃ
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">
                        সায়েকপুর, খেজমতপুর, পীরগঞ্জ, রংপুর। মোবাঃ ০১৭১৫৫৯৭৫৯১,০১৭১৯৭০৮৬২১, ০১৭৩২১১২১৯৬, হেড অফিসঃ ধানমণ্ডি, ঢাকা
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:20px;">
                        সিরিয়াল রেজিষ্টার
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">
                        তারিখ: <asp:Label ID="lblfrom" runat="server" Text=""></asp:Label>
                        হতেঃ <asp:Label ID="lblto" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size:12px;">
                        <asp:Label runat="server" ID="lblParty"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        পূর্বের জের : <asp:Label runat="server" ID="lblOpBalance"></asp:Label>
                    </td>
                </tr>
                <%--<tr>
                    <td align="center" valign="top">
                        &nbsp;
                    </td>
                </tr>--%>
                <tr>
                    <td align="center" style="font-size:10px;">
                        <asp:GridView ID="grvRegister"  runat="server" Width="100%"
                            AutoGenerateColumns="False" CellPadding="3" HorizontalAlign="Left" ShowHeaderWhenEmpty="true"
                            EmptyDataText="No Records Found" DataSourceID="dsRegister"  AllowSorting="true"
                            OnPageIndexChanging="grvRegister_PageIndexChanging" OnRowDataBound="grvRegister_RowDataBound">
                            <Columns>
                                <%--<asp:TemplateField HeaderText="Sl #" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="4%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblsl" Text='<%# Eval("SlNo") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="তারিখ" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lvlSerialDate" Text='<%# Eval("SerialDate","{0:dd/MM/yyyy}") %>' runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="এস/আর" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSerialNo" Text='<%# Eval("SerialNo") %>' runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="বস্তা" HeaderStyle-HorizontalAlign="Center"
                                    ItemStyle-HorizontalAlign="Center" ItemStyle-Width="4%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblBags" Text='<%# Eval("Bags") %>' runat="server" HorizontalAlign="Left" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="মোট বস্তা" HeaderStyle-HorizontalAlign="Center" 
                                    ItemStyle-HorizontalAlign="Center" ItemStyle-Width="7%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotalBags" runat="server" HorizontalAlign="Center" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="কোড"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPartyCode" Text='<%# Eval("PartyCode") %>' runat="server" HorizontalAlign="Left" Font-Size="10px" />
                                      </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="নাম"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPartyName" Text='<%# Eval("PartyName") %>' runat="server" HorizontalAlign="Left" Font-Size="12px" />
                                      </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="পিতার নাম" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFatherName" Text='<%# Eval("FatherName") %>' runat="server" HorizontalAlign="Left" Font-Size="12px" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="গ্রাম" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAreaVillageName" Text='<%# Eval("AreaVillageName") %>' runat="server" HorizontalAlign="Left" Font-Size="12px"/>                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="মোবাইল নং" HeaderStyle-HorizontalAlign="Left"
                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblContactNo" Text='<%# Eval("ContactNo") %>' runat="server" HorizontalAlign="Left" Font-Size="10px" />                                       
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                            <EmptyDataRowStyle ForeColor="#CC0000" />
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        </asp:GridView>
                        <asp:SqlDataSource ID="dsRegister" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>" OnSelecting="dsRegister_Selecting"
                             SelectCommand="SELECT ROW_NUMBER() OVER (ORDER BY iss.SerialDate) As SlNo, iss.SerialID, iss.SerialDate, iss.Serial, iss.Bags, iss.SerialNo, iss.PartyID, ip.PartyType, ip.PartyCode, ip.PartyName, ip.FatherName, 
                                ip.ContactNo, ip.AreaVillageName, ip.ParentID
                                FROM INVStockSerial AS iss INNER JOIN
                                INVParty AS ip ON iss.PartyID = ip.PartyID
                                WHERE ((iss.SerialDate >= @DateFrom) AND (iss.SerialDate <= @DateTo)) ORDER BY iss.SerialDate" 
                                FilterExpression="PartyCode LIKE '{0}%'">
                            <FilterParameters>
                                <asp:ControlParameter Name="PartyCode" ControlID="hdnSearch" PropertyName="Value" />
                            </FilterParameters>
                            <SelectParameters>
                                <asp:Parameter Name="DateFrom" Type="DateTime" />
                                <asp:Parameter Name="DateTo" Type="DateTime" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <%--<tr>
                    <td align="center" valign="top">
                        &nbsp;
                    </td>
                </tr>--%>
                <tr>
                    <td align="left" valign="top">
                        মোট প্রাপ্ত বস্তা : <asp:Label runat="server" ID="lblClBalance"></asp:Label>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </form>
</body>
</html>
