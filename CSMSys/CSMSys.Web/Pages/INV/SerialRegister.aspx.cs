﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.INV;

namespace CSMSys.Web.Pages.INV
{
    public partial class SerialRegister : System.Web.UI.Page
    {
        #region Method for Private
        private string printer = ConfigurationManager.AppSettings["StorePrinter"].ToString();
        private string connstring = ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString;
        private int TotalBags = 0;

        private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }

        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }

        private Int32 PartyID
        {
            get
            {
                if (ViewState["PartyID"] == null)
                    ViewState["PartyID"] = -1;
                return (Int32)ViewState["PartyID"];
            }
            set
            {
                ViewState["PartyID"] = value;
            }
        }

        private INVParty _party;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateFrom = string.IsNullOrEmpty(Request.QueryString["FD"]) ? DateTime.Today : Convert.ToDateTime(Request.QueryString["FD"]);
                DateTo = string.IsNullOrEmpty(Request.QueryString["TD"]) ? DateTime.Today : Convert.ToDateTime(Request.QueryString["TD"]);
                string code = string.IsNullOrEmpty(Request.QueryString["PC"]) ? string.Empty : Request.QueryString["PC"];

                lblfrom.Text = DateFrom.ToShortDateString();
                lblto.Text = DateTo.ToShortDateString();
                hdnSearch.Value = code;

                if (string.IsNullOrEmpty(code))
                {
                    lblOpBalance.Text = getOpeningBalance(0, DateFrom).ToString();
                }
                else
                {
                    _party = new PartyManager().SearchParentByCode(code);
                    if (_party != null)
                    {
                        int partyID = _party.PartyID != null ? Convert.ToInt32(_party.PartyID) : 0;
                        int parentID = _party.ParentID != null ? Convert.ToInt32(_party.ParentID) : 0;

                        if (partyID > 0)
                        {
                            if (parentID > 0)
                            {
                                lblOpBalance.Text = getOpeningBalance(partyID, DateFrom).ToString();
                            }
                            else
                            {
                                lblOpBalance.Text = getOpeningBalanceByParent(partyID, DateFrom).ToString();
                                lblOpBalance.Text = (int.Parse(lblOpBalance.Text) + getOpeningBalance(partyID, DateFrom)).ToString();
                            }
                        }
                        lblParty.Text = string.IsNullOrEmpty(_party.PartyCodeName) ? string.Empty : _party.PartyCodeName;
                    }
                }

                TotalBags = int.Parse(string.IsNullOrEmpty(lblOpBalance.Text) ? "0" : lblOpBalance.Text.ToString());
                grvRegister.DataBind();
            }
        }

        private int getOpeningBalance(int PartyID, DateTime dateFrom)
        {
            int opBalance = 0;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstring))
                {
                    sqlConn.Open();
                    string _query = "SELECT SUM(Bags) AS TotalBags ";
                    _query = _query + "FROM INVStockSerial ";
                    _query = _query + "WHERE (SerialDate < CONVERT(DATETIME, '" + dateFrom.ToShortDateString() + " 00:00:00', 102)) ";
                    if (PartyID > 0)
                    {
                        _query = _query + "AND (PartyID = " + PartyID + ")";
                    }
                    SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    if (sqlReader.Read())
                    {
                        if (sqlReader.HasRows) opBalance = string.IsNullOrEmpty(sqlReader["TotalBags"].ToString()) ? 0 : int.Parse(sqlReader["TotalBags"].ToString());
                    }

                    sqlReader.Close();
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }

        private int getOpeningBalanceByParent(int ParentID, DateTime dateFrom)
        {
            int opBalance = 0;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstring))
                {
                    sqlConn.Open();
                    string _query = "SELECT SUM(iss.Bags) AS TotalBags ";
                    _query = _query + "FROM INVStockSerial AS iss INNER JOIN ";
                    _query = _query + "INVParty AS ip ON iss.PartyID = ip.PartyID ";
                    _query = _query + "WHERE (iss.SerialDate < CONVERT(DATETIME, '" + dateFrom.ToShortDateString() + " 00:00:00', 102)) ";
                    if (ParentID > 0)
                    {
                        _query = _query + "GROUP BY ip.ParentID ";
                        _query = _query + "HAVING (ip.ParentID = " + ParentID + ")";
                    }
                    SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    if (sqlReader.Read())
                    {
                        if (sqlReader.HasRows) opBalance = string.IsNullOrEmpty(sqlReader["TotalBags"].ToString()) ? 0 : int.Parse(sqlReader["TotalBags"].ToString());
                    }

                    sqlReader.Close();
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }

        #region Method for Grid
        protected void grvRegister_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvRegister.PageIndex = e.NewPageIndex;
            grvRegister.DataBind();
        }

        protected void grvRegister_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[3].Controls.Count > 0))
            {
                TotalBags += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Bags"));

                Label lblTotalBags = (Label)e.Row.FindControl("lblTotalBags");
                lblTotalBags.Text = TotalBags.ToString();
            }
            else if (e.Row.RowType == DataControlRowType.Footer)
            {
                //Label lblTotal = (Label)e.Row.FindControl("lblTotal");
                //lblTotal.Text = TotalBags.ToString();
            }

            lblClBalance.Text = TotalBags.ToString();
        }

        #region SqlDataSource Control Event Handlers
        protected void dsRegister_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@DateFrom"].Value = DateFrom;
            e.Command.Parameters["@DateTo"].Value = DateTo;
        }
        #endregion
        #endregion
    }
}
