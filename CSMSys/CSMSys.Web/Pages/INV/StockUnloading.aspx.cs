﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;

namespace CSMSys.Web.Pages.INV
{
    public partial class StockUnloading : System.Web.UI.Page
    {
        private string strSearch = string.Empty;

        private DateTime DateFrom
        {
            get
            {
                if (ViewState["DateFrom"] == null)
                    ViewState["DateFrom"] = -1;
                return (DateTime)ViewState["DateFrom"];
            }
            set
            {
                ViewState["DateFrom"] = value;
            }
        }

        private DateTime DateTo
        {
            get
            {
                if (ViewState["DateTo"] == null)
                    ViewState["DateTo"] = -1;
                return (DateTime)ViewState["DateTo"];
            }
            set
            {
                ViewState["DateTo"] = value;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DateFrom = DateTime.Today;
                txtFromDate.Text = DateFrom.ToShortDateString();
                DateTo = DateTime.Today;
                txtToDate.Text = DateTo.ToShortDateString();

                grvLoading.DataBind();
            }
        }

        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }
        #endregion

        #region Methods For Grid
        protected void grvLoading_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            //if (e.CommandName.Equals("Delete"))
            //{
            //    int intPartyID = Convert.ToInt32(e.CommandArgument.ToString());

            //    if (intPartyID > 0)
            //    {
            //        DeleteParty(intPartyID);
            //    }
            //}
        }

        protected void grvLoading_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvLoading.PageIndex = e.NewPageIndex;
            grvLoading.DataBind();
        }

        protected void grvLoading_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                ImageButton btnEdit = (ImageButton)e.Row.FindControl("imgEdit");
                btnEdit.Attributes.Add("onClick", "javascript:ShowEditModal('" + ((Label)e.Row.Cells[1].Controls[1]).Text + "');");

                HyperLink hpl = (HyperLink)e.Row.FindControl("hplprint");
                string check = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "State").ToString());
                if (check == "Chamber To Delivery" || check == "PC To Delivery" || check == "Shed To Delivery")
                {
                    hpl.NavigateUrl = "~/Pages/Reports/GetPassReport.aspx?CID=" + (DataBinder.Eval(e.Row.DataItem, "CollectionID")).ToString() + "&PID=" + (DataBinder.Eval(e.Row.DataItem, "PartyID")).ToString();
                }
                else
                {
                   // hpl.NavigateUrl = "~/Pages/Reports/DiscountReport.aspx?CID=" + (DataBinder.Eval(e.Row.DataItem, "CommisonID")).ToString() + "&PID=" + (DataBinder.Eval(e.Row.DataItem, "PartyID")).ToString();
                }
            }
        }

        #region SqlDataSource Control Event Handlers
        protected void dsLoading_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {
            e.Command.Parameters["@DateFrom"].Value = DateFrom;
            e.Command.Parameters["@DateTo"].Value = DateTo;
        }
        #endregion
        #endregion

        #region Methods For Button
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;

            System.Globalization.DateTimeFormatInfo dateInfo = new System.Globalization.DateTimeFormatInfo();
            dateInfo.ShortDatePattern = "MM/dd/yyyy";

            DateFrom = Convert.ToDateTime(txtFromDate.Text.ToString(), dateInfo);
            DateTo = Convert.ToDateTime(txtToDate.Text.ToString(), dateInfo);
            grvLoading.DataBind();
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvLoading.DataBind();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            grvLoading.DataBind();
        }
        #endregion

        protected void btnReport_Click(object sender, EventArgs e)
        {
            try
            {
                if (!checkValidity()) return;
                lblFailure.Text = "";
                string fromDate = Convert.ToDateTime(txtFromDate.Text).ToString("MM/dd/yyyy");
                string toDate = Convert.ToDateTime(txtToDate.Text).ToString("MM/dd/yyyy");
                Response.Redirect("~/Pages/Reports/UnloadReportNew.aspx?FD=" + fromDate + "&TD=" + toDate);

            }
            catch (Exception ex)
            {
            }
        }

        protected void btnReport1_Click(object sender, EventArgs e)
        {
            try
            {
                if (!checkValidity()) return;
                lblFailure.Text = "";
                string fromDate = Convert.ToDateTime(txtFromDate.Text).ToString("MM/dd/yyyy");
                string toDate = Convert.ToDateTime(txtToDate.Text).ToString("MM/dd/yyyy");
                Response.Redirect("~/Pages/Reports/UnloadReport.aspx?FD=" + fromDate + "&TD=" + toDate);

            }
            catch (Exception ex)
            {
            }
        }
        private bool checkValidity()
        {
            DateTime dt = new DateTime();
            dt.ToString("yyyy/MM/dd");
            //String.Format("{0:yyyy/MM/dd}", dt);
            if (!DateTime.TryParse(txtFromDate.Text, out dt))
            {
                lblFailure.Text = "Select From Date";
                txtFromDate.Focus();
                return false;
            }

            if (!DateTime.TryParse(txtToDate.Text, out dt))
            {
                lblFailure.Text = "Select To Date";
                txtToDate.Focus();
                return false;

            }
            if (txtFromDate.Text == "" || txtToDate.Text == "")
            {
                lblFailure.Text = "Select Date";
                txtFromDate.Focus();
                return false;
            }
            return true;
        }
    }
}