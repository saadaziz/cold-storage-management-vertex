﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SerialReport.aspx.cs" Inherits="CSMSys.Web.Pages.INV.SerialReport" Title="CSMSys :: Stock Serial Report" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <table width="100%" border="0" cellpadding="0" cellspacing="4">
            <tbody>
                <tr>
                    <td align="center" valign="top" style="font-size: 8px;" colspan="2">
                        বিসমিল্লাহির রাহমানির রাহিম
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size: 24px;" colspan="2">
                        শাহ্‌ ইসমাঈল গাজী (রহঃ) কোল্ড ষ্টোরেজ লিঃ
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size: 10px;" colspan="2">
                        সায়েকপুর, খেজমতপুর, পীরগঞ্জ, রংপুর, হেড অফিসঃ ধানমণ্ডি, ঢাকা
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size: 8px;" colspan="2">
                        মোবাঃ ০১৭১৫৫৯৭৫৯১,০১৭১৯৭০৮৬২১, ০১৭৩২১১২১৯৬
                    </td>
                </tr>
                <tr>
                    <td align="center" valign="top" style="font-size: 14px;" colspan="2">
                        আলুর বস্তা প্রাপ্তির রশিদ
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="font-size:12px;">
                        তারিখ : 
                        <asp:Label ID="lbldate" runat="server" Text=""></asp:Label>
                    </td>
                    <td align="right" valign="top" style="font-size:12px;">
                        এস/আর নং : <asp:Label ID="lblsr" runat="server" Text=""></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="font-size:12px;">
                        নাম :
                        <asp:Label ID="lblpartyname" runat="server" Text="" Font-Underline="true"></asp:Label>
                    </td>
                     <td style="font-size:12px;">
                        পার্টির কোড :
                        <asp:Label ID="lblcode" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="font-size:12px;">
                        পিতার নাম :
                        <asp:Label ID="lblfathername" runat="server" Text=""></asp:Label>
                    </td>
                    <td style="font-size:12px;">
                        গ্রাম :
                        <asp:Label ID="lblvillage" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                   
                    <td style="font-size:12px;">
                        এস আর কতৃক প্রাপ্ত খালি বস্তা সংখ্যা : 
                        <asp:Label ID="lblbagcount" runat="server" Text=""></asp:Label>
                    </td>
                      <td colspan="2" style="font-size:12px;">
                        ধন্যবাদের সহিত গৃহিত হইলো
                    </td>
                </tr>

                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" style="font-size:12px;">
                        সহকারী স্টোর কীপারের সাক্ষর
                    </td>
                    <td align="right" valign="top" style="font-size:12px;">
                        প্রধান স্টোর কীপারের সাক্ষর&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    </form>
</body>
</html>
