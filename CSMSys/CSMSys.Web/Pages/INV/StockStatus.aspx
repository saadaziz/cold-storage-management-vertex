﻿<%@ Page Language="C#" Title="CSMSys :: Stock Status" AutoEventWireup="true" MasterPageFile="~/Default.Master"
    CodeBehind="StockStatus.aspx.cs" Inherits="CSMSys.Web.Pages.INV.StockStatus" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <link href="../../../App_Themes/TableSorter/Blue/style.css" rel="stylesheet" type="text/css" />
    <link href="../../../App_Themes/TableSorter/Green/style.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div class="content" style="height:630px;">
    <asp:ScriptManager ID="ScriptManager1" runat="server"/>
    <div class="title">
    <table width="100%" border="0" cellpadding="2" cellspacing="4">
        <tbody>
            <tr>
                <td align="left" style="width: 20%;">
                    <h2>
                        Stock Status
                    </h2>
                </td>
                <td align="right" style="width: 78%;">
                    Search by Party Code
                    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                    <%--&nbsp; And Date from : 
                        <asp:TextBox ID="txtFromDate" runat="server" Text="From" Width="87px"></asp:TextBox>
                    <cc1:CalendarExtender ID="ceFromDate" runat="server" TargetControlID="txtFromDate" />
                    &nbsp;To&nbsp;
                    <asp:TextBox ID="txtToDate" runat="server" Text="To" Width="87px"></asp:TextBox>
                    <cc1:CalendarExtender ID="ceToDate" runat="server" TargetControlID="txtToDate" />--%>
                </td>
                <td align="center" style="width: 2%;">
                    <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" ImageUrl="~/App_Themes/Default/Images/gridview/Search.png"
                        ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch_Click" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <table width="100%" border="0" cellpadding="2" cellspacing="4">
                        <tbody>
                            <tr>
                                <td align="left" style="width: 80%;">
                                    <div class="feature-box-actionBar">
                                        <span class="successNotification">
                                            <asp:Literal ID="lblFailure" runat="server"></asp:Literal>
                                        </span>
                                    </div>
                                </td>
                                <td align="right" valign="bottom" style="width: 20%;">
                                    Top: 
                                    <asp:DropDownList ID="ddltop" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddltop_changed">
                                    <asp:ListItem>5</asp:ListItem>
                                    <asp:ListItem>10</asp:ListItem>
                                    <asp:ListItem>50</asp:ListItem>
                                    <asp:ListItem>100</asp:ListItem>
                                    <asp:ListItem>500</asp:ListItem>
                                    <asp:ListItem>1000</asp:ListItem>
                                    <asp:ListItem>All</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <asp:GridView ID="grvStockSerial" runat="server" Width="100%"
                        AutoGenerateColumns="False" CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvStockSerial_PageIndexChanging"
                        ShowHeaderWhenEmpty="true" 
                        EmptyDataText="No Records Found" CssClass="tablesorterBlue" AllowPaging="True"
                        PageSize="15" DataSourceID="dsStockSerial">
                        <Columns>
                            <asp:TemplateField HeaderText="Code" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="15%">
                                <ItemTemplate>
                                    <asp:Label ID="lblcode" Text='<%# Eval("partycode") %>'
                                        runat="server" HorizontalAlign="Left" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                    ItemStyle-Width="15%">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPartyName" Text='<%# Eval("PartyName") %>'
                                            runat="server" HorizontalAlign="Left" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            <asp:TemplateField HeaderText="Father" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lblfather" Text='<%# Eval("fathername") %>'
                                        runat="server" HorizontalAlign="Left" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Village" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="10%">
                                <ItemTemplate>
                                    <asp:Label ID="lblvillage" Text='<%# Eval("areavillagename") %>'
                                        runat="server" HorizontalAlign="Left" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Bags" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                ItemStyle-Width="8%">
                                <ItemTemplate>
                                    <asp:Label ID="lblPartyName" Text='<%# Eval("smbags") %>'
                                        runat="server" HorizontalAlign="Left" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:SqlDataSource ID="dsStockSerial" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>"
                        SelectCommand="select top 5 partycode,PartyName,sum(bagcount) as smbags,Areavillagename,fathername
                            from INVParty
                            GROUP BY PartyCode,PartyName,Areavillagename,fathername
                            ORDER BY sum(bagcount) desc">
                    </asp:SqlDataSource>
                </td>
            </tr>
        </tbody>
    </table>
</div>
</asp:Content>
