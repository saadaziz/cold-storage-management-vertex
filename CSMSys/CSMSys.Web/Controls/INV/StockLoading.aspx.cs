﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.INV;

namespace CSMSys.Web.Controls.INV
{
    public partial class StockLoading : System.Web.UI.Page
    {
        #region Private Properties
        private enum UIMODE
        {
            NEW,
            EDIT
        }

        private UIMODE UIMode
        {
            get
            {
                if (ViewState["UIMODE"] == null)
                    ViewState["UIMODE"] = new UIMODE();
                return (UIMODE)ViewState["UIMODE"];
            }
            set
            {
                ViewState["UIMODE"] = value;
            }
        }

        private int PartyID
        {
            get
            {
                if (ViewState["PartyID"] == null)
                    ViewState["PartyID"] = -1;
                return (int)ViewState["PartyID"];
            }
            set
            {
                ViewState["PartyID"] = value;
            }
        }

        private int SerialID
        {
            get
            {
                if (ViewState["SerialID"] == null)
                    ViewState["SerialID"] = -1;
                return (int)ViewState["SerialID"];
            }
            set
            {
                ViewState["SerialID"] = value;
            }
        }

        private int LoadingID
        {
            get
            {
                if (ViewState["LoadingID"] == null)
                    ViewState["LoadingID"] = -1;
                return (int)ViewState["LoadingID"];
            }
            set
            {
                ViewState["LoadingID"] = value;
            }
        }

        private string strSearch = string.Empty;
        private INVStockSerial _Serial;
        private INVStockLoading _Loading;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtDate.Text = System.DateTime.Today.ToShortDateString();
                string qsUIMODE = string.IsNullOrEmpty(Request.QueryString["UIMODE"]) ? "NEW" : Request.QueryString["UIMODE"];
                if (string.IsNullOrEmpty(qsUIMODE) == false)
                {
                    UIMode = (UIMODE)Enum.Parse(typeof(UIMODE), qsUIMODE);
                    LoadingID = Convert.ToInt32(Request.QueryString["PID"]);
                    hdnWindowUIMODE.Value = UIMode.ToString();

                    if (UIMode == UIMODE.EDIT)
                    {
                        LoadToAllControlValue(LoadingID);

                        pnlNew.Visible = true;
                        btnSave.Text = "Update";
                    }
                    else if (UIMode == UIMODE.NEW)
                    {
                        pnlNew.Visible = true;
                        btnSave.Text = "Load";
                    }
                }
                MultiViewSerial.ActiveViewIndex = 0;
            }
        }

        #region Methods for Button
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if ((UIMode == UIMODE.NEW) || (UIMode == UIMODE.EDIT))
                {
                    if (!checkValidity()) return;

                    SaveLoading();

                    //lblFailure.Text = (lblFailure.Text.Contains("already")) ? lblFailure.Text += " Loading information saved." : " Loading information saved.";
                    grvParty.DataBind();
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onSuccess();", true);
                MultiViewSerial.ActiveViewIndex = 1;
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onError();", true);
                MultiViewSerial.ActiveViewIndex = 2;
            }
        }

        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;
            grvParty.DataBind();
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvParty.DataBind();
        }
        #endregion
        
        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }

        private bool checkValidity()
        {
            if (string.IsNullOrEmpty(lblSerialNo.Text))
            {
                lblFailure.Text = "Serial No is Required";
                return false;
            }
            if (string.IsNullOrEmpty(txtDate.Text))
            {
                lblFailure.Text = "Date is Required";
                txtDate.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtchamber.Text))
            {
                lblFailure.Text = "Chamber Number is Required";
                txtchamber.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtfloor.Text))
            {
                lblFailure.Text = "Floor Number is Required";
                txtfloor.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtpocket.Text))
            {
                lblFailure.Text = "Pocket Number is Required";
                txtpocket.Focus();
                return false;
            }
            INVStockLoading tsl = new LoadManager().checkLocation(Convert.ToInt32(txtchamber.Text), Convert.ToInt32(txtfloor.Text), txtpocket.Text, txtLine.Text);
            if (tsl != null)
            {
                if (tsl.SerialNo != lblSerialNo.Text)
                {
                    if (tsl != null)
                    {
                        lblFailure.Text = "That Location is already been given to " + tsl.SerialNo + ".";
                        txtLine.Focus();
                        return false;
                    }
                }
            }
            return true;
        }

        private void ClearForm()
        {
            lblparty.Text = string.Empty;
            lblSerialNo.Text = string.Empty;
            lblbags.Text = string.Empty;
            txtchamber.Text = string.Empty;
            txtpocket.Text = string.Empty;
            txtfloor.Text = string.Empty;
            txtRemarks.Text = string.Empty;
            lblSerialNo.Text = string.Empty;
        }

        private INVStockLoading FormToObject(int id)
        {
            INVStockLoading _Loading = new INVStockLoading();

            if (id > 0)
            {
                _Loading = new LoadManager().GetLoadByID(id);
            }

            _Loading.RelocatedCount = 0;

            _Loading.LoadingID = id;
            _Loading.PartyID = PartyID;
            _Loading.Bags = float.Parse(lblbags.Text);
            _Loading.SerialNo = lblSerialNo.Text;
            _Loading.SerialID = SerialID;
            Int32 chamber = Int32.Parse(txtchamber.Text.ToString());
            _Loading.ChamberNo = chamber;
            _Loading.Floor = Int32.Parse(txtfloor.Text);
            _Loading.Pocket = txtpocket.Text.ToString();
            _Loading.Line = txtLine.Text.ToString();
            _Loading.Remarks = txtRemarks.Text.ToString();
            _Loading.LoadedDate = DateTime.Parse(txtDate.Text);

            return _Loading;
        }

        private void LoadToAllControlValue(int loadID)
        {
            if (loadID > 0)
            {
                _Loading = new LoadManager().GetLoadByID(loadID);

                lblSerialNo.Text = _Loading.SerialNo.ToString();
                lblbags.Text = _Loading.Bags.ToString();
                txtchamber.Text = _Loading.ChamberNo.ToString();
                txtpocket.Text = _Loading.Pocket.ToString();
                txtfloor.Text = _Loading.Floor.ToString();
                txtLine.Text = string.IsNullOrEmpty(_Loading.Line) ? string.Empty : _Loading.Line.ToString();
                txtRemarks.Text = _Loading.Remarks.ToString();
                txtDate.Text = DateTime.Parse(_Loading.LoadedDate.ToString()).ToShortDateString();

                int intSRID = (int)_Loading.SerialID;
                SerialID = intSRID;

                _Serial = new SerialManager().GetSerialByID(intSRID);

                lblparty.Text = _Serial.PartyCode;
            }
        }
        #endregion

        #region Methods For Save
        private void SaveLoading()
        {
            try
            {
                INVStockLoading isl = new INVStockLoading();
                isl = FormToObject(LoadingID);

                if (new LoadManager().SaveStockLoading(isl))
                {
                    ClearForm();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For Grid
        protected void grvParty_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                string text = e.CommandArgument.ToString();
                string[] words = text.Split('@');
                string strSRID = words[0].Trim().ToString();
                string strPID = words[1].Trim().ToString();

                PartyID = Convert.ToInt32(strPID);
                int intSRID = Convert.ToInt32(strSRID);

                if (PartyID > 0)
                {
                    ReportManager rptManager = new ReportManager();
                    INVParty _Party = rptManager.GetPartyByID(PartyID);

                    lblparty.Text = _Party.PartyCode;

                    _Serial = new SerialManager().GetSerialByID(intSRID);
                    hdnPartyID.Value = _Serial.PartyID.ToString();
                    txtDate.Text = DateTime.Parse(_Serial.SerialDate.ToString()).ToShortDateString();
                    lblbags.Text = _Serial.Bags.ToString();
                    lblSerialNo.Text = _Serial.SerialNo;
                    SerialID = intSRID;
                }

            }
        }

        protected void grvParty_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvParty.PageIndex = e.NewPageIndex;
            grvParty.DataBind();
        }

        protected void grvParty_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                ImageButton btnselect = (ImageButton)e.Row.FindControl("imgselect");
                btnselect.CommandArgument = ((Label)e.Row.Cells[2].Controls[1]).Text + "@" + ((Label)e.Row.Cells[5].Controls[1]).Text;

                //btnselect.CommandArgument = (DataBinder.Eval(e.Row.DataItem, "SlNo")).ToString() + "@" +
                //    (DataBinder.Eval(e.Row.DataItem, "Bags")).ToString() + "@" +
                //      (DataBinder.Eval(e.Row.DataItem, "PartyCode")).ToString() + "@" +
                //    (DataBinder.Eval(e.Row.DataItem, "SerialNo")).ToString();

                //Label lblserial = (Label)e.Row.FindControl("lblSerialID");
                //Label lblloaded = (Label)e.Row.FindControl("lblloaded");
                //LoadManager lm = new LoadManager();
                //INVStockLoading sl = new INVStockLoading();
                //sl = lm.GetLoadBySerial(lblserial.Text);
                //lblloaded.Text = (sl == null) ? "No" : "Yes";

                //btnselect.CommandArgument = ((Label)e.Row.Cells[1].Controls[1]).Text;
            }
        }
        #endregion
    }
}