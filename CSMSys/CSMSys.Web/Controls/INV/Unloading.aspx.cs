﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.INV;
using CSMSys.Lib.DataAccessLayer.Implementations;
using System.Text.RegularExpressions;
using System.Data;
namespace CSMSys.Web.Controls.INV
{
    public partial class Unloading : System.Web.UI.Page
    {
        #region Private Properties
        private enum UIMODE
        {
            NEW,
            EDIT
        }

        private UIMODE UIMode
        {
            get
            {
                if (ViewState["UIMODE"] == null)
                    ViewState["UIMODE"] = new UIMODE();
                return (UIMODE)ViewState["UIMODE"];
            }
            set
            {
                ViewState["UIMODE"] = value;
            }
        }
        private int UnloadID
        {
            get
            {
                if (ViewState["UnloadID"] == null)
                    ViewState["UnloadID"] = -1;
                return (int)ViewState["UnloadID"];
            }
            set
            {
                ViewState["UnloadID"] = value;
            }
        }
        private long SerialID
        {
            get
            {
                if (ViewState["SerialID"] == null)
                    ViewState["SerialID"] = -1;
                return (long)ViewState["SerialID"];
            }
            set
            {
                ViewState["SerialID"] = value;
            }
        }
        private int partyID
        {
            get
            {
                if (ViewState["partyID"] == null)
                    ViewState["partyID"] = -1;
                return (int)ViewState["partyID"];
            }
            set
            {
                ViewState["partyID"] = value;
            }
        }
        private int CollectionID
        {
            get
            {
                if (ViewState["CollectionID"] == null)
                    ViewState["CollectionID"] = -1;
                return (int)ViewState["CollectionID"];
            }
            set
            {
                ViewState["CollectionID"] = value;
            }
        }
        private int CollectionDetailID
        {
            get
            {
                if (ViewState["CollectionDetailID"] == null)
                    ViewState["CollectionDetailID"] = -1;
                return (int)ViewState["CollectionDetailID"];
            }
            set
            {
                ViewState["CollectionDetailID"] = value;
            }
        }
        private INVStockUnloading invunload;
        private UnloadingDAOLinq unloaddao=new UnloadingDAOLinq();
        private string strSearch = string.Empty;
        static IList<INVStockUnloading> allUnload = new List<INVStockUnloading>();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //UIUtility.BindCustomerDDL(ddlCustomer, false);
                txtDate.Text = System.DateTime.Today.ToShortDateString();
                string qsUIMODE = string.IsNullOrEmpty(Request.QueryString["UIMODE"]) ? "NEW" : Request.QueryString["UIMODE"];
                if (string.IsNullOrEmpty(qsUIMODE) == false)
                {
                    UIMode = (UIMODE)Enum.Parse(typeof(UIMODE), qsUIMODE);
                    UnloadID = Convert.ToInt32(Request.QueryString["UID"]);
                    hdnWindowUIMODE.Value = UIMode.ToString();

                    if (UIMode == UIMODE.EDIT)
                    {
                        LoadToAllControlValue(UnloadID);
                        grvUnloading.Visible = false;
                        pnlNew.Visible = true;
                        btnSave.Text = "Update";
                    }
                    else if (UIMode == UIMODE.NEW)
                    {
                        allUnload = new UnloadingDAOLinq().All();
                       
                        pnlNew.Visible = true;
                        btnSave.Text = "Save";
                    }
                }
                MultiViewSerial.ActiveViewIndex = 0;
            }
        }
        private void LoadToAllControlValue(int unloadid)
        {
            if (unloadid != 0)
            { 
                invunload = unloaddao.PickByID(unloadid);

                txtDate.Text = invunload.Date.Value.ToShortDateString();

                INVStockSerial invss = new SerialDAOLinq().PickByID(Convert.ToInt16(invunload.SerialID));
                SerialID = invss.SerialID;
                INVParty invp = new PartyDAOLinq().PickByID(Convert.ToInt32(invss.PartyID));
                lblserNo.Text = invunload.SerialNo;
                partyID = invp.PartyID;
                lblCode.Text = invp.PartyCode;
                lblType.Text = invp.PartyType;
                lblName.Text = invp.PartyName;
                txtshedCharge.Text = invunload.ShedCharge.ToString();
                txtbags.Text = invunload.Bags.ToString();
                string state = invunload.State;
                float var =(float)new VariableDAOLinq().SearchVariable("Shed Charge").variable;
                lblshedChargeInfo.Text = "Shed Charge should be: " +( var * (int.Parse(txtbags.Text))).ToString();
                foreach (ListItem li in ddlState.Items)
                {
                    if (li.Text == state) li.Selected = true;
                }
                CollectionID = invunload.CollectionID;
                CollectionDetailID = invunload.CollectionDetailID;
            }
        }
        protected void ddlStateIndexChanged(object sender, EventArgs e)
        {
            if (ddlState.SelectedItem.Value == "2" || ddlState.SelectedItem.Value == "4")
            {
                txtshedCharge.Enabled = true;
                float var = (float)new VariableDAOLinq().SearchVariable("Shed Charge").variable;
                lblshedChargeInfo.Text = "Shed Charge should be: " + (var * (int.Parse(txtbags.Text))).ToString();
            }
            else
                txtshedCharge.Enabled = false;

        }
        #region for save
        public INVStockUnloading FormToObject(int id)
        {
            INVStockUnloading res = new INVStockUnloading();
            if (id != 0)
            {
                res.UnloadID = id;
                res.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                res.ModifiedDate=DateTime.Now;
            }
            res.CreatedBy=WebCommonUtility.GetCSMSysUserKey();
            res.CreatedDate=DateTime.Now;
            res.ShedCharge = float.Parse( txtshedCharge.Text);
            res.Date = DateTime.Parse(txtDate.Text);
            res.SerialID = SerialID;
            res.Bags=int.Parse( txtbags.Text);
            res.PartyID=partyID;
            res.State = ddlState.SelectedItem.Text;
            res.SerialNo = lblserNo.Text;
            res.CollectionDetailID = CollectionDetailID;
            res.CollectionID = CollectionID;
            return res;
        }
        protected void btnSave_click(object sender, EventArgs e)
        {
            try
            {
                //if ((UIMode == UIMODE.NEW) || (UIMode == UIMODE.EDIT))
                //{
                if (!checkValidity()) return;

                INVStockUnloading invsu = FormToObject(UnloadID);
                if (UnloadID != 0)
                    unloaddao.Edit(invsu);
                else
                    unloaddao.Add(invsu);
                //Thread.Sleep(2000);
                //}
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onSuccess();", true);
                MultiViewSerial.ActiveViewIndex = 1;

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onError();", true);
                MultiViewSerial.ActiveViewIndex = 2;
            }
        }
        #endregion
        #region for methods
        private bool checkValidity()
        {
            if (string.IsNullOrEmpty(lblserNo.Text))
            {
                lblFailure.Text = "Serial No is Required";
                return false;
            }
            if (string.IsNullOrEmpty(txtDate.Text))
            {
                lblFailure.Text = "Date is Required";
                txtDate.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtbags.Text))
            {
                lblFailure.Text = "Chamber Number is Required";
                txtbags.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(lblCode.Text))
            {
                lblFailure.Text = "Floor Number is Required";
                
                return false;
            }
            
            return true;
        }
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }
        #endregion
        #region for buttons
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;
            grvUnloading.DataBind();
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvUnloading.DataBind();
        }
        #endregion
        #region Methods For Grid
        protected void grvUnloading_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                string text = e.CommandArgument.ToString();
                SRVLoanCollectionDetail srvlcd = new LoanCollectionDetailDAOLinq().PickByID(int.Parse(text));

                INVStockSerial invss = new SerialDAOLinq().PickByID(Convert.ToInt16(srvlcd.SerialID));
                INVParty invp = new PartyDAOLinq().PickByID(Convert.ToInt32(invss.PartyID));
                SerialID = invss.SerialID;
                partyID = invp.PartyID;
                lblserNo.Text = invss.SerialNo;
                lblCode.Text = invp.PartyCode;
                lblType.Text = invp.PartyType;
                lblName.Text = invp.PartyName;
                CollectionDetailID = srvlcd.CollectionDetailID;
                CollectionID =Convert.ToInt32( srvlcd.CollectionID);

            }
        }

        protected void grvUnloading_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvUnloading.PageIndex = e.NewPageIndex;
            grvUnloading.DataBind();
        }

        protected void grvUnloading_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                ImageButton btnselect = (ImageButton)e.Row.FindControl("imgselect");
                Label lblSerialIDs = (Label)e.Row.FindControl("lblserial");
                Label lblBags1 = (Label)e.Row.FindControl("lblBags");
                IList<INVStockUnloading> stocksListed = allUnload.Where(u => u.SerialNo.Equals(lblSerialIDs.Text)).ToList();
                int bagsUnloaded = 0; bool res = false;
                foreach (INVStockUnloading su in stocksListed)
                {
                    bagsUnloaded += su.Bags;
                    if (bagsUnloaded == int.Parse(lblBags1.Text))
                        res = true;

                }
                if (!res)
                {
                    lblBags1.Text = (int.Parse(lblBags1.Text) - bagsUnloaded).ToString();
                    btnselect.CommandArgument = DataBinder.Eval(e.Row.DataItem, "CollectionDetailID").ToString();
                }
                else
                {
                    btnselect.Visible = false;
                    lblBags1.Text ="0";
                }
            }
        }

        
        #endregion
    }
}