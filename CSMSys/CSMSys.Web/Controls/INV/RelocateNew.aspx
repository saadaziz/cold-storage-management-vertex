﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RelocateNew.aspx.cs" Inherits="CSMSys.Web.Controls.INV.RelocateNew" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta content="blendTrans(Duration=0.5)" http-equiv="Page-Enter" />
    <meta content="blendTrans(Duration=0.5)" http-equiv="Page-Exit" />
    <title></title>
    <link href="../../App_Themes/Default/Styles/Default.css" rel="stylesheet" type="text/css" />
    <script src="../../App_Themes/Default/Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <link href="../../App_Themes/TableSorter/Blue/style.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function getbacktostepone() {
            window.location = "RelocateNew.aspx";
        }
        function onSuccess() {
            setTimeout(okay, 2000);
        }
        function onError() {
            setTimeout(cancel, 2000);
        }
        function okay() {
            var UIMODE = $get('hdnWindowUIMODE').value;
            if (UIMODE == "EDIT")
                window.parent.document.getElementById('ButtonEditDone').click();
            else {
                window.parent.document.getElementById('ButtonNewDone').click();
                getbacktostepone();
            }
        }
        function cancel() {
            var UIMODE = $get('hdnWindowUIMODE').value;
            if (UIMODE == "EDIT")
                window.parent.document.getElementById('ButtonEditCancel').click();
            else
                window.parent.document.getElementById('ButtonNewCancel').click();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input type="hidden" value="" runat="server" id="hdnWindowUIMODE" />
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
    </asp:ScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                Stock Relocate
            </div>
            <div class="TitlebarRight" onclick="cancel();">
            </div>
        </div>
        <div class="popup_Body">
            <asp:MultiView ID="MultiViewparty" runat="server">
                <%--<asp:UpdatePanel id="updtpnl" runat="server">  </asp:UpdatePanel>--%>
                <asp:View ID="ViewInput" runat="server">
                    <asp:UpdatePanel ID="pnlNew" runat="server" Width="100%">
                        <contenttemplate>
                    <table width="100%" border="0" cellpadding="0" cellspacing="4">
                        <tbody>
                            <tr>
                                <td align="left" valign="top" style="width: 45%;">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                        <tbody>
                                            <tr>
                                                <td align="left" colspan="2">
                                                    <strong>Stock Relocation </strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="2" style="background-color: #000066; color: #FFFFFF; padding-left: 5px;">
                                                    <strong>General Information</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Party Code :
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblPartyCode" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Party Name :
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblpartyName" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Father Name :
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblfatherName" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" colspan="2" style="background-color: #000066; color: #FFFFFF; padding-left: 5px;">
                                                    <strong>Stock Information</strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Serial No :
                                                </td>
                                                <td align="left">
                                                    <asp:Label ID="lblSerialNo" runat="server" Text=""></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Chamber No :
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtchamber" runat="server" Text=""></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Floor No :
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtfloor" runat="server" Width="258px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Pocket No :
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtpocket" runat="server" Width="258px"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Line No :
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtlineno" runat="server" Width="258px"></asp:TextBox>
                                                    <%--  <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtlineno"
                                        CssClass="failureNotification" ErrorMessage="Line Number is required." ToolTip="Line Number is required."
                                        ValidationGroup="PartyValidationGroup"><img src="../../../App_Themes/Default/Images/Left_Arrow.png" 
                                alt="*" /></asp:RequiredFieldValidator>--%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Damaged Bags :
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtdamaged" runat="server" Width="258px" Text="0"></asp:TextBox>
                                                </td>
                                            </tr>       
                                            <tr>
                                                <td align="left">
                                                    Relocation No :
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtrelocno" runat="server" Width="258px" Text="1"></asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left">
                                                    Date :
                                                </td>
                                                <td align="left">
                                                    <asp:TextBox ID="txtDate" runat="server" Text='<%# DateTime.Now.ToShortDateString() %>'></asp:TextBox>
                                                    &nbsp;&nbsp;
                                                    <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server" Enabled="True"
                                                        TargetControlID="txtDate" PopupPosition="BottomLeft">
                                                    </cc1:CalendarExtender>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtDate"
                                                        CssClass="failureNotification" ErrorMessage="Relocation Date is required." ToolTip="Relocation Date is required."
                                                        ValidationGroup="SerialValidationGroup"><img src="../../../App_Themes/Default/Images/Left_Arrow.png" 
                                    alt="*" /></asp:RequiredFieldValidator>
                                                </td>
                                            </tr>
                                           
                                            <tr>
                                                    <td align="left" colspan="2">
                                                        <div class="feature-box-actionBar">
                                                            <span class="failureNotification">
                                                            <asp:Literal ID="lblFailure" runat="server"></asp:Literal>
                                                            </span>     
                                                            <asp:Button ID="btnEdit" runat="server" CssClass="button" 
                                                                OnClick="btnEdit_Click" Text="Edit" ValidationGroup="SerialValidationGroup" />
                                                            <asp:Button ID="btnSave" runat="server" CssClass="button" 
                                                                OnClick="btnSave_Click" Text="Save" ValidationGroup="SerialValidationGroup" />
                                                        </div>
                                                    </td>
                                            </tr>
                                           
                                            </tbody>
                                    </table>  
                                     </td>
                                <td align="left" style="width: 55%;" valign="top">
                                                    <table border="0" cellpadding="0" cellspacing="4" width="100%">
                                                        <tbody>
                                                            <tr>
                                                                <td align="right">
                                                                    Search Party by Name/Code/Serial &nbsp;
                                                                    <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                                                                    <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" 
                                                                        Height="16px" ImageUrl="~/App_Themes/Default/Images/gridview/Search.png" 
                                                                        OnClick="imgSearch_Click" ToolTip="Search" Width="16px" />
                                                                    &nbsp;
                                                                    <asp:ImageButton ID="imgRefresh" runat="server" CommandName="Refresh" 
                                                                        Height="16px" ImageUrl="~/App_Themes/Default/Images/gridview/Refresh.png" 
                                                                        OnClick="imgRefresh_Click" ToolTip="Refresh" Width="16px" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="left">
                                                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                        <tbody>
                                                                            <tr>
                                                                                <td align="left">
                                                                                    <asp:GridView ID="grvLoading" runat="server" AllowPaging="True" 
                                                                                        AutoGenerateColumns="False" CellPadding="4" CssClass="tablesorterBlue" 
                                                                                        DataKeyNames="PartyID" DataSourceID="dsLoading" 
                                                                                        EmptyDataText="No Records Found" HorizontalAlign="Left" 
                                                                                        OnPageIndexChanging="grvLoading_PageIndexChanging" 
                                                                                        OnRowCommand="grvLoading_RowCommand" OnRowDataBound="grvLoading_RowDataBound" 
                                                                                        PageSize="10" ShowHeaderWhenEmpty="true" Width="100%">
                                                                                        <Columns>
                                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Sl #" 
                                                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="3%" Visible="false">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblSl" runat="server" HorizontalAlign="Left" 
                                                                                                        Text='<%# Eval("SlNo") %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="LoadingID" 
                                                                                                ItemStyle-HorizontalAlign="Left" Visible="false">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblLoadingID" runat="server" HorizontalAlign="Left" 
                                                                                                        Text='<%# Eval("LoadingID") %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="2%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:ImageButton ID="imgselect" runat="server" CommandName="Select" 
                                                                                                        ImageUrl="~/App_Themes/Default/Images/gridview/select.png" ToolTip="Edit" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="কোড" 
                                                                                                ItemStyle-Width="4%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblPartyCode" runat="server" HorizontalAlign="Left" 
                                                                                                        Text='<%# HighlightText(Eval("PartyCode").ToString()) %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="নাম" 
                                                                                                ItemStyle-HorizontalAlign="Left">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblPartyName" runat="server" Font-Size="14px" 
                                                                                                        Text='<%# HighlightText(Eval("PartyName").ToString()) %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="মোবাইল নং" 
                                                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="6%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblContactNo" runat="server" HorizontalAlign="Left" 
                                                                                                        Text='<%# HighlightText(Eval("ContactNo").ToString()) %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="এস আর" 
                                                                                                ItemStyle-Width="6%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblSerialNo" runat="server" HorizontalAlign="Left" 
                                                                                                        Text='<%# HighlightText(Eval("SerialNo").ToString()) %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="তারিখ" 
                                                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="6%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblLoadedDate" runat="server" HorizontalAlign="Left" 
                                                                                                        Text='<%# Eval("LoadedDate","{0:dd/MM/yyyy}") %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="লট" 
                                                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="3%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblBags" runat="server" HorizontalAlign="Left" 
                                                                                                        Text='<%# Eval("Bags") %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="চেম্বার" 
                                                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="3%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblChamberNo" runat="server" HorizontalAlign="Left" 
                                                                                                        Text='<%# Eval("ChamberNo") %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="ফ্লোর" 
                                                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="2%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblFloor" runat="server" HorizontalAlign="Left" 
                                                                                                        Text='<%# Eval("Floor") %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="পকেট" 
                                                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="2%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblPocket" runat="server" HorizontalAlign="Left" 
                                                                                                        Text='<%# Eval("Pocket") %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="লাইন" 
                                                                                                ItemStyle-HorizontalAlign="Left" ItemStyle-Width="2%">
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="lblLine" runat="server" HorizontalAlign="Left" 
                                                                                                        Text='<%# Eval("Line") %>' />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <PagerStyle BackColor="#e6EEEE" Font-Bold="true" Font-Underline="false" 
                                                                                            HorizontalAlign="Right" />
                                                                                        <AlternatingRowStyle BackColor="#E5EAE8" />
                                                                                        <EditRowStyle BackColor="#999999" />
                                                                                        <EmptyDataRowStyle ForeColor="#CC0000" />
                                                                                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                                                                                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                                                    </asp:GridView>
                                                                                    <asp:SqlDataSource ID="dsLoading" runat="server" 
                                                                                        ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>" 
                                                                                        FilterExpression="PartyName LIKE '%{0}%' OR PartyCode LIKE '{1}%'  OR SerialNo LIKE '{2}%' OR ContactNo LIKE '{3}%'" SelectCommand="SELECT ROW_NUMBER() OVER (ORDER BY sl.LoadingID) As SlNo, sl.LoadingID, sl.SerialID, ss.SerialDate, ss.Bags, ss.SerialNo, ss.PartyID, ip.PartyType, ip.PartyCode, ip.PartyName, ip.FatherName, ip.ContactNo, 
                ip.AreaVillageName, ip.AreaPOName, sl.ChamberNo, sl.Floor, sl.Pocket, sl.Line, sl.Remarks, sl.Relocated, sl.RelocatedCount, sl.LoadedDate
                FROM INVStockLoading AS sl INNER JOIN
                INVStockSerial AS ss ON sl.SerialID = ss.SerialID INNER JOIN
                INVParty AS ip ON sl.PartyID = ip.PartyID ">
                                                                                        <FilterParameters>
                                                                                            <asp:ControlParameter ControlID="txtSearch" Name="PartyName" 
                                                                                                PropertyName="Text" />
                                                                                            <asp:ControlParameter ControlID="txtSearch" Name="PartyCode" 
                                                                                                PropertyName="Text" />
                                                                                            <asp:ControlParameter ControlID="txtSearch" Name="SerialNo" 
                                                                                                PropertyName="Text" />
                                                                                            <asp:ControlParameter ControlID="txtSearch" Name="ContactNo" 
                                                                                                PropertyName="Text" />
                                                                                        </FilterParameters>
                                                                                    </asp:SqlDataSource>
                                                                                </td>
                                                                            </tr>
                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                            </tr>
                        </tbody>
                    </table>
                    </contenttemplate>
                    </asp:UpdatePanel>
                </asp:View>
                <asp:View ID="ViewSuccess" runat="server">
                    <asp:Panel ID="pnlSuccess" runat="server" Width="100%">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td align="center">
                                        <span class="succesNotification">Loan Disburse Saved/Edited Successfully.
                                            <br />
                                            Dialog will Close automatically within 2 Seconds </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:Panel>
                </asp:View>
                <asp:View ID="ViewError" runat="server">
                    <asp:Panel ID="pnlError" runat="server" Width="100%">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td align="center" valign="middle">
                                        <span class="failureNotification">Error Occured Saving/Editing Loan Disburse<br />
                                            Dialog will Close automatically within 2 Seconds </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:Panel>
                </asp:View>
            </asp:MultiView>
        </div>
        <div class="popup_Buttons" style="display: none;">
            <asp:Button ID="btnOkay" Text="Done" runat="server" />
            <input id="btnCancel" value="Cancel" type="button" onclick="cancel();" />
        </div>
    </div>
    </form>
</body>
</html>
