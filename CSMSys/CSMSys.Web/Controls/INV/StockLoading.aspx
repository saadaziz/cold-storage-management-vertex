﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StockLoading.aspx.cs" Inherits="CSMSys.Web.Controls.INV.StockLoading" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta content="blendTrans(Duration=0.5)" http-equiv="Page-Enter" />
	<meta content="blendTrans(Duration=0.5)" http-equiv="Page-Exit" />
        <link href="../../../App_Themes/Default/Styles/Default.css" rel="stylesheet" type="text/css" />
<link href="../../../App_Themes/TableSorter/Blue/style.css" rel="stylesheet" type="text/css" />
    <link href="../../../App_Themes/TableSorter/Green/style.css" rel="stylesheet" type="text/css" />
    <title></title>

    <script language="javascript" type="text/javascript">
        function getbacktostepone() {
            window.location = "StockLoading.aspx";
        }
        function onSuccess() {
            setTimeout(okay, 2000);
        }
        function onError() {
            setTimeout(cancel, 2000);
        }
        function okay() {
            var UIMODE = $get('hdnWindowUIMODE').value;
            if (UIMODE == "EDIT")
                window.parent.document.getElementById('ButtonEditDone').click();
            else {
                window.parent.document.getElementById('ButtonNewDone').click();
                getbacktostepone();
            }
        }
        function cancel() {
            var UIMODE = $get('hdnWindowUIMODE').value;
            if (UIMODE == "EDIT")
                window.parent.document.getElementById('ButtonEditCancel').click();
            else
                window.parent.document.getElementById('ButtonNewCancel').click();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <input type="hidden" value="" runat="server" id="hdnWindowUIMODE" />
        <input type="hidden" value="" runat="server" id="hdnPartyID" />
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <div class="popup_Container">
            <div class="popup_Titlebar" id="PopupHeader">
                <div class="TitlebarLeft">
                    Stock Loading
                </div>
                <div class="TitlebarRight" onclick="cancel();"></div>
            </div>
            <div class="popup_Body">
                <asp:MultiView ID="MultiViewSerial" runat="server">
                    <asp:View ID="ViewInput" runat="server">
                        <asp:Panel ID="pnlNew" runat="server" width="100%">
                            <table width="100%" border="0" cellpadding="0" cellspacing="4">
			                    <tbody>
			                    <tr>
				                    <td align="left" valign="top" style="width:35%;">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                            <tbody>
                                                <tr>
                                                    <td align="left" colspan="2" style="background-color:#000066; color:#FFFFFF; padding-left:5px;">
                                                        <strong>General Information</strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 30%;">
                                                        Party Code:
                                                    </td>
                                                    <td align="left" style="width: 70%;">
                                                        <asp:Label ID="lblparty" runat="server" Enabled="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 30%;">
                                                        Serial No:
                                                    </td>
                                                    <td align="left" style="width: 70%;">
                                                        <asp:Label ID="lblSerialNo" runat="server" Enabled="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 30%;">
                                                        Bags :
                                                    </td>
                                                    <td align="left" style="width: 70%;">
                                                        <asp:Label ID="lblbags" runat="server" Enabled="false"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="2" style="background-color:#000066; color:#FFFFFF; padding-left:5px;">
                                                        <strong>Loading Information</strong>
                                                    </td>
                                                </tr>
			                                    <tr>
				                                    <td align="left">Date : </td>
				                                    <td align="left">
                                                        <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                                        &nbsp;&nbsp;
                                                        <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server" 
                                                            Enabled="True" TargetControlID="txtDate" PopupPosition="BottomLeft">
                                                        </cc1:CalendarExtender>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDate"
                                                        CssClass="failureNotification" ErrorMessage="Loading Date is required." ToolTip="Loading Date is required."
                                                        ValidationGroup="SerialValidationGroup"><img src="../../../App_Themes/Default/Images/Left_Arrow.png" 
                                                        alt="*" /></asp:RequiredFieldValidator>
                                                    </td>
			                                    </tr>
                                                <tr>
                                                    <td align="left">
                                                        Chamber No :
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtchamber" runat="server"></asp:TextBox>
                                                        &nbsp;&nbsp;
                                                        <asp:RequiredFieldValidator ID="rfValidator1" runat="server" ControlToValidate="txtchamber"
                                                            CssClass="failureNotification" ErrorMessage="Chamber No is required." ToolTip="Chamber No is required."
                                                            ValidationGroup="SerialValidationGroup"><img src="../../../App_Themes/Default/Images/Left_Arrow.png" 
                                                            alt="*" /></asp:RequiredFieldValidator>
                                                    
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        Floor No :
                                                    </td>
                                                    <td align="left">
                                                        <asp:TextBox ID="txtfloor" runat="server"></asp:TextBox>
                                                        &nbsp;&nbsp;
                                                        <asp:RequiredFieldValidator ID="rfValidator2" runat="server" ControlToValidate="txtfloor"
                                                            CssClass="failureNotification" ErrorMessage="Floor no is required." ToolTip="Floor No is required."
                                                            ValidationGroup="SerialValidationGroup"><img src="../../../App_Themes/Default/Images/Left_Arrow.png" 
                                                            alt="*" /></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 30%;">
                                                        Pocket No :
                                                    </td>
                                                    <td align="left" style="width: 70%;">
                                                        <asp:TextBox ID="txtpocket" runat="server"></asp:TextBox>
                                                        &nbsp;&nbsp;
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtpocket"
                                                            CssClass="failureNotification" ErrorMessage="Floor no is required." ToolTip="Floor No is required."
                                                            ValidationGroup="SerialValidationGroup"><img src="../../../App_Themes/Default/Images/Left_Arrow.png" 
                                                            alt="*" /></asp:RequiredFieldValidator>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 30%;">
                                                        Line No :
                                                    </td>
                                                    <td align="left" style="width: 70%;">
                                                        <asp:TextBox ID="txtLine" runat="server"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 30%;">
                                                        Remarks :
                                                    </td>
                                                    <td align="left" style="width: 70%;">
                                                        <asp:TextBox ID="txtRemarks" runat="server" TextMode="MultiLine" Width="258px"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" colspan="2">
                                                        <span class="failureNotification">
                                                            <asp:Literal ID="lblFailure" runat="server"></asp:Literal>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="right" colspan="2">
                                                        <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Load" ValidationGroup="SerialValidationGroup"
                                                            OnClick="btnSave_Click" />
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
				                    <td align="left" valign="top" style="width:65%;">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="4">
			                                <tbody>
			                                <tr>
				                                <td align="right">
                                                    Search by Name/Code/Father/Contact/SR No &nbsp;
                                                    <asp:TextBox ID="txtSearch" runat="server" ></asp:TextBox>
                                                    <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" ImageUrl="~/App_Themes/Default/Images/gridview/Search.png" ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch_Click" />
                                                    &nbsp;
                                                    <asp:ImageButton ID="imgRefresh" runat="server" CommandName="Refresh" ImageUrl="~/App_Themes/Default/Images/gridview/Refresh.png"
                                                        ToolTip="Refresh" Width="16px" Height="16px" OnClick="imgRefresh_Click" />
                                                </td>
			                                </tr>
			                                <tr>
				                                <td align="left">
                                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
			                                            <tbody>
			                                            <tr>
				                                            <td align="left">
                                                                <asp:GridView ID="grvParty" DataKeyNames="PartyID" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                    CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvParty_PageIndexChanging"
                                                                    ShowHeaderWhenEmpty="true" OnRowDataBound="grvParty_RowDataBound" OnRowCommand="grvParty_RowCommand"
                                                                    EmptyDataText="No Records Found" CssClass="tablesorterBlue" AllowPaging="True"
                                                                    PageSize="10" DataSourceID="dsParty">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                                                                            <ItemTemplate>
                                                                                <asp:ImageButton ID="imgselect" runat="server" CommandName="Select" ImageUrl="~/App_Themes/Default/Images/gridview/Select.png"
                                                                                    ToolTip="Select" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Sl #" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="2%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSl" Text='<%# Eval("SlNo") %>' runat="server" HorizontalAlign="Left" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>  
                                                                        <asp:TemplateField HeaderText="SerialID" Visible="false" HeaderStyle-HorizontalAlign="Left"
                                                                            ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSerialID" Text='<%# Eval("SerialID") %>' runat="server" HorizontalAlign="Left" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="এস আর" SortExpression="SerialNo" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblserno" Text='<%# HighlightText(Eval("SerialNo").ToString()) %>' runat="server" HorizontalAlign="Left" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="তারিখ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCreatedDate" Text='<%# Eval("SerialDate","{0:dd/MM/yyyy}") %>' runat="server" HorizontalAlign="Left" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="লট" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblbags" Text='<%# Eval("Bags") %>' runat="server" HorizontalAlign="Left" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="PartyID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPartyID" Text='<%# Eval("PartyID") %>' runat="server" HorizontalAlign="Left" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="কোড" SortExpression="PartyCode" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPartyCode" Text='<%# HighlightText(Eval("PartyCode").ToString()) %>' runat="server" HorizontalAlign="Left" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="নাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblPartyName" Text='<%# HighlightText( Eval("PartyName").ToString()) %>' runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="পিতার নাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblFatherName" Text='<%# HighlightText( Eval("FatherName").ToString()) %>' runat="server" HorizontalAlign="Left" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="মোবাইল নং" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="11%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblContactNo" Text='<%# HighlightText( Eval("ContactNo").ToString()) %>' runat="server" HorizontalAlign="Left" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="বস্তা লোড" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblloaded" Text="" runat="server" HorizontalAlign="Left" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                    <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                                                                    <AlternatingRowStyle BackColor="#E5EAE8" />
                                                                    <EditRowStyle BackColor="#999999" />
                                                                    <EmptyDataRowStyle ForeColor="#CC0000" />
                                                                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                                                                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                                </asp:GridView>
                                                                <asp:SqlDataSource ID="dsParty" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>"
                                                                    SelectCommand="SELECT ROW_NUMBER() OVER (ORDER BY ss.SerialID) As SlNo, ss.SerialID, ss.SerialDate, ss.SerialNo, ss.Bags, ss.PartyID, ip.PartyType, ip.PartyCode, ip.PartyName, ip.FatherName, ip.ContactNo, 
                                                                        ip.AreaVillageName, ip.AreaPOName, sl.LoadingID
                                                                        FROM INVParty AS ip INNER JOIN
                                                                        INVStockSerial AS ss ON ip.PartyID = ss.PartyID LEFT OUTER JOIN
                                                                        INVStockLoading AS sl ON ss.SerialID = sl.SerialID
                                                                        WHERE (sl.LoadingID IS NULL)
                                                                        ORDER BY ss.SerialID"
                                                                    FilterExpression="PartyName LIKE '%{0}%' OR PartyCode LIKE '{1}%'  OR SerialNo LIKE '{2}%' OR ContactNo LIKE '{3}%'">
                                                                    <FilterParameters>
                                                                        <asp:ControlParameter Name="PartyName" ControlID="txtSearch" PropertyName="Text" />
                                                                        <asp:ControlParameter Name="PartyCode" ControlID="txtSearch" PropertyName="Text" />
                                                                        <asp:ControlParameter Name="SerialNo" ControlID="txtSearch" PropertyName="Text" />
                                                                        <asp:ControlParameter Name="ContactNo" ControlID="txtSearch" PropertyName="Text" />
                                                                        <asp:ControlParameter Name="FatherName" ControlID="txtSearch" PropertyName="Text" />
                                                                    </FilterParameters>
                                                                </asp:SqlDataSource>
                                                            </td>
			                                            </tr>
			                                            </tbody>
		                                            </table>
                                                </td>
			                                </tr>
			                                </tbody>
		                                </table>
                                    </td>  
			                    </tr>
                                </tbody>
		                    </table>
                        </asp:Panel>
                    </asp:View>
                    <asp:View ID="ViewSuccess" runat="server">
                        <asp:Panel ID="pnlSuccess" runat="server" width="100%">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
			                    <tbody>
                                <tr>
                                    <td align="center">
                                        <span class="succesNotification">
                                            Stock Loading Saved/Edited Successfully. <br />
                                            Dialog will Close automatically within 2 Seconds
                                        </span>
                                    </td>
                                </tr>
			                    </tbody>
		                    </table>
                        </asp:Panel>
                    </asp:View>
                    <asp:View ID="ViewError" runat="server">
                        <asp:Panel ID="pnlError" runat="server" width="100%">
                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
			                    <tbody>
                                <tr>
                                    <td align="center" valign="middle">
                                        <span class="failureNotification">
                                            Error Occured Saving/Editing Stock Loading<br />
                                            Dialog will Close automatically within 2 Seconds
                                        </span>
                                    </td>
                                </tr>
			                    </tbody>
		                    </table>
                        </asp:Panel>
                    </asp:View>
                </asp:MultiView>
            </div>
            <div class="popup_Buttons" style="display:none;">
                <asp:Button ID="btnOkay" Text="Done" runat="server" OnClick="btnSave_Click" />
                <input id="btnCancel" value="Cancel" type="button" onclick="cancel();" />
            </div>
        </div>
    </form>
</body>
</html>
