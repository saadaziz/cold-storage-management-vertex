﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Unloading.aspx.cs" Inherits="CSMSys.Web.Controls.INV.Unloading" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta content="blendTrans(Duration=0.5)" http-equiv="Page-Enter" />
    <meta content="blendTrans(Duration=0.5)" http-equiv="Page-Exit" />
    <title></title>
    <link href="../../App_Themes/Default/Styles/Default.css" rel="stylesheet" type="text/css" />
    <script src="../../App_Themes/Default/Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <link href="../../App_Themes/TableSorter/Blue/style.css" rel="stylesheet" type="text/css" />
    <script language="javascript" type="text/javascript">
        function getbacktostepone() {
            window.location = "Unloading.aspx";
        }
        function onSuccess() {
            setTimeout(okay, 2000);
        }
        function onError() {
            setTimeout(cancel, 2000);
        }
        function okay() {
            var UIMODE = $get('hdnWindowUIMODE').value;
            if (UIMODE == "EDIT")
                window.parent.document.getElementById('ButtonEditDone').click();
            else {
                window.parent.document.getElementById('ButtonNewDone').click();
                getbacktostepone();
            }
        }
        function cancel() {
            var UIMODE = $get('hdnWindowUIMODE').value;
            if (UIMODE == "EDIT")
                window.parent.document.getElementById('ButtonEditCancel').click();
            else
                window.parent.document.getElementById('ButtonNewCancel').click();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input type="hidden" value="" runat="server" id="hdnWindowUIMODE" />
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                Stock Unload
            </div>
            <div class="TitlebarRight" onclick="cancel();">
            </div>
        </div>
        <div class="popup_Body">
           <asp:UpdatePanel ID="updtpanel" runat="server">
                    <ContentTemplate>  
            <asp:MultiView ID="MultiViewSerial" runat="server">
                <asp:View ID="ViewInput" runat="server">
                    <asp:Panel ID="pnlNew" runat="server" Width="100%">
                        
                        <table width="100%" border="0" cellpadding="0" cellspacing="4">
                            <tbody>
                                <tr>
                                    <td align="left" valign="top" style="width: 35%;">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                            <tbody>
                                                <tr>
                                                    <td align="left" colspan="2" style="background-color:#000066; color:#FFFFFF; padding-left:5px;">
                                                        <strong>General Information </strong>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" valign="top">
                                                        Party Type :
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblType" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        Party Name :
                                                    </td>
                                                    <td align="left">
                                                        <asp:Label ID="lblName" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 40%;">
                                                        Party Code:
                                                    </td>
                                                    <td align="left" style="width: 60%;">
                                                        <asp:Label ID="lblCode" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 40%;">
                                                        Serial No:
                                                    </td>
                                                    <td align="left" style="width: 60%;">
                                                        <asp:Label ID="lblserNo" runat="server"></asp:Label>
                                                    </td>
                                                </tr>
                                              
                                                <tr>
                                                    <td align="left" style="width: 40%;">
                                                        Bags:
                                                    </td>
                                                    <td align="left" style="width: 60%;">
                                                        <asp:TextBox ID="txtbags" runat="server" Text="0"></asp:TextBox>
                                                    </td>
                                                </tr>
                                                  <tr>
                                                    <td align="left" style="width: 40%;">
                                                        State:
                                                    </td>
                                                    <td align="left" style="width: 60%;">
                                                        <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStateIndexChanged">
                                                        <asp:ListItem Value="1">Chamber To PC</asp:ListItem>
                                                        <asp:ListItem Value="2">Chamber To Shed</asp:ListItem>
                                                        <asp:ListItem Value="3">Chamber To Delivery</asp:ListItem>
                                                        <asp:ListItem Value="4">PC To Shed</asp:ListItem>
                                                        <asp:ListItem Value="5">PC To Delivery</asp:ListItem>
                                                        <asp:ListItem Value="6">Shed To Delivery</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 40%;">
                                                        Shed Charge:
                                                    </td>
                                                    <td align="left" style="width: 60%;">
                                                        <asp:TextBox ID="txtshedCharge" runat="server" Text="0" Enabled="false"></asp:TextBox>
                                                        
                                                        <br />
                                                         <asp:Label ID="lblshedChargeInfo" runat="server" ></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="width: 40%;">
                                                        Date:
                                                    </td>
                                                    <td align="left" style="width: 60%;">
                                                         <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                                    &nbsp;&nbsp;
                                                    <cc1:CalendarExtender ID="txtDate_CalendarExtender" runat="server" 
                                                        Enabled="True" TargetControlID="txtDate" PopupPosition="BottomLeft">
                                                    </cc1:CalendarExtender>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" colspan="2">
                                                        <div class="feature-box-actionBar">
                                                            <span class="failureNotification">
                                                                <asp:Literal ID="lblFailure" runat="server"></asp:Literal>
                                                            </span>
                                                            <asp:Button ID="btnSave" runat="server" CssClass="button" Text="Disburse" ValidationGroup="SerialValidationGroup" OnClick="btnSave_click" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td align="left" valign="top" style="width: 65%;">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                            <tbody>
                                                <tr>
                                                    <td align="right">
                                                        Search Party by Name/Code/Contact &nbsp;
                                                        <asp:TextBox ID="txtSearch" runat="server"></asp:TextBox>
                                                        <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" ImageUrl="~/App_Themes/Default/Images/gridview/Search.png"
                                                            ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch_Click" />
                                                        &nbsp;
                                                        <asp:ImageButton ID="imgRefresh" runat="server" CommandName="Refresh" ImageUrl="~/App_Themes/Default/Images/gridview/Refresh.png"
                                                            ToolTip="Refresh" Width="16px" Height="16px" OnClick="imgRefresh_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td align="left">
                                                                        <asp:GridView ID="grvUnloading" DataKeyNames="CollectionDetailID" runat="server" Width="100%" AutoGenerateColumns="False"
                                                                            CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvUnloading_PageIndexChanging" ShowHeaderWhenEmpty="true" 
                                                                            OnRowDataBound="grvUnloading_RowDataBound" OnRowCommand="grvUnloading_RowCommand"
                                                                            EmptyDataText="No Records Found" CssClass="tablesorterBlue" DataSourceID    ="dsUnloading" AllowPaging="true" PageSize="10" >
                                                                            <Columns>
                                                                                <asp:TemplateField ItemStyle-Width="2%"  ItemStyle-HorizontalAlign="Center">
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="imgSelect" runat="server" CommandName="Select" ImageUrl="~/App_Themes/Default/Images/gridview/Select.png" ToolTip="Select" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="Sl #" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="3%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblSl" Text='<%# Eval("CollectionDetailID") %>' runat="server" HorizontalAlign="Left" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                
                                                                                <asp:TemplateField HeaderText="এস আর"  HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="7%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblSerialIDs" Text='<%# HighlightText(Eval("SerialNo").ToString()) %>' runat="server" HorizontalAlign="Left" />
                                                                                        <asp:Label ID="lblserial" Text='<%# Eval("SerialNo")  %>' runat="server" HorizontalAlign="Left"  Visible="false"/>
                                                                                   
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="PartyID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblPartyID" Text='<%# Eval("PartyID") %>' runat="server" HorizontalAlign="Left" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                
                                                                                <asp:TemplateField HeaderText="কোড" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="7%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblpc" Text='<%# (Eval("PartyCode")) %>' runat="server" HorizontalAlign="Left" Visible="false" />
                                                                                        <asp:Label ID="lblPartyCode" Text='<%# HighlightText(Eval("PartyCode").ToString()) %>' runat="server" HorizontalAlign="Left" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="নাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblpn" Text='<%# Eval("PartyName") %>' runat="server" Font-Size="12px" Visible="false" />
                                                                                        <asp:Label ID="lblPartyName" Text='<%# HighlightText(Eval("PartyName").ToString()) %>' runat="server" Font-Size="12px" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                 <asp:TemplateField HeaderText="পিতার নাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblFatherName" Text='<%# Eval("FatherName") %>' runat="server" Font-Size="12px" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                 
                                                                                <asp:TemplateField HeaderText="মোবাইল নং" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblContactNo" Text='<%# Eval("ContactNo") %>' runat="server" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                                <asp:TemplateField HeaderText="উত্তোলনকৃত বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                                                    <ItemTemplate>
                                                                                        <asp:Label ID="lblBags" Text='<%# Eval("withdrawnbags") %>' runat="server" HorizontalAlign="Left" />
                                                                                    </ItemTemplate>
                                                                                </asp:TemplateField>
                                                                               
                                                                                
                                                                            </Columns>
                                                                            <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                                                                            <AlternatingRowStyle BackColor="#E5EAE8" />
                                                                            <EditRowStyle BackColor="#999999" />
                                                                            <EmptyDataRowStyle ForeColor="#CC0000" />
                                                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                                                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                                                                        </asp:GridView>
                                                                        <asp:SqlDataSource ID="dsUnloading" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>" 
                                                                            SelectCommand="SELECT srvlcd.CollectionDetailID, srvlcd.SerialNo,srvlcd.SerialID,ip.PartyID,ip.PartyName,ip.PartyCode, ip.ContactNo,srvlcd.WBags as withdrawnbags,ip.FatherName,srvlcd.SerialID,srvlcd.CollectionDetailID,srvlcd.CollectionID
                                                                                from SRVLoanCollectionDetail as srvlcd
                                                                                INNER JOIN INVStockSerial as invss on invss.SerialID=srvlcd.SerialID
                                                                                INNER JOIN INVParty as ip on invss.PartyID=ip.PartyID" 
                                                                            FilterExpression="PartyName LIKE '%{0}%' OR ContactNo LIKE '{1}%' OR SerialNo LIKE '{2}%' OR PartyCode LIKE '{3}%' ">
                                                                            <FilterParameters>
                                                                                <asp:ControlParameter Name="PartyName" ControlID="txtSearch" PropertyName="Text" />
                                                                                <asp:ControlParameter Name="ContactNo" ControlID="txtSearch" PropertyName="Text" />
                                                                                <asp:ControlParameter Name="SerialNo" ControlID="txtSearch" PropertyName="Text" />
                                                                                <asp:ControlParameter Name="PartyCode" ControlID="txtSearch" PropertyName="Text" />
                                                                            </FilterParameters>
                                                                        </asp:SqlDataSource>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                    </asp:Panel>
                </asp:View>
                <asp:View ID="ViewSuccess" runat="server">
                    <asp:Panel ID="pnlSuccess" runat="server" Width="100%">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td align="center">
                                        <span class="succesNotification">Stock Unload Saved/Edited Successfully.
                                            <br />
                                            Dialog will Close automatically within 2 Seconds </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:Panel>
                </asp:View>
                <asp:View ID="ViewError" runat="server">
                    <asp:Panel ID="pnlError" runat="server" Width="100%">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td align="center" valign="middle">
                                        <span class="failureNotification">Error Occured Saving/Editing Stock Unloading<br />
                                            Dialog will Close automatically within 2 Seconds </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:Panel>
                </asp:View>
            </asp:MultiView>
            </ContentTemplate>
            </asp:UpdatePanel>
        </div>
        <div class="popup_Buttons" style="display: none;">
            <asp:Button ID="btnOkay" Text="Done" runat="server" />
            <input id="btnCancel" value="Cancel" type="button" onclick="cancel();" />
        </div>
    </div>
    </form>
</body>
</html>
