﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;       
using System.Text.RegularExpressions;
using CSMSys.Lib.Model;
using CSMSys.Lib.Manager.INV;
using CSMSys.Web.Utility;
using CSMSys.Lib.DataAccessLayer.Implementations;
using CSMSys.Lib.Manager.SRV;
namespace CSMSys.Web.Controls.INV
{
    public partial class RelocateNew : System.Web.UI.Page
    {
        #region private
        private string strSearch = string.Empty;
        private int LocationID
        {
            get
            {
                if (ViewState["LocationID"] == null)
                    ViewState["LocationID"] = -1;
                return (int)ViewState["LocationID"];
            }
            set
            {
                ViewState["LocationID"] = value;
            }
        }

        private int PartyID
        {
            get
            {
                if (ViewState["PartyID"] == null)
                    ViewState["PartyID"] = -1;
                return (int)ViewState["PartyID"];
            }
            set
            {
                ViewState["PartyID"] = value;
            }
        }

        private int SerialID
        {
            get
            {
                if (ViewState["SerialID"] == null)
                    ViewState["SerialID"] = -1;
                return (int)ViewState["SerialID"];
            }
            set
            {
                ViewState["SerialID"] = value;
            }
        }

        private int RelocID
        {
            get
            {
                if (ViewState["RelocID"] == null)
                    ViewState["RelocID"] = -1;
                return (int)ViewState["RelocID"];
            }
            set
            {
                ViewState["RelocID"] = value;
            }
        }

        private INVStockLoading _Loading;

        private enum UIMODE
        {
            NEW,
            EDIT
        }

        private UIMODE UIMode
        {
            get
            {
                if (ViewState["UIMODE"] == null)
                    ViewState["UIMODE"] = new UIMODE();
                return (UIMODE)ViewState["UIMODE"];
            }
            set
            {
                ViewState["UIMODE"] = value;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtDate.Text = System.DateTime.Today.ToShortDateString();
                string qsUIMODE = string.IsNullOrEmpty(Request.QueryString["UIMODE"]) ? "NEW" : Request.QueryString["UIMODE"];
                if (string.IsNullOrEmpty(qsUIMODE) == false)
                {
                    UIMode = (UIMODE)Enum.Parse(typeof(UIMODE), qsUIMODE);
                    RelocID = Convert.ToInt32(Request.QueryString["PID"]);
                    hdnWindowUIMODE.Value = UIMode.ToString();

                    if (UIMode == UIMODE.EDIT)
                    {
                        LoadToAllControlValueRelocate(RelocID);

                        grvLoading.Visible = false;
                        btnSave.Visible = false;
                        btnEdit.Visible = true;
                    }
                    else if (UIMode == UIMODE.NEW)
                    {
                        btnSave.Visible = true;

                        grvLoading.Visible = true;
                        btnEdit.Visible = false;
                    }
                }
                MultiViewparty.ActiveViewIndex = 0;
            }
        }
        #region edit
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {
                //if ((UIMode == UIMODE.NEW) || (UIMode == UIMODE.EDIT))
                //{
                if (!checkValidity()) return;

                editloc();
                //Thread.Sleep(2000);
                //}
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onSuccess();", true);
                MultiViewparty.ActiveViewIndex = 1;

            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onError();", true);
                MultiViewparty.ActiveViewIndex = 2;
            }
        }
        #endregion

        #region for save

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                //if ((UIMode == UIMODE.NEW) || (UIMode == UIMODE.EDIT))
                //{
                if (!checkValidity()) return;

                saveloc();
                //Thread.Sleep(2000);
                //}
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onSuccess();", true);
                MultiViewparty.ActiveViewIndex = 1;

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onError();", true);
                MultiViewparty.ActiveViewIndex = 2;
            }
        }
        private void editloc()
        {
            INVStockRelocate INVStockRelocate1 = new RelocateDAOLinq().PickByID(RelocID);
            INVStockRelocate INVStockRelocate = new INVStockRelocate();
            INVStockRelocate.RelocateID = INVStockRelocate1.RelocateID;
            INVStockRelocate.LoadingID = LocationID;
            INVStockRelocate.RelocatedCount = int.Parse(txtrelocno.Text);
            INVStockRelocate.CreatedDate = INVStockRelocate1.CreatedDate;
            INVStockRelocate.CreatedBy = INVStockRelocate1.CreatedBy;
            INVStockRelocate.ChamberNo = int.Parse(txtchamber.Text);
            INVStockRelocate.Floor = int.Parse(txtfloor.Text);
            INVStockRelocate.Pocket = (txtpocket.Text);
            INVStockRelocate.Line = txtlineno.Text;
            INVStockRelocate.SerialNo = INVStockRelocate1.SerialNo;
            INVStockRelocate.SerialID = INVStockRelocate1.SerialID;
            INVStockRelocate.PartyID = INVStockRelocate1.PartyID;
            INVStockRelocate.RelocateDate = DateTime.Parse(txtDate.Text);
            INVStockRelocate.Remarks = "";
            INVStockRelocate.Damaged = int.Parse(txtdamaged.Text);
            new RelocateManager().SaveRelocate(INVStockRelocate);
            //updateSRVRegistration(invStockLoading.SerialID);
            updateStockSerial(Convert.ToInt32(INVStockRelocate1.SerialID), int.Parse(txtdamaged.Text));
            ClearForm();
        }
        private void saveloc()
        {
            INVStockLoading invStockLoading = new LoadManager().GetLoadByID(LocationID);
            if (new LoadManager().SaveStockLoading(FormToObject(LocationID)))
            {
                INVStockRelocate INVStockRelocate = new INVStockRelocate();
                INVStockRelocate.LoadingID = LocationID;
                INVStockRelocate.RelocatedCount = int.Parse(txtrelocno.Text);
                INVStockRelocate.CreatedDate = DateTime.Now;
                INVStockRelocate.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
                INVStockRelocate.ChamberNo = invStockLoading.ChamberNo;
                INVStockRelocate.Floor = invStockLoading.Floor;
                INVStockRelocate.Pocket = invStockLoading.Pocket;
                INVStockRelocate.Line = invStockLoading.Line;
                INVStockRelocate.SerialNo = invStockLoading.SerialNo;
                INVStockRelocate.SerialID = invStockLoading.SerialID;
                INVStockRelocate.PartyID = invStockLoading.PartyID;
                INVStockRelocate.RelocateDate = DateTime.Parse(txtDate.Text);
                INVStockRelocate.Remarks = invStockLoading.Remarks;
                INVStockRelocate.Damaged = int.Parse(txtdamaged.Text);
                new RelocateManager().SaveRelocate(INVStockRelocate);
                //updateSRVRegistration(invStockLoading.SerialID);
                updateStockSerial(invStockLoading.SerialID, 0);
                ClearForm();

            }
        }
        private void updateStockSerial(long serid, int damaged)
        {
            SerialManager serdao = new SerialManager();
            INVStockSerial ss = serdao.GetSerialByID(Convert.ToInt32(serid));

            INVStockSerial _temp_serial = new INVStockSerial();

            _temp_serial.SerialID = ss.SerialID;
            _temp_serial.CreatedBy = ss.CreatedBy;
            _temp_serial.CreatedDate = ss.CreatedDate;


            _temp_serial.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
            _temp_serial.ModifiedDate = System.DateTime.Now;

            //_temp_serial.SerialID = id;

            _temp_serial.Serial = ss.Serial;
            _temp_serial.SerialNo = ss.SerialNo;
            _temp_serial.PartyCode = ss.PartyCode;
            _temp_serial.PartyID = ss.PartyID;
            _temp_serial.SerialDate = ss.SerialDate;
            _temp_serial.Remarks = ss.Remarks;


            _temp_serial.Bags = (damaged == 0) ? ss.Bags - float.Parse(txtdamaged.Text) : ss.Bags = ss.Bags + damaged - float.Parse(txtdamaged.Text);
            new SerialManager().SaveSerial(_temp_serial);
        }
        private void updateSRVRegistration(long serid)
        {
            RegistrationManager regdao = new RegistrationManager();
            SRVRegistration srvreg = new SRVRegistration();
            srvreg = regdao.GetAllRegistration().Where(u => u.SerialID == serid).Single();
            srvreg = regdao.GetRegistrationByID(Convert.ToInt32(srvreg.RegistrationID));
            SRVRegistration tsrv = new SRVRegistration();

            tsrv.RegistrationID = srvreg.RegistrationID;
            tsrv.CreatedBy = srvreg.CreatedBy;
            tsrv.CreatedDate = srvreg.CreatedDate;

            tsrv.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
            tsrv.ModifiedDate = DateTime.Now;

            // tsrv.RegistrationID = id;
            tsrv.RegistrationDate = srvreg.RegistrationDate;
            tsrv.SerialID = srvreg.SerialID;
            tsrv.PartyID = srvreg.PartyID;
            tsrv.CarryingLoan = srvreg.CarryingLoan;
            tsrv.Remarks = srvreg.Remarks;
            tsrv.SerialNo = srvreg.SerialNo;
            tsrv.Requisitioned = srvreg.Requisitioned;
            tsrv.BagLoan = srvreg.BagLoan;
            //tsrv.LoanDisbursed = srvreg.LoanDisbursed;

            tsrv.BagWeight = srvreg.BagWeight;
            tsrv.TMID = srvreg.TMID;

            tsrv.Bags = srvreg.Bags - int.Parse(txtdamaged.Text);
            regdao.SaveRegistration(tsrv);
        }
        private INVStockLoading FormToObject(int id)
        {
            INVStockLoading _tempLoading = new LoadManager().GetLoadByID(id);
            _tempLoading.Relocated = System.DateTime.Now;
            _tempLoading.RelocatedCount = (int.Parse(txtrelocno.Text));
            Int32 chamber = Int32.Parse(txtchamber.Text.ToString());
            _tempLoading.ChamberNo = chamber;
            _tempLoading.Floor = Int32.Parse(txtfloor.Text);
            _tempLoading.Pocket = (txtpocket.Text);
            _tempLoading.Line = txtlineno.Text;
            //_tempLoading.Remarks = txtremarks.Text.ToString();
            _tempLoading.Bags -= int.Parse(txtdamaged.Text);
            return _tempLoading;
        }
        #endregion
        #region Methods for Button

        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;
            grvLoading.DataBind();
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvLoading.DataBind();
        }
        #endregion

        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }

        private bool checkValidity()
        {
            if (string.IsNullOrEmpty(lblSerialNo.Text))
            {
                lblFailure.Text = "Serial No is Required";
                return false;
            }
            if (string.IsNullOrEmpty(txtDate.Text))
            {
                lblFailure.Text = "Date is Required";
                txtDate.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtchamber.Text))
            {
                lblFailure.Text = "Chamber Number is Required";
                txtchamber.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtfloor.Text))
            {
                lblFailure.Text = "Floor Number is Required";
                txtfloor.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtpocket.Text))
            {
                lblFailure.Text = "Pocket Number is Required";
                txtpocket.Focus();
                return false;
            }

            return true;
        }

        private void ClearForm()
        {
            txtchamber.Text = string.Empty;
            txtfloor.Text = string.Empty;
            txtdamaged.Text = string.Empty;
            txtpocket.Text = string.Empty;
            txtDate.Text = string.Empty;
            lblpartyName.Text = string.Empty;
            lblPartyCode.Text = string.Empty;
            lblfatherName.Text = string.Empty;
            lblSerialNo.Text = string.Empty;
            // txtremarks.Text = string.Empty;
        }
        INVStockRelocate _relocate = new INVStockRelocate();
        private void LoadToAllControlValueRelocate(int rid)
        {
            if (rid > 0)
            {
                _relocate = new RelocateDAOLinq().PickByID(rid);
                INVParty ip = new PartyManager().GetPartyByID(Convert.ToInt32(_relocate.PartyID));

                lblSerialNo.Text = _relocate.SerialNo;
                lblPartyCode.Text = ip.PartyCode;
                lblpartyName.Text = ip.PartyName;
                lblfatherName.Text = ip.FatherName;
                txtchamber.Text = _relocate.ChamberNo.ToString();
                txtfloor.Text = _relocate.Floor.ToString();
                txtpocket.Text = _relocate.Pocket.ToString();
                txtlineno.Text = _relocate.Line.ToString();
                txtdamaged.Text = _relocate.Damaged.ToString();
                txtrelocno.Text = _relocate.RelocatedCount.ToString();

            }
        }
        private void LoadToAllControlValue(int lid)
        {
            if (lid > 0)
            {
                LocationID = lid;
                _Loading = new LoadManager().GetLoadByID(lid);
                INVParty ip = new PartyManager().GetPartyByID(_Loading.PartyID);

                lblSerialNo.Text = _Loading.SerialNo;
                lblPartyCode.Text = ip.PartyCode;
                lblpartyName.Text = ip.PartyName;
                lblfatherName.Text = ip.FatherName;
                txtchamber.Text = _Loading.ChamberNo.ToString();
                txtfloor.Text = _Loading.Floor.ToString();
                txtpocket.Text = _Loading.Pocket.ToString();
                // txtremarks.Text = _Loading.Remarks;

            }
        }
        #endregion

        #region Methods For Grid
        protected void grvLoading_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                string text = e.CommandArgument.ToString();
                LoadToAllControlValue(int.Parse(text));

            }
        }

        protected void grvLoading_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvLoading.PageIndex = e.NewPageIndex;
            grvLoading.DataBind();
        }

        protected void grvLoading_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                ImageButton btnselect = (ImageButton)e.Row.FindControl("imgselect");
                btnselect.CommandArgument = DataBinder.Eval(e.Row.DataItem, "LoadingID").ToString();

                //btnselect.CommandArgument = (DataBinder.Eval(e.Row.DataItem, "SlNo")).ToString() + "@" +
                //    (DataBinder.Eval(e.Row.DataItem, "Bags")).ToString() + "@" +
                //      (DataBinder.Eval(e.Row.DataItem, "PartyCode")).ToString() + "@" +
                //    (DataBinder.Eval(e.Row.DataItem, "SerialNo")).ToString();

                //Label lblserial = (Label)e.Row.FindControl("lblSerialID");
                //Label lblloaded = (Label)e.Row.FindControl("lblloaded");
                //LoadManager lm = new LoadManager();
                //INVStockLoading sl = new INVStockLoading();
                //sl = lm.GetLoadBySerial(lblserial.Text);
                //lblloaded.Text = (sl == null) ? "No" : "Yes";

                //btnselect.CommandArgument = ((Label)e.Row.Cells[1].Controls[1]).Text;
            }
        }
        #endregion
    }
}