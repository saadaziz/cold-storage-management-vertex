﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Delivery.aspx.cs" Inherits="CSMSys.Web.Controls.SRV.Delivery" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta content="blendTrans(Duration=0.5)" http-equiv="Page-Enter" />
    <meta content="blendTrans(Duration=0.5)" http-equiv="Page-Exit" />
    <title></title>
    <link href="../../App_Themes/Default/Styles/Default.css" rel="stylesheet" type="text/css" />
    <link href="../../App_Themes/TableSorter/Green/style.css" rel="stylesheet" type="text/css" />
    <link href="../../App_Themes/TableSorter/Blue/style.css" rel="stylesheet" type="text/css" />
    <script src="../../App_Themes/Default/Scripts/jquery-1.4.1.min.js" type="text/javascript"></script>
    <script src="../../App_Themes/Default/Scripts/CalculateLoan.js" type="text/javascript"></script>

    <script type="text/javascript">
        // format current row
        function SelectRow(row) {
            var _selectColor = "#303030";
            var _normalColor = "#909090";
            var _selectFontSize = "3em";
            var _normalFontSize = "2em";
            // get all data rows - siblings to current
            var _rows = row.parentNode.childNodes;
            // deselect all data rows
            try {
                for (i = 0; i < _rows.length; i++) {
                    var _firstCell = _rows[i].getElementsByTagName("td")[0];
                    _firstCell.style.color = _normalColor;
                    _firstCell.style.fontSize = _normalFontSize;
                    _firstCell.style.fontWeight = "normal";
                }
            }
            catch (e) { }
            // select current row (formatting applied to first cell)
            var _selectedRowFirstCell = row.getElementsByTagName("td")[0];
            _selectedRowFirstCell.style.color = _selectColor;
            _selectedRowFirstCell.style.fontSize = _selectFontSize;
            _selectedRowFirstCell.style.fontWeight = "bold";
        }
    </script>

    <script language="javascript" type="text/javascript">
        //To allow numeric character only
        $('#txtPaid').keydown(function (event) {
            // Allow: backspace, delete, tab, escape, and enter
            if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            else {
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                    event.preventDefault();
                }
            }
        });
    </script>
    <script language="javascript" type="text/javascript">
        function enableTextBox(chk,txtW,txtE)  {
             if (chk.checked) {
                 txtW.disabled = false;
                 txtE.disabled = false;
             }
             else {
                 txtW.disabled = true;
                 txtE.disabled = true;
             }
          }
    </script>

    <script language="javascript" type="text/javascript">
        function getbacktostepone() {
            window.location = "LCollection.aspx";
        }
        function onSuccess() {
            setTimeout(okay, 2000);
        }
        function onError() {
            setTimeout(cancel, 2000);
        }
        function okay() {
            var UIMODE = $get('hdnWindowUIMODE').value;
            if (UIMODE == "EDIT")
                window.parent.document.getElementById('ButtonEditDone').click();
            else {
                window.parent.document.getElementById('ButtonNewDone').click();
                getbacktostepone();
            }
        }
        function cancel() {
            var UIMODE = $get('hdnWindowUIMODE').value;
            if (UIMODE == "EDIT")
                window.parent.document.getElementById('ButtonEditCancel').click();
            else
                window.parent.document.getElementById('ButtonNewCancel').click();
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <input type="hidden" value="" runat="server" id="hdnWindowUIMODE" />
    <input type="hidden" value="" runat="server" id="hdnPartyID" />
    <input type="hidden" value="" runat="server" id="hdn18Interest" />
    <input type="hidden" value="" runat="server" id="hdn4SCharge" />
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div class="popup_Container">
        <div class="popup_Titlebar" id="PopupHeader">
            <div class="TitlebarLeft">
                Loan Collection
            </div>
            <div class="TitlebarRight" onclick="cancel();">
            </div>
        </div>
        <div class="popup_Body">
            <asp:MultiView ID="MultiViewDelivery" runat="server">
                <asp:View ID="ViewInput" runat="server">
                    <asp:Panel ID="pnlNew" runat="server" Width="100%">
                        <table width="100%" border="0" cellpadding="0" cellspacing="4">
                            <tbody>
                                <tr>
                                    <td align="left" valign="top" style="width: 70%;">
                                        <asp:UpdatePanel ID="pnlupdate" runat="server" Width="100%">
                                            <ContentTemplate>   
                                                <table width="100%" border="0" cellpadding="0" cellspacing="4">
			                                        <tbody>
                                                    <tr>
                                                        <td align="left" colspan="4" style="background-color:#000066; color:#FFFFFF; padding-left:5px;">
                                                            <strong>Party Information</strong>
                                                        </td>
                                                    </tr>
			                                        <tr>
				                                        <td align="left" style="width:20%;">পার্টির নাম : </td>
				                                        <td align="left" style="Width:35%;">
                                                            <asp:Label ID="lblName" runat="server"></asp:Label>
                                                        </td>
				                                        <td align="left" style="width:25%;">পার্টির কোড :</td>
				                                        <td align="left" style="Width:20%;">
                                                            <asp:Label ID="lblCode" runat="server"></asp:Label>
                                                        </td>
			                                        </tr> 
                                                    <%--<tr>
                                                        <td colspan="4" align="left" style="background-color:#000066; color:#FFFFFF; padding-left:5px;">
                                                            <strong>দলিলের তথ্য</strong>
                                                        </td>
                                                    </tr>--%>
                                                    <tr>
                                                        <td colspan="4" align="left" valign="top">
                                                            <div style="overflow:auto; height:158px; border: 1px solid #996600;">
                                                                <asp:GridView ID="grvSerial" DataKeyNames="SerialID" runat="server" Width="98%" AutoGenerateColumns="False"
                                                                    CellPadding="4" ShowHeaderWhenEmpty="true" OnRowDataBound="grvSerial_RowDataBound"
                                                                    EmptyDataText="No Records Found" CssClass="tablesorterBlue" AllowPaging="False">
                                                                    <Columns>
                                                                        <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                                                                            <HeaderTemplate>
                                                                                <%--<asp:CheckBox ID="chkAll" runat="server" onclick="javascript:selectAll(this);" />--%>
                                                                            </HeaderTemplate>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="chkSelect" runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="SerialID" Visible="false" HeaderStyle-HorizontalAlign="Left"
                                                                            ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSerialID" Text='<%# Eval("SerialID") %>' runat="server" HorizontalAlign="Left" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="SR No" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="7%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblSerialNo" Text='<%# Eval("SerialNo") %>' runat="server" HorizontalAlign="Left" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="5%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblBags" Text='<%# Eval("Bags") %>' runat="server" CssClass="Bags" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="LoanID" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblLoanID" Text='<%# Eval("LoanID") %>' runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ঋণ প্রদান" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblLoanDate" Text='<%# Eval("LoanDate") %>' runat="server" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="দিন" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="3%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblDays" runat="server" CssClass="Days" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="LoanPerBag" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblLoanPerBag" runat="server" CssClass="LoanPerBag" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="উঃ বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="8%">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtWBags" runat="server" Text="0" Width="35px" Enabled="false" CssClass="txtWBags" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ঋণ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="8%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblLoan" runat="server" CssClass="Loan" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ইন্টারেস্ট" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="8%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbl18Interest" runat="server" CssClass="18Interest" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ইন্টারেস্ট" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="8%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lbl4SCharge" runat="server" CssClass="4SCharge" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ইন্টারেস্ট" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="8%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblInterest" runat="server" CssClass="Interest" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="ভাড়া" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="8%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblBagCharge" runat="server" CssClass="Rent" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="খালি বস্তা" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="8%">
                                                                            <ItemTemplate>
                                                                                <asp:TextBox ID="txtEmptyBags" runat="server" Text="0" Width="35px" Enabled="false" CssClass="txtEmptyBags" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="বস্তার নুল্য" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblBagPrice" runat="server" CssClass="BPrice" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="বস্তা ঋণ" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblBagLoan" runat="server" CssClass="BLoan" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="পরিবহন" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="7%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCarryingLoan" Text='<%# Eval("CarryingLoan","{0:N}") %>' runat="server" CssClass="CarryingLoan" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="পরিবহন" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="7%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCarryingPerBag" runat="server" CssClass="CarryingPerBag" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="পরিবহন" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="7%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblCarrying" runat="server" CssClass="Carrying" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="মোট" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                            ItemStyle-Width="10%">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lblLineTotal" runat="server" CssClass="LineTotal" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
				                                        <td align="left">
                                                            <asp:Button ID="btnCalculate" runat="server" onclick="btnCalculate_Click" 
                                                                Text="Calculate" />
                                                        </td>
				                                        <td align="left">
                                                            কালেকশন তাং : <asp:TextBox ID="txtDate" runat="server" ></asp:TextBox>
                                                            <asp:RequiredFieldValidator ID="rfValidator1" runat="server" ControlToValidate="txtDate"
                                                                CssClass="failureNotification" ErrorMessage="Date is required." ToolTip="Date is required."
                                                                ValidationGroup="SerialValidationGroup"><img src="../../App_Themes/Default/Images/Left_Arrow.png" 
                                                                alt="*" /></asp:RequiredFieldValidator>
                                                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDate" PopupPosition="BottomLeft" />
                                                        </td>
				                                        <td align="right">মোট উত্তোলণকৃত বস্তা : </td>
				                                        <td align="left">
                                                            <asp:Label ID="lblWBags" runat="server" Text="0" ></asp:Label>  
                                                        </td>
			                                        </tr> 
			                                        <tr>
				                                        <td align="right">বস্তা প্রতি : </td>
				                                        <td align="left">
                                                            টাকা <asp:Label ID="lblAmtPerBag" runat="server" Text="0"></asp:Label> &nbsp; হারে
                                                        </td>
				                                        <td align="right">ঋণ : টাকা </td>
				                                        <td align="left">
                                                            <asp:Label ID="lblTLoan" runat="server" Text="0"></asp:Label>
                                                        </td>
			                                        </tr>
			                                        <tr>
				                                        <td align="right" colspan="3">ইন্টারেস্ট : টাকা </td>
				                                        <td align="left">
				                                            <asp:Label ID="lblTInterest" runat="server" Text="0"></asp:Label>
                                                        </td>
			                                        </tr>
			                                        <tr>
				                                        <td align="right">বস্তা প্রতি : </td>
				                                        <td align="left">
                                                            টাকা <asp:Label ID="lblPricePerBag" runat="server" Text="0"></asp:Label> &nbsp; হারে
                                                        </td>
				                                        <td align="right">খালি বস্তা : টাকা </td>
				                                        <td align="left">
                                                            <asp:Label ID="lblTBagLoan" runat="server"></asp:Label>
                                                        </td>
			                                        </tr>
			                                        <tr>
				                                        <td align="right">বস্তা প্রতি : </td>
				                                        <td align="left">
                                                            টাকা <asp:Label ID="lblChargePerBag" runat="server" Text="0"></asp:Label> &nbsp; হারে
                                                            &nbsp;&nbsp;&nbsp;
                                                            <asp:Label ID="lblTEBags" runat="server" Text="0"></asp:Label> &nbsp; Bag
                                                        </td>
				                                        <td align="right">ভাড়া : টাকা </td>
				                                        <td align="left">
                                                            <asp:Label ID="lblTCharge" runat="server"></asp:Label>
                                                        </td>
			                                        </tr>
                                                    <tr>
				                                        <td align="right" colspan="3">বকেয়া : টাকা </td>
				                                        <td align="left">
                                                            <asp:Label ID="lblPreDues" runat="server"></asp:Label>
                                                        </td>
			                                        </tr>
                                                    <tr>
				                                        <td align="right" colspan="3">পরিবহন : টাকা </td>
				                                        <td align="left">
                                                            <asp:Label ID="lblTCarrying" runat="server"></asp:Label>
                                                        </td>
			                                        </tr>
                                                    <%--<tr>
				                                        <td align="right" colspan="3">মোট প্রদেয় : টাকা </td>
				                                        <td align="left">
                                                            <asp:Label ID="lblPayable" runat="server"></asp:Label>
                                                        </td>
			                                        </tr>--%>
                                                     <tr>
				                                        <td align="right" colspan="3">মোট প্রদেয় : টাকা </td>
				                                        <td align="left">
                                                            <asp:Label ID="lbltotalpayable" runat="server"></asp:Label>
                                                            <asp:Label ID="lblPayable" runat="server" Visible="false"></asp:Label>

                                                        </td>
			                                        </tr>
                                                    <tr>
				                                        <td align="right" colspan="3">প্রদান : টাকা </td>
				                                        <td align="left">
                                                            <asp:TextBox ID="txtPaid" runat="server" Text="0" Width="108px"  
                                                                AutoPostBack="true" ontextchanged="txtPaid_TextChanged"></asp:TextBox>
                                                        </td>
			                                        </tr>
                                                    <tr>
				                                        <td align="right">মন্তব্য: </td>
				                                        <td align="left">
                                                            <asp:TextBox ID="txtRemarks" runat="server" Width="255px"></asp:TextBox>
                                                        </td>
				                                        <td align="right">বকেয়া : টাকা </td>
				                                        <td align="left">
                                                            <asp:Label ID="lblDues" runat="server"></asp:Label>
                                                        </td>
			                                        </tr>
                                                    <tr>
                                                        <td align="left" colspan="4">
                                                            <div class="feature-box-actionBar">
                                                                <span class="failureNotification">
                                                                    <asp:Literal ID="lblFailure" runat="server"></asp:Literal>
                                                                </span>
                                                                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClick="btnSave_Click" />
                                                            </div>
                                                        </td>
                                                    </tr>
			                                        </tbody>
		                                        </table> 
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                    <td align="left" valign="top" style="width: 30%;">
                                        <table width="100%" border="0" cellpadding="0" cellspacing="4">
                                            <tbody>
                                                <tr>
                                                    <td align="right">
                                                        Search :
                                                        <asp:TextBox ID="txtSearch" runat="server" Width="107px"></asp:TextBox>
                                                    </td>
                                                    <td align="center" valign="bottom" style="width: 2%;">
                                                        <asp:ImageButton ID="imgSearch" runat="server" CommandName="Search" ImageUrl="~/App_Themes/Default/Images/gridview/Search.png"
                                                            ToolTip="Search" Width="16px" Height="16px" OnClick="imgSearch_Click" />
                                                    </td>
                                                    <td align="center" valign="bottom" style="width: 2%;">
                                                        <asp:ImageButton ID="imgRefresh" runat="server" CommandName="Refresh" ImageUrl="~/App_Themes/Default/Images/gridview/Refresh.png"
                                                            ToolTip="Refresh" Width="16px" Height="16px" OnClick="imgRefresh_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="center" valign="top" colspan="3">
                                                        <asp:GridView ID="grvParty" DataKeyNames="PartyID" runat="server" Width="100%"
                                                            AutoGenerateColumns="False" CellPadding="4" HorizontalAlign="Left" OnPageIndexChanging="grvParty_PageIndexChanging"
                                                            ShowHeaderWhenEmpty="true" OnRowDataBound="grvParty_RowDataBound" OnRowCommand="grvParty_RowCommand" 
                                                            EmptyDataText="No Records Found" CssClass="tablesorterBlue" AllowPaging="True"
                                                            PageSize="14" DataSourceID="dsParty">
                                                            <Columns>
                                                                <asp:TemplateField ItemStyle-Width="2%" ItemStyle-HorizontalAlign="Center">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="imgSelect" runat="server" CommandName="Select" ImageUrl="~/App_Themes/Default/Images/gridview/Select.png"
                                                                            ToolTip="Select" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="pid" Visible="false" HeaderStyle-HorizontalAlign="Left"
                                                                    ItemStyle-HorizontalAlign="Left" ItemStyle-Width="8%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPartyID" Text='<%# Eval("PartyID") %>' runat="server" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="কোড" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"
                                                                    ItemStyle-Width="12%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPartyCode" Text='<%# HighlightText(Eval("PartyCode").ToString()) %>'
                                                                            runat="server" HorizontalAlign="Left" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="নাম" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblPartyName" Text='<%# HighlightText(Eval("PartyName").ToString()) %>'
                                                                            runat="server" Font-Size="12px" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="গ্রাম" Visible="false" HeaderStyle-HorizontalAlign="Left">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblvill" Text='<%# HighlightText(Eval("AreaVillageName").ToString()) %>' runat="server" Font-Size="12px" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="মোবাইল নং" Visible="false" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="10%">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lblContactNo" Text='<%# HighlightText(Eval("ContactNo").ToString()) %>' runat="server" HorizontalAlign="Left" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <PagerStyle HorizontalAlign="Right" Font-Bold="true" Font-Underline="false" BackColor="#e6EEEE" />
                                                            <AlternatingRowStyle BackColor="#E5EAE8" />
                                                            <EditRowStyle BackColor="#999999" />
                                                            <EmptyDataRowStyle ForeColor="#CC0000" />
                                                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="#333333" />
                                                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#3333CC" />
                                                        </asp:GridView>
                                                        <asp:SqlDataSource ID="dsParty" runat="server" ConnectionString="<%$ ConnectionStrings:CSMSysConnection %>"
                                                            SelectCommand="SELECT ROW_NUMBER() OVER (ORDER BY PartyID) As SlNo, PartyID, PartyType, PartyCode, PartyName, FatherName, ContactNo, AreaVillageName
                                                                FROM INVParty WHERE (PartyID IN (SELECT PartyID FROM INVStockSerial WHERE (Bags > 0))) ORDER BY PartyID" 
                                                                FilterExpression="PartyCode = '{0}' or PartyName LIKE '%{1}%' OR ContactNo LIKE '%{2}%'">
                                                            <FilterParameters>
                                                                <asp:ControlParameter Name="PartyCode" ControlID="txtsearch" PropertyName="Text" />
                                                                <asp:ControlParameter Name="PartyName" ControlID="txtsearch" PropertyName="Text" />
                                                                <asp:ControlParameter Name="ContactNo" ControlID="txtsearch" PropertyName="Text" />
                                                            </FilterParameters>
                                                        </asp:SqlDataSource>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:Panel>
                </asp:View>
                <asp:View ID="ViewSuccess" runat="server">
                    <asp:Panel ID="pnlSuccess" runat="server" Width="100%">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td align="center">
                                        <span class="succesNotification">
                                            Collection/Delivery Saved/Edited Successfully.
                                            <br />
                                            Dialog will Close automatically within 2 Seconds </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:Panel>
                </asp:View>
                <asp:View ID="ViewError" runat="server">
                    <asp:Panel ID="pnlError" runat="server" Width="100%">
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tbody>
                                <tr>
                                    <td align="center" valign="middle">
                                        <span class="failureNotification">
                                            Error Occured Saving/Editing Collection/Delivery
                                            <br />
                                            Dialog will Close automatically within 2 Seconds </span>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:Panel>
                </asp:View>
            </asp:MultiView>
        </div>
        <div class="popup_Buttons" style="display: none;">
            <asp:Button ID="btnOkay" Text="Done" runat="server" />
            <input id="btnCancel" value="Cancel" type="button" onclick="cancel();" />
        </div>
    </div>
    </form>
</body>
</html>
