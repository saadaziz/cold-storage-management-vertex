﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.Administration.Application;
using CSMSys.Lib.Manager.INV;
using CSMSys.Lib.Manager.SRV;
using CSMSys.Lib.DataAccessLayer.Implementations;

using System.Data.SqlClient;
using System.Configuration;
using CSMSys.Lib.AccountingEntity;
using CSMSys.Lib.AccountingUtility;
using CSMSys.Lib.AccountingDataAccess;
using System.Web.Services;

namespace CSMSys.Web.Controls.SRV
{
    public partial class Registration : System.Web.UI.Page
    {
        #region Private Properties
        SqlConnection formCon = null;
        private string connstring = ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString;
        
        private enum UIMODE
        {
            NEW,
            EDIT
        }

        private UIMODE UIMode
        {
            get
            {
                if (ViewState["UIMODE"] == null)
                    ViewState["UIMODE"] = new UIMODE();
                return (UIMODE)ViewState["UIMODE"];
            }
            set
            {
                ViewState["UIMODE"] = value;
            }
        }

        private int TransactionID
        {
            get
            {
                if (ViewState["TransactionID"] == null)
                    ViewState["TransactionID"] = -1;
                return (int)ViewState["TransactionID"];
            }
            set
            {
                ViewState["TransactionID"] = value;
            }
        }

        private int ParentAccID
        {
            get
            {
                if (ViewState["ParentAccID"] == null)
                    ViewState["ParentAccID"] = -1;
                return (int)ViewState["ParentAccID"];
            }
            set
            {
                ViewState["ParentAccID"] = value;
            }
        }

        private int AccountID
        {
            get
            {
                if (ViewState["AccountID"] == null)
                    ViewState["AccountID"] = -1;
                return (int)ViewState["AccountID"];
            }
            set
            {
                ViewState["AccountID"] = value;
            }
        }

        private string AccountNo
        {
            get
            {
                if (ViewState["AccountNo"] == null)
                    ViewState["AccountNo"] = -1;
                return (string)ViewState["AccountNo"];
            }
            set
            {
                ViewState["AccountNo"] = value;
            }
        }
        
        private int RegistrationID
        {
            get
            {
                if (ViewState["RegistrationID"] == null)
                    ViewState["RegistrationID"] = -1;
                return (int)ViewState["RegistrationID"];
            }
            set
            {
                ViewState["RegistrationID"] = value;
            }
        }

        private int PartyID
        {
            get
            {
                if (ViewState["PartyID"] == null)
                    ViewState["PartyID"] = -1;
                return (int)ViewState["PartyID"];
            }
            set
            {
                ViewState["PartyID"] = value;
            }
        }

        private int SerialID
        {
            get
            {
                if (ViewState["SerialID"] == null)
                    ViewState["SerialID"] = -1;
                return (int)ViewState["SerialID"];
            }
            set
            {
                ViewState["SerialID"] = value;
            }
        }

        private SRVRegistration _Registration;
        private INVParty _Party;
        ComboData _ComboData = new ComboData();
        string strSearch = string.Empty;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UIUtility.BindBagDDL(ddlWeight, 0, false);
                lblAmountPerBag.Text = getAmountPerBags().ToString();

                string qsUIMODE = string.IsNullOrEmpty(Request.QueryString["UIMODE"]) ? "NEW" : Request.QueryString["UIMODE"];
                if (string.IsNullOrEmpty(qsUIMODE) == false)
                {
                    UIMode = (UIMODE)Enum.Parse(typeof(UIMODE), qsUIMODE);
                    RegistrationID = Convert.ToInt32(Request.QueryString["RID"]);
                    hdnWindowUIMODE.Value = UIMode.ToString();

                    if (UIMode == UIMODE.EDIT)
                    {
                        LoadToAllControlValue(RegistrationID);

                        pnlNew.Visible = true;
                        btnSave.Text = "Update";
                    }
                    else if (UIMode == UIMODE.NEW)
                    {
                        pnlNew.Visible = true;
                        btnSave.Text = "Save";

                        ddlWeight.SelectedIndex = 1;
                        lblRegistrationID.Text = ((new RegistrationManager().getNextRegistrationID())).ToString();
                    }
                }
                MultiViewRegistration.ActiveViewIndex = 0;
            }
        }

        #region Methods For Grid
        protected void grvStockSerial_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                string text = e.CommandArgument.ToString();
                string[] words = text.Split('@');
                string strSerialID = words[0].Trim().ToString();
                string strPartyID = words[1].Trim().ToString();

                PartyID = Convert.ToInt32(strPartyID);
                SerialID = Convert.ToInt32(strSerialID);

                if ((PartyID > 0) && (SerialID > 0))
                {
                    try
                    {
                        LoadToAllPartyAndSerialValue(PartyID, SerialID);
                        _Registration = new RegistrationManager().SearchSRVBySerialPartyID(SerialID, PartyID);
                        if (_Registration != null)
                        {
                            if (_Registration.RegistrationID > 0)
                            {
                                lblFailure.Text = "This SR Exists in Registration";
                                btnSave.Enabled = false;
                            }
                            else
                            {
                                btnSave.Enabled = true;
                            }
                        }
                        else
                        {
                            btnSave.Enabled = true;
                        }
                    }
                    catch (InvalidCastException err)
                    {
                        lblFailure.Text = err.ToString();
                    }
                }
            }
        }

        protected void grvStockSerial_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvStockSerial.PageIndex = e.NewPageIndex;
            grvStockSerial.DataBind();
        }

        protected void grvStockSerial_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                ImageButton btnEdit = (ImageButton)e.Row.FindControl("imgEdit");
                btnEdit.CommandArgument = ((Label)e.Row.FindControl("lblSerialID")).Text + "@" + ((Label)e.Row.FindControl("lblPartyID")).Text;

                //SerialID = int.Parse(((Label)e.Row.FindControl("lblSerialID")).Text);
                //PartyID = int.Parse(((Label)e.Row.FindControl("lblPartyID")).Text);
                //LoadToAllPartyAndSerialValue(PartyID, SerialID);

                //SRVRegistration srv = new SRVRegistration();
                //srv = FormToObject(0);

                //if (new RegistrationManager().SaveRegistration(srv))
                //{
                //    ClearForm();
                //}
            }
        }
        #endregion
        
        #region Methods
        private float getAmountPerBags()
        {
            SYSVariable _var = new VariableManager().SearchVariable("bag price");
            if (_var == null) return 0;

            float variable =_var.variable != null ? (float)_var.variable : 0;
            return variable;
        }
        
        private float getPartyAmountPerBags(int partyID)
        {
            float AmtPerBag = 0;
            INVParty _party = new PartyManager().GetPartyByID(partyID);
            string type = _party.PartyType;
            if (type.Contains("Farmer"))
            {
                int parentID = (int)_party.ParentID;
                _party = new PartyManager().GetPartyByID(parentID);

                type = _party.PartyType;
                if (!type.Contains("Company"))
                {
                    IList<SRVBagLoan> _bagLoan = new BagLoanManager().GetBagLoanByParty(parentID);
                    foreach (SRVBagLoan _bag in _bagLoan)
                    {
                        AmtPerBag = (float)_bag.AmountPerBag;
                    }
                }
            }

            return AmtPerBag;
        }

        private void ClearForm()
        {
            lblCode.Text = string.Empty;
            lblName.Text = string.Empty;
            lblAgreementNo.Text = string.Empty;
            lblPartyType.Text = string.Empty;
            txtCarryingCost.Text = "0";
            lblRegistrationID.Text = ((new RegistrationManager().getNextRegistrationID())).ToString();
            lblSRBags.Text = "0";

            btnSave.Enabled = false;
            txtCarryingCost.Enabled = false;
            txtRemarks.Enabled = false;
            grvStockSerial.DataBind();
        }

        private SRVRegistration FormToObject(int id)
        {
            SRVRegistration tsrv = new SRVRegistration();

            if (id > 0)
            {
                tsrv = new RegistrationDAOLinq().PickByID(id);
            }
            else
            {
                tsrv.CreatedDate = DateTime.Now;
                tsrv.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
            }

            tsrv.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
            tsrv.ModifiedDate = DateTime.Now;

            tsrv.RegistrationID = id;
            tsrv.RegistrationDate = DateTime.Parse(txtDate.Text);
            tsrv.SerialID = SerialID;
            tsrv.PartyID = PartyID;
            tsrv.CarryingLoan = string.IsNullOrEmpty(txtCarryingCost.Text) ? 0 : float.Parse(txtCarryingCost.Text);
            tsrv.Remarks = txtRemarks.Text;
            tsrv.SerialNo = lblAgreementNo.Text;
            tsrv.Requisitioned = "Not Applied";
            tsrv.BagLoan = string.IsNullOrEmpty(lblBagLoan.Text) ? 0 : Convert.ToInt32(lblBagLoan.Text);
            tsrv.LoanDate = null;
            tsrv.Bags = int.Parse(lblSRBags.Text);
            tsrv.BagWeight = ddlWeight.SelectedIndex <= 0 ? 85 : Convert.ToInt32(ddlWeight.SelectedItem.Text);
            tsrv.TMID = TransactionID;

            return tsrv;
        }

        private void LoadToAllControlValue(int intRegistrationID)
        {
            if (intRegistrationID > 0)
            {
                _Registration = new RegistrationManager().GetRegistrationByID(intRegistrationID);

                SerialID = (Int32)_Registration.SerialID;
                lblAgreementNo.Text = _Registration.SerialNo;
                lblRegistrationID.Text = _Registration.RegistrationID.ToString();
                txtDate.Text = DateTime.Parse(_Registration.RegistrationDate.ToString()).ToShortDateString();
                txtCarryingCost.Text = _Registration.CarryingLoan.ToString();
                txtEmptyBag.Text = _Registration.Bags.ToString();
                lblAmountPerBag.Text = _Registration.AmountPerBag.ToString();
                lblBagLoan.Text = _Registration.BagLoan.ToString();
                txtRemarks.Text = _Registration.Remarks;
                ddlWeight.SelectedValue = _Registration.BagWeight.ToString();

                TransactionID = string.IsNullOrEmpty(_Registration.TMID.ToString()) ? 0 : (Int32)_Registration.TMID;

                INVStockSerial _serial = new SerialManager().GetSerialByID(SerialID);

                PartyID = (Int32)_serial.PartyID;
                LoadToAllPartyAndSerialValue(PartyID, SerialID);
            }
        }

        private void LoadToAllPartyAndSerialValue(int intPartyID, int intSerialID)
        {
            if (intPartyID > 0)
            {
                _Party = new ReportManager().GetPartyByID(intPartyID);

                lblCode.Text = _Party.PartyCode;
                lblName.Text = _Party.PartyName;
                lblPartyType.Text = _Party.PartyType;
                AccountID = (Int32)_Party.AccountID;
                AccountNo = _Party.AccountNo;

                int groupID = (int)_Party.ParentID;

                INVParty _group = new ReportManager().GetPartyByID(groupID);
                if (_group != null)
                {
                    lblGroup.Text = string.IsNullOrEmpty(_group.PartyCode) ? string.Empty : _group.PartyCode;
                    ParentAccID = (Int32)_group.AccountID;
                }
                else
                {
                    lblGroup.Text = string.Empty;
                    ParentAccID = 0;
                }

                INVStockSerial _serial = new SerialManager().GetSerialByID(intSerialID);
                lblAgreementNo.Text = _serial.SerialNo;
                lblSRBags.Text = _serial.Bags.ToString();
                txtDate.Text = DateTime.Parse(_serial.SerialDate.ToString()).ToShortDateString();
                lblAmountPerBag.Text = getPartyAmountPerBags(intPartyID) > 0 ? getPartyAmountPerBags(intPartyID).ToString() : getAmountPerBags().ToString();
                txtCarryingCost.Text = "0";
                txtEmptyBag.Text = "0";
                lblBagLoan.Text = "0";

                string partyType = lblPartyType.Text;

                if (!partyType.Contains("Company"))
                {
                    if (!string.IsNullOrEmpty(lblGroup.Text))
                    {
                        INVParty _party = new PartyManager().SearchParentByCode(lblGroup.Text);
                        string type = _party.PartyType;

                        if (!type.Contains("Company"))
                        {
                            txtEmptyBag.Enabled = true;
                            txtCarryingCost.Enabled = false;
                        }
                        else
                        {
                            txtEmptyBag.Enabled = false;
                            txtCarryingCost.Enabled = true;
                        }
                    }
                }
                else
                {
                    txtEmptyBag.Enabled = false;
                    txtCarryingCost.Enabled = true;
                }

                lblFailure.Text = string.Empty;
            }
        }

        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtsearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }

        private bool checkValidity()
        {
            if (string.IsNullOrEmpty(txtDate.Text))
            {
                lblFailure.Text = "Date is Required";
                txtDate.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(lblCode.Text))
            {
                lblFailure.Text = "Select a Customer";
                lblCode.Focus();
                return false;
            }
            if (ddlWeight.SelectedIndex == 0)
            {
                lblFailure.Text = "Weight is Required";
                lblCode.Focus();
                return false;
            }
            if ((float.Parse(txtCarryingCost.Text) > 0) || (float.Parse(txtEmptyBag.Text) > 0))
            {
                float loanAmount = 0;
                string partyType = lblPartyType.Text;
                if (partyType.Contains("Farmer"))
                {
                    if (!string.IsNullOrEmpty(lblGroup.Text))
                    {
                        INVParty _party = new PartyManager().SearchParentByCode(lblGroup.Text);
                        string type = _party.PartyType;

                        if (!type.Contains("Company"))
                        {
                            if (float.Parse(txtCarryingCost.Text) > 0)
                            {
                                loanAmount = string.IsNullOrEmpty(txtCarryingCost.Text) ? 0 : float.Parse(txtCarryingCost.Text);
                                if (getAccountBalance(37) < loanAmount)
                                {
                                    lblFailure.Text = "Insufficient Balance";
                                    txtCarryingCost.Focus();
                                    return false;
                                }
                                else
                                {
                                    if (AccountID <= 0)
                                    {
                                        SaveLedger();
                                    }
                                }
                            }
                            else if (float.Parse(txtEmptyBag.Text) > 0)
                            {
                                loanAmount = string.IsNullOrEmpty(txtEmptyBag.Text) ? 0 : float.Parse(txtEmptyBag.Text);
                                if (getAccountBalance(ParentAccID) < loanAmount)
                                {
                                    lblFailure.Text = "Insufficient Balance";
                                    txtEmptyBag.Focus();
                                    return false;
                                }
                                else
                                {
                                    if (AccountID <= 0)
                                    {
                                        SaveLedger();
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (float.Parse(txtCarryingCost.Text) > 0)
                            {
                                loanAmount = string.IsNullOrEmpty(txtCarryingCost.Text) ? 0 : float.Parse(txtCarryingCost.Text);
                                if (getAccountBalance(37) < loanAmount)
                                {
                                    lblFailure.Text = "Insufficient Balance";
                                    txtCarryingCost.Focus();
                                    return false;
                                }
                                else
                                {
                                    if (AccountID <= 0)
                                    {
                                        SaveLedger();
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    if (float.Parse(txtCarryingCost.Text) > 0)
                    {
                        loanAmount = string.IsNullOrEmpty(txtCarryingCost.Text) ? 0 : float.Parse(txtCarryingCost.Text);
                        if (getAccountBalance(37) < loanAmount)
                        {
                            lblFailure.Text = "Insufficient Balance";
                            txtCarryingCost.Focus();
                            return false;
                        }
                        else
                        {
                            if (AccountID <= 0)
                            {
                                SaveLedger();
                            }
                        }
                    }
                }
            }
            return true;
        }

        private string getAccountNo()
        {
            formCon = new SqlConnection(connstring);
            formCon.Open();
            string accountNo = "0";

            accountNo = new DaAccount().GenerateAccountNo(formCon, 5);

            return accountNo;
        }

        private float getAccountBalance(int accountID)
        {
            float balance = 0;

            using (SqlConnection sqlConn = new SqlConnection(connstring))
            {
                sqlConn.Open();
                string _query = "SELECT [CurrentBalance] FROM [T_Account] WHERE [AccountID] = " + accountID;
                SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                if (sqlReader.Read())
                {
                    if (sqlReader.HasRows) balance = float.Parse(sqlReader["CurrentBalance"].ToString());
                }

                sqlReader.Close();
                sqlConn.Close();
            }
            return balance;
        }
        #endregion
        
        #region Methods For Button
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtsearch.Text;
            grvStockSerial.DataBind();
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtsearch.Text = string.Empty;
            grvStockSerial.DataBind();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            grvStockSerial.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if ((UIMode == UIMODE.NEW) || (UIMode == UIMODE.EDIT))
                {
                    if (!checkValidity()) return;
                    if ((float.Parse(txtCarryingCost.Text) > 0) || (float.Parse(txtEmptyBag.Text) > 0))
                    {
                        SaveLoanCrAccount();
                        SaveLoanDrAccount();
                    }

                    SaveRegistration();
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onSuccess();", true);
                MultiViewRegistration.ActiveViewIndex = 1;
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onError();", true);
                MultiViewRegistration.ActiveViewIndex = 2;
            }
        }

        protected void txtEmptyBag_TextChanged(object sender, EventArgs e)
        {
            float amtPerBag = string.IsNullOrEmpty(lblAmountPerBag.Text) ? 0 : float.Parse(lblAmountPerBag.Text.ToString());
            float bags = string.IsNullOrEmpty(txtEmptyBag.Text) ? 0 : float.Parse(txtEmptyBag.Text.ToString());
            float srBags = string.IsNullOrEmpty(lblSRBags.Text) ? 0 : float.Parse(lblSRBags.Text.ToString());

            if (bags > srBags)
            {
                lblFailure.Text = "No of Bag Loans >> SR Bags";
                txtEmptyBag.Focus();
            }
            else
            {
                lblBagLoan.Text = (amtPerBag * bags).ToString();
            }
        }
        #endregion

        #region Methods For Save
        private void SaveRegistration()
        {
            SRVRegistration srv = new SRVRegistration();
            srv = FormToObject(RegistrationID);

            if (new RegistrationManager().SaveRegistration(srv))
            {
                ClearForm();
            }
        }


        private void SaveLedger()
        {
            formCon = new SqlConnection(connstring);
            formCon.Open();
            try
            {
                int LdgrID = -1, AccID = -1;

                if (AccountID <= 0)
                {
                    Ledgers objLedger = CreateLedgerObject();
                    LdgrID = new DaLedger().InsertUpdateLedgers(objLedger, formCon);

                    Accounts objAccount = CreateAccountObject(LdgrID);
                    AccountID = new DaAccount().InsertUpdateAccounts(objAccount, formCon);
                }
                else
                {
                    new DaLedger().UpdateAccountID(formCon, LdgrID, AccID);
                    new DaAccount().UpdateLedgerID(formCon, AccID, LdgrID);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void SaveLoanCrAccount()
        {
            if (AccountID <= 0) return;

            formCon = new SqlConnection(connstring);
            formCon.Open();

            try
            {
                TransactionMaster objTM = new TransactionMaster();
                TransactionDetail objTD = new TransactionDetail();
                DaTransaction objDaTrans;
                float fltDrAmount = 0;
                float fltCrAmount = 0;
                int intTDID = 0;
                string comment = string.Empty;

                objTM = CreateTransMasterObject("Journal", "Cr");

                if (float.Parse(txtCarryingCost.Text) > 0)
                {
                    fltDrAmount = string.IsNullOrEmpty(txtCarryingCost.Text) ? 0 : Convert.ToInt32(txtCarryingCost.Text);
                    comment = "Carrying Loan from Cash for Carrying Loan";
                }
                //else if (float.Parse(txtEmptyBag.Text) > 0)
                //{
                //    string partyType = lblPartyType.Text;
                //    if (partyType.Contains("Farmer"))
                //    {
                //        if (!string.IsNullOrEmpty(lblGroup.Text))
                //        {
                //            INVParty _party = new PartyManager().SearchParentByCode(lblGroup.Text);
                //            string type = _party.PartyType;

                //            if (type.Contains("Company"))
                //            {
                //                //comment = "Bags Amount from Gunny Bags";
                //            }
                //            else
                //            {
                //                fltDrAmount = string.IsNullOrEmpty(txtEmptyBag.Text) ? 0 : Convert.ToInt32(int.Parse(txtEmptyBag.Text) * int.Parse(lblAmountPerBag.Text));

                //                comment = "Bags Amount from " + getAccountTitle(ParentAccID);
                //            }
                //        }
                //    }
                //    else
                //    {
                //        //comment = "Bags Amount from Gunny Bags";
                //    }
                //}
                //else if ((float.Parse(txtCarryingCost.Text) > 0) && (float.Parse(txtEmptyBag.Text) > 0))
                //{
                //    string partyType = lblPartyType.Text;
                //    if (partyType.Contains("Farmer"))
                //    {
                //        if (!string.IsNullOrEmpty(lblGroup.Text))
                //        {
                //            INVParty _party = new PartyManager().SearchParentByCode(lblGroup.Text);
                //            string type = _party.PartyType;

                //            if (type.Contains("Company"))
                //            {
                //                fltDrAmount = string.IsNullOrEmpty(txtCarryingCost.Text) && string.IsNullOrEmpty(txtEmptyBag.Text) ? 0 : Convert.ToInt32(txtCarryingCost.Text);
                //                comment = "Carrying Loan from Cash for Carrying Loan";
                //            }
                //            else
                //            {
                //                fltDrAmount = string.IsNullOrEmpty(txtCarryingCost.Text) && string.IsNullOrEmpty(txtEmptyBag.Text) ? 0 : Convert.ToInt32(txtCarryingCost.Text) + (Convert.ToInt32(int.Parse(txtEmptyBag.Text) * int.Parse(lblAmountPerBag.Text)));
                //                comment = "Carrying Loan from Cash for Carrying Loan and Bags Amount from " + getAccountTitle(ParentAccID);
                //            }
                //        }
                //    }
                //    else
                //    {
                //        fltDrAmount = string.IsNullOrEmpty(txtCarryingCost.Text) && string.IsNullOrEmpty(txtEmptyBag.Text) ? 0 : Convert.ToInt32(txtCarryingCost.Text);
                //        comment = "Carrying Loan from Cash for Carrying Loan";
                //    }
                //}

                intTDID = getTransDetailID(TransactionID, AccountID);

                objTD = CreateTransDetailObject(intTDID, TransactionID, AccountID, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), comment);

                objDaTrans = new DaTransaction();
                TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                formCon.Close();
            }
        }

        private void SaveLoanDrAccount()
        {
            formCon = new SqlConnection(connstring);
            formCon.Open();

            try
            {
                TransactionMaster objTM = new TransactionMaster();
                TransactionDetail objTD = new TransactionDetail();
                DaTransaction objDaTrans;
                float fltDrAmount = 0;
                float fltCrAmount = 0;
                int intTDID = 0;

                objTM = CreateTransMasterObject("Journal", "Dr");

                if (float.Parse(txtCarryingCost.Text) > 0)
                {
                    fltCrAmount = string.IsNullOrEmpty(txtCarryingCost.Text) ? 0 : Convert.ToInt32(txtCarryingCost.Text);
                    intTDID = getTransDetailID(TransactionID, 37);

                    objTD = CreateTransDetailObject(intTDID, TransactionID, 37, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), "Carrying Loan Amount to " + lblCode.Text + " - " + lblName.Text);

                    objDaTrans = new DaTransaction();
                    TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);
                }
                //if (float.Parse(txtEmptyBag.Text) > 0)
                //{
                //    string partyType = lblPartyType.Text;
                //    if (partyType.Contains("Farmer"))
                //    {
                //        if (!string.IsNullOrEmpty(lblGroup.Text))
                //        {
                //            INVParty _party = new PartyManager().SearchParentByCode(lblGroup.Text);
                //            string type = _party.PartyType;

                //            if (type.Contains("Company"))
                //            {
                //                //intTDID = getTransDetailID(TransactionID, 36);
                //                //objTD = CreateTransDetailObject(intTDID, TransactionID, 36, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), "Bags Amount Loan to " + lblCode.Text + " - " + lblName.Text);
                //            }
                //            else
                //            {
                //                fltCrAmount = string.IsNullOrEmpty(txtEmptyBag.Text) ? 0 : Convert.ToInt32(int.Parse(txtEmptyBag.Text) * int.Parse(lblAmountPerBag.Text));
                //                intTDID = getTransDetailID(TransactionID, ParentAccID);
                //                objTD = CreateTransDetailObject(intTDID, TransactionID, ParentAccID, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), "Bags Amount Loan to " + lblCode.Text + " - " + lblName.Text);

                //                objDaTrans = new DaTransaction();
                //                TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);
                //            }
                //        }
                //    }
                //    else
                //    {
                //        //intTDID = getTransDetailID(TransactionID, 36);
                //        //objTD = CreateTransDetailObject(intTDID, TransactionID, 36, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), "Bags Amount Loan to " + lblCode.Text + " - " + lblName.Text);
                //    }
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                formCon.Close();
            }
        }

        private int getTransDetailID(int TMID, int accountID)
        {
            int intTDID = 0;

            using (SqlConnection sqlConn = new SqlConnection(connstring))
            {
                sqlConn.Open();
                string _query = "SELECT [TransDID] FROM [T_Transaction_Detail] WHERE [AccountID] = " + accountID + " AND [TransMID] = " + TMID;
                SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                if (sqlReader.Read())
                {
                    if (sqlReader.HasRows) intTDID = int.Parse(sqlReader["TransDID"].ToString());
                }

                sqlReader.Close();
                sqlConn.Close();
            }
            return intTDID;
        }

        #region create objects
        private Accounts CreateAccountObject(int LdgrID)
        {
            Accounts objAcc = null;
            try
            {
                objAcc = new Accounts();
                objAcc.AccountID = AccountID;
                objAcc.AccountNo = getAccountNo();
                objAcc.AccountTitle = lblCode.Text.Trim() + " - " + lblName.Text.Trim();
                objAcc.AccountOrGroup = "Account";

                objAcc.AccountCreateDate = DateTime.Today;
                objAcc.AccountStatus = "Active";
                objAcc.OpeningBalance = 0;
                objAcc.IsInventoryRelated = 0;

                objAcc.LedgerTypeID = 2;
                objAcc.ParentID = 5;
                objAcc.AccountDepth = 2;
                objAcc.AccountNature = 1;


                objAcc.LedgerID = LdgrID;
                if (AccountID <= 0)
                {
                    objAcc.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
                    objAcc.CreatedDate = DateTime.Today;
                    objAcc.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    objAcc.ModifiedDate = DateTime.Today;
                }
                else
                {
                    objAcc.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    objAcc.ModifiedDate = DateTime.Today;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objAcc;
        }

        private Ledgers CreateLedgerObject()
        {
            Ledgers objLdgr = null;
            try
            {
                objLdgr = new Ledgers();

                objLdgr.LedgerID = 0;
                objLdgr.LedgerName = lblName.Text.Trim();
                objLdgr.LedgerTypeID = 2;
                objLdgr.Address = string.Empty;
                objLdgr.CountryID = 0;
                objLdgr.CurrencyID = 1;
                objLdgr.ContactPerson = lblName.Text.Trim();
                objLdgr.BankAccountType = "NULL";
                objLdgr.BusinessType = lblPartyType.Text;
                objLdgr.Phone = string.Empty;
                objLdgr.Fax = string.Empty;
                objLdgr.Email = string.Empty;
                objLdgr.TeamMemberID = -1;
                objLdgr.Remarks = string.Empty;
                objLdgr.AccountID = -1;

            }
            catch (Exception ex)
            {
                throw ex;
            }

            return objLdgr;
        }

        private TransactionMaster CreateTransMasterObject(string TabName, string accType)
        {
            TransactionMaster objTM = null;
            try
            {
                objTM = new TransactionMaster();

                if (TabName == "Debit")
                {
                    //objTM.TransactionMasterID = TransactionID;
                    //objTM.TransactionDate = DateTime.Today;
                    //objTM.VoucherNo = getVoucherNo(1);
                    //objTM.VoucherPayee = "";
                    //objTM.VoucherType = 1;
                    //objTM.TransactionMethodID = -1;
                    //objTM.MethodRefID = -1;
                    //objTM.MethodRefNo = string.Empty;
                    //objTM.TransactionDescription = "Carrying Loan to " + lblCode.Text + " - " + lblName.Text;
                    //objTM.Module = "Voucher";
                    //objTM.ApprovedBy = string.Empty;
                    //objTM.ApprovedDate = new DateTime(1900, 1, 1);

                    //if (objTM.TransactionMasterID <= 0)
                    //{
                    //    objTM.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.CreatedDate = DateTime.Today;
                    //    objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.ModifiedDate = DateTime.Today;
                    //}
                    //else
                    //{
                    //    objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.ModifiedDate = DateTime.Today;
                    //}
                }
                else if (TabName == "Credit")
                {
                    //objTM.TransactionMasterID = TransactionID;
                    //objTM.TransactionDate = DateTime.Parse(txtCrDate.Text);
                    //objTM.VoucherNo = txtCrVoucherNo.Text.Trim();
                    //objTM.VoucherPayee = "";
                    //objTM.VoucherType = VoucherType;
                    //objTM.TransactionMethodID = Convert.ToInt32(ddlCollMethod.SelectedValue);
                    //objTM.MethodRefID = -1;
                    //objTM.MethodRefNo = txtCrRefNo.Text.Trim();
                    //objTM.TransactionDescription = txtCrDesc.Text;
                    //objTM.Module = "Voucher";
                    //if (chkCrAppvBy.Checked)
                    //{
                    //    objTM.ApprovedBy = txtCrAppvBy.Text;
                    //    objTM.ApprovedDate = DateTime.Parse(txtCrAppvDate.Text);
                    //}
                    //else
                    //{
                    //    objTM.ApprovedBy = string.Empty;
                    //    objTM.ApprovedDate = new DateTime(1900, 1, 1);
                    //}

                    //if (objTM.TransactionMasterID <= 0)
                    //{
                    //    objTM.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.CreatedDate = DateTime.Today;
                    //    objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.ModifiedDate = DateTime.Today;
                    //}
                    //else
                    //{
                    //    objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.ModifiedDate = DateTime.Today;
                    //}
                }
                else if (TabName == "Journal")
                {
                    objTM.TransactionMasterID = TransactionID;
                    objTM.TransactionDate = string.IsNullOrEmpty(txtDate.Text) ? DateTime.Today : DateTime.Parse(txtDate.Text);
                    objTM.VoucherNo = getVoucherNo(3);
                    objTM.VoucherPayee = "";
                    objTM.VoucherType = 3;
                    objTM.TransactionMethodID = -1;
                    objTM.MethodRefID = -1;
                    objTM.MethodRefNo = string.Empty;

                    string partyType = lblPartyType.Text;
                    if (accType == "Cr")
                    {
                        if (float.Parse(txtCarryingCost.Text) > 0)
                        {
                            objTM.TransactionDescription = "Carrying Loan to " + lblCode.Text + " - " + lblName.Text;
                        }
                        //else if (float.Parse(txtEmptyBag.Text) > 0)
                        //{
                        //    objTM.TransactionDescription = "Bags Amount Loan to " + lblCode.Text + " - " + lblName.Text + " for " + txtEmptyBag.Text + " Bags";
                        //}
                        //else if ((float.Parse(txtEmptyBag.Text) > 0) && (float.Parse(txtCarryingCost.Text) > 0))
                        //{
                        //    objTM.TransactionDescription = "Carrying Loan and Bags Amount Loan to " + lblCode.Text + " - " + lblName.Text + " for " + txtEmptyBag.Text + " Bags";
                        //}
                    }
                    else if (accType == "Dr")
                    {
                        if (float.Parse(txtCarryingCost.Text) > 0)
                        {
                            objTM.TransactionDescription = "Carrying Loan from Cash for Carrying Loan to " + lblCode.Text + " - " + lblName.Text;
                        }
                        //else if (float.Parse(txtEmptyBag.Text) > 0)
                        //{
                        //    if (partyType.Contains("Farmer"))
                        //    {
                        //        if (!string.IsNullOrEmpty(lblGroup.Text))
                        //        {
                        //            INVParty _party = new PartyManager().SearchParentByCode(lblGroup.Text);
                        //            string type = _party.PartyType;

                        //            if (!type.Contains("Company"))
                        //            {
                        //                objTM.TransactionDescription = "Bags Amount from " + getAccountTitle(ParentAccID) + " to " + lblCode.Text + " - " + lblName.Text + " for " + txtEmptyBag.Text + " Bags";
                        //            }
                        //        }
                        //    }
                        //}
                        //else if ((float.Parse(txtEmptyBag.Text) > 0) && (float.Parse(txtCarryingCost.Text) > 0))
                        //{
                        //    if (partyType.Contains("Farmer"))
                        //    {
                        //        if (!string.IsNullOrEmpty(lblGroup.Text))
                        //        {
                        //            INVParty _party = new PartyManager().SearchParentByCode(lblGroup.Text);
                        //            string type = _party.PartyType;

                        //            if (!type.Contains("Company"))
                        //            {
                        //                objTM.TransactionDescription = "Carrying Loan from Cash for Carrying Loan and Bags Amount Loan from " + getAccountTitle(ParentAccID) + " to " + lblCode.Text + " - " + lblName.Text + " for " + txtEmptyBag.Text + " Bags";
                        //            }
                        //        }
                        //    }
                        //}
                    }

                    objTM.Module = "Voucher";
                    objTM.ApprovedBy = string.Empty;
                    objTM.ApprovedDate = new DateTime(1900, 1, 1);

                    if (objTM.TransactionMasterID <= 0)
                    {
                        objTM.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
                        objTM.CreatedDate = DateTime.Today;
                        objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                        objTM.ModifiedDate = DateTime.Today;
                    }
                    else
                    {
                        objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                        objTM.ModifiedDate = DateTime.Today;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objTM;
        }

        private TransactionDetail CreateTransDetailObject(int TDID, int TMID, int AccountID, double CrAmt, double DrAmt, string cmnt)
        {
            TransactionDetail objTD = null;
            try
            {
                objTD = new TransactionDetail();
                objTD.TransactionDetailID = TDID;
                objTD.TransactionMasterID = TMID;
                objTD.TransactionAccountID = AccountID;
                objTD.CreditAmount = CrAmt;
                objTD.DebitAmount = DrAmt;
                objTD.Comments = cmnt; // string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objTD;
        }
        #endregion

        private string getVoucherNo(int voucherType)
        {
            string voucherNo = string.Empty;

            using (SqlConnection sqlConn = new SqlConnection(connstring))
            {
                sqlConn.Open();
                string _query = "SELECT [VoucherNo] FROM [T_Transaction_Master] WHERE [VoucherType] = " + voucherType + " AND [TransMID] = (SELECT MAX([TransMID]) FROM [T_Transaction_Master] WHERE [VoucherType] = " + voucherType + ")";
                SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                if (sqlReader.Read())
                {
                    if (sqlReader.HasRows) voucherNo = sqlReader["VoucherNo"].ToString();

                    if (!string.IsNullOrEmpty(voucherNo))
                    {
                        int voucher = int.Parse(voucherNo.Substring(voucherNo.Length - 5)) + 1;
                        if (voucherType == 1) voucherNo = "D" + voucher.ToString("00000");
                        if (voucherType == 2) voucherNo = "C" + voucher.ToString("00000");
                        if (voucherType == 3) voucherNo = "J" + voucher.ToString("00000");
                    }
                    else
                    {
                        if (voucherType == 1) voucherNo = "D00001";
                        if (voucherType == 2) voucherNo = "C00001";
                        if (voucherType == 3) voucherNo = "J00001";
                    }
                }
                else
                {
                    if (voucherType == 1) voucherNo = "D00001";
                    if (voucherType == 2) voucherNo = "C00001";
                    if (voucherType == 3) voucherNo = "J00001";
                }

                sqlReader.Close();
                sqlConn.Close();
            }
            return voucherNo;
        }

        private string getAccountTitle(int accID)
        {
            string title = string.Empty;

            using (SqlConnection sqlConn = new SqlConnection(connstring))
            {
                sqlConn.Open();
                string _query = "SELECT [AccountTitle] FROM [dbo].[T_Account] WHERE [AccountID] = " + accID;
                SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                if (sqlReader.Read())
                {
                    if (sqlReader.HasRows) title = sqlReader["AccountTitle"].ToString();
                }

                sqlReader.Close();
                sqlConn.Close();
            }
            return title;
        }
        #endregion

        [WebMethod]
        public static string getLoanAmount(int bags)
        {
            SYSVariable _var = new VariableManager().SearchVariable("bag price");

            if (_var == null) return "0";

            float variable = _var.variable != null ? (float)_var.variable : 0;
            float loan = (float)bags * variable;
            string loanAmt = loan.ToString();
            return loanAmt;
        }
    }
}