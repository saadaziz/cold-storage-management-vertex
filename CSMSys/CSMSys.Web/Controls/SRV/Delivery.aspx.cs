﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.BusinessObjects;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.SRV;
using CSMSys.Lib.Manager.INV;
using CSMSys.Lib.Manager.Loan;
using CSMSys.Lib.Manager.Administration.Application;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CSMSys.Lib.AccountingEntity;
using CSMSys.Lib.AccountingUtility;
using CSMSys.Lib.AccountingDataAccess;
using System.Web.Services;
using CSMSys.Lib.DataAccessLayer.Implementations;

namespace CSMSys.Web.Controls.SRV
{
    public partial class Delivery : System.Web.UI.Page
    {
        #region Private Properties
        private string strSearch = string.Empty;
        private string strSerials = string.Empty;
        private int wBagsTotal = 0;
        private int eBagsTotal = 0;
        private float loanTotal = 0;
        private float Interest18Total = 0;
        private float SChargeTotal = 0;
        private float InterestTotal = 0;
        private float bLoanTotal = 0;
        private float RentTotal = 0;
        private float carryingTotal = 0;
        private float lineTotal = 0;

        SqlConnection formCon = null;
        LoanCollectionDetailManager lcdManager = new LoanCollectionDetailManager();
        private string connstring = ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString;

        private enum UIMODE
        {
            NEW,
            EDIT
        }

        private UIMODE UIMode
        {
            get
            {
                if (ViewState["UIMODE"] == null)
                    ViewState["UIMODE"] = new UIMODE();
                return (UIMODE)ViewState["UIMODE"];
            }
            set
            {
                ViewState["UIMODE"] = value;
            }
        }

        private int PartyID
        {
            get
            {
                if (ViewState["PartyID"] == null)
                    ViewState["PartyID"] = -1;
                return (int)ViewState["PartyID"];
            }
            set
            {
                ViewState["PartyID"] = value;
            }
        }

        private int CollectionID
        {
            get
            {
                if (ViewState["CollectionID"] == null)
                    ViewState["CollectionID"] = -1;
                return (int)ViewState["CollectionID"];
            }
            set
            {
                ViewState["CollectionID"] = value;
            }
        }

        private int DeliveryID
        {
            get
            {
                if (ViewState["DeliveryID"] == null)
                    ViewState["DeliveryID"] = -1;
                return (int)ViewState["DeliveryID"];
            }
            set
            {
                ViewState["DeliveryID"] = value;
            }
        }

        private int TransactionID
        {
            get
            {
                if (ViewState["TransactionID"] == null)
                    ViewState["TransactionID"] = -1;
                return (int)ViewState["TransactionID"];
            }
            set
            {
                ViewState["TransactionID"] = value;
            }
        }

        private int AccountID
        {
            get
            {
                if (ViewState["AccountID"] == null)
                    ViewState["AccountID"] = -1;
                return (int)ViewState["AccountID"];
            }
            set
            {
                ViewState["AccountID"] = value;
            }
        }

        private string AccountNo
        {
            get
            {
                if (ViewState["AccountNo"] == null)
                    ViewState["AccountNo"] = -1;
                return (string)ViewState["AccountNo"];
            }
            set
            {
                ViewState["AccountNo"] = value;
            }
        }

        private float CreditLimit
        {
            get
            {
                if (ViewState["CreditLimit"] == null)
                    ViewState["CreditLimit"] = -1;
                return (float)ViewState["CreditLimit"];
            }
            set
            {
                ViewState["CreditLimit"] = value;
            }
        }

        private float Credit
        {
            get
            {
                if (ViewState["Credit"] == null)
                    ViewState["Credit"] = -1;
                return (float)ViewState["Credit"];
            }
            set
            {
                ViewState["Credit"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtDate.Text = DateTime.Today.ToShortDateString();

                string qsUIMODE = string.IsNullOrEmpty(Request.QueryString["UIMODE"]) ? "NEW" : Request.QueryString["UIMODE"];
                if (string.IsNullOrEmpty(qsUIMODE) == false)
                {
                    UIMode = (UIMODE)Enum.Parse(typeof(UIMODE), qsUIMODE);
                    DeliveryID = Convert.ToInt32(Request.QueryString["DID"]);
                    hdnWindowUIMODE.Value = UIMode.ToString();

                    if (UIMode == UIMODE.EDIT)
                    {
                        LoadToAllControlValue(DeliveryID);

                        pnlNew.Visible = true;
                        btnSave.Text = "Update";
                    }
                    else if (UIMode == UIMODE.NEW)
                    {
                        pnlNew.Visible = true;
                        btnSave.Text = "Save";
                    }
                }
                MultiViewDelivery.ActiveViewIndex = 0;
                grvSerial.DataBind();

            }
        }
        IList<SRVLoanCollectionDetail> srvlcds = new List<SRVLoanCollectionDetail>();
        private void LoadToAllControlValue(int id)
        {
            SRVLoanCollection srvlc = new LoanCollectionManager().GetLoanCollectionByID(id);
            
            PartyID =Convert.ToInt32( srvlc.PartyID);
            if (PartyID > 0)
            {
                INVParty _party = new PartyManager().GetPartyByID(PartyID);

                if ((_party != null) && (_party.PartyID > 0))
                {
                    lblName.Text = _party.PartyName;
                    lblCode.Text = _party.PartyCode;
                    lblAmtPerBag.Text = string.Format("{0:N}", getLoanAmountPerBagsByParty(PartyID));

                    SYSVariable bag_rent = new VariableManager().SearchVariable("Potato Rent");
                    SYSVariable bag_price = new VariableManager().SearchVariable("Bag Price");
                    lblChargePerBag.Text = float.Parse(bag_rent.variable.ToString()).ToString();
                    lblPricePerBag.Text = float.Parse(bag_price.variable.ToString()).ToString();

                    LoanDetailByParty(PartyID);

                    CreditLimit = _party.CreditLimit != 0 ? (float)_party.CreditLimit : 0;
                    Credit = _party.Credit != 0 ? (float)_party.Credit : 0;
                    lblPreDues.Text = string.Format("{0:N}", Credit);
                    //lblFailure.Text = "You have already crossed Credit Limit which is " + CreditLimit.ToString();

                    //if (Credit >= CreditLimit)
                    //{
                    //    txtPaid.Enabled = false;
                    //    btnSave.Visible = false;
                    //    lblFailure.Text = "You have already crossed Credit Limit which is " + CreditLimit.ToString();
                    //}
                    //else
                    //{
                    //    txtPaid.Enabled = true;
                    //}
                    lblPayable.Text = srvlc.TotalAmount.ToString();
                    txtPaid.Text = srvlc.PaidAmount.ToString();
                    double PLoanAmtPerBag=0;
                    int WBags=0, EBags = 0;
                    srvlcds = new LoanCollectionDetailManager().GetByColID(Convert.ToInt32(srvlc.CollectionID));
                    foreach (SRVLoanCollectionDetail srvlcd in srvlcds)
                    {
                        PLoanAmtPerBag +=Convert.ToDouble( srvlcd.PLoanAmtPerBag);
                        WBags += Convert.ToInt32(srvlcd.WBags);
                        EBags += Convert.ToInt32(srvlcd.EBags);
                        //foreach (GridViewRow rw in grvSerial.Rows)
                        //{
                        //    Label lblSerialNo = (Label)rw.FindControl("lblSerialNo");
                        //    if (lblSerialNo != null)
                        //    {
                        //        if (lblSerialNo.Text == srvlcd.SerialNo)
                        //        {
                        //            CheckBox chkSelect = (CheckBox)rw.FindControl("chkSelect");
                        //            TextBox txtWBags = (TextBox)rw.FindControl("txtWBags");
                        //            TextBox txtEmptyBags = (TextBox)rw.FindControl("txtEmptyBags");
                        //            txtWBags.Enabled = true;
                        //            txtEmptyBags.Enabled = true;
                        //            chkSelect.Checked = true;
                        //        }
                        //    }
                        //}
                    } 
                    lblAmtPerBag.Text = PLoanAmtPerBag.ToString();
                    lblWBags.Text = WBags.ToString();
                    lblTBagLoan.Text = (bag_price.variable * EBags).ToString();
                }
            }
        }
        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }

        private void ClearForm()
        {
            //txtDate.Text = System.DateTime.Today.ToShortDateString();
            lblName.Text = string.Empty;
            lblTInterest.Text = string.Empty;
            lblTLoan.Text = string.Empty;
            lblWBags.Text = string.Empty;
            lblCode.Text = string.Empty;
            txtRemarks.Text = string.Empty;
        }

        private float getLoanAmountPerBagsByParty(int partyID)
        {
            float amtPerBag = 0;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);
            SqlCommand command = connection.CreateCommand();
            SqlDataReader Reader;
            string _query = "SELECT SUM(sld.LoanAmount) AS LoanAmount, SUM(slr.Bags) AS Bags, SUM(sld.LoanAmount) / SUM(slr.Bags) AS LoanAmtPerBag ";
            _query = _query + "FROM SRVLoanRequisition AS slr LEFT OUTER JOIN ";
            _query = _query + "SRVLoanDisburse AS sld ON slr.RequisitionID = sld.RequisitionID ";
            _query = _query + "WHERE (slr.PartyID = " + partyID + ")";
            command.CommandText = _query;
            connection.Open();
            Reader = command.ExecuteReader();
            //long log = 0;
            while (Reader.Read())
            {
                if (Reader["LoanAmtPerBag"] != DBNull.Value)
                    amtPerBag = float.Parse((Reader["LoanAmtPerBag"]).ToString());
                break;

            }
            Reader.Close();
            connection.Close();

            //string strSQL = "";
            return amtPerBag;

            //using (SqlConnection sqlConn = new SqlConnection(connstring))
            //{
            //    try{
            //    sqlConn.Open();
            //    string _query = "SELECT SUM(sld.LoanAmount) AS LoanAmount, SUM(slr.Bags) AS Bags, SUM(sld.LoanAmount) / SUM(slr.Bags) AS LoanAmtPerBag ";
            //        _query = _query + "FROM SRVLoanRequisition AS slr LEFT OUTER JOIN ";
            //        _query = _query + "SRVLoanDisburse AS sld ON slr.RequisitionID = sld.RequisitionID ";
            //        _query = _query + "WHERE (slr.PartyID = " + partyID + ")";
            //    SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
            //    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

            //    if (sqlReader.Read())
            //    {
                    
            //        amtPerBag =(sqlReader["LoanAmtPerBag"]!=null)? float.Parse(sqlReader["LoanAmtPerBag"].ToString()):0;
            //    }

            //    sqlReader.Close();
            //    sqlConn.Close();
            //    }catch(Exception ex)
            //    {
            //        amtPerBag=0;
            //    }
            //}
            //return amtPerBag;
        }
        #endregion

        #region Methods For Party Grid
        protected void grvParty_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                lblFailure.Text = string.Empty;

                string strID = e.CommandArgument.ToString();
                PartyID = int.Parse(strID);

                if (PartyID > 0)
                {
                    INVParty _party = new PartyManager().GetPartyByID(PartyID);

                    if ((_party != null) && (_party.PartyID > 0))
                    {
                        lblName.Text = _party.PartyName;
                        lblCode.Text = _party.PartyCode;
                        lblAmtPerBag.Text = string.Format("{0:N}", getLoanAmountPerBagsByParty(PartyID));

                        SYSVariable bag_rent = new VariableManager().SearchVariable("Potato Rent");
                        SYSVariable bag_price = new VariableManager().SearchVariable("Bag Price");
                        lblChargePerBag.Text = float.Parse(bag_rent.variable.ToString()).ToString();
                        lblPricePerBag.Text = float.Parse(bag_price.variable.ToString()).ToString();

                        LoanDetailByParty(PartyID);

                        CreditLimit = _party.CreditLimit != 0 ? (float)_party.CreditLimit : 0;
                        Credit = _party.Credit != 0 ? (float)_party.Credit : 0;
                        lblPreDues.Text = string.Format("{0:N}", Credit);
                        //lblFailure.Text = "You have already crossed Credit Limit which is " + CreditLimit.ToString();

                        //if (Credit >= CreditLimit)
                        //{
                        //    txtPaid.Enabled = false;
                        //    btnSave.Visible = false;

                        //}
                        //else
                        //{
                        //    txtPaid.Enabled = true;
                        //}
                    }
                }
            }
        }

        protected void grvParty_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvParty.PageIndex = e.NewPageIndex;
            grvParty.DataBind();
        }

        protected void grvParty_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                ImageButton btnSelect = (ImageButton)e.Row.FindControl("imgSelect");
                btnSelect.CommandArgument = ((Label)e.Row.FindControl("lblPartyID")).Text;

                e.Row.Attributes.Add("onClick", "javascript:void SelectRow(this);");
            }
        }
        #endregion

        #region Method for Detail Grid
        private void LoanDetailByParty(int partyID)
        {
            if (partyID > 0)
            {
                INVParty _party = new PartyManager().GetPartyByID(partyID);
                partyID = (_party != null) && (_party.ParentID > 0) ? (int)_party.ParentID : partyID;

                IList<SRVRegistration> _registrations = new RegistrationManager().GetAllRegistrationByParty(partyID);
                DataTable dtSR = new DataTable("Serial");
                dtSR.Columns.Add("SerialID", typeof(string));
                dtSR.Columns.Add("SerialNo", typeof(string));
                dtSR.Columns.Add("Bags", typeof(string));
                dtSR.Columns.Add("LoanID", typeof(string));
                dtSR.Columns.Add("LoanDate", typeof(string));
                dtSR.Columns.Add("CarryingLoan", typeof(string));

                foreach (SRVRegistration _reg in _registrations)
                {
                    //IList<SRVLoanCollectionDetail> allLoanCollectionDetail = new LoanCollectionDetailDAOLinq().getLoanCollectionListDetailByPartyIDSerialID(PartyID,Convert.ToInt32( _reg.SerialID));
                    //if (allLoanCollectionDetail.Count == 0)
                        dtSR.Rows.Add(new string[] { _reg.SerialID.ToString(), _reg.SerialNo, _reg.Bags.ToString(), (_reg.LoanID != null) ? _reg.LoanID.ToString() : "0", _reg.LoanDate != null ? DateTime.Parse(_reg.LoanDate.ToString()).ToShortDateString() : "", (_reg.CarryingLoan != null) ? _reg.CarryingLoan.ToString() : "0" });
                    //else
                    //{
                        //int bgcount = 0;
                        //foreach (SRVLoanCollectionDetail srvlcd in allLoanCollectionDetail)
                        //{
                        //    bgcount += Convert.ToInt32( srvlcd.WBags);
                        //}
                        //dtSR.Rows.Add(new string[] { _reg.SerialID.ToString(), _reg.SerialNo,( _reg.Bags-bgcount).ToString(), (_reg.LoanID != null) ? _reg.LoanID.ToString() : "0", _reg.LoanDate != null ? DateTime.Parse(_reg.LoanDate.ToString()).ToShortDateString() : "", (_reg.CarryingLoan != null) ? _reg.CarryingLoan.ToString() : "0" });

                    //}
                }

                grvSerial.DataSource = dtSR;
                grvSerial.DataBind();
            }
        }

        protected void grvSerial_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                CheckBox chkSelect = (CheckBox)e.Row.FindControl("chkSelect");
                if (chkSelect != null)
                {
                    TextBox txtWBags = (TextBox)e.Row.FindControl("txtWBags");
                    TextBox txtEmptyBags = (TextBox)e.Row.FindControl("txtEmptyBags");
                    List<SRVLoanCollectionDetail> tmp = srvlcds.Where(u => u.SerialNo == (DataBinder.Eval(e.Row.DataItem, "SerialNo").ToString())).ToList();
                    if (tmp.Count != 0)
                    {
                        chkSelect.Checked = true;
                        txtWBags.Enabled = true;
                        txtWBags.Text = tmp[0].WBags.ToString();
                        txtEmptyBags.Text = tmp[0].EBags.ToString();
                        txtEmptyBags.Enabled = true;
                    }
                    //else
                    chkSelect.Attributes.Add("onclick", string.Format("javascript:enableTextBox({0},{1},{2});", chkSelect.ClientID, txtWBags.ClientID, txtEmptyBags.ClientID));

                }

                Label lblLoanDate = (Label)e.Row.FindControl("lblLoanDate");
                if (lblLoanDate != null)
                {
                    Label lblDays = (Label)e.Row.FindControl("lblDays");
                    DateTime colDate = (lblLoanDate.Text != null) && (lblLoanDate.Text != "") ? DateTime.Parse(lblLoanDate.Text) : DateTime.Today;
                    DateTime loanDate = (txtDate.Text != null) ? DateTime.Parse(txtDate.Text) : DateTime.Today;
                    lblDays.Text = Math.Round((loanDate - colDate).TotalDays, 2).ToString();

                    Label lblLoanPerBag = (Label)e.Row.FindControl("lblLoanPerBag");
                    lblLoanPerBag.Text = Math.Round(float.Parse(lblAmtPerBag.Text), 2).ToString();
                }
            }
        }
        #endregion

        #region Methods For Button
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;
            grvParty.DataBind();
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvParty.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if ((UIMode == UIMODE.NEW) || (UIMode == UIMODE.EDIT))
                {
                    if (!checkValidity()) return;
                    if ((float.Parse(lblPayable.Text) > 0) || (float.Parse(txtPaid.Text) > 0))
                    {
                        SaveCollectionCrAccount();
                        SaveCollectionDrAccount();
                    }

                    SaveLoanCollectionAndDelivery();
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onSuccess();", true);
                MultiViewDelivery.ActiveViewIndex = 1;
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onError();", true);
                MultiViewDelivery.ActiveViewIndex = 2;
            }
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            SYSVariable interest_percentage = new VariableManager().SearchVariable("Interest");
            SYSVariable service_charge = new VariableManager().SearchVariable("Service Charge");
            SYSVariable bag_rent = new VariableManager().SearchVariable("Potato Rent");
            SYSVariable bag_price = new VariableManager().SearchVariable("Bag Price");

            foreach (GridViewRow gVRow in grvSerial.Rows)
            {
                CheckBox chkSelect = (CheckBox)gVRow.FindControl("chkSelect");
                if (chkSelect.Checked == true)
                {
                    TextBox txtWBags = (TextBox)gVRow.FindControl("txtWBags");
                    TextBox txtEmptyBags = (TextBox)gVRow.FindControl("txtEmptyBags");

                    Label lblDays = (Label)gVRow.FindControl("lblDays");
                    Label lblLoanPerBag = (Label)gVRow.FindControl("lblLoanPerBag");
                    Label lblLoan = (Label)gVRow.FindControl("lblLoan");
                    Label lbl18Interest = (Label)gVRow.FindControl("lbl18Interest");
                    Label lbl4SCharge = (Label)gVRow.FindControl("lbl4SCharge");
                    Label lblInterest = (Label)gVRow.FindControl("lblInterest");
                    Label lblbags = (Label)gVRow.FindControl("lblbags");

                    Label lblRent = (Label)gVRow.FindControl("lblBagCharge");
                    if (int.Parse(lblbags.Text) >= int.Parse(txtWBags.Text))
                    {
                        if ((lblLoanPerBag.Text != null) && (txtWBags.Text != "0"))
                        {
                            lblLoan.Text = Math.Round((float)(float.Parse(lblLoanPerBag.Text) * float.Parse(txtWBags.Text)), 2).ToString();
                            lbl18Interest.Text = Math.Round((float)((float)((float.Parse(interest_percentage.variable.ToString()) * float.Parse(lblLoan.Text) * float.Parse(lblDays.Text)) / (float)(100 * 365))), 2).ToString();
                            lbl4SCharge.Text = Math.Round((float)((float)((float.Parse(service_charge.variable.ToString())) * float.Parse(lblLoan.Text) * float.Parse(lblDays.Text)) / (float)(100 * 365)), 2).ToString();
                            lblInterest.Text = Math.Round((float)((float)((float.Parse(interest_percentage.variable.ToString()) + float.Parse(service_charge.variable.ToString())) * float.Parse(lblLoan.Text) * float.Parse(lblDays.Text)) / (float)(100 * 365)), 2).ToString();
                            lblRent.Text = (lblRent.Text != null) && (lblRent.Text != "0") ? Math.Round((float)(float.Parse(bag_rent.variable.ToString()) * float.Parse(txtWBags.Text)), 2).ToString() : "0";
                        }
                        else
                        {
                            lblLoan.Text = "0";
                            lbl18Interest.Text = "0";
                            lbl4SCharge.Text = "0";
                            lblInterest.Text = "0";
                            lblRent.Text = "0";
                        }

                        Label lblBags = (Label)gVRow.FindControl("lblBags");
                        Label lblCarrying = (Label)gVRow.FindControl("lblCarrying");
                        Label lblCarryingLoan = (Label)gVRow.FindControl("lblCarryingLoan");
                        if (lblCarryingLoan != null)
                        {
                            Label lblCarryingPerBag = (Label)gVRow.FindControl("lblCarryingPerBag");
                            lblCarryingPerBag.Text = Math.Round((float)(float.Parse(lblCarryingLoan.Text) / float.Parse(lblBags.Text)), 2).ToString();
                            lblCarrying.Text = Math.Round((float)(float.Parse(lblCarryingPerBag.Text) * float.Parse(txtWBags.Text)), 2).ToString();
                        }

                        Label lblBagPrice = (Label)gVRow.FindControl("lblBagPrice");
                        Label lblBagLoan = (Label)gVRow.FindControl("lblBagLoan");
                        Label lblLineTotal = (Label)gVRow.FindControl("lblLineTotal");

                        if ((lblRent.Text != null) && (txtEmptyBags.Text != "0"))
                        {
                            lblBagPrice.Text = Math.Round(float.Parse(bag_price.variable.ToString()), 2).ToString();
                            lblBagLoan.Text = Math.Round((float)(float.Parse(lblBagPrice.Text) * float.Parse(txtEmptyBags.Text)), 2).ToString();
                            lblLineTotal.Text = Math.Round((float)(float.Parse(lblLoan.Text) + float.Parse(lblInterest.Text) + float.Parse(lblRent.Text) + float.Parse(lblBagLoan.Text) + float.Parse(lblCarrying.Text)), 2).ToString();
                        }
                        else
                        {
                            lblBagPrice.Text = Math.Round(float.Parse(bag_price.variable.ToString()), 2).ToString();
                            lblBagLoan.Text = "0";
                            lblLineTotal.Text = "0";
                        }

                        wBagsTotal += (txtWBags.Text != "0") ? int.Parse(txtWBags.Text) : 0;
                        eBagsTotal += (txtEmptyBags.Text != "0") ? int.Parse(txtEmptyBags.Text) : 0;
                        loanTotal += (lblLoan.Text != "0") ? float.Parse(lblLoan.Text) : 0;
                        Interest18Total += (lbl18Interest.Text != "0") ? float.Parse(lbl18Interest.Text) : 0;
                        SChargeTotal += (lbl4SCharge.Text != "0") ? float.Parse(lbl4SCharge.Text) : 0;
                        InterestTotal += (lblInterest.Text != "0") ? float.Parse(lblInterest.Text) : 0;
                        RentTotal += (lblRent.Text != "0") ? float.Parse(lblRent.Text) : 0;
                        bLoanTotal += (lblBagLoan.Text != "0") ? float.Parse(lblBagLoan.Text) : 0;
                        carryingTotal += (lblCarrying.Text != "0") ? float.Parse(lblCarrying.Text) : 0;
                        lineTotal += (lblLineTotal.Text != "0") ? float.Parse(lblLineTotal.Text) : 0;

                        Label lblSerialNo = (Label)gVRow.FindControl("lblSerialNo");
                        strSerials += string.IsNullOrEmpty(strSerials) ? lblSerialNo.Text : ", " + lblSerialNo.Text;
                    }
                }
            }

            lblWBags.Text = wBagsTotal.ToString();
            lblTEBags.Text = eBagsTotal.ToString();
            lblTLoan.Text = loanTotal.ToString();   // String.Format("{0:N}", loanTotal);
            hdn18Interest.Value = Interest18Total.ToString();   // String.Format("{0:N}", Interest18Total);
            hdn4SCharge.Value = SChargeTotal.ToString();   // String.Format("{0:N}", SChargeTotal);
            lblTInterest.Text = InterestTotal.ToString();   // String.Format("{0:N}", InterestTotal);
            lblTCharge.Text = RentTotal.ToString();   // String.Format("{0:N}", RentTotal);
            lblTBagLoan.Text = bLoanTotal.ToString();   // String.Format("{0:N}", bLoanTotal);
            lblTCarrying.Text = carryingTotal.ToString();   // String.Format("{0:N}", carryingTotal);
            float preDues = (lblPreDues.Text != null) ? float.Parse(lblPreDues.Text) : 0;
            lblPayable.Text = (lineTotal + preDues).ToString();   // String.Format("{0:N}", (float)(lineTotal + preDues));
            //txtPaid.Text = lineTotal.ToString();   // String.Format("{0:N}", lineTotal);
            lbltotalpayable.Text = (float.Parse(lblTBagLoan.Text) + float.Parse(lblTCharge.Text) + float.Parse(lblPreDues.Text)
                + float.Parse(lblTCarrying.Text)+ float.Parse(lblTLoan.Text)+ float.Parse(lblTInterest.Text)).ToString();
            lblDues.Text = "0";
        }

        protected void txtPaid_TextChanged(object sender, EventArgs e)
        {
            float payable = (lbltotalpayable.Text != null) && (lbltotalpayable.Text != "0") ? float.Parse(lbltotalpayable.Text) : 0;
            float paid = (txtPaid.Text != null) && (txtPaid.Text != "0") ? float.Parse(txtPaid.Text) : 0;
            float dues = (float)(payable - paid);

            lblDues.Text = dues.ToString(); // String.Format("{0:N}", dues);
            if (dues > (CreditLimit - Credit))
            {
                lblFailure.Text = "Dues Amount Must be Less than " + (CreditLimit - Credit);
                txtPaid.Focus();
                btnSave.Visible = false;
            }
            else
                btnSave.Visible = true;

        }
        #endregion

        #region methods for save
        private bool checkValidity()
        {
            if (string.IsNullOrEmpty(lblName.Text))
            {
                lblFailure.Text = "Select Party from the List";
                return false;
            }
            if (string.IsNullOrEmpty(lblTInterest.Text) || string.IsNullOrEmpty(lblTLoan.Text) || string.IsNullOrEmpty(lblPayable.Text))
            {
                lblFailure.Text = "Press Calculate Button";
                btnCalculate.Focus();
                return false;
            }
            if (float.Parse(lblDues.Text) > (CreditLimit - Credit))
            {
                lblFailure.Text = "Dues Amount Must be Less than " + (CreditLimit - Credit);
                btnCalculate.Focus();
                return false;
            }

            lblFailure.Text = string.Empty;
            return true;
        }

        private void SaveLoanCollectionAndDelivery()
        {
            SRVLoanCollection slc = new SRVLoanCollection();
            if (DeliveryID != 0)
            {
                CollectionID = DeliveryID; 
            }
            slc = FormToCollectionObject(CollectionID);
            if (new LoanCollectionManager().SaveLoanCollection(slc))
            {
                //SRVLoanCollection lcm = new LoanCollectionManager().GetLoanCollectionByPartyDateSerials(PartyID, string.IsNullOrEmpty(txtDate.Text) ? DateTime.Today : DateTime.Parse(txtDate.Text), strSerials);
                //CollectionID = (slc.CollectionID != 0) && (lcm.CollectionID != null) ? (int)lcm.CollectionID : 0;
                //SaveLoanCollectionDetail(CollectionID);
                if (slc.CollectionID != 0)
                {
                    SaveLoanCollectionDetail(slc.CollectionID);
                        
                }
                if ((lblDues.Text != null) && (lblDues.Text != "0"))
                {
                    UpdateINVParty(PartyID, float.Parse(lblDues.Text));
                }
            }
        }

        #region Method for Accounts
        private void SaveCollectionCrAccount()
        {
            if (AccountID <= 0) return;

            formCon = new SqlConnection(connstring);
            formCon.Open();

            try
            {
                TransactionMaster objTM = new TransactionMaster();
                TransactionDetail objTD = new TransactionDetail();
                DaTransaction objDaTrans;
                float fltDrAmount = 0;
                float fltRlAmount = 0;
                float fltRdAmount = 0;
                float fltCrAmount = 0;
                int intTDID = 0;
                string comment = string.Empty;

                objTM = CreateTransMasterObject("Journal", "Cr");

                if (float.Parse(lblTLoan.Text) > 0)
                {
                    fltCrAmount = string.IsNullOrEmpty(lblTLoan.Text) ? 0 : Convert.ToInt32(lblTLoan.Text);
                    fltRlAmount = string.IsNullOrEmpty(lblDues.Text) ? 0 : Convert.ToInt32(lblDues.Text);
                    fltRdAmount = string.IsNullOrEmpty(lblPreDues.Text) ? 0 : Convert.ToInt32(lblPreDues.Text);
                    fltCrAmount = fltCrAmount + fltRdAmount - fltRlAmount;
                    intTDID = getTransDetailID(TransactionID, 35);

                    objTD = CreateTransDetailObject(intTDID, TransactionID, 35, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), "Potato Loan Amount Received from " + lblCode.Text + " - " + lblName.Text);

                    objDaTrans = new DaTransaction();
                    TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);

                    //Dues
                    if (!string.IsNullOrEmpty(lblDues.Text))
                    {
                        intTDID = getTransDetailID(TransactionID, AccountID);

                        objTD = CreateTransDetailObject(intTDID, TransactionID, AccountID, 0, Convert.ToDouble(fltRlAmount), "Dues Amount Receivable from " + lblCode.Text + " - " + lblName.Text);

                        objDaTrans = new DaTransaction();
                        TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);
                    }
                    if (!string.IsNullOrEmpty(lblPreDues.Text))
                    {
                        intTDID = getTransDetailID(TransactionID, AccountID);

                        objTD = CreateTransDetailObject(intTDID, TransactionID, AccountID, Convert.ToDouble(fltRdAmount), 0, "Dues Amount Received from " + lblCode.Text + " - " + lblName.Text);

                        objDaTrans = new DaTransaction();
                        TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);
                    }
                }
                if (float.Parse(lblTCarrying.Text) > 0)
                {
                    fltCrAmount = string.IsNullOrEmpty(lblTCarrying.Text) ? 0 : Convert.ToInt32(lblTCarrying.Text);
                    intTDID = getTransDetailID(TransactionID, 37);

                    objTD = CreateTransDetailObject(intTDID, TransactionID, 37, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), "Carrying Loan Amount Received from " + lblCode.Text + " - " + lblName.Text);

                    objDaTrans = new DaTransaction();
                    TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);
                }
                if (float.Parse(lblTBagLoan.Text) > 0)
                {
                    fltCrAmount = string.IsNullOrEmpty(lblTBagLoan.Text) ? 0 : float.Parse(lblTBagLoan.Text);
                    intTDID = getTransDetailID(TransactionID, 36);
                    objTD = CreateTransDetailObject(intTDID, TransactionID, 36, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), "Bags Loan Amount Received from " + lblCode.Text + " - " + lblName.Text);

                    objDaTrans = new DaTransaction();
                    TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);
                }

                //Rent, Interest and Service Charge
                fltCrAmount = 0;
                if (float.Parse(hdn18Interest.Value) > 0) fltCrAmount = fltCrAmount + (string.IsNullOrEmpty(hdn18Interest.Value) ? 0 : float.Parse(hdn18Interest.Value));
                if (float.Parse(hdn4SCharge.Value) > 0) fltCrAmount = fltCrAmount + (string.IsNullOrEmpty(hdn4SCharge.Value) ? 0 : float.Parse(hdn4SCharge.Value));
                if (float.Parse(lblTCharge.Text) > 0) fltCrAmount = fltCrAmount + (string.IsNullOrEmpty(lblTCharge.Text) ? 0 : float.Parse(lblTCharge.Text));

                intTDID = getTransDetailID(TransactionID, AccountID);
                objTD = CreateTransDetailObject(intTDID, TransactionID, AccountID, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), "Rent, Interest and Service Charge Amount Receivable from " + lblCode.Text + " - " + lblName.Text);

                objDaTrans = new DaTransaction();
                TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);

                fltCrAmount = 0;
                if (float.Parse(hdn18Interest.Value) > 0)
                {
                    fltCrAmount = string.IsNullOrEmpty(hdn18Interest.Value) ? 0 : float.Parse(hdn18Interest.Value);
                    intTDID = getTransDetailID(TransactionID, 218);
                    objTD = CreateTransDetailObject(intTDID, TransactionID, 218, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), "Interest Amount Received from " + lblCode.Text + " - " + lblName.Text);

                    objDaTrans = new DaTransaction();
                    TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);
                }
                if (float.Parse(hdn4SCharge.Value) > 0)
                {
                    fltCrAmount = string.IsNullOrEmpty(hdn4SCharge.Value) ? 0 : float.Parse(hdn4SCharge.Value);
                    intTDID = getTransDetailID(TransactionID, 219);
                    objTD = CreateTransDetailObject(intTDID, TransactionID, 219, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), "Service Charge Received from " + lblCode.Text + " - " + lblName.Text);

                    objDaTrans = new DaTransaction();
                    TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);
                }
                if (float.Parse(lblTCharge.Text) > 0)
                {
                    fltCrAmount = string.IsNullOrEmpty(lblTCharge.Text) ? 0 : float.Parse(lblTCharge.Text);
                    intTDID = getTransDetailID(TransactionID, 146);
                    objTD = CreateTransDetailObject(intTDID, TransactionID, 146, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), "Potato Rent Amount Received from " + lblCode.Text + " - " + lblName.Text);

                    objDaTrans = new DaTransaction();
                    TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                formCon.Close();
            }
        }

        private void SaveCollectionDrAccount()
        {
            formCon = new SqlConnection(connstring);
            formCon.Open();

            try
            {
                TransactionMaster objTM = new TransactionMaster();
                TransactionDetail objTD = new TransactionDetail();
                DaTransaction objDaTrans;
                float fltDrAmount = 0;
                float fltRlAmount = 0;
                float fltRdAmount = 0;
                float fltCrAmount = 0;
                int intTDID = 0;

                objTM = CreateTransMasterObject("Journal", "Dr");

                if (float.Parse(lblTLoan.Text) > 0)
                {
                    fltDrAmount = string.IsNullOrEmpty(lblTLoan.Text) ? 0 : float.Parse(lblTLoan.Text);
                    fltRlAmount = string.IsNullOrEmpty(lblDues.Text) ? 0 : float.Parse(lblDues.Text);
                    fltRdAmount = string.IsNullOrEmpty(lblPreDues.Text) ? 0 : float.Parse(lblPreDues.Text);
                    fltDrAmount = fltDrAmount + fltRdAmount - fltRlAmount;
                }
                if (float.Parse(lblTCarrying.Text) > 0)
                {
                    fltDrAmount = fltDrAmount + (string.IsNullOrEmpty(lblTCarrying.Text) ? 0 : Convert.ToInt32(lblTCarrying.Text));
                }
                if (float.Parse(lblTBagLoan.Text) > 0)
                {
                    fltDrAmount = fltDrAmount + (string.IsNullOrEmpty(lblTBagLoan.Text) ? 0 : float.Parse(lblTBagLoan.Text));
                }

                //Rent, Interest and Service Charge
                if (!string.IsNullOrEmpty(hdn18Interest.Value))
                {
                    fltDrAmount = fltDrAmount + (string.IsNullOrEmpty(hdn18Interest.Value) ? 0 : float.Parse(hdn18Interest.Value));
                }
                if (!string.IsNullOrEmpty(hdn4SCharge.Value))
                {
                    fltDrAmount = fltDrAmount + (string.IsNullOrEmpty(hdn4SCharge.Value) ? 0 : float.Parse(hdn4SCharge.Value));
                }
                if (!string.IsNullOrEmpty(lblTCharge.Text))
                {
                    fltDrAmount = fltDrAmount + (string.IsNullOrEmpty(lblTCharge.Text) ? 0 : float.Parse(lblTCharge.Text));
                }

                intTDID = getTransDetailID(TransactionID, AccountID);

                objTD = CreateTransDetailObject(intTDID, TransactionID, AccountID, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), "Loan, Rent, Interest and Service Charge Received from " + lblCode.Text + " - " + lblName.Text);

                objDaTrans = new DaTransaction();
                TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                formCon.Close();
            }
        }

        private float getAccountBalance(int accountID)
        {
            float balance = 0;

            using (SqlConnection sqlConn = new SqlConnection(connstring))
            {
                sqlConn.Open();
                string _query = "SELECT [CurrentBalance] FROM [T_Account] WHERE [AccountID] = " + accountID;
                SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                if (sqlReader.Read())
                {
                    if (sqlReader.HasRows) balance = float.Parse(sqlReader["CurrentBalance"].ToString());
                }

                sqlReader.Close();
                sqlConn.Close();
            }
            return balance;
        }

        private int getTransDetailID(int TMID, int accountID)
        {
            int intTDID = 0;

            using (SqlConnection sqlConn = new SqlConnection(connstring))
            {
                sqlConn.Open();
                string _query = "SELECT [TransDID] FROM [T_Transaction_Detail] WHERE [AccountID] = " + accountID + " AND [TransMID] = " + TMID;
                SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                if (sqlReader.Read())
                {
                    if (sqlReader.HasRows) intTDID = int.Parse(sqlReader["TransDID"].ToString());
                }

                sqlReader.Close();
                sqlConn.Close();
            }
            return intTDID;
        }

        #region create objects
        private TransactionMaster CreateTransMasterObject(string TabName, string accType)
        {
            TransactionMaster objTM = null;
            try
            {
                objTM = new TransactionMaster();

                if (TabName == "Debit")
                {
                    //objTM.TransactionMasterID = TransactionID;
                    //objTM.TransactionDate = DateTime.Parse(txtDrDate.Text);
                    //objTM.VoucherNo = txtDrVoucherNo.Text.Trim();
                    //objTM.VoucherPayee = "";
                    //objTM.VoucherType = VoucherType;
                    //objTM.TransactionMethodID = Convert.ToInt32(ddlPayMethod.SelectedValue);
                    //objTM.MethodRefID = -1;
                    //objTM.MethodRefNo = txtDrRefNo.Text.Trim();
                    //objTM.TransactionDescription = txtDrDesc.Text;
                    //objTM.Module = "Voucher";
                    //if (chkDrAppvBy.Checked)
                    //{
                    //    objTM.ApprovedBy = txtDrAppvBy.Text;
                    //    objTM.ApprovedDate = DateTime.Parse(txtDrAppvDate.Text);
                    //}
                    //else
                    //{
                    //    objTM.ApprovedBy = string.Empty;
                    //    objTM.ApprovedDate = new DateTime(1900, 1, 1);
                    //}

                    //if (objTM.TransactionMasterID <= 0)
                    //{
                    //    objTM.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.CreatedDate = DateTime.Today;
                    //    objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.ModifiedDate = DateTime.Today;
                    //}
                    //else
                    //{
                    //    objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.ModifiedDate = DateTime.Today;
                    //}

                }
                else if (TabName == "Credit")
                {
                    //objTM.TransactionMasterID = TransactionID;
                    //objTM.TransactionDate = DateTime.Parse(txtCrDate.Text);
                    //objTM.VoucherNo = txtCrVoucherNo.Text.Trim();
                    //objTM.VoucherPayee = "";
                    //objTM.VoucherType = VoucherType;
                    //objTM.TransactionMethodID = Convert.ToInt32(ddlCollMethod.SelectedValue);
                    //objTM.MethodRefID = -1;
                    //objTM.MethodRefNo = txtCrRefNo.Text.Trim();
                    //objTM.TransactionDescription = txtCrDesc.Text;
                    //objTM.Module = "Voucher";
                    //if (chkCrAppvBy.Checked)
                    //{
                    //    objTM.ApprovedBy = txtCrAppvBy.Text;
                    //    objTM.ApprovedDate = DateTime.Parse(txtCrAppvDate.Text);
                    //}
                    //else
                    //{
                    //    objTM.ApprovedBy = string.Empty;
                    //    objTM.ApprovedDate = new DateTime(1900, 1, 1);
                    //}

                    //if (objTM.TransactionMasterID <= 0)
                    //{
                    //    objTM.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.CreatedDate = DateTime.Today;
                    //    objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.ModifiedDate = DateTime.Today;
                    //}
                    //else
                    //{
                    //    objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.ModifiedDate = DateTime.Today;
                    //}
                }
                else if (TabName == "Journal")
                {
                    objTM.TransactionMasterID = TransactionID;
                    objTM.TransactionDate = string.IsNullOrEmpty(txtDate.Text) ? DateTime.Today : DateTime.Parse(txtDate.Text);
                    objTM.VoucherNo = getVoucherNo(3);
                    objTM.VoucherPayee = "";
                    objTM.VoucherType = 3;
                    objTM.TransactionMethodID = -1;
                    objTM.MethodRefID = -1;
                    objTM.MethodRefNo = string.Empty;

                    if (accType == "Cr")
                    {
                        objTM.TransactionDescription = "Potato Loan Amount from " + lblCode.Text + " - " + lblName.Text;
                    }
                    else if (accType == "Dr")
                    {
                        objTM.TransactionDescription = "Potato Loan Amount to Different Accounts from " + lblCode.Text + " - " + lblName.Text;
                    }

                    objTM.Module = "Voucher";
                    objTM.ApprovedBy = string.Empty;
                    objTM.ApprovedDate = new DateTime(1900, 1, 1);

                    if (objTM.TransactionMasterID <= 0)
                    {
                        objTM.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
                        objTM.CreatedDate = DateTime.Today;
                        objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                        objTM.ModifiedDate = DateTime.Today;
                    }
                    else
                    {
                        objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                        objTM.ModifiedDate = DateTime.Today;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objTM;
        }

        private TransactionDetail CreateTransDetailObject(int TDID, int TMID, int AccountID, double CrAmt, double DrAmt, string cmnt)
        {
            TransactionDetail objTD = null;
            try
            {
                objTD = new TransactionDetail();
                objTD.TransactionDetailID = TDID;
                objTD.TransactionMasterID = TMID;
                objTD.TransactionAccountID = AccountID;
                objTD.CreditAmount = CrAmt;
                objTD.DebitAmount = DrAmt;
                objTD.Comments = cmnt; // string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objTD;
        }
        #endregion

        private string getVoucherNo(int voucherType)
        {
            string voucherNo = string.Empty;

            using (SqlConnection sqlConn = new SqlConnection(connstring))
            {
                sqlConn.Open();
                string _query = "SELECT [VoucherNo] FROM [T_Transaction_Master] WHERE [VoucherType] = " + voucherType + " AND [TransMID] = (SELECT MAX([TransMID]) FROM [T_Transaction_Master] WHERE [VoucherType] = " + voucherType + ")";
                SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                if (sqlReader.Read())
                {
                    if (sqlReader.HasRows) voucherNo = sqlReader["VoucherNo"].ToString();

                    if (!string.IsNullOrEmpty(voucherNo))
                    {
                        int voucher = int.Parse(voucherNo.Substring(voucherNo.Length - 5)) + 1;
                        if (voucherType == 1) voucherNo = "D" + voucher.ToString("00000");
                        if (voucherType == 2) voucherNo = "C" + voucher.ToString("00000");
                        if (voucherType == 3) voucherNo = "J" + voucher.ToString("00000");
                    }
                    else
                    {
                        if (voucherType == 1) voucherNo = "D00001";
                        if (voucherType == 2) voucherNo = "C00001";
                        if (voucherType == 3) voucherNo = "J00001";
                    }
                }
                else
                {
                    if (voucherType == 1) voucherNo = "D00001";
                    if (voucherType == 2) voucherNo = "C00001";
                    if (voucherType == 3) voucherNo = "J00001";
                }

                sqlReader.Close();
                sqlConn.Close();
            }
            return voucherNo;
        }
        #endregion

        private void UpdateINVParty(int partyID, float credit)
        {
            INVParty _party = new INVParty();
            INVParty _oldParty = new PartyManager().GetPartyByID(partyID);

            _party.CreatedBy = _oldParty.CreatedBy;
            _party.CreatedDate = _oldParty.CreatedDate;
            _party.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
            _party.ModifiedDate = System.DateTime.Now;

            _party.CreditPerc = _oldParty.CreditPerc;
            _party.CreditLimit = _oldParty.CreditLimit;
            _party.Credit = credit;

            _party.PartyID = partyID;
            _party.bagcount = _oldParty.bagcount;
            _party.PartyCode = _oldParty.PartyCode;
            _party.PartyName = _oldParty.PartyName;
            _party.FatherName = _oldParty.FatherName;
            _party.PartyType = _oldParty.PartyType;
            _party.ContactNo = _oldParty.ContactNo;
            _party.Gender = string.Empty; //ddlGender.SelectedValue.ToString();
            _party.Religion = string.Empty; //ddlReligion.SelectedValue.ToString();
            _party.BloanPerc = _oldParty.BloanPerc;

            _party.AreaVillageName = _oldParty.AreaVillageName;
            _party.AreaPOName = _oldParty.AreaPOName;
            _party.UpazilaPSID = _oldParty.UpazilaPSID;
            _party.DistrictID = _oldParty.DistrictID;
            _party.ParentID = _oldParty.ParentID;
            _party.AccountID = _oldParty.AccountID;
            _party.AccountNo = _oldParty.AccountNo;

            _party.PartyCodeName = _oldParty.PartyCodeName;
            _party.IsActive = _oldParty.IsActive;

            new PartyManager().SaveParty(_party);
        }

        private void UpdateINVStockLoading(int serialID, int remBags)
        {
            INVStockLoading _Loading = new INVStockLoading();
            INVStockLoading _stockLoading = new LoadManager().GetLoadingBySerialID(serialID);

            _Loading.RelocatedCount = _stockLoading.RelocatedCount;

            _Loading.LoadingID = _stockLoading.LoadingID;
            _Loading.PartyID = _stockLoading.PartyID;
            _Loading.Bags = float.Parse(remBags.ToString());
            _Loading.SerialNo = _stockLoading.SerialNo;
            _Loading.SerialID = serialID;
            _Loading.ChamberNo = _stockLoading.ChamberNo;
            _Loading.Floor = _stockLoading.Floor;
            _Loading.Pocket = _stockLoading.Pocket;
            _Loading.Line = _stockLoading.Line;
            _Loading.Remarks = txtRemarks.Text.ToString();
            _Loading.LoadedDate = _stockLoading.LoadedDate;

            new LoadManager().SaveStockLoading(_Loading);
        }

        private bool UpdateINVStockSerial(int serialID, int remBags, float remBagLoan)
        {
            SRVRegistration _sReg = new SRVRegistration();
            SRVRegistration _preSReg = new RegistrationManager().GetRegistrationBySerialID(serialID);

            _sReg.CreatedBy = _preSReg.CreatedBy;
            _sReg.CreatedDate = _preSReg.CreatedDate;
            _sReg.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
            _sReg.ModifiedDate = DateTime.Now;

            _sReg.RegistrationID = _preSReg.RegistrationID;
            _sReg.RegistrationDate = _preSReg.RegistrationDate;
            _sReg.SerialID = serialID;
            _sReg.PartyID = _preSReg.PartyID;
            _sReg.CarryingLoan = _preSReg.CarryingLoan;
            _sReg.Remarks = txtRemarks.Text;
            _sReg.SerialNo = _preSReg.SerialNo;
            _sReg.Requisitioned = remBags > 0 ? "Partialy Delivered" : _preSReg.Requisitioned;
            _sReg.BagLoan = remBagLoan >= 0 ? remBagLoan : _preSReg.BagLoan;
            _sReg.LoanDate = _preSReg.LoanDate;
            _sReg.Bags = remBags > 0 ? remBags : _preSReg.Bags;
            _sReg.BagWeight = _preSReg.BagWeight;
            _sReg.TMID = _preSReg.TMID;

            if (new RegistrationManager().SaveRegistration(_sReg))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool UpdateSRVRegistration(int serialID, int remBags, float remBagLoan)
        {
            SRVRegistration _sReg = new SRVRegistration();
            SRVRegistration _preSReg = new RegistrationManager().GetRegistrationBySerialID(serialID);

            _sReg.CreatedBy = _preSReg.CreatedBy;
            _sReg.CreatedDate = _preSReg.CreatedDate;
            _sReg.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
            _sReg.ModifiedDate = DateTime.Now;

            _sReg.RegistrationID = _preSReg.RegistrationID;
            _sReg.RegistrationDate = _preSReg.RegistrationDate;
            _sReg.SerialID = serialID;
            _sReg.PartyID = _preSReg.PartyID;
            _sReg.CarryingLoan = _preSReg.CarryingLoan;
            _sReg.Remarks = txtRemarks.Text;
            _sReg.SerialNo = _preSReg.SerialNo;
            _sReg.Requisitioned = remBags > 0 ? "Partialy Delivered" :"Delivered";
            _sReg.BagLoan = remBagLoan > 0 ? remBagLoan : _preSReg.BagLoan;
            _sReg.LoanDate = _preSReg.LoanDate;
            _sReg.Bags = remBags >= 0 ? remBags : _preSReg.Bags;
            _sReg.BagWeight = _preSReg.BagWeight;
            _sReg.TMID = _preSReg.TMID;

            if (new RegistrationManager().SaveRegistration(_sReg))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private SRVLoanCollection FormToCollectionObject(int collectionID)
        {
            SRVLoanCollection srvLoan = new SRVLoanCollection();

            if (collectionID > 0)
            {
                srvLoan = new LoanCollectionManager().GetLoanCollectionByID(collectionID);

                srvLoan.ModifiedDate = DateTime.Today;
                srvLoan.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
            }
            else
            {
                srvLoan.CreatedDate = DateTime.Today;
                srvLoan.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
            }

            srvLoan.CollectionID = collectionID;
            srvLoan.PartyID = PartyID;
            srvLoan.CollectionDate = string.IsNullOrEmpty(txtDate.Text) ? DateTime.Today : DateTime.Parse(txtDate.Text);
            srvLoan.TotalBagLoan = (lblTBagLoan.Text != "0") ? Math.Round((float.Parse(lblTBagLoan.Text)), 2) : 0;
            srvLoan.TotalCarryingLoan = 0;
            srvLoan.TotalPotatoLoan = (lblTLoan.Text != "0") ? Math.Round((float.Parse(lblTLoan.Text)), 2) : 0;
            srvLoan.TotalInterest = (lblTInterest.Text != "0") ? Math.Round((float.Parse(lblTInterest.Text)), 2) : 0;
            srvLoan.TotalBagCharge = (lblTCharge.Text != "0") ? Math.Round((float.Parse(lblTCharge.Text)), 2) : 0;
            srvLoan.TotalAmount = (lblPayable.Text != "0") ? Math.Round((float.Parse(lblPayable.Text)), 2) : 0;
            srvLoan.PaidAmount = (txtPaid.Text != "0") ? Math.Round((float.Parse(txtPaid.Text)), 2) : 0;
            srvLoan.DuesAmount = (lblDues.Text != "0") ? Math.Round((float.Parse(lblDues.Text)), 2) : 0;
            srvLoan.SerialIDs = strSerials;
            srvLoan.TMID = TransactionID;
            srvLoan.Remarks = txtRemarks.Text;

            return srvLoan;
        }

        private void SaveLoanCollectionDetail(int collectionID)
        {
            SRVLoanCollectionDetail collDetail;

            foreach (GridViewRow gVRow in grvSerial.Rows)
            {
                CheckBox chkSelect = (CheckBox)gVRow.FindControl("chkSelect");
                if (chkSelect.Checked == true)
                {
                    int loanID = int.Parse(((Label)gVRow.FindControl("lblLoanID")).Text);
                    int bags = int.Parse(((Label)gVRow.FindControl("lblBags")).Text);
                    int wBags = int.Parse(((TextBox)gVRow.FindControl("txtWBags")).Text);
                    int emptyBags = int.Parse(((TextBox)gVRow.FindControl("txtEmptyBags")).Text);
                    int days = int.Parse(((Label)gVRow.FindControl("lblDays")).Text);
                    float loanPerBag = float.Parse(((Label)gVRow.FindControl("lblLoanPerBag")).Text);
                    float loan = float.Parse(((Label)gVRow.FindControl("lblLoan")).Text);
                    float interest = float.Parse(((Label)gVRow.FindControl("lblInterest")).Text);
                    float rent = float.Parse(((Label)gVRow.FindControl("lblBagCharge")).Text);
                    float bagPrice = float.Parse(((Label)gVRow.FindControl("lblBagPrice")).Text);
                    float bagLoan = float.Parse(((Label)gVRow.FindControl("lblBagLoan")).Text);
                    float lineTotal = float.Parse(((Label)gVRow.FindControl("lblLineTotal")).Text);
                    string serialNo = ((Label)gVRow.FindControl("lblSerialNo")).Text;
                    int serialID = int.Parse(((Label)gVRow.FindControl("lblSerialID")).Text);

                    collDetail = new LoanCollectionDetailManager().getLoanCollectionDetailBySerialID(collectionID, serialID);
                    int collDetailID = (collDetail != null) && (collDetail.CollectionDetailID != null) ? (int)collDetail.CollectionDetailID : 0;

                    if (collDetailID <= 0)
                    {
                        collDetail = new SRVLoanCollectionDetail();
                    }
                    collDetail.CollectionID = collectionID;
                    collDetail.CollectionDetailID = collDetailID;
                    collDetail.SerialID = serialID;
                    collDetail.SerialNo = serialNo;
                    collDetail.Bags = bags;
                    collDetail.WBags = wBags;
                    collDetail.EBags = emptyBags;
                    collDetail.BagPrice = bagPrice;
                    collDetail.BagLoan = bagLoan;
                    collDetail.CarryingLoan = 0;
                    collDetail.PLoanAmtPerBag = loanPerBag;
                    collDetail.PotatoLoan = loan;
                    collDetail.Days = days;
                    collDetail.Interest = interest;
                    collDetail.LoanID = loanID;
                    collDetail.CaseID = 0;
                    collDetail.RentPerBag = float.Parse(lblChargePerBag.Text);
                    collDetail.BagCharge = rent;
                    collDetail.TotalAmount = lineTotal;

                    if (new LoanCollectionDetailManager().SaveLoanCollectionDetail(collDetail))
                    {
                        float remBagLoan = (float)(bags - wBags) * bagPrice;
                        if (UpdateSRVRegistration(serialID, bags - wBags, remBagLoan))
                        {
                            if (UpdateINVStockSerial(serialID, bags - wBags, remBagLoan))
                            {
                                //UpdateINVStockLoading(serialID, bags - wBags);
                            }
                        }
                    }
                }
            }
        }
        #endregion
    }
}