﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using CSMSys.Lib.Utility;
using CSMSys.Lib.Manager.SRV;
using CSMSys.Lib.Manager.INV;

namespace CSMSys.Web.Controls.SRV
{
    public partial class BookIssue : System.Web.UI.Page
    {
        #region Private Properties
        private enum UIMODE
        {
            NEW,
            EDIT
        }

        private UIMODE UIMode
        {
            get
            {
                if (ViewState["UIMODE"] == null)
                    ViewState["UIMODE"] = new UIMODE();
                return (UIMODE)ViewState["UIMODE"];
            }
            set
            {
                ViewState["UIMODE"] = value;
            }
        }

        private int PartyID
        {
            get
            {
                if (ViewState["PartyID"] == null)
                    ViewState["PartyID"] = -1;
                return (int)ViewState["PartyID"];
            }
            set
            {
                ViewState["PartyID"] = value;
            }
        }

        private int BookID
        {
            get
            {
                if (ViewState["BookID"] == null)
                    ViewState["BookID"] = -1;
                return (int)ViewState["BookID"];
            }
            set
            {
                ViewState["BookID"] = value;
            }
        }

        private string strSearch = string.Empty;
        private INVParty _Party;
        ComboData _ComboData = new ComboData();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string qsUIMODE = string.IsNullOrEmpty(Request.QueryString["UIMODE"]) ? "NEW" : Request.QueryString["UIMODE"];
                if (string.IsNullOrEmpty(qsUIMODE) == false)
                {
                    UIMode = (UIMODE)Enum.Parse(typeof(UIMODE), qsUIMODE);
                    BookID = Convert.ToInt32(Request.QueryString["BID"]);
                    hdnWindowUIMODE.Value = UIMode.ToString();

                    if (UIMode == UIMODE.EDIT)
                    {
                        LoadToAllControlValue(BookID);

                        pnlNew.Visible = true;
                        btnSave.Text = "Update";
                    }
                    else if (UIMode == UIMODE.NEW)
                    {
                        pnlNew.Visible = true;
                        btnSave.Text = "Issue";
                        txtDate.Text = System.DateTime.Today.ToShortDateString();
                    }
                }
                MultiViewSerial.ActiveViewIndex = 0;
            }
        }

        #region Methods for Button
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if ((UIMode == UIMODE.NEW) || (UIMode == UIMODE.EDIT))
                {
                    if (!checkValidity()) return;

                    SaveSRVBookIssue();
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onSuccess();", true);
                MultiViewSerial.ActiveViewIndex = 1;
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onError();", true);
                MultiViewSerial.ActiveViewIndex = 2;
            }
        }

        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;
            grvParty.DataBind();
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvParty.DataBind();
        }
        #endregion

        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }

        private bool checkValidity()
        {
            if (string.IsNullOrEmpty(txtBookNo.Text))
            {
                lblFailure.Text = "Book Number is Required";
                txtBookNo.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtPageNo.Text))
            {
                lblFailure.Text = "Page No is Required";
                txtPageNo.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtCode.Text))
            {
                lblFailure.Text = "Customer Code is Required";
                txtCode.Focus();
                return false;
            }
            return true;
        }

        private void ClearForm()
        {
            txtDate.Text = System.DateTime.Today.ToShortDateString();
            txtBookNo.Text = string.Empty;
            txtPageNo.Text = string.Empty;
            txtRemarks.Text = string.Empty;
            txtContact.Text = string.Empty;
            txtCode.Text = string.Empty;
            txtName.Text = string.Empty;
            txtType.Text = string.Empty;
            txtGroup.Text = string.Empty;
        }

        private SRVBookIssue FormToObject(int id)
        {
            SRVBookIssue srvBookIssue = new SRVBookIssue();

            if (id > 0)
            {
                srvBookIssue = new BookIssueManager().GetBookIssueByID(id);
            }
            else
            {
                srvBookIssue.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
                srvBookIssue.CreatedDate = System.DateTime.Now;
            }

            srvBookIssue.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
            srvBookIssue.ModifiedDate = System.DateTime.Now;

            srvBookIssue.BookNumber = Convert.ToInt32(txtBookNo.Text);
            srvBookIssue.PageNo = txtPageNo.Text;
            srvBookIssue.Remarks = txtRemarks.Text;
            srvBookIssue.PartyID = PartyID;
            srvBookIssue.IssueDate = DateTime.Parse(txtDate.Text);
            srvBookIssue.Remarks = txtRemarks.Text;

            return srvBookIssue;
        }

        private void LoadToAllControlValue(int intBookID)
        {
            if (intBookID > 0)
            {
                SRVBookIssue srvBookIssue = new SRVBookIssue();
                srvBookIssue = new BookIssueManager().GetBookIssueByID(intBookID);

                PartyID = (int)srvBookIssue.PartyID;
                txtBookNo.Text = srvBookIssue.BookNumber.ToString();
                txtPageNo.Text = srvBookIssue.PageNo;
                txtDate.Text = DateTime.Parse(srvBookIssue.IssueDate.ToString()).ToShortDateString();
                txtRemarks.Text = srvBookIssue.Remarks;

                ReportManager rptManager = new ReportManager();
                INVParty _Party = rptManager.GetPartyByID(PartyID);

                txtContact.Text = _Party.ContactNo;
                txtCode.Text = _Party.PartyCode;
                txtName.Text = _Party.PartyName;
                txtType.Text = _Party.PartyType;
                int groupID = (int)_Party.ParentID;

                INVParty _group = rptManager.GetPartyByID(groupID);
                if (_group != null)
                {
                    txtGroup.Text = string.IsNullOrEmpty(_group.PartyCode) ? string.Empty : _group.PartyCode;
                }
                else
                {
                    txtGroup.Text = string.Empty;
                }
            }
        }
        #endregion

        #region Methods For Save
        private void SaveSRVBookIssue()
        {
            try
            {
                SRVBookIssue srvBookIssue = new SRVBookIssue();
                srvBookIssue = FormToObject(BookID);
                if (new BookIssueManager().SaveBookIssueDetails(srvBookIssue))
                {
                    ClearForm();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Methods For Grid
        protected void grvParty_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                string text = e.CommandArgument.ToString();
                string[] words = text.Split('/');
                string strID = words[0].Trim().ToString();
                string strCode = words[1].Trim().ToString();

                PartyID = Convert.ToInt32(strID);

                if (PartyID > 0)
                {
                    ReportManager rptManager = new ReportManager();
                    INVParty _Party = rptManager.GetPartyByID(PartyID);

                    txtContact.Text = _Party.ContactNo;
                    txtCode.Text = _Party.PartyCode;
                    txtName.Text = _Party.PartyName;
                    txtType.Text = _Party.PartyType;
                    int groupID = (int)_Party.ParentID;

                    INVParty _group = rptManager.GetPartyByID(groupID);
                    if (_group != null)
                    {
                        txtGroup.Text = string.IsNullOrEmpty(_group.PartyCode) ? string.Empty : _group.PartyCode;
                    }
                    else
                    {
                        txtGroup.Text = string.Empty;
                    }
                }
            }
        }

        protected void grvParty_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvParty.PageIndex = e.NewPageIndex;
            grvParty.DataBind();
        }

        protected void grvParty_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                ImageButton btnSelect = (ImageButton)e.Row.FindControl("imgSelect");
                btnSelect.CommandArgument = ((Label)e.Row.Cells[1].Controls[1]).Text + " / " + ((Label)e.Row.Cells[3].Controls[1]).Text;
            }
        }
        #endregion
    }
}