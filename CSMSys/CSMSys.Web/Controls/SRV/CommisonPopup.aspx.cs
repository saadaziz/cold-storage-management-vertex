﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using CSMSys.Lib.Model;
using CSMSys.Lib.Manager.INV;
using CSMSys.Lib.DataAccessLayer.Implementations;
using CSMSys.Lib.Manager.SRV;
using CSMSys.Web.Utility;
namespace CSMSys.Web.Controls.SRV
{
    public partial class CommisonPopup : System.Web.UI.Page
    {
        #region private
        private string strSearch = string.Empty;
        private int CommisionID
        {
            get
            {
                if (ViewState["CommisionID"] == null)
                    ViewState["CommisionID"] = -1;
                return (int)ViewState["CommisionID"];
            }
            set
            {
                ViewState["CommisionID"] = value;
            }
        }
        private int bags
        {
            get
            {
                if (ViewState["bags"] == null)
                    ViewState["bags"] = -1;
                return (int)ViewState["bags"];
            }
            set
            {
                ViewState["bags"] = value;
            }
        }
        private int partyid
        {
            get
            {
                if (ViewState["partyid"] == null)
                    ViewState["partyid"] = -1;
                return (int)ViewState["partyid"];
            }
            set
            {
                ViewState["partyid"] = value;
            }
        }
        INVParty invparty;
        private enum UIMODE
        {
            NEW,
            EDIT
        }

        private UIMODE UIMode
        {
            get
            {
                if (ViewState["UIMODE"] == null)
                    ViewState["UIMODE"] = new UIMODE();
                return (UIMODE)ViewState["UIMODE"];
            }
            set
            {
                ViewState["UIMODE"] = value;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtDate.Text = System.DateTime.Today.ToShortDateString();
                string qsUIMODE = string.IsNullOrEmpty(Request.QueryString["UIMODE"]) ? "NEW" : Request.QueryString["UIMODE"];
                if (string.IsNullOrEmpty(qsUIMODE) == false)
                {
                    UIMode = (UIMODE)Enum.Parse(typeof(UIMODE), qsUIMODE);
                    CommisionID = Convert.ToInt32(Request.QueryString["CID"]);
                    hdnWindowUIMODE.Value = UIMode.ToString();

                    if (UIMode == UIMODE.EDIT)
                    {
                        LoadToAllControlValueCommision(CommisionID);

                        grvCommison.Visible = false;
                        btnSave.Text = "Edit";
                    }
                    else if (UIMode == UIMODE.NEW)
                    {
                        btnSave.Text = "Save";
                        grvCommison.Visible = true;

                    }
                }
                MultiViewparty.ActiveViewIndex = 0;
            }
        }
        protected void LoadToAllControlValueCommision(int CommisionID)
        {
            if (CommisionID > 0)
            {
                SRVCommision srvcom = new CommisionDAOLinq().PickByID(CommisionID);
                invparty = new PartyDAOLinq().PickByID(srvcom.PartyID);
                lblpartyName.Text = invparty.PartyName;
                lblPartyCode.Text = invparty.PartyCode;
                lblfatherName.Text = invparty.FatherName;
                lbltotalbags.Text = srvcom.BagCount.ToString();
                lbltotalcomm.Text = srvcom.TotalCommision.ToString();
                lbltotaldiscount.Text = srvcom.TotalDiscout.ToString();
                txtdiscperBag.Text = srvcom.DiscountPerBag.ToString();
                txtcommperbag.Text = srvcom.CommisionPerBag.ToString();
                txtDate.Text = srvcom.Date.ToShortDateString();
            }
        }
        #region Methods For Button
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;
            grvCommison.DataBind();
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvCommison.DataBind();
        }
        #endregion
        #region save
        protected void btnsave_click(object sender, EventArgs e)
        {
            try
            {
                SRVCommision srvcomm = new SRVCommision();
                srvcomm = FormToObject(CommisionID);
                if (new CommisionManager().SaveCommision(srvcomm))
                {
                    ClearForm();
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onSuccess();", true);
                MultiViewparty.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onError();", true);
                MultiViewparty.ActiveViewIndex = 2;
                throw ex;
            }
        }
        private SRVCommision FormToObject(int id)
        {
            SRVCommision srvcomm = new SRVCommision();

            if (id > 0)
            {
                srvcomm.CommisonID=id;
            }
            else
            {
                srvcomm.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
                srvcomm.CreatedDate = System.DateTime.Now;
            }

            srvcomm.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
            srvcomm.ModifiedDate = System.DateTime.Now;

            srvcomm.BagCount=int.Parse(lbltotalbags.Text);
            srvcomm.DiscountPerBag = float.Parse(txtdiscperBag.Text);
            srvcomm.CommisionPerBag = float.Parse(txtcommperbag.Text);
            srvcomm.TotalCommision = float.Parse(lbltotalcomm.Text);
            srvcomm.TotalDiscout = float.Parse(lbltotaldiscount.Text);
            srvcomm.PartyID = partyid;
            srvcomm.Date = DateTime.Parse(txtDate.Text);
            //srvcomm.Date = (txtDate.Text).ToString("dd/MM/yyyy");
            
            return srvcomm;
        }
        private void ClearForm()
        {
            txtDate.Text = System.DateTime.Today.ToShortDateString();
            txtdiscperBag.Text = string.Empty;
            txtcommperbag.Text = string.Empty;
            lbltotaldiscount.Text = string.Empty;
            lbltotalcomm.Text = string.Empty;
            lblpartyName.Text = string.Empty;
            lblPartyCode.Text = string.Empty;
            lblfatherName.Text = string.Empty;
            lbltotalbags.Text = string.Empty;
        }
        #endregion

        #region Methods
        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }
        #endregion
        protected void txtchanged(object sender, EventArgs e)
        {
            if (((TextBox)sender).ID == "txtdiscperBag")
            {
                if (txtdiscperBag.Text != string.Empty)
                {
                    lbltotaldiscount.Text = (float.Parse(txtdiscperBag.Text) * float.Parse(lbltotalbags.Text)).ToString();
                    txtcommperbag.Enabled = false;
                }
                else
                {
                    txtcommperbag.Enabled = true;
                    lbltotaldiscount.Text = "0";
                }
            }
            else
            {
                if (txtcommperbag.Text != string.Empty)
                {
                    lbltotalcomm.Text = (float.Parse(txtcommperbag.Text) * float.Parse(lbltotalbags.Text)).ToString();
                    txtdiscperBag.Enabled = false;
                }
                else
                {
                    txtdiscperBag.Enabled = true;
                    lbltotalcomm.Text = "0";
                    
                }
            }
        }

        #region Methods For Grid
        PartyManager pmanager = new PartyManager();
        protected void grvCommison_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName.Equals("Select"))
            {
                string[] text = e.CommandArgument.ToString().Split('@');
                invparty = pmanager.GetPartyByID(int.Parse(text[0]));
                partyid = invparty.PartyID;
                lblpartyName.Text = invparty.PartyName;
                lblPartyCode.Text = invparty.PartyCode;
                lblfatherName.Text = invparty.FatherName;
                bags = int.Parse(text[1]);
                lbltotalbags.Text = bags.ToString();
            }
        }

        protected void grvCommison_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvCommison.PageIndex = e.NewPageIndex;
            grvCommison.DataBind();
        }

        protected void grvCommison_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                Label lblbags=((Label)e.Row.FindControl("lblBags"));
                SerialDAOLinq _serial = new SerialDAOLinq();
                
                int bagcount = Convert.ToInt32(_serial.GetSumByParty("SELECT SUM(Bags) as smbags from INVStockSerial WHERE partyid = " +DataBinder.Eval(e.Row.DataItem, "partyID").ToString() + ";"));Convert.ToInt32(_serial.GetSumByParty("SELECT SUM(Bags) as smbags from INVStockSerial WHERE partyid = " +DataBinder.Eval(e.Row.DataItem, "partyID").ToString() + ";"));
                bagcount += Convert.ToInt32(_serial.GetSumByParty("SELECT SUM(Bags) as smbags from INVStockSerial WHERE partycode like '" + DataBinder.Eval(e.Row.DataItem, "partycode").ToString() + "-%';"));
                lblbags.Text = bagcount.ToString();
                ImageButton btnselect = (ImageButton)e.Row.FindControl("imgselect");
                btnselect.CommandArgument = DataBinder.Eval(e.Row.DataItem, "partyID").ToString()+"@"+
                    lblbags.Text;
                
            }
        }
        #endregion
    }
}