﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CSMSys.Lib.Manager.Administration.Application;
using CSMSys.Lib.Manager.INV;
using CSMSys.Lib.Manager.SRV;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CSMSys.Lib.AccountingEntity;
using CSMSys.Lib.AccountingUtility;
using CSMSys.Lib.AccountingDataAccess;
using System.Globalization;

namespace CSMSys.Web.Controls.Loan
{
    public partial class BagLoan : System.Web.UI.Page
    {
        #region Private
        private string strSearch = string.Empty;
        private SRVBagLoan _bagLoan;
        private INVParty _Party;

        SqlConnection formCon = null;
        private string connstring = ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString;

        private enum UIMODE
        {
            NEW,
            EDIT
        }

        private UIMODE UIMode
        {
            get
            {
                if (ViewState["UIMODE"] == null)
                    ViewState["UIMODE"] = new UIMODE();
                return (UIMODE)ViewState["UIMODE"];
            }
            set
            {
                ViewState["UIMODE"] = value;
            }
        }

        private int LoanID
        {
            get
            {
                if (ViewState["LoanID"] == null)
                    ViewState["LoanID"] = -1;
                return (int)ViewState["LoanID"];
            }
            set
            {
                ViewState["LoanID"] = value;
            }
        }

        private int TransactionID
        {
            get
            {
                if (ViewState["TransactionID"] == null)
                    ViewState["TransactionID"] = -1;
                return (int)ViewState["TransactionID"];
            }
            set
            {
                ViewState["TransactionID"] = value;
            }
        }

        private int AccountID
        {
            get
            {
                if (ViewState["AccountID"] == null)
                    ViewState["AccountID"] = -1;
                return (int)ViewState["AccountID"];
            }
            set
            {
                ViewState["AccountID"] = value;
            }
        }

        private string AccountNo
        {
            get
            {
                if (ViewState["AccountNo"] == null)
                    ViewState["AccountNo"] = -1;
                return (string)ViewState["AccountNo"];
            }
            set
            {
                ViewState["AccountNo"] = value;
            }
        }

        private int PartyID
        {
            get
            {
                if (ViewState["PartyID"] == null)
                    ViewState["PartyID"] = -1;
                return (int)ViewState["PartyID"];
            }
            set
            {
                ViewState["PartyID"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtAmtPerBag.Text = getAmountPerBags().ToString();
                txtDate.Text = DateTime.Today.ToShortDateString();

                string qsUIMODE = string.IsNullOrEmpty(Request.QueryString["UIMODE"]) ? "NEW" : Request.QueryString["UIMODE"];
                LoanID = string.IsNullOrEmpty(Request.QueryString["LID"]) ? 0 : int.Parse(Request.QueryString["LID"]);
                TransactionID = string.IsNullOrEmpty(Request.QueryString["TMID"]) ? 0 : int.Parse(Request.QueryString["TMID"]);
                if (string.IsNullOrEmpty(qsUIMODE) == false)
                {
                    UIMode = (UIMODE)Enum.Parse(typeof(UIMODE), qsUIMODE);
                    hdnWindowUIMODE.Value = UIMode.ToString();

                    if (UIMode == UIMODE.EDIT)
                    {
                        LoadToAllLoanlValue(LoanID);

                        pnlNew.Visible = true;
                        btnSave.Text = "Update";
                    }
                    else if (UIMode == UIMODE.NEW)
                    {
                        pnlNew.Visible = true;
                        btnSave.Text = "Save";
                    }
                }
                MultiViewSerial.ActiveViewIndex = 0;
            }
        }

        #region Method
        private float getAmountPerBags()
        {
            SYSVariable _var = new VariableManager().SearchVariable("bag price");
            if (_var == null) return 0;

            float variable = _var.variable != null ? (float)_var.variable : 0;
            return variable;
        }

        private float getBagLoanAmount()
        {
            float amtPerBag = string.IsNullOrEmpty(txtAmtPerBag.Text) ? 0 : float.Parse(txtAmtPerBag.Text.ToString());
            float bags = string.IsNullOrEmpty(txtBag.Text) ? 0 : float.Parse(txtBag.Text.ToString());
            float loan = amtPerBag * bags;

            return loan;
        }

        private int getBagsLoadedByParty(int PartyID, DateTime dateFrom)
        {
            int opBalance = 0;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstring))
                {
                    sqlConn.Open();
                    string _query = "SELECT SUM(Bags) AS TotalBags ";
                    _query = _query + "FROM INVStockSerial ";
                    _query = _query + "WHERE (SerialDate < CONVERT(DATETIME, '" + dateFrom.ToShortDateString() + " 00:00:00', 102)) ";
                    if (PartyID > 0)
                    {
                        _query = _query + "AND (PartyID = " + PartyID + ")";
                    }
                    SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    if (sqlReader.Read())
                    {
                        if (sqlReader.HasRows) opBalance = string.IsNullOrEmpty(sqlReader["TotalBags"].ToString()) ? 0 : int.Parse(sqlReader["TotalBags"].ToString());
                    }

                    sqlReader.Close();
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }

        private int getBagsLoadedByParent(int ParentID, DateTime dateFrom)
        {
            int opBalance = 0;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstring))
                {
                    sqlConn.Open();
                    string _query = "SELECT SUM(iss.Bags) AS TotalBags ";
                    _query = _query + "FROM INVStockSerial AS iss INNER JOIN ";
                    _query = _query + "INVParty AS ip ON iss.PartyID = ip.PartyID ";
                    _query = _query + "WHERE (iss.SerialDate < CONVERT(DATETIME, '" + dateFrom.ToShortDateString() + " 00:00:00', 102)) ";
                    if (ParentID > 0)
                    {
                        _query = _query + "GROUP BY ip.ParentID ";
                        _query = _query + "HAVING (ip.ParentID = " + ParentID + ")";
                    }
                    SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    if (sqlReader.Read())
                    {
                        if (sqlReader.HasRows) opBalance = string.IsNullOrEmpty(sqlReader["TotalBags"].ToString()) ? 0 : int.Parse(sqlReader["TotalBags"].ToString());
                    }

                    sqlReader.Close();
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }

        private int getBagsLoaded(int intPartyID)
        {
            DateTime DateFrom = string.IsNullOrEmpty(txtDate.Text) ? DateTime.Today : DateTime.Parse(txtDate.Text);

            ReportManager rptManager = new ReportManager();
            _Party = rptManager.GetPartyByID(intPartyID);

            int parentID = _Party.ParentID != null ? Convert.ToInt32(_Party.ParentID) : 0;
            int bagsLoaded = 0;
            if (intPartyID > 0)
            {
                if (parentID > 0)
                {
                    bagsLoaded = getBagsLoadedByParty(intPartyID, DateFrom);
                }
                else
                {
                    bagsLoaded = getBagsLoadedByParent(intPartyID, DateFrom);
                    bagsLoaded = bagsLoaded + getBagsLoadedByParty(intPartyID, DateFrom);
                }
            }

            return bagsLoaded;
        }

        private void LoadToAllLoanlValue(int intLoanID)
        {
            if (intLoanID > 0)
            {
                BagLoanManager _Manager = new BagLoanManager();
                _bagLoan = _Manager.GetBagLoanByID(intLoanID);

                PartyID = (Int32)_bagLoan.PartyID;
                LoadToAllControlValue(PartyID);

                txtBag.Text = _bagLoan.BagNumber.ToString();
                txtAmtPerBag.Text = _bagLoan.AmountPerBag.ToString();
                lblAmnt.Text = _bagLoan.LoanAmount.ToString();
                txtDate.Text = DateTime.Parse(_bagLoan.LoanDate.ToString()).ToShortDateString();
                TransactionID = (Int32)_bagLoan.TMID;
                txtRemarks.Text = _bagLoan.Remarks;
            }
        }

        private void LoadToAllControlValue(int intPartyID)
        {
            if (intPartyID > 0)
            {
                ReportManager rptManager = new ReportManager();
                _Party = rptManager.GetPartyByID(intPartyID);

                lblCode.Text = _Party.PartyCode;
                PartyID = _Party.PartyID;
                lblName.Text = _Party.PartyName;
                lblPerc.Text = _Party.BloanPerc.ToString();
                lblBLoaned.Text = new BagLoanManager().GetAllBagLoansByparty(_Party.PartyID).ToString();

                lblBStock.Text = getBagsLoaded(intPartyID) > 0 ? getBagsLoaded(intPartyID).ToString() : "0";
                lblBStockPerc.Text = lblBLoaned.Text != "0" ? ((int.Parse(lblPerc.Text)) * int.Parse(lblBLoaned.Text) / 100).ToString() : "0";

                AccountID = (Int32)_Party.AccountID;
                AccountNo = _Party.AccountNo;
            }
        }

        private void ClearForm()
        {
            lblCode.Text = string.Empty;
            lblName.Text = string.Empty;
            txtBag.Text = "0";
            txtAmtPerBag.Text = "0";
            lblAmnt.Text = "0";
            lblCode.Text = string.Empty;
            txtAmtPerBag.Text = getAmountPerBags().ToString();

            LoanID = 0;
            TransactionID = 0;
            PartyID = 0;
            AccountID = 0;
            AccountNo = string.Empty;

            lblFailure.Text = string.Empty;
        }

        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }
        #endregion

        #region Methods For Button
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvParty.DataBind();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            grvParty.DataBind();
        }

        protected void txtAmtPerBag_TextChanged(object sender, EventArgs e)
        {
            lblAmnt.Text = getBagLoanAmount().ToString();
        }

        protected void txtBag_TextChanged(object sender, EventArgs e)
        {
            lblAmnt.Text = getBagLoanAmount().ToString();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if ((UIMode == UIMODE.NEW) || (UIMode == UIMODE.EDIT))
                {
                    if (!checkValidity()) return;
                    float total = float.Parse(txtBag.Text) * float.Parse(txtAmtPerBag.Text);
                    if (total > 0)
                    {
                        SaveLoanCrAccount();
                        SaveLoanDrAccount();
                    }

                    SaveSRVBagLoan();
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onSuccess();", true);
                MultiViewSerial.ActiveViewIndex = 1;
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onError();", true);
                MultiViewSerial.ActiveViewIndex = 2;
            }
        }

        protected void txtDate_TextChanged(object sender, EventArgs e)
        {
            lblBStock.Text = getBagsLoaded(PartyID) > 0 ? getBagsLoaded(PartyID).ToString() : "0";
            lblBStockPerc.Text = lblBLoaned.Text != "0" ? ((int.Parse(lblPerc.Text)) * int.Parse(lblBLoaned.Text) / 100).ToString() : "0";

        }
        #endregion

        #region methods for save
        private bool checkValidity()
        {
            if (PartyID <= 0)
            {
                lblFailure.Text = "Please Select a Customer";
                return false;
            }
            if (!string.IsNullOrEmpty(txtBag.Text))
            {
                if (int.Parse(txtBag.Text) <= 0)
                {
                    lblFailure.Text = "No of Bags is Required";
                    txtBag.Focus();
                    return false;
                }
            }
            else
            {
                lblFailure.Text = "No of Bags is Required";
                txtBag.Focus();
                return false;
            }
            if (string.IsNullOrEmpty(txtAmtPerBag.Text))
            {
                lblFailure.Text = "Amount/Bag is Required";
                txtAmtPerBag.Focus();
                return false;
            }

            if (hdnWindowUIMODE.Value == "NEW")
            {
                int bagloaned = new BagLoanManager().GetAllBagLoansByparty(PartyID);
                if (bagloaned != 0)
                {
                    double bags = (int.Parse(lblPerc.Text)) * bagloaned / 100;
                    if ((int.Parse(lblBStock.Text)) < bags)
                    {
                        lblFailure.Text = "Not enough Bags Loaded in Store.";
                        txtBag.Focus();
                        return false;
                    }
                }
                float loanAmount = string.IsNullOrEmpty(lblAmnt.Text) ? 0 : float.Parse(lblAmnt.Text);
                if (getAccountBalance(36) < loanAmount)
                {
                    lblFailure.Text = "Insufficient Balance";
                    txtBag.Focus();
                    return false;
                }
                float total = float.Parse(txtBag.Text) * float.Parse(txtAmtPerBag.Text);
                if (total > 0)
                {
                    if (AccountID <= 0)
                    {
                        lblFailure.Text = "Customer has not any A/C No";
                        txtBag.Focus();
                        return false;
                    }
                }
            }
            else if (hdnWindowUIMODE.Value == "EDIT")
            {
                _bagLoan = new BagLoanManager().GetBagLoanByID(LoanID);

                float amount = (float)_bagLoan.LoanAmount;
                float loanAmount = string.IsNullOrEmpty(lblAmnt.Text) ? 0 : float.Parse(lblAmnt.Text);
                float editAmount = loanAmount > amount ? loanAmount - amount : amount - loanAmount;
                //editAmount = editAmount + amount;
                if (getAccountBalance(36) < editAmount)
                {
                    lblFailure.Text = "Insufficient Balance";
                    txtBag.Focus();
                    return false;
                }
            }
            
            lblFailure.Text = string.Empty;
            return true;
        }

        private void SaveSRVBagLoan()
        {
            SRVBagLoan sbl = new SRVBagLoan();
            sbl = FormToObject(LoanID);
            if (new BagLoanManager().SaveBagLoanDetails(sbl))
            {
                ClearForm();
            }
        }

        private void SaveLoanCrAccount()
        {
            if (AccountID <= 0) return;

            formCon = new SqlConnection(connstring);
            formCon.Open();

            try
            {
                TransactionMaster objTM = new TransactionMaster();
                TransactionDetail objTD = new TransactionDetail();

                objTM = CreateTransMasterObject("Journal", "Cr");

                float fltDrAmount = string.IsNullOrEmpty(txtBag.Text) ? 0 : Convert.ToInt32(int.Parse(txtBag.Text) * int.Parse(txtAmtPerBag.Text));
                float fltCrAmount = 0;
                int intTDID = getTransDetailID(TransactionID, AccountID);

                objTD = CreateTransDetailObject(intTDID, TransactionID, AccountID, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), "Bags Amount from Gunny Bags");

                DaTransaction objDaTrans = new DaTransaction();
                //TMID = objDaTrans.SaveEditTransactionMaster(objTM, formCon, trans);

                TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                formCon.Close();
            }
        }

        private void SaveLoanDrAccount()
        {
            formCon = new SqlConnection(connstring);
            formCon.Open();

            try
            {
                TransactionMaster objTM = new TransactionMaster();
                TransactionDetail objTD = new TransactionDetail();

                objTM = CreateTransMasterObject("Journal", "Dr");

                float fltDrAmount = 0;
                float fltCrAmount = string.IsNullOrEmpty(txtBag.Text) ? 0 : Convert.ToInt32(int.Parse(txtBag.Text) * int.Parse(txtAmtPerBag.Text));
                int intTDID = getTransDetailID(TransactionID, 36);

                objTD = CreateTransDetailObject(intTDID, TransactionID, 36, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), "Bags Amount Loan to " + lblCode.Text + " - " + lblName.Text);

                DaTransaction objDaTrans = new DaTransaction();
                //TMID = objDaTrans.SaveEditTransactionMaster(objTM, formCon, trans);

                TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                formCon.Close();
            }
        }

        private float getAccountBalance(int accountID)
        {
            float balance = 0;

            using (SqlConnection sqlConn = new SqlConnection(connstring))
            {
                sqlConn.Open();
                string _query = "SELECT [CurrentBalance] FROM [T_Account] WHERE [AccountID] = " + accountID;
                SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                if (sqlReader.Read())
                {
                    if (sqlReader.HasRows) balance = float.Parse(sqlReader["CurrentBalance"].ToString());
                }

                sqlReader.Close();
                sqlConn.Close();
            }
            return balance;
        }

        private int getTransDetailID(int TMID, int accountID)
        {
            int intTDID = 0;

            using (SqlConnection sqlConn = new SqlConnection(connstring))
            {
                sqlConn.Open();
                string _query = "SELECT [TransDID] FROM [T_Transaction_Detail] WHERE [AccountID] = " + accountID + " AND [TransMID] = " + TMID;
                SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                if (sqlReader.Read())
                {
                    if (sqlReader.HasRows) intTDID = int.Parse(sqlReader["TransDID"].ToString());
                }

                sqlReader.Close();
                sqlConn.Close();
            }
            return intTDID;
        }

        #region create objects
        private TransactionMaster CreateTransMasterObject(string TabName, string accType)
        {
            TransactionMaster objTM = null;
            try
            {
                objTM = new TransactionMaster();

                if (TabName == "Debit")
                {
                    //objTM.TransactionMasterID = TransactionID;
                    //objTM.TransactionDate = DateTime.Parse(txtDrDate.Text);
                    //objTM.VoucherNo = txtDrVoucherNo.Text.Trim();
                    //objTM.VoucherPayee = "";
                    //objTM.VoucherType = VoucherType;
                    //objTM.TransactionMethodID = Convert.ToInt32(ddlPayMethod.SelectedValue);
                    //objTM.MethodRefID = -1;
                    //objTM.MethodRefNo = txtDrRefNo.Text.Trim();
                    //objTM.TransactionDescription = txtDrDesc.Text;
                    //objTM.Module = "Voucher";
                    //if (chkDrAppvBy.Checked)
                    //{
                    //    objTM.ApprovedBy = txtDrAppvBy.Text;
                    //    objTM.ApprovedDate = DateTime.Parse(txtDrAppvDate.Text);
                    //}
                    //else
                    //{
                    //    objTM.ApprovedBy = string.Empty;
                    //    objTM.ApprovedDate = new DateTime(1900, 1, 1);
                    //}

                    //if (objTM.TransactionMasterID <= 0)
                    //{
                    //    objTM.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.CreatedDate = DateTime.Today;
                    //    objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.ModifiedDate = DateTime.Today;
                    //}
                    //else
                    //{
                    //    objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.ModifiedDate = DateTime.Today;
                    //}

                }
                else if (TabName == "Credit")
                {
                    //objTM.TransactionMasterID = TransactionID;
                    //objTM.TransactionDate = DateTime.Parse(txtCrDate.Text);
                    //objTM.VoucherNo = txtCrVoucherNo.Text.Trim();
                    //objTM.VoucherPayee = "";
                    //objTM.VoucherType = VoucherType;
                    //objTM.TransactionMethodID = Convert.ToInt32(ddlCollMethod.SelectedValue);
                    //objTM.MethodRefID = -1;
                    //objTM.MethodRefNo = txtCrRefNo.Text.Trim();
                    //objTM.TransactionDescription = txtCrDesc.Text;
                    //objTM.Module = "Voucher";
                    //if (chkCrAppvBy.Checked)
                    //{
                    //    objTM.ApprovedBy = txtCrAppvBy.Text;
                    //    objTM.ApprovedDate = DateTime.Parse(txtCrAppvDate.Text);
                    //}
                    //else
                    //{
                    //    objTM.ApprovedBy = string.Empty;
                    //    objTM.ApprovedDate = new DateTime(1900, 1, 1);
                    //}

                    //if (objTM.TransactionMasterID <= 0)
                    //{
                    //    objTM.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.CreatedDate = DateTime.Today;
                    //    objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.ModifiedDate = DateTime.Today;
                    //}
                    //else
                    //{
                    //    objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.ModifiedDate = DateTime.Today;
                    //}
                }
                else if (TabName == "Journal")
                {
                    objTM.TransactionMasterID = TransactionID;
                    objTM.TransactionDate = string.IsNullOrEmpty(txtDate.Text) ? DateTime.Today : DateTime.Parse(txtDate.Text);
                    objTM.VoucherNo = getVoucherNo(3);
                    objTM.VoucherPayee = "";
                    objTM.VoucherType = 3;
                    objTM.TransactionMethodID = -1;
                    objTM.MethodRefID = -1;
                    objTM.MethodRefNo = string.Empty;

                    if (accType == "Cr")
                    {
                        objTM.TransactionDescription = "Bags Amount Loan to " + lblCode.Text + " - " + lblName.Text + " for " + txtBag.Text + " Bags";
                    }
                    else if (accType == "Dr")
                    {
                        objTM.TransactionDescription = "Bags Amount from Gunny Bags to " + lblCode.Text + " - " + lblName.Text + " for " + txtBag.Text + " Bags"; 
                    }

                    objTM.Module = "Voucher";
                    objTM.ApprovedBy = string.Empty;
                    objTM.ApprovedDate = new DateTime(1900, 1, 1);

                    if (objTM.TransactionMasterID <= 0)
                    {
                        objTM.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
                        objTM.CreatedDate = DateTime.Today;
                        objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                        objTM.ModifiedDate = DateTime.Today;
                    }
                    else
                    {
                        objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                        objTM.ModifiedDate = DateTime.Today;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objTM;
        }

        private TransactionDetail CreateTransDetailObject(int TDID, int TMID, int AccountID, double CrAmt, double DrAmt, string cmnt)
        {
            TransactionDetail objTD = null;
            try
            {
                objTD = new TransactionDetail();
                objTD.TransactionDetailID = TDID;
                objTD.TransactionMasterID = TMID;
                objTD.TransactionAccountID = AccountID;
                objTD.CreditAmount = CrAmt;
                objTD.DebitAmount = DrAmt;
                objTD.Comments = cmnt; // string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objTD;
        }
        #endregion

        private string getVoucherNo(int voucherType)
        {
            string voucherNo = string.Empty;

            using (SqlConnection sqlConn = new SqlConnection(connstring))
            {
                sqlConn.Open();
                string _query = "SELECT [VoucherNo] FROM [T_Transaction_Master] WHERE [VoucherType] = " + voucherType + " AND [TransMID] = (SELECT MAX([TransMID]) FROM [T_Transaction_Master] WHERE [VoucherType] = " + voucherType + ")";
                SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                if (sqlReader.Read())
                {
                    if (sqlReader.HasRows) voucherNo = sqlReader["VoucherNo"].ToString();

                    if (!string.IsNullOrEmpty(voucherNo))
                    {
                        int voucher = int.Parse(voucherNo.Substring(voucherNo.Length - 5)) + 1;
                        if (voucherType == 1) voucherNo = "D" + voucher.ToString("00000");
                        if (voucherType == 2) voucherNo = "C" + voucher.ToString("00000");
                        if (voucherType == 3) voucherNo = "J" + voucher.ToString("00000");
                    }
                    else
                    {
                        if (voucherType == 1) voucherNo = "D00001";
                        if (voucherType == 2) voucherNo = "C00001";
                        if (voucherType == 3) voucherNo = "J00001";
                    }
                }
                else
                {
                    if (voucherType == 1) voucherNo = "D00001";
                    if (voucherType == 2) voucherNo = "C00001";
                    if (voucherType == 3) voucherNo = "J00001";
                }

                sqlReader.Close();
                sqlConn.Close();
            }
            return voucherNo;
        }

        private SRVBagLoan FormToObject(int loanID)
        {
            SRVBagLoan srvBagLoan = new SRVBagLoan();

            if (loanID > 0)
            {
                srvBagLoan = new BagLoanManager().GetBagLoanByID(loanID);
            }
            else
            {
                srvBagLoan.CreatedDate = DateTime.Today;
                srvBagLoan.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
            }

            srvBagLoan.ModifiedDate = DateTime.Today;
            srvBagLoan.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();

            srvBagLoan.BagLoanID = loanID;
            srvBagLoan.PartyID = PartyID;
            srvBagLoan.LoanDate = string.IsNullOrEmpty(txtDate.Text) ? DateTime.Today : DateTime.Parse(txtDate.Text);
            srvBagLoan.BagNumber = Convert.ToInt32(txtBag.Text);
            srvBagLoan.AmountPerBag = (txtAmtPerBag.Text != "0") ? float.Parse(txtAmtPerBag.Text) : 0;
            srvBagLoan.LoanAmount = (txtBag.Text != "0") ? Convert.ToInt32(int.Parse(txtBag.Text) * int.Parse(txtAmtPerBag.Text)) : 0;
            srvBagLoan.BagsLoaned = (lblBLoaned.Text != "0") ? Convert.ToInt32(lblBLoaned.Text) : 0;
            srvBagLoan.BagsLoaded = (lblBStock.Text != "0") ? Convert.ToInt32(lblBStock.Text) : 0;
            srvBagLoan.TMID = TransactionID;
            srvBagLoan.Remarks = txtRemarks.Text;

            return srvBagLoan;
        }
        #endregion

        #region Methods For Grid
        protected void grvParty_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName.Equals("Select"))
            {
                btnSave.Visible = true;
                PartyID = Convert.ToInt32(e.CommandArgument.ToString());

                if (PartyID > 0)
                {
                    try
                    {
                        LoadToAllControlValue(PartyID);
                        lblFailure.Text = string.Empty;
                    }
                    catch (InvalidCastException err)
                    {
                        lblFailure.Text = err.ToString();
                    }
                }
            }
        }

        protected void grvParty_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvParty.PageIndex = e.NewPageIndex;
            grvParty.DataBind();
        }

        protected void grvParty_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[1].Controls.Count > 0))
            {
                ImageButton btnEdit = (ImageButton)e.Row.FindControl("imgEdit");
                btnEdit.CommandArgument = (DataBinder.Eval(e.Row.DataItem, "PartyID")).ToString();

            }
        }
        #endregion
    }
}