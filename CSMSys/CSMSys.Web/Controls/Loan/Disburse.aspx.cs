﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CSMSys.Lib.Manager.INV;
using CSMSys.Lib.Manager.SRV;
using CSMSys.Lib.Model;
using CSMSys.Web.Utility;
using System.Text.RegularExpressions;
using CSMSys.Lib.BusinessObjects;
using CSMSys.Lib.DataAccessLayer.Implementations;

using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using CSMSys.Lib.AccountingEntity;
using CSMSys.Lib.AccountingUtility;
using CSMSys.Lib.AccountingDataAccess;
using System.Globalization;

namespace CSMSys.Web.Controls.Loan
{
    public partial class Disburse : System.Web.UI.Page
    {
        #region Private
        private string strSearch = string.Empty;
        private SRVLoanDisburse _Loan;
        private INVParty _Party;

        SqlConnection formCon = null;
        private string connstring = ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString;

        private enum UIMODE
        {
            NEW,
            EDIT
        }

        private UIMODE UIMode
        {
            get
            {
                if (ViewState["UIMODE"] == null)
                    ViewState["UIMODE"] = new UIMODE();
                return (UIMODE)ViewState["UIMODE"];
            }
            set
            {
                ViewState["UIMODE"] = value;
            }
        }

        private int LoanID
        {
            get
            {
                if (ViewState["LoanID"] == null)
                    ViewState["LoanID"] = -1;
                return (int)ViewState["LoanID"];
            }
            set
            {
                ViewState["LoanID"] = value;
            }
        }

        private int TransactionID
        {
            get
            {
                if (ViewState["TransactionID"] == null)
                    ViewState["TransactionID"] = -1;
                return (int)ViewState["TransactionID"];
            }
            set
            {
                ViewState["TransactionID"] = value;
            }
        }

        private int AccountID
        {
            get
            {
                if (ViewState["AccountID"] == null)
                    ViewState["AccountID"] = -1;
                return (int)ViewState["AccountID"];
            }
            set
            {
                ViewState["AccountID"] = value;
            }
        }

        private string AccountNo
        {
            get
            {
                if (ViewState["AccountNo"] == null)
                    ViewState["AccountNo"] = -1;
                return (string)ViewState["AccountNo"];
            }
            set
            {
                ViewState["AccountNo"] = value;
            }
        }

        private int PartyID
        {
            get
            {
                if (ViewState["PartyID"] == null)
                    ViewState["PartyID"] = -1;
                return (int)ViewState["PartyID"];
            }
            set
            {
                ViewState["PartyID"] = value;
            }
        }

        private int RequisitionID
        {
            get
            {
                if (ViewState["RequisitionID"] == null)
                    ViewState["RequisitionID"] = -1;
                return (int)ViewState["RequisitionID"];
            }
            set
            {
                ViewState["RequisitionID"] = value;
            }
        }

        private int CaseID
        {
            get
            {
                if (ViewState["CaseID"] == null)
                    ViewState["CaseID"] = -1;
                return (int)ViewState["CaseID"];
            }
            set
            {
                ViewState["CaseID"] = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtDate.Text = DateTime.Today.ToShortDateString();

                string qsUIMODE = string.IsNullOrEmpty(Request.QueryString["UIMODE"]) ? "NEW" : Request.QueryString["UIMODE"];
                LoanID = string.IsNullOrEmpty(Request.QueryString["LID"]) ? 0 : Convert.ToInt32(Request.QueryString["LID"]);
                if (string.IsNullOrEmpty(qsUIMODE) == false)
                {
                    UIMode = (UIMODE)Enum.Parse(typeof(UIMODE), qsUIMODE);
                    hdnWindowUIMODE.Value = UIMode.ToString();

                    if (UIMode == UIMODE.EDIT)
                    {
                        LoadToAllLoanlValue(LoanID);

                        pnlNew.Visible = true;
                        btnSave.Text = "Update";
                    }
                    else if (UIMode == UIMODE.NEW)
                    {
                        pnlNew.Visible = true;
                        btnSave.Text = "Disburse";

                        CaseID = new LoanManager().GetNextCaseNo() + 200;
                    }
                }
                MultiViewSerial.ActiveViewIndex = 0;
            }
        }

        #region Method
        private int getBagsLoadedByParty(int PartyID, DateTime dateFrom)
        {
            int opBalance = 0;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstring))
                {
                    sqlConn.Open();
                    string _query = "SELECT SUM(Bags) AS TotalBags ";
                    _query = _query + "FROM INVStockSerial ";
                    _query = _query + "WHERE (SerialDate < CONVERT(DATETIME, '" + dateFrom.ToShortDateString() + " 00:00:00', 102)) ";
                    if (PartyID > 0)
                    {
                        _query = _query + "AND (PartyID = " + PartyID + ")";
                    }
                    SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    if (sqlReader.Read())
                    {
                        if (sqlReader.HasRows) opBalance = string.IsNullOrEmpty(sqlReader["TotalBags"].ToString()) ? 0 : int.Parse(sqlReader["TotalBags"].ToString());
                    }

                    sqlReader.Close();
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }

        private int getBagsLoadedByParent(int ParentID, DateTime dateFrom)
        {
            int opBalance = 0;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstring))
                {
                    sqlConn.Open();
                    string _query = "SELECT SUM(iss.Bags) AS TotalBags ";
                    _query = _query + "FROM INVStockSerial AS iss INNER JOIN ";
                    _query = _query + "INVParty AS ip ON iss.PartyID = ip.PartyID ";
                    _query = _query + "WHERE (iss.SerialDate < CONVERT(DATETIME, '" + dateFrom.ToShortDateString() + " 00:00:00', 102)) ";
                    if (ParentID > 0)
                    {
                        _query = _query + "GROUP BY ip.ParentID ";
                        _query = _query + "HAVING (ip.ParentID = " + ParentID + ")";
                    }
                    SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    if (sqlReader.Read())
                    {
                        if (sqlReader.HasRows) opBalance = string.IsNullOrEmpty(sqlReader["TotalBags"].ToString()) ? 0 : int.Parse(sqlReader["TotalBags"].ToString());
                    }

                    sqlReader.Close();
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }

        private int getBagsLoaded(int intPartyID)
        {
            DateTime DateFrom = string.IsNullOrEmpty(txtDate.Text) ? DateTime.Today : DateTime.Parse(txtDate.Text);

            ReportManager rptManager = new ReportManager();
            _Party = rptManager.GetPartyByID(intPartyID);

            int parentID = _Party.ParentID != null ? Convert.ToInt32(_Party.ParentID) : 0;
            int bagsLoaded = 0;
            if (intPartyID > 0)
            {
                if (parentID > 0)
                {
                    bagsLoaded = getBagsLoadedByParty(intPartyID, DateFrom);
                }
                else
                {
                    bagsLoaded = getBagsLoadedByParent(intPartyID, DateFrom);
                    bagsLoaded = bagsLoaded + getBagsLoadedByParty(intPartyID, DateFrom);
                }
            }

            return bagsLoaded;
        }

        private float getBagsLoanedByParty(int PartyID, DateTime dateFrom)
        {
            int opBalance = 0;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstring))
                {
                    sqlConn.Open();
                    string _query = "SELECT SUM(LoanAmount) AS TotalBagLoan ";
                    _query = _query + "FROM SRVBagLoan ";
                    _query = _query + "WHERE (LoanDate < CONVERT(DATETIME, '" + dateFrom.ToShortDateString() + " 00:00:00', 102)) ";
                    if (PartyID > 0)
                    {
                        _query = _query + "AND (PartyID = " + PartyID + ")";
                    }
                    SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    if (sqlReader.Read())
                    {
                        if (sqlReader.HasRows) opBalance = string.IsNullOrEmpty(sqlReader["TotalBagLoan"].ToString()) ? 0 : int.Parse(sqlReader["TotalBagLoan"].ToString());
                    }

                    sqlReader.Close();
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }

        private float getCarryingByParty(int PartyID, DateTime dateFrom)
        {
            int opBalance = 0;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstring))
                {
                    sqlConn.Open();
                    string _query = "SELECT SUM(CarryingLoan) AS CarryingLoan ";
                    _query = _query + "FROM SRVRegistration ";
                    _query = _query + "WHERE (RegistrationDate < CONVERT(DATETIME, '" + dateFrom.ToShortDateString() + " 00:00:00', 102)) ";
                    if (PartyID > 0)
                    {
                        _query = _query + "AND (PartyID = " + PartyID + ")";
                    }
                    SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    if (sqlReader.Read())
                    {
                        if (sqlReader.HasRows) opBalance = string.IsNullOrEmpty(sqlReader["CarryingLoan"].ToString()) ? 0 : int.Parse(sqlReader["CarryingLoan"].ToString());
                    }

                    sqlReader.Close();
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }

        private float getPreviousLoanByParty(int PartyID, DateTime dateFrom)
        {
            int opBalance = 0;
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(connstring))
                {
                    sqlConn.Open();
                    string _query = "SELECT SUM(LoanAmount) AS TotalLoan ";
                    _query = _query + "FROM SRVLoanDisburse ";
                    _query = _query + "WHERE (LoanDate < CONVERT(DATETIME, '" + dateFrom.ToShortDateString() + " 00:00:00', 102)) ";
                    if (PartyID > 0)
                    {
                        _query = _query + "AND (PartyID = " + PartyID + ")";
                    }
                    SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                    SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                    if (sqlReader.Read())
                    {
                        if (sqlReader.HasRows) opBalance = string.IsNullOrEmpty(sqlReader["TotalLoan"].ToString()) ? 0 : int.Parse(sqlReader["TotalLoan"].ToString());
                    }

                    sqlReader.Close();
                    sqlConn.Close();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return opBalance;
        }

        private void LoadToAllLoanlValue(int intLoanID)
        {
            if (intLoanID > 0)
            {
                _Loan = new LoanManager().GetLoanDByID(intLoanID);

                PartyID = (Int32)_Loan.PartyID;
                LoadToAllControlValue(PartyID);

                lblBag.Text = _Loan.Bags.ToString();
                lblAmtPerBag.Text = _Loan.AmountPerBag.ToString();
                lblAmnt.Text = _Loan.LoanAmount.ToString();
                txtDate.Text = DateTime.Parse(_Loan.LoanDate.ToString()).ToShortDateString();

                lblBLoaned.Text = !string.IsNullOrEmpty(_Loan.BagsLoaned.ToString()) ? _Loan.BagsLoaned.ToString() : "0";
                lblBStock.Text = !string.IsNullOrEmpty(_Loan.BagsLoaded.ToString()) ? _Loan.BagsLoaded.ToString() : "0";
                lblCarrying.Text = !string.IsNullOrEmpty(_Loan.Carrying.ToString()) ? _Loan.Carrying.ToString() : "0";
                lblPLoan.Text = !string.IsNullOrEmpty(_Loan.PreviousLoaned.ToString()) ? _Loan.PreviousLoaned.ToString() : "0";

                lblTLoan.Text = (float.Parse(lblBLoaned.Text) + float.Parse(lblCarrying.Text) + float.Parse(lblPLoan.Text)).ToString();

                CaseID = _Loan.CaseID > 0 ? (int)_Loan.CaseID : new LoanManager().GetNextCaseNo();
                RequisitionID = (Int32)_Loan.RequisitionID;
                TransactionID = (Int32)_Loan.TMID;
                txtRemarks.Text = _Loan.Remarks;
            }
        }

        private void LoadToAllControlValue(int intPartyID)
        {
            if (intPartyID > 0)
            {
                ReportManager rptManager = new ReportManager();
                _Party = rptManager.GetPartyByID(intPartyID);

                lblCode.Text = _Party.PartyCode;
                lblName.Text = _Party.PartyName;
                lblType.Text = _Party.PartyType;

                int groupID = (int)_Party.ParentID;

                INVParty _group = new ReportManager().GetPartyByID(groupID);
                if (_group != null)
                {
                    lblGroup.Text = string.IsNullOrEmpty(_group.PartyCode) ? string.Empty : _group.PartyCode;
                }
                else
                {
                    lblGroup.Text = string.Empty;
                }

                AccountID = (Int32)_Party.AccountID;
                AccountNo = _Party.AccountNo;

                if (RequisitionID > 0)
                {
                    SRVLoanRequisition _loanReq = new RequisitionManager().GetRequisitionByID(RequisitionID);

                    lblBag.Text = _loanReq.ApprovedBags.ToString();
                    lblAmtPerBag.Text = String.Format("{0:N}", _loanReq.AmountPerBag);
                    lblAmnt.Text = String.Format("{0:N}", _loanReq.LoanAmount);
                    txtDate.Text = DateTime.Parse(_loanReq.RequisitionDate.ToString()).ToShortDateString();
                }

                lblBLoaned.Text = getBagsLoanedByParty(intPartyID, DateTime.Today) > 0 ? getBagsLoanedByParty(intPartyID, DateTime.Today).ToString() : "0";
                lblBStock.Text = getBagsLoaded(intPartyID) > 0 ? getBagsLoaded(intPartyID).ToString() : "0";
                lblCarrying.Text = getCarryingByParty(intPartyID, DateTime.Today) > 0 ? getCarryingByParty(intPartyID, DateTime.Today).ToString() : "0";
                lblPLoan.Text = getPreviousLoanByParty(intPartyID, DateTime.Today) > 0 ? getPreviousLoanByParty(intPartyID, DateTime.Today).ToString() : "0";

                lblTLoan.Text = (float.Parse(lblBLoaned.Text) + float.Parse(lblCarrying.Text) + float.Parse(lblPLoan.Text)).ToString();
            }
        }

        private void ClearForm()
        {
            lblCode.Text = string.Empty;
            lblName.Text = string.Empty;
            lblBag.Text = "0";
            lblAmtPerBag.Text = "0";
            lblAmnt.Text = "0";
            lblCode.Text = string.Empty;

            LoanID = 0;
            TransactionID = 0;
            PartyID = 0;
            AccountID = 0;
            AccountNo = string.Empty;

            lblFailure.Text = string.Empty;
        }

        public string HighlightText(string InputTxt)
        {
            string SearchStr = txtSearch.Text;
            // Setup the regular expression and add the Or operator.
            Regex RegExp = new Regex(SearchStr.Replace(" ", "|").Trim(), RegexOptions.IgnoreCase);
            // Highlight keywords by calling the 
            //delegate each time a keyword is found.
            return RegExp.Replace(InputTxt, new MatchEvaluator(ReplaceKeyWords));
        }

        public string ReplaceKeyWords(Match m)
        {
            return ("<span class=highlight>" + m.Value + "</span>");
        }
        #endregion

        #region Methods For Button
        protected void imgSearch_Click(object sender, ImageClickEventArgs e)
        {
            strSearch = txtSearch.Text;
        }

        protected void imgRefresh_Click(object sender, ImageClickEventArgs e)
        {
            txtSearch.Text = string.Empty;
            grvRequisition.DataBind();
        }

        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            grvRequisition.DataBind();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if ((UIMode == UIMODE.NEW) || (UIMode == UIMODE.EDIT))
                {
                    if (!checkValidity()) return;
                    float total = float.Parse(lblBag.Text) * float.Parse(lblAmtPerBag.Text);
                    if (total > 0)
                    {
                        //SaveLoanCrAccount();
                        //SaveLoanDrAccount();
                    }

                    SaveSRVLoanDisburse();
                }
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onSuccess();", true);
                MultiViewSerial.ActiveViewIndex = 1;
            }
            catch
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "onload", "onError();", true);
                MultiViewSerial.ActiveViewIndex = 2;
            }
        }

        protected void txtDate_TextChanged(object sender, EventArgs e)
        {
            lblBLoaned.Text = getBagsLoanedByParty(PartyID, DateTime.Today) > 0 ? getBagsLoanedByParty(PartyID, DateTime.Today).ToString() : "0";
            lblBStock.Text = getBagsLoaded(PartyID) > 0 ? getBagsLoaded(PartyID).ToString() : "0";
            lblCarrying.Text = getCarryingByParty(PartyID, DateTime.Today) > 0 ? getCarryingByParty(PartyID, DateTime.Today).ToString() : "0";
            lblPLoan.Text = getPreviousLoanByParty(PartyID, DateTime.Today) > 0 ? getPreviousLoanByParty(PartyID, DateTime.Today).ToString() : "0";

            lblTLoan.Text = (float.Parse(lblBLoaned.Text) + float.Parse(lblCarrying.Text) + float.Parse(lblPLoan.Text)).ToString();
        }
        #endregion

        #region methods for save
        private bool checkValidity()
        {
            if (PartyID <= 0)
            {
                lblFailure.Text = "Please Select a Customer";
                return false;
            }
            if (string.IsNullOrEmpty(txtDate.Text))
            {
                lblFailure.Text = "Date is Required";
                txtDate.Focus();
                return false;
            }

            if (hdnWindowUIMODE.Value == "NEW")
            {
                float loanAmount = string.IsNullOrEmpty(lblAmnt.Text) ? 0 : float.Parse(lblAmnt.Text);
                if (loanAmount > 0)
                {
                    if (getAccountBalance(35) < loanAmount)
                    {
                        lblFailure.Text = "Insufficient Balance";
                        lblBag.Focus();
                        return false;
                    }
                    else
                    {
                        if (AccountID <= 0)
                        {
                            lblFailure.Text = "Customer has not any A/C No";
                            lblBag.Focus();
                            return false;
                        }
                    }
                }
            }
            else if (hdnWindowUIMODE.Value == "EDIT")
            {
                float loanAmount = string.IsNullOrEmpty(lblAmnt.Text) ? 0 : float.Parse(lblAmnt.Text);
                if (loanAmount > 0)
                {
                    if (getAccountBalance(35) < loanAmount)
                    {
                        lblFailure.Text = "Insufficient Balance";
                        lblBag.Focus();
                        return false;
                    }
                }
            }

            lblFailure.Text = string.Empty;
            return true;
        }

        private void SaveSRVLoanDisburse()
        {
            SRVLoanDisburse sbl = new SRVLoanDisburse();
            sbl = FormToObject(LoanID);
            if (new LoanManager().SaveOrEditLoanDisburse(sbl))
            {
                if (UpdateLoanRequisition())
                {
                    if (LoanDisbursed())
                    {
                        ClearForm();
                    }
                }
            }
        }

        private void SaveLoanCrAccount()
        {
            if (AccountID <= 0) return;

            formCon = new SqlConnection(connstring);
            formCon.Open();

            try
            {
                TransactionMaster objTM = new TransactionMaster();
                TransactionDetail objTD = new TransactionDetail();

                objTM = CreateTransMasterObject("Journal", "Cr");

                float fltDrAmount = string.IsNullOrEmpty(lblBag.Text) ? 0 : float.Parse(lblBag.Text) * float.Parse(lblAmtPerBag.Text);
                float fltCrAmount = 0;
                int intTDID = getTransDetailID(TransactionID, AccountID);

                objTD = CreateTransDetailObject(intTDID, TransactionID, AccountID, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), "Potato Loan Amount from Cash for Potato Loan");

                DaTransaction objDaTrans = new DaTransaction();
                TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                formCon.Close();
            }
        }

        private void SaveLoanDrAccount()
        {
            formCon = new SqlConnection(connstring);
            formCon.Open();

            try
            {
                TransactionMaster objTM = new TransactionMaster();
                TransactionDetail objTD = new TransactionDetail();

                objTM = CreateTransMasterObject("Journal", "Dr");

                float fltDrAmount = 0;
                float fltCrAmount = string.IsNullOrEmpty(lblBag.Text) ? 0 : float.Parse(lblBag.Text) * float.Parse(lblAmtPerBag.Text);
                int intTDID = getTransDetailID(TransactionID, 35);

                objTD = CreateTransDetailObject(intTDID, TransactionID, 35, Convert.ToDouble(fltCrAmount), Convert.ToDouble(fltDrAmount), "Potato Loan Amount to " + lblCode.Text + " - " + lblName.Text);

                DaTransaction objDaTrans = new DaTransaction();
                TransactionID = objDaTrans.InsertUpdateTransactionMaster(objTM, formCon, objTD);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                formCon.Close();
            }
        }

        private float getAccountBalance(int accountID)
        {
            float balance = 0;

            using (SqlConnection sqlConn = new SqlConnection(connstring))
            {
                sqlConn.Open();
                string _query = "SELECT [CurrentBalance] FROM [T_Account] WHERE [AccountID] = " + accountID;
                SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                if (sqlReader.Read())
                {
                    if (sqlReader.HasRows) balance = float.Parse(sqlReader["CurrentBalance"].ToString());
                }

                sqlReader.Close();
                sqlConn.Close();
            }
            return balance;
        }

        private int getTransDetailID(int TMID, int accountID)
        {
            int intTDID = 0;

            using (SqlConnection sqlConn = new SqlConnection(connstring))
            {
                sqlConn.Open();
                string _query = "SELECT [TransDID] FROM [T_Transaction_Detail] WHERE [AccountID] = " + accountID + " AND [TransMID] = " + TMID;
                SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                if (sqlReader.Read())
                {
                    if (sqlReader.HasRows) intTDID = int.Parse(sqlReader["TransDID"].ToString());
                }

                sqlReader.Close();
                sqlConn.Close();
            }
            return intTDID;
        }

        #region create objects
        private TransactionMaster CreateTransMasterObject(string TabName, string accType)
        {
            TransactionMaster objTM = null;
            try
            {
                objTM = new TransactionMaster();

                if (TabName == "Debit")
                {
                    //objTM.TransactionMasterID = TransactionID;
                    //objTM.TransactionDate = DateTime.Parse(txtDrDate.Text);
                    //objTM.VoucherNo = txtDrVoucherNo.Text.Trim();
                    //objTM.VoucherPayee = "";
                    //objTM.VoucherType = VoucherType;
                    //objTM.TransactionMethodID = Convert.ToInt32(ddlPayMethod.SelectedValue);
                    //objTM.MethodRefID = -1;
                    //objTM.MethodRefNo = txtDrRefNo.Text.Trim();
                    //objTM.TransactionDescription = txtDrDesc.Text;
                    //objTM.Module = "Voucher";
                    //if (chkDrAppvBy.Checked)
                    //{
                    //    objTM.ApprovedBy = txtDrAppvBy.Text;
                    //    objTM.ApprovedDate = DateTime.Parse(txtDrAppvDate.Text);
                    //}
                    //else
                    //{
                    //    objTM.ApprovedBy = string.Empty;
                    //    objTM.ApprovedDate = new DateTime(1900, 1, 1);
                    //}

                    //if (objTM.TransactionMasterID <= 0)
                    //{
                    //    objTM.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.CreatedDate = DateTime.Today;
                    //    objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.ModifiedDate = DateTime.Today;
                    //}
                    //else
                    //{
                    //    objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.ModifiedDate = DateTime.Today;
                    //}

                }
                else if (TabName == "Credit")
                {
                    //objTM.TransactionMasterID = TransactionID;
                    //objTM.TransactionDate = DateTime.Parse(txtCrDate.Text);
                    //objTM.VoucherNo = txtCrVoucherNo.Text.Trim();
                    //objTM.VoucherPayee = "";
                    //objTM.VoucherType = VoucherType;
                    //objTM.TransactionMethodID = Convert.ToInt32(ddlCollMethod.SelectedValue);
                    //objTM.MethodRefID = -1;
                    //objTM.MethodRefNo = txtCrRefNo.Text.Trim();
                    //objTM.TransactionDescription = txtCrDesc.Text;
                    //objTM.Module = "Voucher";
                    //if (chkCrAppvBy.Checked)
                    //{
                    //    objTM.ApprovedBy = txtCrAppvBy.Text;
                    //    objTM.ApprovedDate = DateTime.Parse(txtCrAppvDate.Text);
                    //}
                    //else
                    //{
                    //    objTM.ApprovedBy = string.Empty;
                    //    objTM.ApprovedDate = new DateTime(1900, 1, 1);
                    //}

                    //if (objTM.TransactionMasterID <= 0)
                    //{
                    //    objTM.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.CreatedDate = DateTime.Today;
                    //    objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.ModifiedDate = DateTime.Today;
                    //}
                    //else
                    //{
                    //    objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                    //    objTM.ModifiedDate = DateTime.Today;
                    //}
                }
                else if (TabName == "Journal")
                {
                    objTM.TransactionMasterID = TransactionID;
                    objTM.TransactionDate = string.IsNullOrEmpty(txtDate.Text) ? DateTime.Today : DateTime.Parse(txtDate.Text);
                    objTM.VoucherNo = getVoucherNo(3);
                    objTM.VoucherPayee = "";
                    objTM.VoucherType = 3;
                    objTM.TransactionMethodID = -1;
                    objTM.MethodRefID = -1;
                    objTM.MethodRefNo = string.Empty;

                    if (accType == "Cr")
                    {
                        objTM.TransactionDescription = "Potato Loan Amount to " + lblCode.Text + " - " + lblName.Text;
                    }
                    else if (accType == "Dr")
                    {
                        objTM.TransactionDescription = "Potato Loan Amount from Cash for Potato Loan to " + lblCode.Text + " - " + lblName.Text;
                    }

                    objTM.Module = "Voucher";
                    objTM.ApprovedBy = string.Empty;
                    objTM.ApprovedDate = new DateTime(1900, 1, 1);

                    if (objTM.TransactionMasterID <= 0)
                    {
                        objTM.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
                        objTM.CreatedDate = DateTime.Today;
                        objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                        objTM.ModifiedDate = DateTime.Today;
                    }
                    else
                    {
                        objTM.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                        objTM.ModifiedDate = DateTime.Today;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objTM;
        }

        private TransactionDetail CreateTransDetailObject(int TDID, int TMID, int AccountID, double CrAmt, double DrAmt, string cmnt)
        {
            TransactionDetail objTD = null;
            try
            {
                objTD = new TransactionDetail();
                objTD.TransactionDetailID = TDID;
                objTD.TransactionMasterID = TMID;
                objTD.TransactionAccountID = AccountID;
                objTD.CreditAmount = CrAmt;
                objTD.DebitAmount = DrAmt;
                objTD.Comments = cmnt; // string.Empty;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return objTD;
        }
        #endregion

        private string getVoucherNo(int voucherType)
        {
            string voucherNo = string.Empty;

            using (SqlConnection sqlConn = new SqlConnection(connstring))
            {
                sqlConn.Open();
                string _query = "SELECT [VoucherNo] FROM [T_Transaction_Master] WHERE [VoucherType] = " + voucherType + " AND [TransMID] = (SELECT MAX([TransMID]) FROM [T_Transaction_Master] WHERE [VoucherType] = " + voucherType + ")";
                SqlCommand sqlCmd = new SqlCommand(_query, sqlConn);
                SqlDataReader sqlReader = sqlCmd.ExecuteReader();

                if (sqlReader.Read())
                {
                    if (sqlReader.HasRows) voucherNo = sqlReader["VoucherNo"].ToString();

                    if (!string.IsNullOrEmpty(voucherNo))
                    {
                        int voucher = int.Parse(voucherNo.Substring(voucherNo.Length - 5)) + 1;
                        if (voucherType == 1) voucherNo = "D" + voucher.ToString("00000");
                        if (voucherType == 2) voucherNo = "C" + voucher.ToString("00000");
                        if (voucherType == 3) voucherNo = "J" + voucher.ToString("00000");
                    }
                    else
                    {
                        if (voucherType == 1) voucherNo = "D00001";
                        if (voucherType == 2) voucherNo = "C00001";
                        if (voucherType == 3) voucherNo = "J00001";
                    }
                }
                else
                {
                    if (voucherType == 1) voucherNo = "D00001";
                    if (voucherType == 2) voucherNo = "C00001";
                    if (voucherType == 3) voucherNo = "J00001";
                }

                sqlReader.Close();
                sqlConn.Close();
            }
            return voucherNo;
        }

        private bool LoanDisbursed()
        {
            bool res = false;
            SRVLoanRequisition _loanReq = new RequisitionManager().GetRequisitionByID(RequisitionID);
            string serialIDs = _loanReq.SerialIDs;

            IList<INVStockSerial> serialList = new StockSerialNo().getSerialFromSerialIDS(serialIDs);

            foreach (INVStockSerial serial in serialList)
            {
                SRVRegistration temp = new SRVRegistration();
                SRVRegistration sr = new RegistrationDAOLinq().SearchRegistrationByNo(serial.SerialNo);

                SRVLoanDisburse _sld = new LoanManager().GetLoanIDByPartyAndRequisition(PartyID, RequisitionID);
                LoanID = (_sld != null) && (_sld.LoanID > 0) ? _sld.LoanID : 0;

                temp.Requisitioned = "Disbursed";
                temp.RegistrationID = sr.RegistrationID;
                temp.RegistrationDate = sr.RegistrationDate;
                temp.AmountPerBag = sr.AmountPerBag;
                temp.BagWeight = sr.BagWeight;
                temp.SerialID = sr.SerialID;
                temp.SerialNo = sr.SerialNo;
                temp.Remarks = sr.Remarks;
                temp.BagLoan = sr.BagLoan;
                temp.CarryingLoan = sr.CarryingLoan;
                temp.PartyID = sr.PartyID;
                temp.LoanDate = string.IsNullOrEmpty(txtDate.Text) ? DateTime.Today : DateTime.Parse(txtDate.Text);
                temp.Bags = sr.Bags;
                temp.CaseID = sr.CaseID;
                temp.LoanID = LoanID;
                temp.TMID = sr.TMID;
                temp.CreatedBy = sr.CreatedBy;
                temp.CreatedDate = sr.CreatedDate;
                temp.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
                temp.ModifiedDate = DateTime.Now;

                res = (new RegistrationManager().SaveRegistration(temp));
            }

            return res;
        }

        private bool UpdateLoanRequisition()
        {
            SRVLoanRequisition _loanReq = new SRVLoanRequisition();
            SRVLoanRequisition _preReq = new RequisitionManager().GetRequisitionByID(RequisitionID);

            _loanReq.CreatedBy = _preReq.CreatedBy;
            _loanReq.CreatedDate = _preReq.CreatedDate;
            _loanReq.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();
            _loanReq.ModifiedDate = System.DateTime.Now;

            _loanReq.RequisitionID = RequisitionID;
            _loanReq.PartyID = PartyID;
            _loanReq.RequisitionDate = _preReq.RequisitionDate;
            _loanReq.ApprovalDate = _preReq.ApprovalDate;
            _loanReq.Bags = _preReq.Bags;
            _loanReq.ApprovedBags = _preReq.ApprovedBags;
            _loanReq.AmountPerBag = _preReq.AmountPerBag;
            _loanReq.LoanAmount = _preReq.LoanAmount;
            _loanReq.Remarks = txtRemarks.Text;
            _loanReq.SerialIDs = _preReq.SerialIDs.ToString();
            _loanReq.IsApproved = true;
            _loanReq.IsDisbursed = true;

            if (new RequisitionManager().SaveRequisition(_loanReq))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private SRVLoanDisburse FormToObject(int loanID)
        {
            SRVLoanDisburse srvLoan = new SRVLoanDisburse();

            if (loanID > 0)
            {
                srvLoan = new LoanManager().GetLoanDByID(loanID);
            }
            else
            {
                srvLoan.CreatedDate = DateTime.Today;
                srvLoan.CreatedBy = WebCommonUtility.GetCSMSysUserKey();
            }

            srvLoan.ModifiedDate = DateTime.Today;
            srvLoan.ModifiedBy = WebCommonUtility.GetCSMSysUserKey();

            srvLoan.LoanID = loanID;
            srvLoan.PartyID = PartyID;
            srvLoan.LoanDate = string.IsNullOrEmpty(txtDate.Text) ? DateTime.Today : DateTime.Parse(txtDate.Text);
            srvLoan.Bags = Convert.ToInt32(lblBag.Text);
            srvLoan.AmountPerBag = (lblAmtPerBag.Text != "0") ? Math.Round((float.Parse(lblAmtPerBag.Text)), 2) : 0;
            srvLoan.LoanAmount = (lblBag.Text != "0") ? Math.Round((float.Parse(lblBag.Text) * float.Parse(lblAmtPerBag.Text)),2) : 0;
            srvLoan.BagsLoaned = (lblBLoaned.Text != "0") ? float.Parse(lblBLoaned.Text) : 0;
            srvLoan.BagsLoaded = (lblBStock.Text != "0") ? Convert.ToInt32(lblBStock.Text) : 0;
            srvLoan.PreviousLoaned = (lblPLoan.Text != "0") ? float.Parse(lblPLoan.Text) : 0;
            srvLoan.Carrying = (lblCarrying.Text != "0") ? float.Parse(lblCarrying.Text) : 0;
            srvLoan.RequisitionID = RequisitionID;
            srvLoan.CaseID = CaseID;
            srvLoan.TMID = TransactionID;
            srvLoan.Remarks = txtRemarks.Text;

            SRVLoanRequisition _loanReq = new RequisitionManager().GetRequisitionByID(RequisitionID);
            srvLoan.SerialIDs = _loanReq.SerialIDs;

            return srvLoan;
        }
        #endregion

        #region Methods For Grid
        protected void grvRequisition_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName.Equals("Select"))
            {
                string[] word = e.CommandArgument.ToString().Split('@');

                PartyID = Convert.ToInt32(word[1].Trim());
                RequisitionID = Convert.ToInt32(word[0].Trim());

                if (PartyID > 0)
                {
                    try
                    {
                        LoadToAllControlValue(PartyID);
                        lblFailure.Text = string.Empty;
                    }
                    catch (InvalidCastException err)
                    {
                        lblFailure.Text = err.ToString();
                    }
                }
            }
        }

        protected void grvRequisition_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            grvRequisition.PageIndex = e.NewPageIndex;
            grvRequisition.DataBind();
        }

        protected void grvRequisition_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if ((e.Row.RowType == DataControlRowType.DataRow) && (e.Row.Cells[2].Controls.Count > 0))
            {
                ImageButton btnSelect = (ImageButton)e.Row.FindControl("imgSelect");
                btnSelect.CommandArgument = (DataBinder.Eval(e.Row.DataItem, "RequisitionID")).ToString() + "@" + (DataBinder.Eval(e.Row.DataItem, "PartyID")).ToString();
            }
        }
        #endregion
    }
}