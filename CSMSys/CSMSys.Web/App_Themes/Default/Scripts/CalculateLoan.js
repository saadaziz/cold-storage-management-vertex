﻿
    $('#<%=grvSerial_txtWBags.ClientID %>').blur(function () {
        var $tr = $(this).parents('tr');
        if ($tr.length > 0) {
            if (parseInt($tr.find('#<%=grvSerial_Bags.ClientID %>').html()) < $(this).val()) {
                alert('W Bags must not exceed SR Bags.');
                var $ctrl = $(this);
                window.setTimeout(function () {
                    $ctrl.focus();
                }, 50);
                return false;
            }
//            $tr.find('#<%=grvSerial_Loan.ClientID %>').html(parseFloat($tr.find('#<%=grvSerial_LoanPerBag.ClientID %>').html()) * parseInt($(this).val()));
        }
//        CalculateLineTotal();
//        CalculateGrandTotal();
    });

    //Line Total
//    function CalculateLineTotal() {
//        var lineTotal = 0.0;
//        $('#<%=grvSerial.ClientID %> tr:gt(0)').each(function () {
//            lineTotal = parseFloat($(this).find('#<%=grvSerial_LineTotal.ClientID %>').length == 0 || !$(this).find('#<%=grvSerial_LineTotal.ClientID %>').html() ? 0 : $(this).find('#<%=grvSerial_LineTotal.ClientID %>').html());
//        });
//        $('#<%=grvSerial_LineTotal.ClientID %>').html(lineTotal);
//    }

    //Grand Total
//    function CalculateGrandTotal() {
//                var grandTotal = 0.0;
//                $('#grvSerial tr:gt(0)').each(function () {
//                    grandTotal = grandTotal + parseFloat($(this).find('.totalPrice').length == 0 || !$(this).find('.totalPrice').html() ? 0 : $(this).find('.totalPrice').html());
//                });
//                $('#grvSerial .grandtotal').html(grandTotal);
//    }

    //First Time to display all total amount and grand total
//    function initGrid() {
//        $('#<%=grvSerial.ClientID %> tr:gt(0)').each(function () {
//            $(this).find('#<%=grvSerial_LineTotal.ClientID %>').html(parseFloat($(this).find('#<%=grvSerial_Loan.ClientID %>').html()) * parseInt($(this).find('#<%=grvSerial_txtWBags.ClientID %>').val()));
//            //            $(this).find('.totalPrice').html(parseFloat($(this).find('.price').html()) * parseInt($(this).find('.txtQty').val()));
//        });
//        CalculateGrandTotal();
    //    }

    //To allow numeric character only
    $('#<%=grvSerial_txtWBags.ClientID %>').keydown(function (event) {
        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
        // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
        // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            // let it happen, don't do anything
            return;
        }
        else {
            // Ensure that it is a number and stop the keypress
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });

//    initGrid();

