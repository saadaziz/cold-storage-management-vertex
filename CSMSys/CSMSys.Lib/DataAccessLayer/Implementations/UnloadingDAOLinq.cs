﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration;
using CSMSys.Lib.Model;
using CSMSys.Lib.DataAccessLayer.Interfaces;
using CSMSys.Lib.Utility;
using System.Data.Linq;
namespace CSMSys.Lib.DataAccessLayer.Implementations
{
    public class UnloadingDAOLinq : BaseDAORepository<INVStockUnloading, CSMSysConfiguration>, IUnloading
    {
        protected override System.Linq.Expressions.Expression<Func<INVStockUnloading, bool>> GetIDSelector(int id)
        {
            return (item) => item.UnloadID == id;
        }

        //public INVStockUnloading GetUnLoadByID(int ID)
        //{
        //    try
        //    {
        //        string whereClause = string.Empty;
        //        string queryTable = "INVStockUnloading";

        //        DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

        //        string strSQL = "SELECT * FROM " + queryTable;

        //        if (ID > 0)
        //        {
        //            whereClause += " WHERE UnloadID = " + ID + " ";
        //        }

        //        strSQL += whereClause;

        //        return dc.ExecuteQuery<INVStockUnloading>(@strSQL).FirstOrDefault();
        //    }
        //    catch (Exception ex)
        //    {
        //        _Logger.Error(ex);
        //        return null;
        //    }
        //}
    }
}
