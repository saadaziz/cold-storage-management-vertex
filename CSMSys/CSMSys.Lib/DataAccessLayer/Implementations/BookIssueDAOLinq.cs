﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.Configuration;
using CSMSys.Lib.Model;
using CSMSys.Lib.DataAccessLayer.Interfaces;
using CSMSys.Lib.Utility;


namespace CSMSys.Lib.DataAccessLayer.Implementations
{
    public class BookIssueDAOLinq : BaseDAORepository<SRVBookIssue, CSMSysConfiguration>, IBookIssueDAO
    {
        protected override System.Linq.Expressions.Expression<Func<SRVBookIssue, bool>> GetIDSelector(int id)
        {
            return (item) => item.BookID == id;
        }

        public IList<SRVBookIssue> GetBookIssueByPartyID(int partyID)
        {
            try
            {
                string whereClause = string.Empty;
                string queryTable = "SRVBookIssue";

                DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

                string strSQL = "SELECT * FROM " + queryTable;

                if (partyID > 0)
                {
                    whereClause += " WHERE PartyID = " + partyID + " ";
                }

                strSQL += whereClause;

                return dc.ExecuteQuery<SRVBookIssue>(@strSQL).ToList();
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return null;
            }
        }
    }
}
