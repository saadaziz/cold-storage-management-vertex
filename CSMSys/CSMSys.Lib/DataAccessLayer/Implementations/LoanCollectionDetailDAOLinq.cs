﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CSMSys.Lib.Model;
using CSMSys.Lib.DataAccessLayer.Interfaces;
using CSMSys.Lib.Utility;
using System.Data.Linq;
using System.Configuration;
using System.Data.SqlClient;

namespace CSMSys.Lib.DataAccessLayer.Implementations
{
    public class LoanCollectionDetailDAOLinq : BaseDAORepository<SRVLoanCollectionDetail, CSMSysConfiguration>, ILoanCollectionDetailDAO
    {
        protected override System.Linq.Expressions.Expression<Func<SRVLoanCollectionDetail, bool>> GetIDSelector(int id)
        {
            return (item) => item.CollectionDetailID == id;
        }

        public SRVLoanCollectionDetail getLoanCollectionDetailBySerialID(int collID, int serialID)
        {
            string whereClause = string.Empty;
            string queryTable = "SRVLoanCollectionDetail";

            DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

            string strSQL = "SELECT * FROM " + queryTable;

            if ((collID != 0) && (serialID != 0))
            {
                whereClause += " WHERE CollectionID = " + collID + " AND SerialID = " + serialID;
            }

            strSQL += whereClause;

            return dc.ExecuteQuery<SRVLoanCollectionDetail>(@strSQL).ToList().FirstOrDefault();
        }
        public IList<SRVLoanCollectionDetail> getLoanCollectionListDetailByCollectionID(int collID)
        {
            string whereClause = string.Empty;
            string queryTable = "SRVLoanCollectionDetail";

            DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

            string strSQL = "SELECT * FROM " + queryTable;

            if (collID != 0)
            {
                whereClause += " WHERE CollectionID = " + collID ;
            }

            strSQL += whereClause;

            return dc.ExecuteQuery<SRVLoanCollectionDetail>(@strSQL).ToList();
        }
        public IList<SRVLoanCollectionDetail> getLoanCollectionListDetailByPartyIDSerialID(int partyID,int serid)
        {
            string whereClause = string.Empty;
            string queryTable = "SRVLoanCollectionDetail";

            DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

            string strSQL = "SELECT * FROM " + queryTable;

            if (serid != 0)
            {
                whereClause += " WHERE SerialID = " + serid; ;
            }

            strSQL += whereClause;

            return dc.ExecuteQuery<SRVLoanCollectionDetail>(@strSQL).ToList();
        }
        //public IList<SRVLoanCollectionDetail> getAllLoansByParty(int partyid)
        //{
        //    string whereClause = string.Empty;
        //    string queryTable = "SRVLoanCollectionDetail";

        //    DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

        //    string strSQL = "SELECT * FROM " + queryTable;

        //    if ((partyid != 0))
        //    {
        //        whereClause += " WHERE PartyID = '" + partyid + "';";
        //    }

        //    strSQL += whereClause;

        //    return dc.ExecuteQuery<SRVLoanCollectionDetail>(@strSQL).ToList();
        //}
    }
}
