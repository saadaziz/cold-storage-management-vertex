﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Configuration;
using CSMSys.Lib.Model;
using CSMSys.Lib.DataAccessLayer.Interfaces;
using CSMSys.Lib.Utility;
using System.Data.Linq;
namespace CSMSys.Lib.DataAccessLayer.Implementations
{
    public class LoadingDAOLinq : BaseDAORepository<INVStockLoading, CSMSysConfiguration>, ILoadingDAO
    {
        protected override System.Linq.Expressions.Expression<Func<INVStockLoading, bool>> GetIDSelector(int id)
        {
            return (item) => item.LoadingID == id;
        }
        public INVStockLoading checkLocation(int chamber, int floor, string pocket, string line)
        {
            INVStockLoading invsl = new INVStockLoading();
            string whereClause = string.Empty;
            string queryTable = "INVStockLoading";

            DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

            string strSQL = "SELECT * FROM " + queryTable;

            if ((chamber != 0))
            {
                whereClause += " WHERE ChamberNo = " + chamber + " and Floor = " + floor + " ";
            }

            if (!string.IsNullOrEmpty(pocket))
            {
                if (string.IsNullOrEmpty(whereClause))
                {
                    whereClause += " WHERE ";
                }
                else
                {
                    whereClause += " AND ";
                }

                whereClause += "Pocket = '" + pocket + "' ";
            }

            if (!string.IsNullOrEmpty(line))
            {
                if (string.IsNullOrEmpty(whereClause))
                {
                    whereClause += " WHERE ";
                }
                else
                {
                    whereClause += " AND ";
                }

                whereClause += "Line = '" + line + "' ";
            }

            strSQL += whereClause;

            int cnt = dc.ExecuteQuery<INVStockLoading>(@strSQL).Count();
            invsl = (cnt > 0) ? dc.ExecuteQuery<INVStockLoading>(@strSQL).First() : null;
            return invsl;
        }

        public INVStockSerial GetPartyBySerial(string serialNo)
        {
            try
            {
                string whereClause = string.Empty;
                string queryTable = "INVStockSerial";

                DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

                string strSQL = "SELECT * FROM " + queryTable;

                if (!string.IsNullOrEmpty(serialNo))
                {
                    whereClause += " WHERE SerialNo = '" + serialNo + "' ";
                }

                strSQL += whereClause;

                return dc.ExecuteQuery<INVStockSerial>(@strSQL).First();
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return null;
            }
        }

        public INVStockLoading GetLoadBySerial(string serialNo)
        {
            try
            {
                string whereClause = string.Empty;
                string queryTable = "INVStockLoading";

                DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

                string strSQL = "SELECT * FROM " + queryTable;

                if (!string.IsNullOrEmpty(serialNo))
                {
                    whereClause += " WHERE SerialID = '" + serialNo + "' ";
                }

                strSQL += whereClause;

                return dc.ExecuteQuery<INVStockLoading>(@strSQL).First();
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return null;
            }
        }

        public INVStockLoading GetLoadingBySerialID(int serialID)
        {
            try
            {
                string whereClause = string.Empty;
                string queryTable = "INVStockLoading";

                DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

                string strSQL = "SELECT * FROM " + queryTable;

                if (serialID > 0)
                {
                    whereClause += " WHERE SerialID = " + serialID + " ";
                }

                strSQL += whereClause;

                return dc.ExecuteQuery<INVStockLoading>(@strSQL).FirstOrDefault();
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return null;
            }
        }
    }
}
