﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.Configuration;
using CSMSys.Lib.Model;
using CSMSys.Lib.DataAccessLayer.Interfaces;
using CSMSys.Lib.Utility;

namespace CSMSys.Lib.DataAccessLayer.Implementations
{
    public class VariableDAOLinq : BaseDAORepository<SYSVariable, CSMSysConfiguration>, IVariableDAO
    {
        protected override System.Linq.Expressions.Expression<Func<SYSVariable, bool>> GetIDSelector(int id)
        {
            return (item) => item.varID == id;
        }


        /// <summary>
        /// DA method to search object by params
        /// </summary>
        /// <param name="countryCode"></param>
        /// <param name="countryName"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public SYSVariable SearchVariable(string variableName)
        {
            try
            {
                string whereClause = string.Empty;
                string queryTable = "dbo.SYSVariables";

                DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

                string strSQL = "SELECT * FROM " + queryTable;

                if (!string.IsNullOrEmpty(variableName))
                {
                    whereClause += " WHERE VariableName = '" + variableName + "'";
                }

                strSQL += whereClause;

                return dc.ExecuteQuery<SYSVariable>(@strSQL).ToList().First();
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return null;
            }
        }
    }
}
