﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CSMSys.Lib.DataAccessLayer.Interfaces;
using CSMSys.Lib.Model;

namespace CSMSys.Lib.DataAccessLayer.Implementations
{
    public class DeliveryDAOLinq  :  BaseDAORepository<SRVDelivery, CSMSysConfiguration>, IDeliveryDAO
    {
        protected override System.Linq.Expressions.Expression<Func<SRVDelivery, bool>> GetIDSelector(int id)
        {
            return (item) => item.DeliveryID == id;
        }
    }
}
