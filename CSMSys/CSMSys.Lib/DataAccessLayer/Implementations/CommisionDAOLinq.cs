﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CSMSys.Lib.Model;

using CSMSys.Lib.DataAccessLayer.Interfaces;
namespace CSMSys.Lib.DataAccessLayer.Implementations
{
    public class CommisionDAOLinq: BaseDAORepository<SRVCommision, CSMSysConfiguration>, ICommision
    {
        protected override System.Linq.Expressions.Expression<Func<SRVCommision, bool>> GetIDSelector(int id)
        {
            return (item) => item.CommisonID == id;
        }
    }
}
