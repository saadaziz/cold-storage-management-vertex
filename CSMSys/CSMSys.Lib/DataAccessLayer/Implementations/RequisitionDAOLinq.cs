﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Linq;
using System.Configuration;
using CSMSys.Lib.Model;
using CSMSys.Lib.DataAccessLayer.Interfaces;
using CSMSys.Lib.Utility;

namespace CSMSys.Lib.DataAccessLayer.Implementations
{
    public class RequisitionDAOLinq : BaseDAORepository<SRVLoanRequisition, CSMSysConfiguration>, IRequisitionDAO
    {
        protected override System.Linq.Expressions.Expression<Func<SRVLoanRequisition, bool>> GetIDSelector(int id)
        {
            return (item) => item.RequisitionID == id;
        }

        /// <summary>
        /// DA method to search object by params
        /// </summary>
        /// <param name="partyCode"></param>
        /// <param name="partyName"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public SRVLoanRequisition SearchRequisitionByNo(string serialNo)
        {
            try
            {
                string whereClause = string.Empty;
                string queryTable = "SRVLoanRequisition";

                DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

                string strSQL = "SELECT * FROM " + queryTable;

                if (!string.IsNullOrEmpty(serialNo))
                {
                    whereClause += " WHERE SerialID = '" + serialNo + "' ";
                }

                strSQL += whereClause;

                return dc.ExecuteQuery<SRVLoanRequisition>(@strSQL).ToList().First();
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return null;
            }
        }

        public SRVLoanRequisition SearchSRVBySerialParty(string serialNo, int partyid)
        {
            try
            {
                string whereClause = string.Empty;
                string queryTable = "SRVLoanRequisition";

                DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

                string strSQL = "SELECT * FROM " + queryTable;

                if (!string.IsNullOrEmpty(serialNo))
                {
                    whereClause += " WHERE SerialNo = '" + serialNo + "' and partyID='" + partyid + "' ";
                }

                strSQL += whereClause;

                return dc.ExecuteQuery<SRVLoanRequisition>(@strSQL).ToList().First();
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return null;
            }
        }

        public SRVLoanRequisition SearchSRVBySerialPartyID(int serialID, int partyid)
        {
            try
            {
                string whereClause = string.Empty;
                string queryTable = "SRVLoanRequisition";

                DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

                string strSQL = "SELECT * FROM " + queryTable;

                if (serialID > 0)
                {
                    whereClause += " WHERE SerialID = " + serialID + " and partyID='" + partyid + "' ";
                }

                strSQL += whereClause;

                return dc.ExecuteQuery<SRVLoanRequisition>(@strSQL).ToList().First();
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return null;
            }
        }
        /// <summary>
        /// DA method to search object by params
        /// </summary>
        /// <param name="partyCode"></param>
        /// <param name="partyName"></param>
        /// <param name="culture"></param>
        /// <returns></returns>
        public IList<SRVLoanRequisition> SearchRequisition(string serialNo, float bag)
        {
            try
            {
                string whereClause = string.Empty;
                string queryTable = "SRVLoanRequisition";

                DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

                string strSQL = "SELECT * FROM " + queryTable;

                if (!string.IsNullOrEmpty(serialNo))
                {
                    whereClause += " WHERE RequisitionNo = '" + serialNo + "' ";
                }

                if (bag > 0)
                {
                    if (string.IsNullOrEmpty(whereClause))
                    {
                        whereClause += " WHERE ";
                    }
                    else
                    {
                        whereClause += " AND ";
                    }

                    whereClause += "Bags = " + bag;
                }

                strSQL += whereClause;

                return dc.ExecuteQuery<SRVLoanRequisition>(@strSQL).ToList();
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return null;
            }
        }
        public IList<SRVLoanRequisition> SearchRegForTotalLoan(int partyid, string requisitioned)
        {
            try
            {
                string whereClause = string.Empty;
                string queryTable = "SRVLoanRequisition";

                DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

                string strSQL = "SELECT * FROM " + queryTable;

                if ((partyid != 0))
                {
                    whereClause += " WHERE partyID = '" + partyid + "' and requisitioned='" + requisitioned + "' ;";
                }



                strSQL += whereClause;

                return dc.ExecuteQuery<SRVLoanRequisition>(@strSQL).ToList();
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return null;
            }

        }
        public IList<SRVLoanRequisition> SearchRegByPartyID(int partyid)
        {
            try
            {
                string whereClause = string.Empty;
                string queryTable = "SRVLoanRequisition";

                DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

                string strSQL = "SELECT * FROM " + queryTable;

                if ((partyid != 0))
                {
                    whereClause += " WHERE partyID = '" + partyid + "'  ;";
                }



                strSQL += whereClause;

                return dc.ExecuteQuery<SRVLoanRequisition>(@strSQL).ToList();
            }
            catch (Exception ex)
            {
                _Logger.Error(ex);
                return null;
            }
        }
    }
}
