﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CSMSys.Lib.Model;
using CSMSys.Lib.DataAccessLayer.Interfaces;
using CSMSys.Lib.Utility;
using System.Data.Linq;
using System.Configuration;
using System.Data.SqlClient;

namespace CSMSys.Lib.DataAccessLayer.Implementations
{
    public class LoanCollectionDAOLinq :  BaseDAORepository<SRVLoanCollection, CSMSysConfiguration>, ILoanCollectionDAO
    {
        protected override System.Linq.Expressions.Expression<Func<SRVLoanCollection, bool>> GetIDSelector(int id)
        {
            return (item) => item.CollectionID == id;
        }

        public SRVLoanCollection GetLoanCollectionByPartyDateSerials(int partyID, DateTime collDate, string serials)
        {
            string date = string.Empty;
            if (collDate != null)
            {
                date = collDate.ToShortDateString();
                string[] words = date.Split('/');
                date = words[2].Trim().ToString() + "-" + words[0].Trim().ToString() + "-" + words[1].Trim().ToString();
            }
            string whereClause = string.Empty;
            string queryTable = "SRVLoanCollection";

            DataContext dc = new DataContext(ConfigurationManager.ConnectionStrings["CSMSysConnection"].ConnectionString);

            string strSQL = "SELECT * FROM " + queryTable;

            if (partyID > 0)
            {
                whereClause += " WHERE PartyID = " + partyID + " ";
            }

            if (string.IsNullOrEmpty(serials))
            {
                if (string.IsNullOrEmpty(whereClause))
                {
                    whereClause += " WHERE ";
                }
                else
                {
                    whereClause += " AND ";
                }

                whereClause += "SerialIDs = '" + serials + "' ";
            }

            if (collDate != null)
            {
                if (string.IsNullOrEmpty(whereClause))
                {
                    whereClause += " WHERE ";
                }
                else
                {
                    whereClause += " AND ";
                }

                whereClause += "(CollectionDate = CONVERT(DATETIME, '" + date + " 00:00:00', 102))";
            }

            strSQL += whereClause;

            return dc.ExecuteQuery<SRVLoanCollection>(@strSQL).ToList().FirstOrDefault();
        }
    }
}
