﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


using CSMSys.Lib.Model;
namespace CSMSys.Lib.DataAccessLayer.Interfaces
{
    public interface ILoanCollectionDAO : IRepository<SRVLoanCollection>
    {
        SRVLoanCollection GetLoanCollectionByPartyDateSerials(int partyID, DateTime collDate, string serials);
    }
}
