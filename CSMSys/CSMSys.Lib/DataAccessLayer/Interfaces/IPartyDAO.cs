﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CSMSys.Lib.Model;

namespace CSMSys.Lib.DataAccessLayer.Interfaces
{
    public interface IPartyDAO : IRepository<INVParty>
    {
        long getnextPartyID();
        INVParty SearchParentByCode(string partyCode);
        IList<INVParty> SearchPartyByCode(string partyCode);
        IList<INVParty> SearchParty(string partyCode, string partyName);
        IList<INVParty> SearchPartyByContactNo(string contact);
        IList<INVParty> GetAllPartyExceptFarmer();
        IList<INVParty> GetAllChildByParent(int partyid);   
    }
}
