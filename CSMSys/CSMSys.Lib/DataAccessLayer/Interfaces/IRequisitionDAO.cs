﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CSMSys.Lib.Model;

namespace CSMSys.Lib.DataAccessLayer.Interfaces
{
    public interface IRequisitionDAO : IRepository<SRVLoanRequisition>
    {
        SRVLoanRequisition SearchRequisitionByNo(string serialNo);
        SRVLoanRequisition SearchSRVBySerialParty(string serialNo, int partyid);
        SRVLoanRequisition SearchSRVBySerialPartyID(int serialID, int partyid);
        IList<SRVLoanRequisition> SearchRequisition(string serialNo, float bag);
        IList<SRVLoanRequisition> SearchRegForTotalLoan(int partyid, string requisitioned);
        IList<SRVLoanRequisition> SearchRegByPartyID(int partyid);
    }
}
