﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CSMSys.Lib.Model;

namespace CSMSys.Lib.DataAccessLayer.Interfaces
{
    public interface ILoanCollectionDetailDAO : IRepository<SRVLoanCollectionDetail>
    {
        SRVLoanCollectionDetail getLoanCollectionDetailBySerialID(int collID, int serialID);
        IList<SRVLoanCollectionDetail> getLoanCollectionListDetailByCollectionID(int collID);
        IList<SRVLoanCollectionDetail> getLoanCollectionListDetailByPartyIDSerialID(int partyID, int serid);
    }
}
