﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CSMSys.Lib.Model;
using CSMSys.Lib.DataAccessLayer.Interfaces;
using CSMSys.Lib.DataAccessLayer.Implementations;
using CSMSys.Lib.Utility;
namespace CSMSys.Lib.Manager.INV
{
    public class UnloadManager
    {
        #region Properties
        IUnloading _UnLoadDAOLinq;    
        #endregion
        #region Constructor
        public UnloadManager()
        {
            _UnLoadDAOLinq = new UnloadingDAOLinq();
        }
        #endregion

        #region Public Methods
        public INVStockUnloading GetUnLoadByID(int id)
        {
            return _UnLoadDAOLinq.PickByID(id);
        }

        #endregion
    }
}
