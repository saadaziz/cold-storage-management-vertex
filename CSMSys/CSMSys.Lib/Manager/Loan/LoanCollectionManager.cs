﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CSMSys.Lib.Model;
using CSMSys.Lib.DataAccessLayer.Interfaces;
using CSMSys.Lib.DataAccessLayer.Implementations;
using CSMSys.Lib.Utility;
using System.Data.SqlClient;
using System.Configuration;

namespace CSMSys.Lib.Manager.Loan
{
    public class LoanCollectionManager
    {
        #region Properties
        ILoanCollectionDAO _ILoanCollectionDAO;
        #endregion

        #region Constructor
        public LoanCollectionManager()
        {
            _ILoanCollectionDAO = new LoanCollectionDAOLinq();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get all District from District table 
        /// </summary>
        /// <returns></returns>
        public IList<SRVLoanCollection> GetAllLoanCollection()
        {
            try
            {
                return _ILoanCollectionDAO.All().ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public SRVLoanCollection GetLoanCollectionByID(int id)
        {
            return _ILoanCollectionDAO.PickByID(id);
        }

        public bool SaveLoanCollection(SRVLoanCollection loanCollection)
        {
            try
            {
                if (loanCollection.CollectionID <= 0)
                {
                    return new LoanCollectionDAOLinq().Add(loanCollection);
                }
                else
                {
                    return new LoanCollectionDAOLinq().Edit(loanCollection);
                }
                // return new RegistrationDAOLinq().Add(party);
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public SRVLoanCollection GetLoanCollectionByPartyDateSerials(int partyID, DateTime collDate, string serials)
        {
            return _ILoanCollectionDAO.GetLoanCollectionByPartyDateSerials(partyID, collDate, serials);
        }
        #endregion
    }
}
