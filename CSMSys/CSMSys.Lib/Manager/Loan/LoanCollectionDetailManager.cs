﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CSMSys.Lib.Model;
using CSMSys.Lib.DataAccessLayer.Interfaces;
using CSMSys.Lib.DataAccessLayer.Implementations;
using CSMSys.Lib.Utility;
using System.Data.SqlClient;
using System.Configuration;

namespace CSMSys.Lib.Manager.Loan
{
    public class LoanCollectionDetailManager
    {
        #region Properties
        ILoanCollectionDetailDAO _ILoanCollectionDetailDAO;
        #endregion

        #region Constructor
        public LoanCollectionDetailManager()
        {
            _ILoanCollectionDetailDAO = new LoanCollectionDetailDAOLinq();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get all District from District table 
        /// </summary>
        /// <returns></returns>
        public IList<SRVLoanCollectionDetail> GetAllLoanCollectionDetail()
        {
            try
            {
                return _ILoanCollectionDetailDAO.All().ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public SRVLoanCollectionDetail GetLoanCollectionDetailByID(int id)
        {
            return _ILoanCollectionDetailDAO.PickByID(id);
        }

        public bool SaveLoanCollectionDetail(SRVLoanCollectionDetail loanCollDetail)
        {
            try
            {
                if (loanCollDetail.CollectionDetailID <= 0)
                {
                    return _ILoanCollectionDetailDAO.Add(loanCollDetail);
                }
                else
                {
                    return _ILoanCollectionDetailDAO.Edit(loanCollDetail);
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public SRVLoanCollectionDetail getLoanCollectionDetailBySerialID(int collID, int serialID)
        {
            return _ILoanCollectionDetailDAO.getLoanCollectionDetailBySerialID(collID, serialID);
        }
        public IList<SRVLoanCollectionDetail> GetByColID(int colid)
        {
            return _ILoanCollectionDetailDAO.getLoanCollectionListDetailByCollectionID(colid);
        }
        #endregion
    }
}
