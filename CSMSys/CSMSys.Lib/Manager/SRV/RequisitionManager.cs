﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CSMSys.Lib.Model;
using CSMSys.Lib.Utility;
using System.Configuration;
using CSMSys.Lib.DataAccessLayer.Interfaces;
using CSMSys.Lib.DataAccessLayer.Implementations;

namespace CSMSys.Lib.Manager.SRV
{
    public class RequisitionManager
    {
        #region Properties
        IRequisitionDAO _RequisitionDAOLinq;
        #endregion

        #region Constructor
        public RequisitionManager()
        {
            _RequisitionDAOLinq = new RequisitionDAOLinq();
        }
        #endregion
        
        #region Public Methods
        /// <summary>
        /// Get all Requisition from Requisition table 
        /// </summary>
        /// <returns></returns>
        public IList<SRVLoanRequisition> GetAllRequisition()
        {
            try
            {
                return _RequisitionDAOLinq.All().ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Method to get party object by OID
        /// </summary>
        /// <param name="oid"></param>
        /// <returns></returns>
        public SRVLoanRequisition GetRequisitionByID(int id)
        {
            return _RequisitionDAOLinq.PickByID(id);
        }

        /// <summary>
        /// Method to save (add/edit) party object
        /// </summary>
        /// <param name="party"></param>
        /// <returns></returns>
        public bool SaveRequisition(SRVLoanRequisition serial)
        {
            try
            {
                if (serial.RequisitionID == 0)
                {
                    return _RequisitionDAOLinq.Add(serial);
                }
                else
                {
                    return _RequisitionDAOLinq.Edit(serial);
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public Int32 GetRequisitionID()
        {
            try
            {
                return Convert.ToInt32(_RequisitionDAOLinq.All().ToList().Last().RequisitionID);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public bool DeleteRequisition(SRVLoanRequisition serial)
        {
            try
            {
                if (serial.RequisitionID > 0)
                {
                    return _RequisitionDAOLinq.Delete(serial);
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public IList<SRVLoanRequisition> SearchRequisition(string serialNo, float bag)
        {
            return _RequisitionDAOLinq.SearchRequisition(serialNo, bag);
        }

        public SRVLoanRequisition SearchSRVBySerialParty(string serialNo, int partyid)
        {
            return _RequisitionDAOLinq.SearchSRVBySerialParty(serialNo, partyid);
        }

        public SRVLoanRequisition SearchSRVBySerialPartyID(int serialID, int partyid)
        {
            return _RequisitionDAOLinq.SearchSRVBySerialPartyID(serialID, partyid);
        }
        #endregion
    }
}
