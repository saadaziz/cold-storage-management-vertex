﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CSMSys.Lib.Model;
using CSMSys.Lib.DataAccessLayer.Interfaces;
using CSMSys.Lib.DataAccessLayer.Implementations;
using CSMSys.Lib.Utility;
using System.Data.SqlClient;
using System.Configuration;

namespace CSMSys.Lib.Manager.SRV
{
    public class DeliveryManager
    {
        #region Properties
        IDeliveryDAO _IDeliveryDAO;
        #endregion

        #region Constructor
        public DeliveryManager()
        {
            _IDeliveryDAO = new DeliveryDAOLinq();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get all District from District table 
        /// </summary>
        /// <returns></returns>
        public IList<SRVDelivery> GetAllDelivery()
        {
            try
            {
                return _IDeliveryDAO.All().ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public SRVDelivery GetDeliveryByID(int id)
        {
            return _IDeliveryDAO.PickByID(id);
        }

        public bool SaveDeliveryDetails(SRVDelivery delivery)
        {
            try
            {
                if (delivery.DeliveryID <= 0)
                {
                    return new DeliveryDAOLinq().Add(delivery);
                }
                else
                {
                    return new DeliveryDAOLinq().Edit(delivery);
                }
                // return new RegistrationDAOLinq().Add(party);
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        #endregion
    }
}
