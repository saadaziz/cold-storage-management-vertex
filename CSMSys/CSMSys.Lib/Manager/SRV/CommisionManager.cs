﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CSMSys.Lib.DataAccessLayer.Interfaces;
using CSMSys.Lib.DataAccessLayer.Implementations;
using CSMSys.Lib.Model;
namespace CSMSys.Lib.Manager.SRV
{
    public class CommisionManager
    {
        #region Properties
        ICommision _ICommision;
        #endregion
         #region Constructor
        public CommisionManager()
        {
            _ICommision = new CommisionDAOLinq();
        }
        #endregion
        #region methods
        public SRVCommision GetCommissionByID(int id)
        {
            return _ICommision.PickByID(id);
        }

        public bool SaveCommision(SRVCommision comm)
        {
            try
            {
                if (comm.CommisonID == 0)
                {
                    return new CommisionDAOLinq().Add(comm);
                }
                else
                {
                    return new CommisionDAOLinq().Edit(comm);
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}
