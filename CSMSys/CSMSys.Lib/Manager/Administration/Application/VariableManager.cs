﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CSMSys.Lib.Model;
using CSMSys.Lib.DataAccessLayer.Interfaces;
using CSMSys.Lib.DataAccessLayer.Implementations;
using CSMSys.Lib.Utility;

namespace CSMSys.Lib.Manager.Administration.Application
{
    public class VariableManager
    {
        #region Properties
        IVariableDAO _VariableDAOLinq;
        #endregion

        #region Constructor
        public VariableManager()
        {
            _VariableDAOLinq = new VariableDAOLinq();
        }
        #endregion
        #region Public Methods
        /// <summary>
        /// Get all Variable from Variable table 
        /// </summary>
        /// <returns></returns>
        public IList<SYSVariable> GetAllVariable()
        {
            try
            {
                return _VariableDAOLinq.All().ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// Method to get country object by OID
        /// </summary>
        /// <param name="oid"></param>
        /// <returns></returns>
        public SYSVariable GetVariableByID(int id)
        {
            return _VariableDAOLinq.PickByID(id);
        }

        /// <summary>
        /// Method to save (add/edit) country object
        /// </summary>
        /// <param name="country"></param>
        /// <returns></returns>
        public bool SaveVariable(SYSVariable variable)
        {
            try
            {
                if (variable.varID == 0)
                {
                    return _VariableDAOLinq.Add(variable);
                }
                else
                {
                    return _VariableDAOLinq.Edit(variable, true);
                }
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public SYSVariable SearchVariable(string variableName)
        {
            return _VariableDAOLinq.SearchVariable(variableName);
        }
        #endregion
    }
}
